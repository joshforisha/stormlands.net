import * as Color from "./lib/Colors";
import * as Icon from "./lib/Icons";
import { parse } from "papaparse";
import { svg, zeroPad } from "./lib/Utils";

const backdrop = document.getElementById("Backdrop");
const downloadButton = document.getElementById("DownloadButton");
const editFactionsButton = document.getElementById("EditFactionsButton");
const editFactionsDiv = document.getElementById("EditFactions");
const factionsList = document.getElementById("FactionsList");
const map = document.querySelector("body > svg");
const saveFactionsButton = document.getElementById("SaveFactions");
const uploadButton = document.getElementById("UploadButton");
const uploadInput = uploadButton.querySelector("input");

let factionColors = {};
let worlds = [];

function closeEditFactionsModal() {
  editFactionsDiv.style.display = "none";
}

function showEditFactionsModal() {
  editFactionsDiv.style.display = "flex";
}

function hexagon(attrs = {}) {
  return svg(
    "polygon",
    Object.assign(
      {
        points: "300,150 225,280 75,280 0,150 75,20 225,20"
      },
      attrs
    )
  );
}

function drawHex(x, y, coord) {
  const hex = svg("g", {
    class: "hex",
    id: `Hex${coord}`,
    transform: `translate(${x} ${y})`,
    x,
    y
  });

  hex.appendChild(
    hexagon({
      "stroke-width": 4,
      fill: Color.black,
      stroke: Color.darkGray,
      x,
      y
    })
  );
  map.appendChild(hex);

  const coordinateText = svg("text", {
    "font-size": 28,
    fill: Color.white,
    opacity: 0.5,
    x: x + 150,
    y: y + 50
  });
  coordinateText.textContent = coord;
  map.appendChild(coordinateText);
}

function downloadSvg() {
  const mapBlob = new Blob([map.outerHTML], { type: "image/svg+xml" });
  const mapURL = window.URL.createObjectURL(mapBlob);

  const canvas = document.createElement("canvas");
  canvas.setAttribute("height", map.getAttribute("height"));
  canvas.setAttribute("width", map.getAttribute("width"));

  const ctx = canvas.getContext("2d");

  const img = new Image();
  img.onload = () => {
    ctx.drawImage(img, 0, 0);
    window.URL.revokeObjectURL(mapURL);
    const w = window.open();
    w.document.body.append(canvas);
  };
  img.src = mapURL;
}

function drawWorld(world) {
  const hex = map.getElementById(`Hex${world.Hex}`);
  const [x, y] = [
    parseInt(hex.getAttribute("x")),
    parseInt(hex.getAttribute("y"))
  ];

  if ("Allegiance" in world && world.Allegiance.length > 0) {
    const factionHex = hexagon({
      fill: factionColors[world.Allegiance],
      stroke: "transparent",
      transform: `translate(${x + 3} ${y + 3}), scale(0.98)`
    });

    map.appendChild(factionHex);
  }

  if (world.Name !== "") {
    const g = svg("g", {
      transform: `translate(${x}, ${y})`
    });

    if (world.Bases.includes("N")) {
      g.appendChild(Icon.navyBase());
    }

    if (world.Bases.includes("P")) {
      g.appendChild(Icon.interlag());
    }

    if (world.Bases.includes("R")) {
      g.appendChild(Icon.researchBase());
    }

    if (world["Gas Giants"] > 0) {
      g.appendChild(Icon.gasGiant());

      if (world["Gas Giants"] > 1) {
        const gasGiantsText = svg("text", {
          "font-size": 24,
          "font-weight": 700,
          fill: Color.black,
          x: 150,
          y: 89
        });
        gasGiantsText.textContent = world["Gas Giants"].toString();
        g.appendChild(gasGiantsText);
      }
    }

    if (world.Bases.includes("S")) {
      g.appendChild(Icon.scoutBase());
    }

    if (world.Bases.includes("T")) {
      g.appendChild(Icon.tasHostel());
    }

    const tradeCodesText = svg("text", {
      "font-size": 22,
      fill: Color.white,
      opacity: 0.4,
      x: 150,
      y: 125
    });
    tradeCodesText.textContent = world["Trade Codes"];
    g.appendChild(tradeCodesText);

    const nameText = svg("text", {
      "font-size": 30,
      fill: Color.white,
      x: 151,
      y: 161
    });
    nameText.textContent = world.Name.toUpperCase();
    g.appendChild(nameText);

    if (world.UWP.length > 0) {
      const r = 26 + parseInt(world.UWP[1], 16);
      if (world["Trade Codes"].includes("As")) {
        g.appendChild(Icon.asteroidWorld(r));
      } else {
        const hydro = parseInt(world.UWP[3], 16);
        if (hydro < 4) g.appendChild(Icon.dryWorld(r));
        else if (hydro < 8) g.appendChild(Icon.terrestrialWorld(r));
        else g.appendChild(Icon.wetWorld(r));
      }

      const uwpText = svg("text", {
        "font-size": 28,
        fill: Color.white,
        x: 150,
        y: 270
      });
      uwpText.textContent = world.UWP;
      g.appendChild(uwpText);
    }

    map.appendChild(g);
  }
}

function drawZone(x, y, code) {
  const outline = hexagon();
  outline.setAttribute("transform", `translate(${x} ${y})`);
  outline.setAttribute("fill", "transparent");
  outline.setAttribute("stroke-width", 5);

  if (code === "Amber") {
    outline.setAttribute("stroke", Color.darkYellow);
  } else if (code === "Red") {
    outline.setAttribute("stroke", Color.red);
  }

  map.appendChild(outline);
}

function editFactions() {
  factionsList.innerHTML = "";

  Object.entries(factionColors).forEach(([faction, color]) => {
    const div = document.createElement("div");

    const label = document.createElement("label");
    label.textContent = faction;
    div.appendChild(label);

    const input = document.createElement("input");
    input.setAttribute("type", "color");
    input.value = color;
    div.appendChild(input);

    factionsList.appendChild(div);
  });

  showEditFactionsModal();
}

function render() {
  map.innerHTML = "";

  const travelHexes = [];

  for (let ssr = 0; ssr < 4; ssr++) {
    for (let ssc = 0; ssc < 4; ssc++) {
      for (let row = 0; row < 10; row++) {
        for (let col = 0; col < 8; col++) {
          /* Large subsector margins
          const x = 28 + ssc * 1860 + col * 225
          const y = 2 + ssr * 2680 + row * 260 + (col % 2 > 0 ? 130 : 0)
          // */

          //* Small subsector margins
          const x = 28 + ssc * 1920 + col * 225;
          const y = 2 + ssr * 2800 + row * 260 + (col % 2 > 0 ? 130 : 0);
          // */

          const [cx, cy] = [ssc * 8 + col + 1, ssr * 10 + row + 1];
          const coordX = cx < 10 ? `0${cx}` : cx.toString();
          const coordY = cy < 10 ? `0${cy}` : cy.toString();
          const coord = `${coordX}${coordY}`;
          const world = worlds.find(w => w.Hex === coord);

          drawHex(x, y, coord, world);

          if (typeof world !== "undefined") {
            drawWorld(world);

            if (world["Travel Code"] !== "") {
              travelHexes.push({ travelCode: world["Travel Code"], x, y });
            }
          }
        }
      }
    }
  }

  travelHexes.forEach(({ x, y, travelCode }) => {
    drawZone(x, y, travelCode);
  });
}

function saveFactions() {
  Array.prototype.forEach.call(factionsList.children, div => {
    const faction = div.querySelector("label").textContent;
    const color = div.querySelector("input").value;
    factionColors[faction] = color;
  });

  render();
  closeEditFactionsModal();
}

function uploadCsv(event) {
  if (event.target.files.length === 1) {
    parse(event.target.files[0], {
      complete: ({ data, errors }) => {
        if (errors.length > 0) {
          errors.forEach(window.console.error);
          return undefined;
        }

        worlds = data
          .map(world =>
            Object.keys(world).reduce((w, key) => {
              w[key.trim()] = world[key].trim();
              return w;
            }, {})
          )
          .map(w => Object.assign({}, w, { Hex: zeroPad(4, w.Hex) }));

        factionColors = worlds.reduce((colors, { Allegiance }) => {
          if (Allegiance && !(Allegiance in colors)) {
            colors[Allegiance] = "#383838";
          }

          return colors;
        }, {});

        if (Object.values(factionColors).length > 0)
          editFactionsButton.removeAttribute("disabled");
        else editFactionsButton.setAttribute("disabled", "");

        render();
      },
      header: true,
      skipEmptyLines: true
    });
  }
}

backdrop.addEventListener("click", closeEditFactionsModal);
downloadButton.addEventListener("click", downloadSvg);
editFactionsButton.addEventListener("click", editFactions);
saveFactionsButton.addEventListener("click", saveFactions);
uploadInput.addEventListener("change", uploadCsv);

render();

if (module.hot) {
  module.hot.dispose(() => {
    downloadButton.removeEventListener("click", downloadSvg);
    uploadInput.removeEventListener("change", uploadCsv);
  });
}
