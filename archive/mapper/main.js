import { makeCylinderSurface } from 'fractal-noise';
import { makeNoise3D } from 'fast-simplex-noise';

const $ = (sel) => document.querySelector(sel);
const $$ = (sel) => Array.from(document.querySelectorAll(sel));

const height = Math.floor(0.5 * window.innerWidth);
const width = 2 * height;

const canvas = $('canvas');
canvas.width = width;
canvas.height = height;

const ctx = canvas.getContext('2d');
const waterInput = $('input#Water');

const elevationField = makeCylinderSurface(width, height, makeNoise3D(), {
  frequency: 0.005,
  octaves: 8,
  scale: (e) => 128 * (e + 1),
});

const temperatureField = makeCylinderSurface(width, height, makeNoise3D(), {
  frequency: 0.01,
  octaves: 4,
  scale: (e) => 128 * (e + 1),
});

const equatorLatitude = 0.5 * height;
let seaLevel = parseInt(waterInput.value);

// Color functions ---------------------------------------------------------------------------

function clamp(x, min = 0, max = 255) {
  return Math.max(0, Math.min(255, x));
}

function elevationColor(x, y) {
  const elevation = elevationField[x][y];
  let span;

  if (elevation > seaLevel) {
    span = (elevation - seaLevel) / (255 - seaLevel);
    return [255 * span, 47 + 208 * span, 255 * span];
  }

  span = elevation / seaLevel;
  return [63 * span, 63 * span, 48 + 127 * span];
}

function temperatureColor(x, y) {
  const latitude =
    y < equatorLatitude
      ? 255 * (y / equatorLatitude)
      : 255 - (255 * (y - equatorLatitude)) / equatorLatitude;

  const noise = temperatureField[x][y];
  const elevation = elevationField[x][y];

  const temperature = clamp(0.5 * latitude + 0.8 * noise - 0.2 * elevation);

  return [temperature, elevation > seaLevel ? 63 : 0, 255 - temperature];
}

function terrainColor(x, y) {
  const elevation = elevationField[x][y];
  const waterLevel = parseInt(waterInput.value);
  if (elevation < waterLevel) return [0, 0, 80];
  return [200, 175, 140];
}

// Render ------------------------------------------------------------------------------------

const Mode = {
  Elevation: 'elevation',
  Precipitation: 'precipitation',
  Temperature: 'temperature',
  Terrain: 'terrain',
  Wind: 'wind',
};

let selectedMode;

function modeColorFn() {
  switch (selectedMode) {
    case 'elevation':
      return elevationColor;
    case 'temperature':
      return temperatureColor;
    case 'terrain':
      return terrainColor;
    default:
      throw new Error(`"${selectedMode}" mode not handled`);
  }
}

function render() {
  const colorFn = modeColorFn();
  const imageData = ctx.createImageData(width, height);

  let i = 0;
  let color;
  for (let y = 0; y < height; y++) {
    for (let x = 0; x < width; x++) {
      color = colorFn(x, y);
      imageData.data[i++] = color[0];
      imageData.data[i++] = color[1];
      imageData.data[i++] = color[2];
      imageData.data[i++] = 255;
    }
  }
  ctx.putImageData(imageData, 0, 0);
}

function updateAndRender() {
  seaLevel = parseInt(waterInput.value);

  render();
}

// Events initialization ---------------------------------------------------------------------

waterInput.addEventListener('change', updateAndRender);

$$('button[data-mode]').forEach((button) => {
  button.addEventListener('click', (event) => {
    $('button[data-selected]')?.removeAttribute('data-selected');
    selectedMode = button.getAttribute('data-mode');
    button.setAttribute('data-selected', '');
    render();
  });
});

// Setup -------------------------------------------------------------------------------------

$('button[data-mode="elevation"]').dispatchEvent(new PointerEvent('click'));
render();
