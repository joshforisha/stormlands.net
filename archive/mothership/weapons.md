| Weapon | Cost | Dmg | Crit | S | M | L | Ammunition | Shots | Special |
| ------ | ----:| --- | ---- | -:| -:| -:| ---------- | -----:| ------- |
| Combat Shotgun | 1,400 | **2d10** | Knockdown. | 10m | 20m | 30m | Knockback. &frac12; DMG at medium range. &frac14; DMG at long range. | 4 | Audio Rangefinder: beeps when non-friendlies approach within 10m. |
| Crowbar | 50 | 1d10 | | — | — | — | N/A | N/A | +5 Mechanical Repair. |
| Flame Thrower | 2,000 | **2d10** | | 2m | 10m | 20m | | 8 | Body Save or get set on fire and take **1d10** DMG per turn. |
| Flare Gun | 85 | 1d10 | | 5m | 10m | 20m | High Intensity Flare: Visible from 25km away. | 2 | |
| Foam Gun | 275 | N/A | No Save. | &lt;1m | 5m | 10m | Quick Hardening Foam: Body/Instinct Save to avoid or become stuck. Foam covers 5m&sup2;. | 10 | Fire retardant. |
| Frag Grenade | 70 | **1d10** | | 20m | 30m | 40m | Deals DMG to all within 15m radius. | 1 | Can deal 1 MDMG to thips (or more if internal). |
| Hand Welder | 250 | 1d10 | | — | — | — | | &infin; | Ultra heat emitter: Cuts through airlocks/heavy doors. -10 vs. Armor Save. |
| Laser Cutter | 1,200 | 1d% or 1MDMG | | 25m | 250m | 700m | Takes 1 round to recharge between shots. | 6 | Takes 1 hour to recharge from a power source or 6 hours in sunlight. |
| Nail Gun | 150 | 2d10 | Double DMG. | &lt;1m | 5m | 10m | Heavy Duty Nails: -10 vs. Armor Save | 32 | |
| Pulse Rifle | 1,600 | 5d10 | Double DMG. | 15m | 125m | 300m | Phosphorus Rounds. Fully automatic. | 1(3) | Smart-link system: +5 Combat if wearing HUD. Pump-Action Grenade Launcher: Holds 6 frag grenades. |
| Revolver | 750 | 3d10 | Knockdown. | 2m | 30m | 125m | Kineti-slugs: -5 vs. Armor Save. | 8 | |
| Rigging Gun | 350 | 2d10 | Impale. Triple DMG. | 10m | 30m | 100m | Micro-filament: 500m. Hard to cut. | 1 | Retractable Harpoon: Body/Instinct Save or become entangled. Does an extra **+1d10** DMG when grapnel is pulled out of target. |
| Scalpel | 50 | 1d10 | +1d10 DMG and bleeding. | — | — | — | N/A | N/A | +10 Surgery |
| Smart Rifle | 12,000 | **1d10** | &times;3 DMG. | 25m | 200m | 500m | Armor Piercing: -10 vs. Armor Save. | 12 | Smart-link system: +10 Combat if wearing HUD. Spectroscope: Night/Thermal Vision. |
| SMG | 1,200 | 4d10 | | 10m | 75m | 150m | Fully automatic. | 1(5) | |
| Stun Baton | 115 | 1d10 | No Save. | — | — | — | N/A | N/A | Body Save or stunned for 1 round. |
| Trang Pistol | 850 | N/A | No Save. | 2m | 10m | 20m | Tranquilizer Darts. | 6 | Body[+] Save or fall unconscious for 1d10 rounds. |
| Vibechete | 75 | 2d10 | Hack off limb. | — | — | — | | | Can hack through limbs or dense forestry but not metal doors, airlocks, etc. |
