| Item | Cost | Description |
| ---- | ----:| ----------- |
| Advanced Battle Dress | 1,500 | Heavy combat outfit worn by marines in battletorn offworld engagements. Confers a +15% bonus to the wearer's Armor Save. Has a small exo-skeleton that allows the wearer to carry twice what they normally could. |
| Automed (&times;6) | 300 | Nanotech pills that assist your body in repairing damage. They give +10% to Body Saves meant to repel disease or poison, as well as attempts to heal, and +10% to Fear Saves made to reduce Stress. |
| Binoculars | 35 | 20&times; magnification. Often come with thermal and night vision options. |
| Bioscanner | 150 | Allows the user to scan the immediate area for signs of life. Generally can scan for 100m in all directions without being blocked by most known metals. Can tell the location of signs of life but not what that life is. |
| Body Cam | 50 | A camera worn on your clothing that can stream video back to a control center so that your other crew members can view what you’re seeing. |
| Camping Gear | 250 | Tent, canteen, stove, backpack, sleeping bag. |
| Crowbar | 25 | Confers Advantage on Strength Checks to open jammed airlocks or lift heavy objects. Can also be used as a weapon. |
| Cybernetic Diagnostic Scanner | 500 | Allows the user to scan androids and other cybernetic organisms in order to diagnose any physical or mental issues they may be having. Often distrusted by androids. Can be used as a locating device for synthetic organisms. |
| Electronic Tool Set | 650 | A full set of tools for detailed repair or construction work on electronics. Confers +10% to Checks seeking to repair electronics. |
| Emergency Beacon | 30 | A small device that sends up a flare and then emits a loud beep every few seconds. Additionally, sends out a call on all radio channels to ships or vehicles in the area. Can be configured to be silent or to only make calls on known channels. |
| Field Recorder | 50 | Used to research alien flora and fauna in the field. Can take vital signs, collect DNA samples and perform basic genetic and material analysis on foreign objects. |
| First Aid Kit | 75 | Adds +10% to rolls made to bandage wounds and stop bleeding. |
| Flashlight | 10 | Handheld or shoulder mounted, illuminates 20m ahead of the user. |
| Frag Grenade | 400 | Often come loaded in pulse rifles, frag grenades deal 1d10 damage to everyone within a 20m radius when they (x6) explode. Can often do 1MDMG to a ship if placed in their engine room or bridge. |
| Hazard Suit | 750 | A standard suit for scientists to wear on alien planets. Not built for outerspace travel like the vaccsuit but does provide air filtration and a small supply of air as well as +5% Armor Save. |
| Heads-Up Display | 75 | Often worn by marines, the HUD allows the wearer to see through the body cams of others in their unit and tap into their guns’ smart-link capabilities. |
| Infrared Goggles | 100 | Allows the wearer to see heat signatures, sometimes several hours old. |
| Locator | 45 | Allows crew members at a control center (or on the bridge of a ship) to track the location of the wearer. |
| Lockpick Set | 40 | A highly advanced set of tools meant for hacking basic airlock and electronic door systems. Confers +10% on Checks made to open these doors. |
| Long-range Comms | 65 | For use in ship-to-surface communication. |
| Mag-Boots | 55 | Grants a magnetic grip to the wearer, allowing them to easily to walk on the surface of a ship (in space, while docked or free-floating), or metal based asteroids. |
| Medscanner | 150 | Allows the user to scan a living or dead body and to analyze it for disease or abnormalities without having to do a biopsy (or autopsy). |
| MRE (&times;7) | 70 | &ldquo;Meal, Ready-to-Eat.&rdquo; Self-contained, individual field rations in lightweight packaging. Each one has sufficient sustenance for a single person for one day (does not include water). |
| Oxygen Tank | 50 | When attached to a vaccsuit allows up to 12 hours of oxygen under normal circumstances. 4 hours under stressful circumstances. Explosive. |
| Pain Pills (&times;6) | 450 | When ingested immediately heals 1d10 points of damage and lowers Stress by 1. There is a danger of addiction and/ or overdose if used frequently. |
| Radio Jammer | 175 | Renders incomprehensible the radio signals of all within 100km. |
| Rebreather | 45 | Filters air and allows for underwater breathing for up to twenty minutes at a time without resurfacing. |
| Scalpel | 50 | Grants +10% to anyone making Surgery Checks. Can be used as a weapon. |
| Short-range Comms | 30 | Allows communication from ship-to-ship within a reasonable distance as well as surface-to-surface within 12 kilometers. |
| Standard Battle Dress | 750 | This light plated armor is the standard dress for marines going into combat and confers a +10% bonus to the wearer’s Armor Save. |
| Standard Crew Attire | 20 | Coveralls, leather jackets, sneakers, tank top or ragged tee. The standard outfit worn by crew members aboard spacecraft. Standard assumed attire for all classes. |
| Stimpak (&times;6) | 600 | Grants an immediate 2d10 to Health and temporarily increases Strength and Combat by 2d10 each for 1d10 hours. There is a danger of addiction and/or overdose if used frequently. |
| Survey Kit | 200 | When used on the surface of a planet allows for quick mapping of a nearby few kilometers as well as data on air breathability, gravity and other important notable features of the surrounding landscape. |
| Vaccsuit | 1,000 | Allows for movement in space without suffering penalties from radiation or lack of oxygen. Requires an oxygen tank to breathe. Often used with Mag-boots and a rigging gun. Confers +7% to Armor Saves. |
| Vibechete | 75 | Machete that vibrates at ultra-high speeds in order to cut cleanly through dense foliage. Can be used as a weapon. |
| Water Filter | 15 | Can pump 50 liters of filtered water an hour from even the most brackish swamps. |
