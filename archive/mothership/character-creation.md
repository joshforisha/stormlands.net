## 1. Roll 6d10 for Each Stat, in order

- **Strength**: How able-bodied you are. Lifting, pushing, hitting things hard, etc.
- **Speed**: How quickly you can act and react under pressure.
- **Intellect**: How knowledgeable and experienced you are.
- **Combat**: How good you are at fighting.

## 2. Pick a Class

- **Teamster**: rough and tumble crew and workers out in space.
  - **+5 Strength, +5 Speed;** Sanity 30 / Fear 35 / Body 30 / Armor 35.
  - **Once per session, a Teamster may re-roll a roll on the Panic Effect Table.**
  - Skills: Zero-G, Mechanical Repair, Pick one: Heavy Machinery or Piloting; **+4 points**.
- **Android**: terrifying, exciting, and unnerving additions to crew.
  - **+5 Speed, +5 Intellect;** Sanity 20 / Fear 85 / Body 40 / Armor 25.
  - **Fear Saves made in the presence of Androids have Disadvantage.**
  - Skills: Linguistics, Computers, Mathematics; **+2 points**.
- **Scientist**: doctors, researchers, and anyone who wants to slice open aliens.
  - **+10 Intellect;** Sanity 40 / Fear 25 / Body 25 / Armor 30.
  - **Whenever a Scientist fails a Sanity Save, every friendly player nearby gains 1 Stress.**
  - Skills: Pick two: Biology, Hydroponics, Geology, Computers, Mathematics, Chemistry; **+3 points**.
- **Marine**: shoot bugs and chew bubblegum; handy in a fight and good grouped together.
  - **+5 Combat (+5 with friendly Marine);** Sanity 25 / Fear 30 / Body 35 / Armor 40.
  - **Whenever a Marine Panics, every friendly player nearby must make a Fear Save.**
  - Skills: Military Training; **+3 points**.
- **Suit**
  - **+5 Intellect, +5 Speed;** Sanity 40 / Fear 20 / Body 30 / Armor 30.
  - **Appeal to Contract: can reroll any other ally's Panic result once per session.**
  - Skills: Linguistics, Pick two: Art, Theology, Rimwise; **+2 points**.
- **Witness**: the practical evolution of the field reporter; never looks away, willing to delve where even light fears to shine.
  - **+10 Speed;** Sanity 40 / Fear 40 / Body 25 / Armor 25.
  - **Panic Checks are made with Advantage.**
  - Skills: Athletics, Rimwise, Pick one: Linguistics, Computers; **+2 points**.
- **Field Biologist**
  - **+5 Intellect;** Sanity 40 / Fear 35 / Body 30 / Armor 30.
  - **Been There, Seen That: Can negate a Panic Check for the entire party once per day, provided they can offer a plausible (even if incorrect) explanation that reassures everyone to remain calm.**
  - Skills: Biology, Hydroponics; **+4 points**.
- **Void Monk**
  - **+10 Strength;** Sanity 40 / Fear 30 / Body 20 / Armor 30.
  - **Touch an enemy, forcing them to make a Body Save or lose a sense of your choice for 1d10 seconds.**
  - Skills: **+3 points**.
- **Zoologist**
  - **+5 Strength, +5 Intellect;** Sanity 30 / Fear 45 / Body 25 / Armor 25.
  - **Gain Advantage on Saves vs. xeno-lifeforms. All characters in the presence of a Zoologist must make a Panic Check if the Zoologist is struck by an attack from an alien lifeform.**
  - Skills: Biology, First Aid; **+3 points**.
- **Bounty Hunter**
  - **+5 Strength, +5 Combat;** Sanity 20 / Fear 30 / Body 30 / Armor 40.
  - **+2 Resolve upon successfully capturing/subduing a target alive.**
  - Skills: First Aid, Close Quarters Combat, Weapon Specialization: Unarmed, Joint Manipulation/Submission.
- **Void Urchin**
  - **-10 to all stats;** Sanity 20 / Fear 20 / Body 20 / Armor 20.
  - **Rolls with Advantage on Stress, Stress Check, and Panic Checks.**
  - Skills: Scavenging.
- **Convict**
  - **+10 Speed;** Sanity 20 / Fear 50 / Body 30 / Armor 20.
  - **Any failed Fear Save by the Convict is considered a Critical Failure.**
  - Skills: Rimwise, Pick one: Scavenging or Athletics; **+2 points**.
- **Hybrid**
  - **+15 Strength, +15 Speed;** Sanity 10 / Fear 20 / Body 30 / Armor 25.
  - **Automatically fail Panic Checks.**
  - Skills: Close Quarters Combat, Athletics.
- **Weaver**
  - **+15 Intellect;** Sanity 50 / Fear 50 / Body 10 / Armor 10.
  - **Panic will override the AI interface and launch the ship to random coordinates within range, when applicable.**
  - Skills: Astrogation, Hyperspace.

## 3. Mark Starting Skills and Spend Skill Points

Each class starts with certain Skills, and has a number of points to spend on further Skills.

- **Trained Skills** cost 1 point.
- **Expert Skills** cost 2 points.
- **Master Skills** cost 3 points.

To take an Expert or Master Skill you must first take one of its prerequisite Skills.

## 4. Pick Starting Loadout

Choose a Loadout:

- **Excavation**
  - Crowbar, Hand Welder, Laser Cutter, Body Cam, Bioscanner, Infrared Goggles, Lockpick Set, Vaccsuit (Oxygen Tank, Mag-Boots, Short-range Comms)
- **Exploration**
  - Vibechete, Rigging Gun, Flare Gun, First Aid Kit, Vaccsuit (Long-range Comms, Oxygen Tank), Survey Kit, Water Filter, Locator, Rebreather, Binoculars, Flashlight, Camping Gear, MREs x7
- **Extermination**
  - SMG, Frag Grenade x6, Standard Battle Dress (Heads-up Display, Body Cam, Short-range Comms), Stimpak x6, Electronic Tool Kit
- **Examination**
  - Scalpel, Tranq Pistol, Stun Baton, Hazard Suit, Medscanner, Automed x6, Pain Pills x6, Stimpak x6, Cybernetic Diagnostic Scanner

Roll for a random Trinket and Patch.

| D100 | Trinket | D100 | Trinket | D100 | Trinket | D100 | Trinket |
| ----:| ------- | ----:| ------- | ----:| ------- | ----:| ------- |
|  0 | Preserved Insectile Aberration             | 25 | Eerie Mask                                    | 50 | Brass Knuckles                                | 75 | Locket, Hair Braid |
|  1 | Faded Green Poker Chip                     | 26 | Vantablack Marble                             | 51 | Fuzzy Handcuffs                               | 76 | Pick, Miniature |
|  2 | Antique Company Script (Asteroid Mine)     | 27 | Ivory Dice                                    | 52 | Journal of Grudges                            | 77 | Blanket, Fire Retardant |
|  3 | Dessicated Husk Doll                       | 28 | Tarot Cards, Worn, Pyrite Gilded edges        | 53 | Stylized Cigarette Case                       | 78 | Hooded Parka, Fleece-Lined |
|  4 | Alien Pressed Flower (common)              | 29 | Bag of Assorted Teeth                         | 54 | Ball of Assorted Gauge Wire                   | 79 | BB Gun |
|  5 | Necklace of Shell Casings                  | 30 | Ashes (A Relative)                            | 55 | Spanner                                       | 80 | Flint Hatchet |
|  6 | Corroded Android Logic Core                | 31 | DNR Beacon Necklace                           | 56 | Switchblade, Ornamental                       | 81 | Pendant: Two Astronauts form a Skull |
|  7 | Pamphlet: *Signs of Parasitical Infection* | 32 | Cigarettes (Grinning Skull)                   | 57 | Powdered Xenomorph Horn                       | 82 | Rubik's Cube |
|  8 | Manual: *Treat Your Rifle Like A Lady*     | 33 | Pills: Areca Nut                              | 58 | Bonsai Tree                                   | 83 | Manual: *Survival: Eat Soup With a Knife* |
|  9 | Bone Knife                                 | 34 | Rejected Application (Colony Ship)            | 59 | Golf Club (Putter)                            | 84 | Sputnik Pin |
| 10 | Calendar: Alien Pin-Up Art                 | 35 | Pamphlet: *Android Overlords*                 | 60 | Trilobite Fossil                              | 85 | Ushanka |
| 11 | Dog Tags (Heirloom)                        | 36 | Smut (Seditious): *The Captain, Ordered*      | 61 | Pamphlet: *A Girl in Every Port*              | 86 | Trucker Cap, Mesh, Grey Alien Logo |
| 12 | Holographic Serpentine Dancer              | 37 | Key (Childhood Home)                          | 62 | Patched Overalls, Personalized                | 87 | Menthol Balm |
| 13 | Snake Whiskey                              | 38 | Manual: *Panic: Harbinger of Catastrophe*     | 63 | Fleshy Thing Sealed in a Murky Jar            | 88 | Pith Helmet |
| 14 | Medical Container, Purple Powder           | 39 | Token: *"Is Your Morale Improving?"*          | 64 | Spiked Bracelet                               | 89 | 10x10 Tarp |
| 15 | Pills: Male Enhancement, Shoddy            | 40 | Phosphorescent Sticks, Neon                   | 65 | Harmonica                                     | 90 | *I Ching*, Missing Sticks |
| 16 | Casino Playing Cards                       | 41 | Pamphlet: *The Indifferent Stars*             | 66 | Manual: *Spacefarer's Almanac* (out of date)  | 91 | Kukri |
| 17 | Lagomorph Foot                             | 42 | Calendar: *Military Battles*                  | 67 | Faded Photograph, A Windswept Heath           | 92 | Trench Shovel |
| 18 | Moonstone Ring                             | 43 | Manual: *Rich Captain, Poor Captain*          | 68 | Stress Ball reads: *Zero Stress in Zero G*    | 93 | Shiv, Sharpened Butter Knife |
| 19 | Manual: *Mining Safety and You*            | 44 | Campaign Poster (Home Planet)                 | 69 | Manual: *Moonshining With Gun Oil & Fuel*     | 94 | Taxidermied Cat |
| 20 | Pamphlet: *Against Human Simulacrum*       | 45 | Pendant: Shell Fragments Suspended in Plastic | 70 | Gyroscope, Bent, Tin                          | 95 | Pamphlet: *Interpreting Sheep Dreams* |
| 21 | Animal Skull, 3 Eyes, Curled Horns         | 46 | Titanium Toothpick                            | 71 | Coffee Cup, Chipped, *HAPPINESS IS MANDATORY* | 96 | Pair of Shot Glasses, Spent Shotgun Shells |
| 22 | Bartender’s Certification (Expired)        | 47 | Gloves, Leather (Xenomorph Hide)              | 72 | Darts, Magnetic                               | 97 | Opera Glasses |
| 23 | Bent Wrench                                | 48 | Pamphlet: *Zen and the Art of Cargo*          | 73 | Spray Paint                                   | 98 | Pamphlet: *The Relic of Flesh* |
| 24 | Prospecting Mug, Dented                    | 49 | Pictorial Pornography, Dogeared, Well Thumbed | 74 | Wanted Poster, Weathered                      | 99 | Miniature Chess Set, Bone, Pieces Missing |

| D100 | Patch | D100 | Patch | D100 | Patch | D100 | Patch |
| ----:| ----- | ----:| ----- | ----:| ----- | ----:| ----- |
|  0 | “#1 Worker” | 25 | Medic Patch (Skull and Crossbones on Logo) | 50 | “SUCK IT UP” | 75 | Grim Reaper Backpatch |
|  1 | Security Guard patch | 26 | HELLO MY NAME IS: | 51 | HMFIC (Head Mother Fucker in Charge) | 76 | отъебись (Get Fucked, Russian) |
|  2 | Blood Type (Reference Patch) | 27 | “Powered By Coffee” | 52 | “Troubleshooter” | 77 | “Smooth Operator” |
|  3 | Red Shirt Logo | 28 | “Take Me To Your Leader” (UFO) | 53 | “IF I’M RUNNING KEEP UP” Backpatch | 78 | Atom Symbol |
|  4 | “Don’t Run You’ll Only Die Tired” Backpatch | 29 | “DO YOUR JOB” | 54 | Crossed Hammers with Wings | 79 | “For Science!” |
|  5 | Poker Hand: Dead Man’s Hand | 30 | “Take My Life (Please)” | 55 | “Keep Well Lubricated” | 80 | “Actually, I AM A Rocket Scientist” |
|  6 | Biohazard Symbol | 31 | “All Out of Fucks To Give” (Astronaut with Turned Out Pockets) | 56 | Soviet Hammer & Sickle | 81 | “Help Wanted” |
|  7 | Mr. Yuck | 32 | Allergic To Bullshit | 57 | “Plays Well With Others” | 82 | Princess |
|  8 | Nuclear Symbol | 33 | “Fix Me First” (Caduceus) | 58 | “Live Free and Die” | 83 | “I Like My Tools Clean / And My Lovers Dirty” |
|  9 | “Eat The Rich” | 34 | “Upstanding Citizen” | 59 | Pin-Up (Nurse): “The Louder You Scream the Faster I Come” | 84 | “GOOD BOY” |
| 10 | “Be Sure: Doubletap” | 35 | NASA Logo | 60 | “Meat Bag” | 85 | Dice (Snake Eyes) |
| 11 | Flame Emoji | 36 | “Cowboy Up” (Crossed Revolvers) | 61 | “I Am Not A Robot” | 86 | “Travel To Distant Exotic Places / Meet Unusual Things / Get Eaten” |
| 12 | Smiley Face (Glow in the Dark) | 37 | Dove in Crosshairs | 62 | Red Gear | 87 | “Good” (Brain) |
| 13 | “Smile: Big Brother is Watching” | 38 | Chibi Cthulhu | 63 | “I Can’t Fix Stupid” | 88 | “Bad Bitch” |
| 14 | Jolly Roger | 39 | “Welcome to the DANGER ZONE” | 64 | “Space IS My Home” (Sad Astronaut) | 89 | “Too Pretty To Die” |
| 15 | Viking Skull | 40 | Skull and Crossed Wrenches | 65 | All Seeing Eye (Medical Style Patch) | 90 | “Fuck Forever” (Roses) |
| 16 | “APEX PREDATOR” (Sabertooth Skull) | 41 | Pin-Up (Succubus) | 66 | “Do I LOOK Like An Expert?” | 91 | Icarus |
| 17 | Pin-Up (Ace of Spades) | 42 | “DILLIGAF?” | 67 | “NOMAD” | 92 | ”Girl’s Best Friend” (Diamond) |
| 18 | Queen of Hearts | 43 | “DRINK / FIGHT / FUCK” | 68 | “I’m Not A Rocket Scientist / But You’re An Idiot” | 93 | Risk of Electrocution Symbol |
| 19 | Pin-Up (Mechanic) | 44 | “Work Hard / Party Harder” | 69 | “LONER” | 94 | Inverted Cross |
| 20 | BOHICA (Bend Over Here It Comes Again) | 45 | Mudflap Girl | 70 | “I Am My Brother’s Keeper” | 95 | “Do You Sign My Paychecks?” Backpatch |
| 21 | Front Towards Enemy (Claymore Mine) | 46 | Fun Meter (reading: Bad Time) | 71 | “Mama Tried” | 96 | “I &hearts; Myself” |
| 22 | Pin-Up (Riding Missile) | 47 | “GAME OVER” (Bride & Groom) | 72 | Black Widow Spider | 97 | Double Cherry |
| 23 | FUBAR | 48 | Heart | 73 | “My Other Ride Married You” | 98 | “Volunteer” |
| 24 | “I’m A (Love) Machine” | 49 | “IMPROVE / ADAPT / OVERCOME” | 74 | “One Size Fits All” (Grenade) | 99 | “Solve Et Coagula” (Baphomet) |
