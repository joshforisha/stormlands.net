| Mercenary | Hits | Combat | Instinct | Loyalty | Advance | Salary | Loadout | Skills |
| --------- | ----:| ------:| --------:| -------:| -------:| ------:| ------- | ------ |
| Archaeologist     | 1 | 20 | 15 | 5d10 |  500 |  750 | Excavation    | Archeology |
| Asteroid Miner    | 2 | 25 | 25 | 4d10 |  125 |  500 | Excavation    | Rimsmart, Asteroid Mining |
| Android           | 2 | 25 | 35 | 5d10 | 1000 | 5000 | Any package   | Pick one Trained and one Expert |
| Captain           | 3 | 30 | 40 | 5d10 | 2000 | 8000 | Exploration   | Piloting, Vehicle Specialization, Command |
| Courier           | 2 | 20 | 30 | 6d10 |   75 |  250 | Exploration   | Zero-G, Rimsmart |
| Doctor            | 1 | 15 | 25 | 6d10 | 2000 | 6000 | Examination   | First Aid, Pathology |
| Engineer          | 2 | 25 | 25 | 6d10 |  750 | 4000 | Exploration   | Mechanical Repair, Engineering |
| Gunner            | 2 | 30 | 25 | 5d10 |  500 | 1500 | Exploration   | Gunnery |
| Marine Grunt      | 2 | 25 | 25 | 4d10 |  150 |  600 | Extermination | Military Training |
| Marine Officer    | 2 | 30 | 35 | 6d10 |  500 | 2000 | Any package   | Military Training, Command |
| Marine Specialist | 3 | 35 | 30 | 5d10 |  275 | 1500 | Extermination | Military Training, Weapon Specialization (Pick Weapon) |
| Navigator         | 1 | 15 | 20 | 5d10 |  400 | 2000 | Exploration   | Astrogation |
| Pilot             | 1 | 15 | 25 | 5d10 |  500 | 3000 | Exploration   | Piloting |
| Priest            | 1 | 15 | 20 | 4d10 |   60 |  200 | None          | Theology or Mysticism
| Psychologist      | 1 | 15 | 15 | 5d10 |  250 | 1000 | Examination   | Psychology |
| Researcher        | 1 | 15 | 10 | 5d10 |  400 | 1500 | Examination   | Pick One: Biology, Geology, Computers, Mathematics, Art, Chemistry, Genetics, Planetology, Physics |
| Sophontologist    | 1 | 15 | 10 | 6d10 |  500 | 1750 | Examination   | Sophontology |
| Surgeon           | 1 | 15 | 20 | 6d10 | 2000 | 7000 | Examination   | First Aid, Pathology, Surgery |
| Void Urchin       | 2 | 25 | 40 | 3d10 |   40 |  100 | None          | Rimsmart, Mysticism |
