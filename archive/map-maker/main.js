import { $, svg } from './utils.js'

const map = $('#Map')
const size = { height: window.innerHeight, width: window.innerWidth }
const transformMatrix = [1, 0, 0, 1, 0, 0]
const worldMap = $('#WorldMap')
const worldSize = 5 // TODO: Drive this with input

function points (scale, pairs) {
  return pairs.map(pair => pair.map(x => x * scale).join(',')).join(' ')
}

const fullHexPoints = [
  [30, 0],
  [56, 15],
  [56, 45],
  [30, 60],
  [4, 45],
  [4, 15]
]

const leftCutoffHexPoints = [
  [30, 0],
  [56, 15],
  [56, 45],
  [30, 60]
]

const northSpanHexPoints = span => {
  const eastEdge = 30 + (span - 1) * 52 + 26

  return [
    [17, 7.5],
    [eastEdge - 13, 7.5],
    [eastEdge, 15],
    [eastEdge, 45],
    [eastEdge - 26, 60],
    [eastEdge - 39, 52.5],
    [43, 52.5],
    [30, 60],
    [4, 45],
    [4, 15]
  ]
}

const rightCutoffHexPoints = [
  [30, 0],
  [30, 60],
  [4, 45],
  [4, 15]
]

const southSpanHexPoints = span => {
  const eastEdge = 30 + (span - 1) * 52 + 26

  return [
    [30, 0],
    [43, 7.5],
    [eastEdge - 39, 7.5],
    [eastEdge - 26, 0],
    [eastEdge, 15],
    [eastEdge, 45],
    [eastEdge - 13, 52.5],
    [17, 52.5],
    [4, 45],
    [4, 15]
  ]
}

function drawTile (parent, { id, cutoff, hemisphere, scale = 1, span = 1, terrain, x, y }) {
  const tx = x - 30
  const ty = y - 30

  const tile = svg('g', {
    class: 'hex-tile',
    id,
    transform: `translate(${tx} ${ty})`
  })

  let hexPoints = fullHexPoints
  if (cutoff === 'left') hexPoints = leftCutoffHexPoints
  if (cutoff === 'right') hexPoints = rightCutoffHexPoints
  if (span > 1 && hemisphere === 'north') hexPoints = northSpanHexPoints(span)
  if (span > 1 && hemisphere === 'south') hexPoints = southSpanHexPoints(span)

  const hex = svg('polygon', {
    class: 'hexagon',
    points: points(scale, hexPoints)
  })
  tile.appendChild(hex)

  const text = svg('text', {
    x: 4 + 26 * span,
    y: 36
  })
  text.textContent = terrain

  tile.appendChild(text)
  parent.querySelector('.hex-tiles')
    .appendChild(tile)

  /*
  parent.querySelector('.highlight-tiles')
    .appendChild(hexagon({ 'data-hex': id, transform: `translate(${tx} ${ty})` }))
  */
}

function pan (dx, dy) {
  transformMatrix[4] += dx
  transformMatrix[5] += dy
  transform()
}

function resize () {
  size.height = window.innerHeight
  size.width = window.innerWidth
  worldMap.setAttribute('height', `${size.height}px`)
  worldMap.setAttribute('width', `${size.width}px`)
  worldMap.setAttributeNS(null, 'viewBox', `0 0 ${size.width} ${size.height}`)
}

function transform () {
  map.setAttributeNS(
    null,
    'transform',
    `matrix(${transformMatrix.join(' ')})`
  )
}

function zoom (scale, center) {
  if (transformMatrix[0] * scale <= 0.5) return
  for (let i = 0; i < 6; i++) transformMatrix[i] *= scale
  transformMatrix[4] += (1 - scale) * center.x
  transformMatrix[5] += (1 - scale) * center.y
  transform()
}

window.addEventListener('resize', resize)

worldMap.addEventListener('wheel', event => {
  event.preventDefault()
  if (event.ctrlKey) {
    zoom(1 - 0.01 * event.deltaY, { x: event.clientX, y: event.clientY })
  } else pan(0 - event.deltaX, 0 - event.deltaY)
})

resize()

// Size 5 World:
// Row 0 -> cols -0-5-10-15-20-25-
// Row 1 -> cols 0-4 5-9 10-14 15-19 20-24
// Row 2 -> cols -0 1-4 5 6-9 10 11-14 15 16-19 20 21-24 25-
// Row 3 -> cols 0 1-3 4 5 6-8 9 10 11-13 14 15 16-18 19 20 21-23 24
// Row 4 -> cols -0 1 2-3 4 5 6 7-8 9 10 11 12-13 14 15 16 17-18 19 20 21 22-23 24 25-
// Row 5 -> all
// Row 6 -> -all-
// Row 7 -> all
// Row 8 -> -all-
// Row 9 -> all
// Row 10 -> -all-
// Row 11 -> cols -0 1 2 3 4-5 6 7 8 9-10 11 12 13 14-15 16 17 18 19-20 21 22 23 24-
// Row 12 -> cols -1 2 3 4-6 7 8 9-11 12 13 14-16 17 18 19-21 22 23 24-
// Row 13 -> cols -1 2 3-6 7 8-11 12 13-16 17 18-21 22 23-
// Row 14 -> cols -2 3-7 8-12 13-17 18-22 23-
// Row 15 -> cols -2-7-12-17-22-

const numColumns = worldSize * 5
const numRows = worldSize * 3 + 1
const startX = 0
const startY = 30

for (let r = 0; r < numRows; r++) {
  for (let c = 0; c < numColumns + 1; c++) {
    if (r % 2 > 0 && c >= numColumns) continue

    let span = 1
    const d = c % worldSize
    if (r === 0 && c > 0) continue
    if (r === 0) span = 26
    if (r === 1 && d !== 0) continue
    if (r === 1 && d === 0) span = 5
    if (r === 2 && (d !== 0 && d !== 1)) continue
    if (r === 2 && d === 1) span = 4
    if (r === 3 && (d !== 0 && d !== 1 && d !== 4)) continue
    if (r === 3 && d === 1) span = 3
    if (r === 4 && d === 3) continue
    if (r === 4 && d === 2) span = 2
    if (r === 11 && c > 0 && d === 0) continue
    if (r === 11 && d === 4) span = 2
    if (r === 12 && (
      (c === 0) ||
      (c > 0 && (d !== 2 && d !== 3 && d !== 4))
    )) continue
    if (r === 12 && d === 4) span = 3
    if (r === 13 && (d !== 2 && d !== 3)) continue
    if (r === 13 && d === 3) span = 4
    if (r === 14 && d !== 3) continue
    if (r === 14 && d === 3) span = 5
    if (r === 15 && c !== 3) continue
    if (r === 15) span = 16

    const x = startX + (r % 2 ? 26 : 0) + c * 52
    const y = startY + r * 45

    let cutoff
    if (r % 2 === 0 && c === 0) cutoff = 'left'
    else if (r % 2 === 0 && c === 25) cutoff = 'right'

    drawTile(worldMap,
      {
        cutoff,
        hemisphere: r < (2 * worldSize) ? 'north' : 'south',
        id: `WorldHex-${c}${r}`,
        span,
        terrain: `${r}.${c}`,
        x,
        y
      })
  }
}

/* Terrain Types ---------------------------------------------------------------
  Air Corridor
  Airpad
  Airport (Heavy)
  Airport (Medium)
  Airport (Very Heavy)
  Airstrip (Light)
  Airstrip (Very Light)
  Arcology
  Baked Lands
  Caverns
  Chasm
  City
  Clear
  Crater
  Cropland
  Desert
  Dome
  Estate
  Exotic
  Frozen Lands
  Grid
  High Speed
  Highway
  Ice Field
  Islands
  Lake
  Marsh
  Mine
  Mountains
  Ocean
  Ocean Abyss
  Ocean Depth
  Oil
  Penal
  Precipice
  Reserve
  Resources
  River
  Road
  Rough
  Rough Woods
  Ruins
  Rural
  Shore
  Starport
  Suburbs
  Swamp
  Town
  Trail
  Twilight
  Volcano
  Wasteland
  Woodlands
  ice Cap
*/
