export const $ = q => document.querySelector(q)
export const $$ = q => document.querySelectorAll(q)

export function hexagon (attrs = {}, pointy = true) {
  return svg(
    'polygon',
    {
      ...attrs,
      class: 'hexagon',
      points: pointy
        ? '30,0 56,15 56,45 30,60 4,45 4,15'
        : '60,30 45,56 15,56 0,30 15,4 45,4'
    }
  )
}

export function svg (el, attributes = {}, nsAttributes = {}, children = []) {
  const e = document.createElementNS('http://www.w3.org/2000/svg', el)

  Object.keys(attributes).forEach(k => {
    e.setAttribute(k, attributes[k])
  })

  Object.keys(nsAttributes).forEach(k => {
    e.setAttributeNS('http://www.w3.org/1999/xlink', k, nsAttributes[k])
  })

  children.forEach(child => {
    e.appendChild(child)
  })

  return e
}
