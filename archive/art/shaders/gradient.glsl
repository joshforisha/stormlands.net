#version 100

#define HASHSCALE3 vec3(0.1031, 0.103, 0.0973)
#define M_PI 3.1415926535897932384626433832795
#define M_2PI 6.2831853071795864769252867665590

precision mediump float;

// uniform float u_seed;
uniform vec2 u_resolution;

float hash1(in float k) {
  float m = 64.0; // 0.5 * (sqrt(5.0) - 1.0); // Power of 2
  float A = 0.5 * (sqrt(5.0) - 1.0); // 9973.0; // Random-looking real number
  float s = k * A;
  float x = fract(s);
  return floor(m * x);
}

vec3 hash(vec3 p) {
  p = fract(p * HASHSCALE3);
  p += dot(p, p.yxz + 19.19);
  return fract((p.xxy + p.yxx) * p.zyx);
}

float noise(in float scale, in vec3 pos) {
  vec3 i = floor(scale * pos);
  vec3 f = fract(scale * pos);
  vec3 u = f * f * (3.0 - 2.0 * f);
  return mix(
    mix(
      mix(dot(hash(i + vec3(0.0, 0.0, 0.0)), f - vec3(0.0, 0.0, 0.0)),
          dot(hash(i + vec3(1.0, 0.0, 0.0)), f - vec3(1.0, 0.0, 0.0)), u.x),
      mix(dot(hash(i + vec3(0.0, 1.0, 0.0)), f - vec3(0.0, 1.0, 0.0)),
          dot(hash(i + vec3(1.0, 1.0, 0.0)), f - vec3(1.0, 1.0, 0.0)), u.x),
      u.y
    ),
    mix(
      mix(dot(hash(i + vec3(0.0, 0.0, 1.0)), f - vec3(0.0, 0.0, 1.0)),
          dot(hash(i + vec3(1.0, 0.0, 1.0)), f - vec3(1.0, 0.0, 1.0)), u.x),
      mix(dot(hash(i + vec3(0.0, 1.0, 1.0)), f - vec3(0.0, 1.0, 1.0)),
          dot(hash(i + vec3(1.0, 1.0, 1.0)), f - vec3(1.0, 1.0, 1.0)), u.x),
      u.y
    ),
    u.z
  ) + 1.0;
}

float noise2(in float scale, in vec2 p) {
  return noise(scale, vec3(p, 0.0));
}

float fractalCylinder(in int octaves, in float scale, in vec2 pos) {
  float circumference = 2.0;
  float radius = circumference / M_2PI;
  float value = 0.0;
  for (int octave = 0; octave < 256; octave++) {
    if (octave >= octaves) break;
    float freq = scale * pow(2.0, float(octave));
    float nx = pos.x / circumference;
    float rads = nx * M_2PI;
    float a = radius * sin(rads);
    float b = radius * cos(rads);
    value += noise(freq, vec3(a, pos.y, b)) * pow(0.5, float(octave));
  }
  return value / (2.0 - (1.0 / pow(2.0, float(octaves - 1))));
}

float pattern(in vec2 p) {
  vec2 q = vec2(
    noise2(11.3, p + vec2(0.0, 0.0)),
    noise2(7.1, p + vec2(5.2, 1.3))
  );

  vec2 r = vec2(
    noise2(1.3, p + 4.0 * q + vec2(1.7, 9.2)),
    noise2(1.3, p + 4.0 * q + vec2(8.3, 2.8))
  );

  return noise2(1.3, 4.0 * r);
}

void main() {
  vec2 st = gl_FragCoord.xy / u_resolution.y;
  float latitudeInfluence = 1.0 - abs(st.y - 0.5) / 0.5;
  // st += u_seed;

  float elevation = 0.003 * hash1(st.x);
  // fractalCylinder(8, 3.5, st);
  // float elevation = pattern(st);

  float rainfall = fractalCylinder(4, 5.1, st);
  float temperature =
    0.6 * latitudeInfluence +
    0.4 * fractalCylinder(2, 4.3, st);

  gl_FragColor = vec4(elevation, rainfall, temperature, 0.0);
}
