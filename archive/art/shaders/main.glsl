#version 100

precision mediump float;

#define HASHSCALE4 vec4(0.1031, 0.103, 0.0973, 0.1099)
#define M_2PI 6.2831853071795864769252867665590
#define M_PI 3.1415926535897932384626433832795

#define BLACK vec3(0.0, 0.0, 0.0)
#define WHITE vec3(1.0, 1.0, 1.0)

uniform int u_octaves;
uniform vec2 u_qxOffset;
uniform vec2 u_qyOffset;
uniform vec2 u_resolution;
uniform vec2 u_rxOffset;
uniform vec2 u_ryOffset;
uniform vec2 u_seed;
uniform vec3 u_highColor;
uniform vec3 u_lowColor;

const float H = 0.5; // Self-similarity of noise
const float G = exp2(-H);

vec3 contrast(in vec3 c, in float f) {
  return vec3(
    pow(c.r, 1.0 - f * (c.r - 0.5)),
    pow(c.g, 1.0 - f * (c.g - 0.5)),
    pow(c.b, 1.0 - f * (c.b - 0.5))
  );
}

float hash(in vec2 p) {
  vec3 p3 = fract(vec3(p.xyx) * 0.1031);
  p3 += dot(p3, p3.yzx + 33.33);
  return fract((p3.x + p3.y) * p3.z);
}

float noise(in vec2 p) {
  vec2 e = floor(p);
  vec2 f = fract(p);
  f = f * f * (3.0 - 2.0 * f);
  float a = hash(p + vec2(0.0, 0.0));
  float b = hash(p + vec2(1.0, 0.0));
  float c = hash(p + vec2(0.0, 1.0));
  float d = hash(p + vec2(1.0, 1.0));
  return mix(mix(a, b, f.x), mix(c, d, f.x), f.y);
}

float fbm(in int octaves, in vec2 p) {
  float f = 1.0;
  float a = 0.5;
  float t = 0.0;
  for (int o = 0; o < 100; o++) {
    if (o >= octaves) break;
    t += a * hash(f * p);
    f *= 2.0;
    a *= G;
  }
  return t;
}

void main() {
  vec2 pos = u_seed.xy + gl_FragCoord.xy;

  // float a = hash(pos);
  float a = fbm(u_octaves, pos);

  // vec3 color = contrast(mix(BLACK, WHITE, a), -19.0);
  vec3 color = mix(BLACK, WHITE, a);

  gl_FragColor = vec4(color, 1.0);
}
