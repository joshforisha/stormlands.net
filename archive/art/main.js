import * as Sketch from 'webgl-sketch'
import fragmentShaderSource from './shaders/main.glsl'
import vertexShaderSource from './shaders/basic-vertex.glsl'

const roff = () => Math.random() * 20 - 10

let sketch

function rand(min, max) {
  return Math.floor(min + Math.random() * (max - min))
}

function random(min, max) {
  return min + Math.random() * (max - min)
}

function render() {
  const existingCanvas = document.querySelector('canvas')
  if (existingCanvas) {
    document.body.removeChild(existingCanvas)
    sketch = null
  }

  const seed = [
    (Math.random() < 0.5 ? -1 : 1) * Math.random() * 999.999,
    (Math.random() < 0.5 ? -1 : 1) * Math.random() * 999.999
  ]

  const largeMonitor = [3008, 1692]
  const macbookScreen = [1728, 1117]

  const a = 0.0
  const b = 0.2
  const c = 0.3
  const d = 0.5

  const lowColor = [random(a, b), random(a, b), random(a, b)]
  const highColor = [random(c, d), random(c, d), random(c, d)]

  // console.log('Seed:', seed)
  // console.log('Colors:', lowColor, highColor)

  sketch = Sketch.create({
    fragmentShaderSource,
    size: macbookScreen,
    uniforms: {
      highColor,
      lowColor,
      octaves: 8,
      qxOffset: [roff(), roff()],
      qyOffset: [roff(), roff()],
      rxOffset: [roff(), roff()],
      ryOffset: [roff(), roff()],
      seed
    },
    vertexShaderSource
  })

  document.body.appendChild(sketch.canvas)
}

document
  .querySelector('button#Render')
  .addEventListener('click', () => render())

render()
