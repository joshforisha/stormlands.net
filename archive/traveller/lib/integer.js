export function constrain (x, min, max) {
  return Math.max(min, Math.min(max, x))
}

export function rand (min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min
}
