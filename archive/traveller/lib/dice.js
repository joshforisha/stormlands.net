export function D (num = 1) {
  const roll = Math.floor(Math.random() * 5 + 1)
  if (num > 1) return roll + D(num - 1)
  return roll
}
