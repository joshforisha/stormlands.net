export function count (xs, match) {
  return xs.filter(match).length
}

export function draw (xs) {
  return xs[Math.floor(Math.random() * xs.length)]
}

export function initialize (count, make) {
  const xs = []
  for (let i = 0; i < count; i++) xs.push(make(i))
  return xs
}

export function omit (i, xs) {
  return [...xs.slice(0, i), ...xs.slice(i + 1)]
}

export function takeRandom (num, xs) {
  if (num === 0) return []
  const i = Math.floor(Math.random() * xs.length)
  if (num === 1) return [xs[i]]
  return [xs[i], ...takeRandom(num - 1, omit(i, xs))]
}
