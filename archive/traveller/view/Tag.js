import * as React from 'react'
import styled from 'styled-components'

const CloseButton = styled.button``

const Container = styled.div``

export default function Tag ({ children, onRemove }) {
  return (
    <Container>
      <CloseButton onClick={onRemove}>&close;</CloseButton>
      {children}
    </Container>
  )
}
