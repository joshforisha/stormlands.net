import styled from 'styled-components'

const Button = styled.button`
  background-color: var(--red);
  border-width: 0px;
  color: var(--white);
  cursor: pointer;
  font-size: 16px;
  padding: 8px 16px;
`

export default Button
