import * as React from 'react'
import styled from 'styled-components'

const Page = styled.div``

export default function MarkdownPage ({ content }) {
  return <Page dangerouslySetInnerHTML={{ __html: content }} />
}
