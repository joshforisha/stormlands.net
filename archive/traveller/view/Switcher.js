import * as React from 'react'
import styled from 'styled-components'

const Option = styled.div`
  background-color: ${({ current }) =>
    current ? 'var(--gray-dark)' : 'var(--gray-light)'};
  color: ${({ current }) => (current ? 'var(--white)' : 'var(--black)')};
  cursor: pointer;
  padding: 4px 8px;

  &:not(:first-of-type) {
    border-left: 1px solid var(--white);
  }
`

const Container = styled.div`
  display: flex;
`

export default function Switcher ({ current, onSelect, options }) {
  const optionButtons = options.map(
    (option, i) => (
      <Option
        current={current === option}
        key={i}
        onClick={() => onSelect(option)}
      >
        {option}
      </Option>
    )
  )

  return <Container>{optionButtons}</Container>
}
