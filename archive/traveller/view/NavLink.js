import * as React from 'react'
import styled, { css } from 'styled-components'

const Anchor = styled.a`
  border-bottom: 1px solid var(--gray);
  color: var(--white);
  display: block;
  font-size: 14px;
  outline: none;
  padding: 4px 8px;
  text-decoration: none;
  text-transform: uppercase;

  &:first-of-type {
    border-top: 1px solid var(--gray);
  }

  &:hover {
    color: var(--white);
    text-decoration: none;

    ${({ current }) =>
      !current &&
      css`
        background-color: var(--gray-darker);
      `}
  }

  ${({ current }) =>
    current &&
    css`
      background-color: var(--gray-dark);
    `}
`

export default function NavLink ({ children, href }) {
  return (
    <Anchor current={window.location.hash === href ? 1 : 0} href={href}>
      {children}
    </Anchor>
  )
}
