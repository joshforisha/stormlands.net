import * as React from 'react'
import NavLink from '~/view/NavLink'
import styled from 'styled-components'

const Container = styled.nav`
  background-color: var(--black);
  color: var(--white);

  @media screen and (min-width: 600px) {
    height: 100vh;
    overflow-y: scroll;
    position: fixed;
    top: 0px;
    width: 240px;
  }
`

const Links = styled.ul`
  list-style-type: none;
  margin: 8px 0px 0px;
  padding: 0px;
`

const Title = styled.a`
  border-top: 4px solid var(--red);
  color: var(--white);
  display: block;
  font-size: 20px;
  font-style: italic;
  font-weight: 500;
  margin-top: 1rem;
  padding-left: 1rem;
  text-decoration: none;

  &:hover {
    color: var(--red);
    text-decoration: none;
  }
`

export default function Navigation () {
  return (
    <Container>
      <Title href='#/'>Traveller</Title>
      <Links>
        <NavLink href='#/combat'>Combat</NavLink>
        <NavLink href='#/computers'>Computers</NavLink>
        <NavLink href='#/dangers'>Dangers</NavLink>
        <NavLink href='#/encounters'>Encounters</NavLink>
        <NavLink href='#/equipment'>Equipment</NavLink>
        <NavLink href='#/healing'>Healing</NavLink>
        <NavLink href='#/law'>Law</NavLink>
        <NavLink href='#/medical-supplies'>Medical Supplies</NavLink>
        <NavLink href='#/skill-list'>Skill List</NavLink>
        <NavLink href='#/skills'>Skills</NavLink>
        <NavLink href='#/spacecraft'>Spacecraft</NavLink>
        <NavLink href='#/spaceports'>Spaceports</NavLink>
        <NavLink href='#/tasks'>Tasks</NavLink>
        <NavLink href='#/trade'>Trade</NavLink>
        <NavLink href='#/vehicles'>Vehicles</NavLink>
        <NavLink href='#/weapons'>Weapons</NavLink>
      </Links>
    </Container>
  )
}
