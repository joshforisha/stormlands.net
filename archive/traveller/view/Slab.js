import * as React from 'react'
import styled from 'styled-components'

const Container = styled.div``

const Label = styled.label`
  line-height: 32px;
`

const Value = styled.div`
  font-size: 40px;
  font-weight: 300;
`

export default function Slab ({ className, label, value }) {
  return (
    <Container className={className}>
      <Value>{value}</Value>
      <Label>{label}</Label>
    </Container>
  )
}
