import * as React from 'react'
import styled, { css } from 'styled-components'

const Td = styled.td`
  ${({ align }) =>
    align &&
    css`
      text-align: ${align};
    `}
`

const Tr = styled.tr`
  ${({ onClick }) =>
    onClick &&
    css`
      cursor: pointer;

      &:hover > td {
        background-color: var(--gray-light) !important;
      }
    `}
`

export const Align = {
  Center: 'center',
  Left: 'left',
  Right: 'right'
}

export default function Table ({ columns, data, onRowClick }) {
  const headerCells = columns.map(
    c => <th key={c.title}>{c.title}</th>
  )

  const bodyRows = data.map(
    (record, i) => {
      const cells = columns.map(
        (c, ci) => {
          let view = null
          if ('view' in c) view = c.view(record)
          else if ('key' in c) view = record[c.key]

          return (
            <Td align={c.align} key={ci}>
              {view}
            </Td>
          )
        }
      )

      let onClick = null
      if (onRowClick) {
        onClick = () => onRowClick(record)
      }

      return (
        <Tr key={i} onClick={onClick}>
          {cells}
        </Tr>
      )
    }
  )

  return (
    <table>
      <thead>
        <tr>{headerCells}</tr>
      </thead>
      <tbody>{bodyRows}</tbody>
    </table>
  )
}
