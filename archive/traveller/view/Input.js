import styled from 'styled-components'

const Input = styled.input`
  font-size: 16px;
  padding: 6px;
`

export default Input
