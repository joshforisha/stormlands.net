import * as React from 'react'
import Navigation from '~/view/Navigation'
import routes from '~/routes'
import styled from 'styled-components'
import useHashRoutes from '~/lib/useHashRoutes'
import { render } from 'react-dom'

const Container = styled.div`
`

const Main = styled.main`
  padding: 16px 32px;

  a {
    color: var(--red);
  }

  @media screen and (min-width: 600px) {
    margin-left: 240px;
  }
`

function App () {
  const content = useHashRoutes(routes)

  return (
    <Container>
      <Navigation />
      <Main>{content}</Main>
    </Container>
  )
}

render(<App />, document.getElementById('App'))
