import * as React from 'react'
import combat from '~/pages/combat.md'
import computers from '~/pages/computers.md'
import dangers from '~/pages/dangers.md'
import encounters from '~/pages/encounters.md'
import equipment from '~/pages/equipment.md'
import healing from '~/pages/healing.md'
import law from '~/pages/law.md'
import MarkdownPage from '~/view/MarkdownPage'
import medicalSupplies from '~/pages/medical-supplies.md'
import robotConstruction from '~/pages/robot-construction.md'
import skillList from '~/pages/skill-list.md'
import skills from '~/pages/skills.md'
import spacecraft from '~/pages/spacecraft.md'
import spaceports from '~/pages/spaceports.md'
import tasks from '~/pages/tasks.md'
import trade from '~/pages/trade.md'
import vehicles from '~/pages/vehicles.md'
import weapons from '~/pages/weapons.md'

const routes = {
  '#/combat': <MarkdownPage content={combat} />,
  '#/computers': <MarkdownPage content={computers} />,
  '#/dangers': <MarkdownPage content={dangers} />,
  '#/encounters': <MarkdownPage content={encounters} />,
  '#/equipment': <MarkdownPage content={equipment} />,
  '#/healing': <MarkdownPage content={healing} />,
  '#/law': <MarkdownPage content={law} />,
  '#/medical-supplies': <MarkdownPage content={medicalSupplies} />,
  '#/robot-construction': <MarkdownPage content={robotConstruction} />,
  '#/skill-list': <MarkdownPage content={skillList} />,
  '#/skills': <MarkdownPage content={skills} />,
  '#/spacecraft': <MarkdownPage content={spacecraft} />,
  '#/spaceports': <MarkdownPage content={spaceports} />,
  '#/tasks': <MarkdownPage content={tasks} />,
  '#/trade': <MarkdownPage content={trade} />,
  '#/vehicles': <MarkdownPage content={vehicles} />,
  '#/weapons': <MarkdownPage content={weapons} />
}

export default routes
