import * as S from '~/data/skill'
import { roll } from '~/data/score'
import { takeRandom } from '~/lib/list'
import {
  addAge,
  addEntryBonuses,
  addHistory,
  addSkills,
  modify
} from '~/data/character'

export const university = {
  apply: ({ education, socialStanding }, term) => {
    const termDM = 1 - term
    const socDM = socialStanding >= 9 ? 1 : 0
    const entrance = roll(education) + termDM + socDM

    return entrance >= 7
  },
  attend: (character) => {
    let student = { ...character }

    const skills = takeRandom(2, [
      'Admin',
      'Advocate',
      ['Animals', S.animalsSpecialties],
      ['Art', S.artSpecialties],
      'Astrogation',
      ['Electronics', S.electronicsSpecialties],
      ['Engineer', S.engineerSpecialties],
      'Language',
      'Medic',
      'Navigation',
      ['Profession', S.professionSpecialties],
      ['Science', S.scienceSpecialties]
    ])

    student = addSkills(
      [
        [S.base(skills[0]), 0],
        [S.chooseSpecialty(skills[1]), 1]
      ],
      student
    )

    student = addHistory('Attended University', student)
    student = modify(1, 'education', student)
    student = addAge(4, student)

    const grad = roll(student.intellect)
    if (grad >= 11) {
      const withHonours = grad >= 11

      student = addSkills(
        [
          [S.chooseSpecialty(skills[0]), 1],
          [S.chooseSpecialty(skills[1]), 2]
        ],
        student
      )
      student = modify(2, 'education', student)
      student = addEntryBonuses(
        {
          Agent: withHonours ? 2 : 1,
          Army: withHonours ? 2 : 1,
          'Citizen (corporate)': withHonours ? 2 : 1,
          'Entertainer (journalist)': withHonours ? 2 : 1,
          Marines: withHonours ? 2 : 1,
          Navy: withHonours ? 2 : 1,
          Scholar: withHonours ? 2 : 1,
          Scouts: withHonours ? 2 : 1
        },
        student
      )
      // TODO: Can roll Commission going into Military career (DM+2 honours)
    }

    return student
  }
}
