import { D } from '~/lib/dice'

export function mod (score) {
  if (score < 1) return -3
  if (score < 3) return -2
  if (score < 6) return -1
  if (score < 9) return 0
  if (score < 12) return 1
  if (score < 15) return 2
  return 3
}

export function roll (score) {
  return D(2) + mod(score)
}
