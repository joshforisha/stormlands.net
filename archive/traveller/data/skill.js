import { takeRandom } from '~/lib/list'

export const animalsSpecialties = ['training', 'veterinary']

export const artSpecialties = [
  'holography',
  'instrument',
  'performer',
  'visual media',
  'write'
]

export const electronicsSpecialties = [
  'comms',
  'computers',
  'remote ops',
  'sensors'
]

export const engineerSpecialties = [
  'j-drive',
  'life support',
  'm-drive',
  'power'
]

export const professionSpecialties = [
  'belter',
  'biologicals',
  'civil engineering',
  'construction',
  'hydroponics',
  'polymers'
]

export const scienceSpecialties = [
  'archaeology',
  'astronomy',
  'biology',
  'chemistry',
  'cosmology',
  'cybernetics',
  'economics',
  'genetics',
  'history',
  'linguistics',
  'philosophy',
  'physics',
  'planetology',
  'psionicology',
  'psychology',
  'robotics',
  'sophontology',
  'xenology'
]

export function base (skill) {
  if (Array.isArray(skill)) return skill[0]
  return skill
}

export function chooseSpecialty (skill) {
  if (Array.isArray(skill)) return [base(skill), ...takeRandom(1, skill[1])]
  return skill
}

export function specialty (skill) {
  if (Array.isArray(skill)) return skill[1]
}
