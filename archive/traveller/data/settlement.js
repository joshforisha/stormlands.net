import { constrain } from '~/lib/integer'
import { D } from '~/lib/dice'
import { initialize } from '~/lib/list'

export const Terrain = {
  Desert: 'Desert',
  Forest: 'Forest',
  ForestedHills: 'Forested Hills',
  Hills: 'Hills',
  Mountains: 'Mountains',
  Plains: 'Plains'
}

function generateTerrain () {
  switch (constrain(D(2) - 7, 0, 5)) {
    case 0:
      return 'Plains'
    case 1:
      return 'Forest'
    case 2:
      return 'Hills'
    case 3:
      return 'Forested Hills'
    case 4:
      return 'Mountains'
    case 5:
      return 'Desert'
  }
}

export function generate () {
  const size = constrain(D(2) - 2, 0, 10)
  const terrain = initialize(Math.max(1, size - 6), generateTerrain)

  // TODO const numDeserts = count(terrain, t => t === Terrain.Desert);

  return { size, terrain }
}
