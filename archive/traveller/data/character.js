import { D } from '~/lib/dice'
import { mod, roll } from '~/data/score'
import { takeRandom } from '~/lib/list'
import { university } from '~/data/education'

const startingBackgroundSkills = [
  'Admin',
  'Animals',
  'Art',
  'Athletics',
  'Carouse',
  'Drive',
  'Electronics',
  'Flyer',
  'Language',
  'Mechanic',
  'Medic',
  'Profession',
  'Science',
  'Seafarer',
  'Streetwise',
  'Survival',
  'Vacc Suit'
]

export function addAge (years, character) {
  return { ...character, age: character.age + years }
}

export function addEntryBonuses (bonuses, character) {
  // TODO
  return character
}

export function addHistory (note, character) {
  return { ...character, history: [...character.history, note] }
}

export function addSkill (skill, score, character) {
  const skills = [...character.skills]
  let newScore = 0

  if (Array.isArray(skill)) {
    const [base] = skill

    if (!skills.some(([s]) => s === base)) {
      skills.push([base, 0])
    }
    newScore = 1
  }

  const index = skills.findIndex(([s]) => s === skill)
  if (index > -1) {
    skills[index] = [
      skill,
      typeof score === 'number' ? score : skills[index][1] + 1
    ]
  } else {
    skills.push([skill, typeof score === 'number' ? score : newScore])
  }

  return { ...character, skills }
}

export function addSkills (skillScores, character) {
  let trav = { ...character }
  skillScores.forEach(([skill, score]) => {
    trav = addSkill(skill, score, trav)
  })
  return trav
}

export function attendEducation (
  term,
  character
) {
  if (roll(character.intellect) + roll(character.education) >= 12) {
    const enterUniversity = university.apply(character, term)
    if (enterUniversity) {
      character = addHistory('Passed entrance into University', character)
      character = university.attend(character)
      return [character, true]
    }

    character = addHistory('Failed entrance into University', character)
    return [character, false]
  }

  return [character, false]
}

export function draft (term, character) {
  // TODO
  return character
}

export function enterCareer (term, character) {
  // TODO
  return [character, false]
}

export function generate () {
  const strength = D(2)
  const dexterity = D(2)
  const endurance = D(2)
  const intellect = D(2)
  const education = D(2)
  const socialStanding = D(2)

  const numBackgroundSkills = mod(education) + 3
  const skills = takeRandom(
    numBackgroundSkills,
    startingBackgroundSkills
  ).map(s => [s, 0])

  let character = {
    age: 18,
    dexterity,
    education,
    endurance,
    history: [],
    intellect,
    skills,
    socialStanding,
    strength
  }

  const attendedPreCareerEducation = false
  for (let term = 1; term < 10; term++) {
    character = addHistory(`Term ${term}`, character)

    if (term < 4 && !attendedPreCareerEducation && D(2) >= 8) {
      let attended;
      [character, attended] = attendEducation(term, character)

      if (!attended) {
        let entered;
        [character, entered] = enterCareer(term, character)

        if (!entered) {
          character = draft(term, character)
        }
      }
    }
  }

  return character
}

export function hasSkill (skill, character) {
  return character.skills.some(([s]) => s === skill)
}

export function modify (num, char, character) {
  return {
    ...character,
    [char]: character[char] + num
  }
}
