import { D } from '~/lib/dice'
import { rand } from '~/lib/integer'

function fit (x, a, b) {
  return x >= a && x <= b
}

function oneOf (x, zs) {
  return zs.some(z => x === z)
}

export function fromUWP (uwp) {
  // St S A H P G L - T
  const planet = {
    atmosphere: parseInt(uwp[2], 16),
    government: parseInt(uwp[5], 16),
    hydrographics: parseInt(uwp[3], 16),
    lawLevel: parseInt(uwp[6], 16),
    population: parseInt(uwp[4], 16),
    size: parseInt(uwp[1], 16),
    starport: parseInt(uwp[0], 16),
    techLevel: parseInt(uwp[8], 16)
  }
  return { ...planet, tradeCodes: tradeCodes(planet) }
}

export function generateSurfaceDetails (planet, importance) {
  console.log(planet)

  /*
  const frozen =
    fit(planet.size, 2, 9) &&
    fit(planet.hydrographics, 1, 10) &&
    Math.random() < 0.2

  const tropic =
    fit(planet.size, 6, 9) &&
    fit(planet.atmosphere, 4, 9) &&
    fit(planet.hydrographics, 3, 7) &&
    Math.random() < 0.3
  */

  const worldResources = D(2)
  const offworldResources = rand(0, 3)
  const resources = worldResources + offworldResources
  const labor = planet.population - 1
  let infrastructure = 0
  if (fit(planet.population, 1, 3)) infrastructure = importance
  else if (fit(planet.population, 4, 6)) infrastructure = D() + importance
  else if (planet.population >= 7) infrastructure = D(2) + importance
  const efficiency = rand(-5, 5)
  const resourceUnits =
    (resources || 1) * (labor || 1) * (infrastructure || 1) * (efficiency || 1)
  console.log(`${resources} resources, ${resourceUnits} resource units`)

  const details = []
  details.push(`${worldResources} resource nodes`)
  details.push(`${D()} mountains`)
  details.push(`${planet.size} chasms`)
  details.push(`${planet.size} precipices`)
  // if (planet.barren) details.push(`

  return details
}

export function tradeCodes (planet) {
  const {
    atmosphere,
    government,
    hydrographics,
    lawLevel,
    population,
    size,
    techLevel
  } = planet

  const codes = []
  if (
    fit(atmosphere, 4, 9) &&
    fit(hydrographics, 4, 8) &&
    fit(population, 5, 7)
  ) {
    codes.push('Ag')
  }
  if (size === 0 && atmosphere === 0 && hydrographics === 0) {
    codes.push('As')
  }
  if (population === 0 && government === 0 && lawLevel === 0) {
    codes.push('Ba')
  }
  if (atmosphere >= 2 && hydrographics === 0) {
    codes.push('De')
  }
  if (atmosphere >= 10 && hydrographics >= 1) {
    codes.push('Fl')
  }
  if (
    fit(size, 6, 8) &&
    oneOf(atmosphere, [5, 6, 8]) &&
    fit(hydrographics, 5, 7)
  ) {
    codes.push('Ga')
  }
  if (population >= 9) {
    codes.push('Hi')
  }
  if (techLevel >= 12) {
    codes.push('Ht')
  }
  if (atmosphere < 2 && hydrographics >= 1) {
    codes.push('Ie')
  }
  if (oneOf(atmosphere, [0, 1, 2, 4, 7, 9]) && population >= 9) {
    codes.push('In')
  }
  if (population <= 3) {
    codes.push('Lo')
  }
  if (techLevel <= 5) {
    codes.push('Lt')
  }
  if (fit(atmosphere, 0, 3) && fit(hydrographics, 0, 3) && population <= 6) {
    codes.push('Na')
  }
  if (fit(population, 0, 6)) {
    codes.push('Ni')
  }
  if (fit(atmosphere, 2, 5) && fit(hydrographics, 0, 3)) {
    codes.push('Po')
  }
  if (
    oneOf(atmosphere, [6, 8]) &&
    fit(population, 6, 8) &&
    fit(government, 4, 9)
  ) {
    codes.push('Ri')
  }
  if (atmosphere === 0) {
    codes.push('Va')
  }
  if (hydrographics >= 10) {
    codes.push('Wa')
  }

  return codes
}
