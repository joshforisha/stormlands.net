# Skills

## Admin

This skill covers bureaucracies and administration of all sorts, including the navigation of bureaucratic obstacles or disasters. It also covers tracking inventories, ship manifests and other records.

*Avoiding Close Examination of Papers:* Average (8+) Admin check (1D x 10 seconds, EDU or SOC)

*Dealing with Police Harassment:* Difficult (10+) Admin check (1D x 10 minutes, EDU or SOC)"

## Advocate

Advocate gives a knowledge of common legal codes and practices, especially interstellar law. It also gives the Traveller experience in oratory, debate and public speaking, making it an excellent skill for lawyers and politicians.

*Arguing in Court:* Opposed Advocate check (1D days, EDU or SOC)

*Debating an Argument:* Opposed Advocate check (1D x 10 minutes, INT)

## Animals

This skill, rare on industrialized or technologically advanced worlds, is for the care of animals.

### Animals (Handling)

The Traveller knows how to handle an animal and ride those trained to bear a rider. Unusual animals raise the difficulty of the check.

*Riding a Horse into Battle:* Difficult (10+) Animals (handling) check (1D seconds, DEX). If successful, the Traveller can control the horse for a number of minutes equal to the Effect before needing to make another check.

### Animals (Training)

The Traveller knows how to tame and train animals.

*Taming a Strange Alien Creature:* Formidable (14+) Animals (training) check (1D days, INT)

### Animals (Veterinary)

The Traveller is trained in veterinary medicine and animal care.

*Applying Medical Care:* See the Medic skill, but use the Animals (veterinary) skill.

## Art

The Traveller is trained in a type of creative art.

### Art (Holography)

Recording and producing aesthetically pleasing and clear holographic images.

*Surreptitiously Switching on Your Recorder While in a Secret Meeting:* Formidable (14+) Art (holography) check (1D seconds, DEX).

### Art (Instrument)

Playing a particular musical instrument, such as flute, piano or organ.

*Playing a Concerto:* Difficult (10+) Art (instrument) check (1D x 10 minutes, EDU).

### Art (Performer)

The Traveller is a trained actor, dancer or singer at home on the stage, screen or holo.

*Performing a Play:* Average (8+) Art (performer) check (1D hours, EDU).

*Convincing a Person you are Actually Someone Else:* Art (performer) check (INT) opposed by Recon check (INT).

### Art (Visual Media)

Making artistic or abstract paintings or sculptures in a variety of media.

*Making a Statue of Someone:* Difficult (10+) Art (visual media) check (1D days, INT).

### Art (Write)

Composing inspiring or interesting pieces of text.

*Rousing the People of a Planet by Exposing Their Government's Corruption:* Difficult (10+) Art (write) check (1D hours, INT or EDU).

*Writing the New Edition of Traveller:* Formidable (14+) Art (write) check (1D months, INT).

## Astrogation

This skill is for plotting the courses of starships and calculating accurate jumps. See Spacecraft Operations chapter.

*Plotting Course to a Target World Using a Gas Giant for a Gravity Slingshot:* Difficult (10+) Astrogation check (1D x 10 minutes, EDU).

*Plotting a Standard Jump:* Easy (4+) Astrogation check (1D x 10 minutes, EDU), with DM- equal to the Jump distance.

## Athletics

The Traveller is a trained athlete and is physically fit. The Athletics skill effectively augments a Traveller's physical characteristics; whatever you can do with Strength alone you can also add your Athletics (strength) DM to, for example. Athletics is also the principal skill used in adverse gravitational environments, specifically Athletics (dexterity) in low or zero-G, and Athletics (strength) in high-G locations.

### Athletics (Dexterity)

Climbing, Juggling, Throwing. For alien races with wings, this also includes flying.

*Climbing:* Difficulty varies. Athletics (dexterity) check (1D x 10 seconds, DEX). So long as he succeeds, the Traveller's Effect is usually irrelevant unless he is trying to do something while climbing, in which case the climbing is part of a task chain or multiple action.

*Sprinting:* Average (8+) Athletics (dexterity) check (1D seconds, DEX). If the Traveller does nothing but sprint flat out he can cover 24 + Effect metres with every check. Avoiding obstacles while sprinting requires another Athletics (dexterity) check (Difficult, because he is performing a multiple action).

*High Jumping:* Average (8+) Athletics (dexterity) check (1D seconds, DEX). The Traveller jumps a number of metres straight up equal to the Effect halved.

*Long Jumping:* Average (8+) Athletics (dexterity) check (1D seconds, DEX). The Traveller jumps a number of metres forward equal to the Effect with a running start.

*Righting Yourself When Artificial Gravity Suddenly Falls on Board a Ship:* Average (8+) Athletics (dexterity) check (1D seconds, DEX).

### Athletics (Endurance)

Long-distance running, hiking

*Long-distance Running:* Average (8+) Athletics (endurance) check (1D x 10 minutes, END).

*Long-distance Swimming:* Average (8+) Athletics (endurance) check (1D x 10 minutes, END).

### Athletics (Strength)

Feats of strength, weight-lifting.

*Arm Wrestling:* Opposed Athletics (strength) check (1D minutes, STR).

*Feats of Strength:* Average (8+) Athletics (strength) check (1D x 10 seconds, STR).

*Performing a Complicated Task in a High Gravity Environment:* Difficult (10+) Athletics (strength) check (1D seconds, STR).

## Broker

The Broker skill allows a Traveller to negotiate trades and arrange fair deals. It is heavily used when trading.

*Negotiating a Deal:* Average (8+) Broker check (1D hours, INT).

*Finding a Buyer:* Average (8+) Broker check (1D hours, SOC).

## Carouse

Carousing is the art of socialising; having fun, but also ensuring other people have fun, and infectious good humour. It also covers social awareness and subterfuge in such situations.

*Drinking Someone Under the Table:* Opposed Carouse check (1D hours, END). Difficulty varies by liquor.

*Gathering Rumours at a Party:* Average (8+) Carouse check (1D hours, SOC).

## Deception

Deception allows a Traveller to lie fluently, disguise himself, perform sleight of hand, and fool onlookers. Most underhanded ways of cheating and lying fall under deception.

*Convincing a Guard to let you Past Without ID:* Very Difficult (12+) Deception check (1D minutes, INT). Alternatively, oppose with a Recon check.

*Palming a Credite Chit:* Average (8+) Deception check (1D seconds, DEX).

*Disguising Yourself as a Wealthy Noble to Fool a Client:* Difficult (10+) Deception check (1D x 10 minutes, INT or SOC). Alternatively, oppose with a Recon check.

## Diplomat

The Diplomat skill is for negotiating deals, establishing peaceful contact and smoothing over social faux pas. It includes how to behave in high society and proper ways to address nobles. It is a much more formal skill than Persuade.

*Greeting the Emperor Properly:* Difficult (10+) Diplomat check (1D minutes, SOC).

*Negotiating a Peace Treaty:* Average (8+) Diplomat check (1D days, EDU).

*Transmitting a Formal Surrender:* Average (8+) Diplomat check (1D x 10 seconds, INT).

## Drive

### Drive (Hovercraft)

### Drive (Mole)

### Drive (Track)

### Drive (Walker)

### Drive (Wheel)

## Electronics

### Electronics (Comms)

### Electronics (Computers)

Using and controlling computer systems, and similar electronics and electrics.

*Accessing Publicly Available Data:* Easy (4+) Electronics (computers) check (1D minutes, INT or EDU).

*Activating a Computer Program on a Ship's Computer:* Routine (6+) Electronics (computers) check (1D x 10 seconds, INT or EDU).

*Searching a Corporate Database for Evidence of Illegal Activity:* Difficult (10+) Electronics (computers) check (1D hours, INT).

*Hacking into a Secure Computer Network:* Formidable (14+) Electronics (computers) check (1D x 10 hours, INT). Hacking is aided by Intrusion programs and made more difficult by Security programs. The Effect determines the amount of data retrieved; failure means the targeted system may be able to trace the hacking attempt.

### Electronics (RemoteOps)

### Electronics (Sensors)

## Engineer

### Engineer (J-Drive)

### Engineer (LifeSupport)

### Engineer (M-Drive)

### Engineer (Power)

## Explosives

## Flyer

### Flyer (Airship)

### Flyer (Grav)

### Flyer (Ornithopter)

### Flyer (Rotor)

### Flyer (Wing)

## Gambler

## Gun Combat

### Gun Combat (Archaic)

### Gun Combat (Energy)

### Gun Combat (Slug)

## Gunner

### Gunner (Capital)

### Gunner (Ortillery)

### Gunner (Screen)

### Gunner (Turret)

## Heavy Weapons

### Heavy Weapons (Artillery)

### Heavy Weapons (Man Portable)

### Heavy Weapons (Vehicle)

## Investigate

## Jack-of-All-Trades

## Language

## Leadership

## Mechanic

## Medic

## Melee

### Melee (Blade)

### Melee (Bludgeon)

### Melee (Natural)

### Melee (Unarmed)

## Navigation

## Persuade

## Pilot

### Pilot (Capital Ships)

### Pilot (SmallCraft)

### Pilot (Spacecraft)

## Profession

### Profession (Belter)

### Profession (Biologicals)

### Profession (Civil Engineering)

### Profession (Construction)

### Profession (Hydroponics)

### Profession (Polymers)

## Recon

## Science

### Science (Archaeology)

### Science (Astronomy)

### Science (Biology)

### Science (Chemistry)

### Science (Cosmology)

### Science (Cybernetics)

### Science (Economics)

### Science (Genetics)

### Science (History)

### Science (Linguistics)

### Science (Philosophy)

### Science (Physics)

### Science (Planetology)

### Science (Psionicology)

### Science (Psychology)

### Science (Robotics)

### Science (Sophontology)

### Science (Xenology)

## Seafarer

### Seafarer (Ocean Ships)

### Seafarer (Sail)

### Seafarer (Submarine)

## Stealth

## Steward

## Streetwise

## Survival

## Tactics

### Tactics (Military)

### Tactics (Naval)

## Vacc Suit
