import * as React from 'react'
import * as Settlement from '~/data/settlement'
import Button from '~/view/Button'

export default function SettlementGenerator () {
  const [settlement, setSettlement] = React.useState(Settlement.generate())

  console.log(settlement)

  return (
    <>
      <header>
        <Button onClick={() => setSettlement(Settlement.generate())}>
          Generate
        </Button>
      </header>
    </>
  )
}
