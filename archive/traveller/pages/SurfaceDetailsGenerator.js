import * as React from 'react'
import Button from '~/view/Button'
import Input from '~/view/Input'
import { fromUWP, generateSurfaceDetails } from '~/data/planet'

export default function SurfaceDetailsGenerator () {
  const [uwp, setUWP] = React.useState('B883865-A')

  React.useEffect(() => {
    generate()
  }, [])

  function generate () {
    const planet = fromUWP(uwp)
    window.console.log(generateSurfaceDetails(planet))
  }

  return (
    <>
      <header>
        <Input
          onChange={e => setUWP(e.target.value)}
          placeholder='UWP (XXXXXXX-X)'
          value={uwp}
        />
        <Button onClick={generate}>Generate</Button>
      </header>
    </>
  )
}
