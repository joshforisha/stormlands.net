# Robot Construction

## 1. Design

Use [Build-A-Bot](https://stormlands.net/build-a-bot) to assemble the robot's design.

By default, a robot can hold (or if mobile, carry) up to 50% of its mass, but requires at least one Arm to lift or place objects.

## 2. Construction Task

Robot construction can only be attempted in an environment or with tools of at least TL8.

Consume the number of listed resources, and roll the build hours, based on the Complexity of the robot design. The construction work can be spread out over any length of time, but the build is not considered complete until the total hours have been worked.

Make a difficult (10+) *Science (robotics)* check (EDU) with the following DMs:

- TL8 environment: DM-2
- TL9 environment: DM-1
- TL11–12 environment: DM+1
- TL13–14 environment: DM+2
- TL15+ environment: DM+3

If the task check is passed, roll *Recover* using the *Effect* of the build check.
You gain back an amount of excess/leftover material, equal to the result.
The final mass of the robot is the noted total mass minus the leftover mass.

If the task check is failed, then the construction must be aborted.
Roll *Scrap* to retrieve material from the build. You can obtain any mounted items back from a scrapped build, but all other material is lost/ruined in the process.

## 3. Computer Internals

A processor kit is needed for the robot's central processing, and cannot be constructed from raw materials.

| TL | Processing |   Cost |
| --:| ---------- | ------:|
|  7 | Computer/0 |  Cr250 |
|  8 | Computer/1 |  Cr125 |
|  9 | Computer/1 |   Cr50 |
| 10 | Computer/2 |  Cr250 |
| 11 | Computer/2 |  Cr150 |
| 12 | Computer/3 |  Cr500 |
| 13 | Computer/4 |  Cr750 |
| 14 | Computer/5 | Cr2500 |
| 15 | Computer/6 | Cr6000 |

## 4. Programming

A completed robot's installed processor determines its programming potential.

## Operating System

To function as an autonomous unit, a robot needs one of three levels of Operating System *(OS)*. This is specialized software that must be at least partially custom-written for each model. It does not impact the potential software that can be installed/enabled on the robot, but is somewhat dependent on the installed hardware profile of the unit. In general, writing and installing the OS for a robot is relatively harder than writing generic software for computer systems elsewhere.

### Basic OS

The simplest form of OS for a robot passively runs a single active program, and lacks an analytical "mind" that would allow the unit to make complex decisions. Most models that run a Basic OS only have the bare minimum processing unit for the single software task they are assigned.

*Programming task:* Difficult (10+) Electronics (computers) check (1D hours, EDU).

### Intermediate OS

A robot with an Intermediate OS is typically useful for multi-tasking a relatively short list of installed programs. It is equipped with straight-forward logical analysis of its assigned tasks and situation, and can react and shift its own active programs accordingly. An onboard computer with a Processing score of at least 2 is required for this OS level, and performance of the robot's task management increases with higher processing power.

*Programming task:* Very Difficult (12+) Electronics (computers) check (1D &times; 4 hours, EDU).

### Advanced OS

A true artificially intelligent robot has an Advanced OS, fully capable of machine learning faculties and subroutines that enable it to interact with other intelligent life in acceptable ways. The exact behavior of the AI at this level is still dependent on what programs are installed in the model. An onboard computer with a Processing score of at least 5 is required.

*Programming task:* Formidable (14+) Electronics (computers) check (1D &times; 10 hours, EDU).

## 5. Software

Once an OS is successfully installed on a robot model, the *root user* (the entity that programmed the OS) is the sole authority over the internal systems of the machine. It can be assumed that all robots of modern usage have some sort of interface for making a computer connection. Over this interface, any number of software packages can be installed and activated, with the active list depending directly on the robot's OS level:

- A robot with a Basic OS can only have a single software active at once.
- A robot with an Intermediate OS can have a number of software active equal to their computer's Processing score at once.
- A robot with an Advanced OS can have *any* number of software active at once.

*Note that the normal Bandwidth limit for each software still applies.*

Even though any robot can have any software *installed* at one time, it takes a computer and a permitted user to interface with the model in order to alter the currently active software.
