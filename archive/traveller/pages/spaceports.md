# Spaceports

- **Class F**: This is assigned to a high-quality port capable of handling a large volume of traffic. Class F spaceports usually have facilities equivalent to a Class B starport or perhaps a high-end Class C. A few are large and capable as Class A ports, and may have additional facilities such as large shipyards or a naval base. Class F ports often serve large cities on a planet with a central starport, or important offworld installations, such as colonies and the like.
- **Class G**: A G-code is assigned to a basic or fairly poor spaceport, equivalent to the lower end of Class C or better than average Class D starports. A Class G port is likely to be found at a minor city or typical offworld asset such as a mining base on a gas giant moon.
- **Class H**: An H-code is typically assigned to a very basic or improvised facility equivalent to a low-end Class D port, or simply a landing area next to a small or temporary installation. A scientific outpost with a dozen personnel, which receives a supply ship once every three months, would probably have Class H port.
- **Class Y**: Code Y indicates no spaceport is present. The starport equivalent code, Class X, can also be used.

## Port Features

| 2D | Special Feature |
| -- | --------------- |
| 2 | The port is build on the ruins of an older structure, or may be all-but-ruined itself. |
| 3 | The port has a large "alien quarter" which may be inhabited by a single culture not normally seen in this region, or a mix of races. |
| 4 | The port lacks capability in an area that would normally be present. For example, a Class A port would normally be expected to have some starship-building capability but for some reason this port lacks it, either temporarily or permanently. |
| 5 | There is no dedicated downport as such. Instead, the orbital port serves a web of spaceports which may be in competition for business. |
| 6 | The port is a specialist type, with minimal facilities outside this area of expertise. |
| 7 | An installation is present. See Installations on page 129. |
| 8 | The port is an independently run freeport rather than being tied to the mainworld government |
| 9 | The port is directly run by a major shipping line or political entity, giving preferential treatment to favoured clients and making life unusually difficult for others. |
| 10 | The port is a major trade nexus. |
| 11 | The system’s main port is not at the mainworld, but instead orbits another body in the system. Sublight craft connect the two, but most visitors stop over at the starport without visiting the mainworld. |
| 12 | The port is of unusual construction. A downport might be a seaport/rail nexus; a highport may be built out of a couple of old starships welded together. |

## Port Events

| 2D | Special Feature |
| -- | --------------- |
| 2 | The port is under embargo or boycott by a major shipping corporation or other influential body. Personnel and vessels may be harassed but there will be no attempts at violence. |
| 3 | The port has been neglected for a long time or suffered recent serious damage. Most ships bypass it, which has greatly reduced the volume of traffic. Quantities of Common Goods available are down to 25% of normal, while Trade Goods originating at the mainworld are up 100%. DM+/-3 applies when determining buying prices – Trade Goods prices are worse for the Travellers, Common Goods are better. |
| 4 | The port is flooded with naval personnel, military troops, or law enforcement officers. They may be responding to a problem at the port or staging for some other trouble spot. In any case, security checks will be much tighter than usual. |
| 5 | The port is obviously run-down and short of investment. Many businesses have closed. Quantities of all goods are halved. |
| 6 | The port is experiencing a slowdown in traffic. The quantity of available Trade Goods is unaffected but Common Goods are halved. Prices for minor items in stores are 10% lower due to reduced demand. |
| 7 | Advantageous economic conditions (rumoured or real) have resulted in large numbers of entrepreneurs and small-ship operators converging on the port. Markets are unusually vibrant, with double the usual number of passengers, cargoes and freight available. Encounters with Rivals or old friends are likely. |
| 8 | The port is experiencing a surge in traffic. Prices in the stores are 20% worse than usual – higher when the Travellers buy and lower when they sell. DM-2 applies on the Modified Price table, reflecting the way greater volume of trade is forcing the price down. |
| 9 | The port is currently receiving a great deal of investment. Areas are being expanded and refurbished, and shipping traffic is up. Quantities of available goods are doubled. |
| 10 | A group of entrepreneurs have opened up a new market – or convinced investors that one exists. The amount of freight bound for one or more nearby worlds is tripled, and investors are desperate to get it there. They are willing to pay 2Dx5% above normal freighting rates. |
| 11 | Piracy, smuggling and conflict have increased in the region, pushing up the cost of repairs and starship components as well as refits and maintenance by 20-60%. (2D3x10%). |
| 12 | The port is undergoing significant expansion or reconstruction. This might be going well or slowly, depending on local conditions. In the meantime, sections are out of order, and workarounds may create opportunities to bypass normal channels. |

## Spaceport Law Level

- Host Law Level - 2D
  - Class A port: +7
  - Class B or F port: +5
  - Class C or G port: +3
  - Class D or H port: +1
  - Class E port: +0

## Law Enforcement

- Class A port: +6
- Class B or F port: +3
- Class C or G port: +0
- Class D or H port: -3
- Class E port: -6

| Result | Law Enforcement | Notes |
| ------ | --------------- | ----- |
| 0 or less | Lawless and violent | Essentially a violent anarchy, with gangs, private guards, or security selfishly protecting influential individuals’ property. |
| 1–2 | Lawless and harmonious | No formal law enforcement, but armed individuals cooperate against threats on a common-interest basis. |
| 3–4 | Minimal security force | A sheriff and deputies, armed with civilian firearms, possibly part-time. |
| 5–6 | Small security force | Small professional security force with civilian weapons. |
| 7–8 | Average security force | Adequate security force with paramilitary equipment including automatic weapons. |
| 9–10 | Well-equipped security force | Adequate security force with paramilitary equipment including support weapons. |
| 11–12 | Paramilitary security force | Large professional security force. Some personnel have access to heavy military weapons. |
| 13–14 | Military security force | Large professional security service organised along military lines, with heavy weapons. Possibly a mercenary formation. |
| 15 or more | Private army | Excessive military-style security service with armed vehicles and support platforms. |
