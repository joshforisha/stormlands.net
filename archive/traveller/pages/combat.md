# Combat

## General

- **Initiative**: DEX or INT check
- **Actions**: one Significant and one Minor OR three Minor
- **Melee Attack**: 2D + Melee (appropriate speciality) +STR or DEX DM
- **Ranged Attack**: 2D + Gun Combat (appropriate speciality) +DEX DM

## Common Modifiers

| Bonuses | | Penalties | |
| ------- | | --------- | |
| Aiming  | +1 per action spent Aiming | Fast Moving Target | -1 for every full 10 metres of target movement relative to the attacker |
| Laser Sight | +1 if Aiming | Long Range | -2 |
| Short Range | +1 | Extreme Range | -4 |
| — | — | Target in Cover | -2 |
| — | — | Prone Target | -1 |

## Reactions

- **Diving for Cover**: DM-2 attackers' attack rolls, within 1.5m, DM-1 if no cover, skip next action
- **Dodging**: Penalty of DEX DM or Athletics (dexterity) to attacker's attack roll, DM-1 on next action
- **Parrying**: Penalty of Melee skill DM to attacker's attack roll

## Damage
- Add attack Effect to weapon damage roll unless Destructive or smaller scale target (p74)
- Attack with effect of 6+ always does at least one point of damage
- Damage to END first, then STR or DEX; unconscious when 2 attributes are 0
- **In cover**: DM-2 against all ranged attacks
- **Destructive weapon**: Multiply damage by 10

## Cover Bonuses

| Cover | Bonus Armor |
| ----- | ----------- |
| Vegetation | +2 |
| Tree Trunk | +6 |
| Stone Wall | +8 |
| Civilian Vehicle | +10 |
| Armoured Vehicle | +15 |
| Fortifications | +20 |

 ## Grappling

- Opposed Melee (unarmed) roll; winner:
  - Force opponent prone on the ground
  - Disarm opponent; can take weapon if Effect if 6+
  - Throw opponent 1D metres, causing 1D damage (ends grapple)
  - Inflict damage equal to 2 + Effect of the Melee check
  - Inflict damage using a pistol or small blade-sized weapon
  - Escape and move away (as normal move; ends grapple)
  - Drag opponent up to 3 metres
  - Continue the grapple with no other effect
- Travellers grappling can only make opposed Melee (unarmed) checks

## Dual Weapons

Can attack with both in same round (cannot aim either); DM-2 on both attack rolls

## Combat Mishaps

Occurs on failure with double-1 result; roll 2D + weapon skill:

| 2D | Result |
| -- | ------ |
| 2 | Shoot or hit themself for normal weapon damage |
| 3–4 | The weapon is dropped somewhere inaccessible. It cannot be retrieved during this fight, and if the Travellers are forced to flee it will not be recoverable. |
| 5–6 | The weapon is dropped but in sight. It can be recovered in a future round using a significant action. |
| 7 | A minor weapon malfunction or loss of proper grip has occurred. Any attack made this round is wasted but the weapon can be brought back into action by making an Easy (6+) skill check next round. This takes significant action and, if failed, the attempt must be repeated every round. |
| 8–9 | A serious weapon malfunction has occurred, putting the weapon out of action until it can be fixed in a workshop. |
| 10–11 | The weapon is destroyed in an ammunition explosion or other serious incident that causes critical structural damage to it. |
| 12+ | The skill attempt fails but no Mishap has occurred beyond that. |

## Disabling Wounds

- Final attack caused less than 3 points of damage: DM+4
- Final attack caused 4–6 points of damage: DM+2
- Final attack caused more than 6 points of damage: DM-2

| 2D | Result |
| -- | ------ |
| Less than 2 | The Traveller is vaporised, shredded, spread all over the landscape or otherwise destroyed in graphic and gruesome fashion. |
| 2–3 | The Traveller is either killed outright in a dramatic fashion such as having their head blown off, or lingers just long enough to make a final speech to their friends/enemies/passersby. They cannot be saved in either case. |
| 4–5 | The Traveller will die unless given prompt medical assistance. If they survive, they will lose the limb that was injured, or an eye, or suffer some similar major permanent injury as determined by the referee. This can of course be repaired with cybernetics, but the process will be slow and expensive. |
| 6–7 | The Traveller will die unless given prompt medical assistance, and suffer permanent effects if they survive at all. Lose 1D from any one of STR, DEX, or END and D3 from the others. |
| 8–9 | The Traveller will survive if given even the most basic emergency assistance, and suffer no ill effects providing they receive proper medical treatment whilst recovering. If it is not, the Traveller will permanently lose D3 points from both STR and END. |
| 10–11 | The Traveller will survive despite their injuries, even without assistance, and will make a full recovery if medical attention is successfully provided. |
| 12+ | The Traveller is terribly hurt but will somehow cling to life and begin to recover even if medical attention is not provided. |

## General Hit Location

| 1D  | Location |
| --- | -------- |
| 1   | Head     |
| 2   | Arm      |
| 3–5 | Torso    |
| 6   | Leg      |

## Wound Effects

- A **Major Wound** is inflicted on damage between half and full END
  - *Heal:* Routine (6+) Medic check (D3 days recuperation)
- A **Severe Wound** is inflicted on damage more than END
  - *Heal:* Average (8+) Medic check (1D days recuperation, then D3 as Major)

| Location | Major Wound | Severe Wound |
| -------- | ----------- | ------------ |
| Head     | DM-1 on all checks | DM-2 on all checks |
| Arms     | DM-1 on all checks involving the arms | DM-2 on all checks involving the arms |
| Torso    | DM-1 on all physical checks; speed reduced by 1m | DM-2 on all physical checks; Traveller can only crawl unless supported |
| Legs     | Speed reduced by 2m; DM-1 on all movement-based checks | Leg disabled; Traveller can only hobble slowly with support; no movement-based activity is possible |

- A **Crippling** wound (2–3 times END): STR, END, or DEX (at random) reduced by D3
- A **Critical** wound (3–4 times END): STR, END, and DEX reduced by D3+1 *each*
- A **Mortal** wound (4–5 times END): removes body part
- A **Devastating** wound (over 5 times END): struck part completely destroyed
