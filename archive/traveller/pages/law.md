# Law

Travellers come under suspicion equal to or lower than world's Law Level:

| Situation | DM | Response |
| --------- | -- | -------- |
| First approach to a planet | +0 | Check |
| Offworlders wandering the streets of a city (daily) | +0 | Check |
| Offworlders acting suspiciously | -1 | Check |
| Bar fight | -1 | Apprehended |
| Shots fired | -2 | Apprehended |
| Breaking and entering | -2 | Investigate |
| Firefight involving armoured Travellers and military weapons | -4 | Apprehended |
| Murder and carnage | -4 | Investigate |

**Check**: This result means the Travellers’ travel documents and identities are checked, either by a police officer or guard, or by electronically by querying the Travellers’ comms. A successful Admin or Streetwise check can allay suspicion but if it is failed, the planetary authorities move on to Investigation.

**Investigate**: This results in a detective or bureaucrat probing deeper into the Travellers’ backgrounds. If the Travellers have a ship, it will be searched. They may be followed or have their communications tapped. They may also be questioned closely.

**Apprehended**: The police show up ready for a fight. Their response will generally be proportional to the threat posed by the Travellers; if the Travellers are just making trouble in a bar, then most police forces will just use batons, stunners, tranq gas and other non- lethal weapons. On the other hand, if the Travellers are in battle dress and firing PGMPs at the palace of the planetary duke, then the police will show up with the best weapons and armour available at the planet’s Tech Level (or even a few levels higher), possibly with the army not far behind them.

Travellers arrested for a crime will face punishment, determined by rolling 2D+DMs on the Sentencing table.

For crimes involving smuggling banned goods, the DM is equal to the difference between the planet’s Law Level and the banned goods in question (for example, laser weapons are banned at Law Level 2, so a Traveller found with a laser weapon on a Law Level 6 world would have DM+4 to his roll on the Sentencing table).

Other crimes have a set DM.

| Crime | DM |
| ----- | -- |
| Assault | Law Level -5 |
| Destruction of Property | Law Level -3 |
| False Identity | Law Level -2 |
| Manslaughter | Law Level -1 |
| Murder | Law Level +0 |

A Traveller with the Advocate skill may attempt to reduce the severity of sentencing by making a check. If successful, reduce the Sentencing DM by the Effect of the check.

| 2D+DM | Sentence |
| ----- | -------- |
| 0 or less | Dismissed or trivial |
| 1–2 | Fine of 1D x Cr1000 (per ton of cargo) |
| 3–4 | Fine of 2D x Cr5000 (per ton of cargo) |
| 5–6 | Exile or a fine of 2D x Cr10,000 (per ton of cargo) |
| 7–8 | Imprisonment for 1D months or exile or fine of 2D x Cr20,000 (per ton of cargo) |
| 9–10 | Imprisonment for 1D years or exile |
| 11–12 | Imprisonment for 2D years or exile |
| 13–14 | Life imprisonment |
| 15 or more | Death |
