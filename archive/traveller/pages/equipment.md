# Equipment

## Armour

| Armour Type | Protection | TL  | Rad | Mass | Cost | Required Skill |
| ----------- | ---------- | ---:| --- | ---- | ---- | -------------- |
| Jack | +1 | 1 | — | 1 kg | Cr50 | None |
| Mesh | +2 | 6 | — | 2 kg | Cr150 | None |
| Cloth | +5 | 7  | — | 10 kg | Cr250 | None |
|       | +8 | 10 | — |  5 kg | Cr500 | |
| Flak Jacket | +3 | 7 | — | 8 kg | Cr100 | None |
|             | +5 | 8 | — | 6 kg | Cr300 | |
| Reflec | +10 (vs. lasers only) | 10 | — | 1 kg | Cr1500 | None |
| Ablat | +1 (+6 vs. lasers) | 9 | — | 2 kg | Cr75 | None |
| Combat Armour | +13 | 10 |  85 | 20 kg |  Cr96,000 | Vacc Suit 1 |
|               | +17 | 12 | 145 | 16 kg |  Cr88,000 | Vacc Suit 0 |
|               | +19 | 14 | 180 | 12 kg | Cr160,000 | Vacc Suit 0 |
| Vacc Suit | +4  |  8 | 10 | 17 kg | Cr12,000 | Vacc Suit 1 |
|           | +8  | 10 | 60 | 10 kg | Cr11,000 | Vacc Suit 0 |
|           | +10 | 12 | 90 |  8 kg | Cr20,000 | Vacc Suit 0 |
| Hostile Environment Vacc Suit | +8  |  9 |  75 | 22 kg | Cr24,000 | Vacc Suit 1 |
|                               | +9  | 10 |  90 | 13 kg | Cr20,000 | Vacc Suit 1 |
|                               | +12 | 11 | 140 | 13 kg | Cr22,000 | Vacc Suit 0 |
|                               | +14 | 13 | 170 | 10 kg | Cr40,000 | Vacc Suit 0 |
|                               | +15 | 14 | 185 |  9 kg | Cr60,000 | Vacc Suit 0 |
| Battle Dress | +22 | 13 | 245 | 100* kg | Cr200,000 | Vacc Suit 2 |
|              | +25 | 14 | 290 | 100* kg | Cr220,000 | Vacc Suit 1 |

## Augments

| Augment | Improvements | TL | Cost |
| ------- | ------------ | --:| ---- |
| Cognitive Augmentation | INT +1 | 12 | Cr500,000 |
| Cognitive Augmentation | INT +2 | 14 | MCr1 |
| Cognitive Augmentation | INT +3 | 16 | MCr5 |
| Dexterity Augmentation | DEX +1 | 11 | Cr500,000 |
| Dexterity Augmentation | DEX +2 | 12 | MCr1 |
| Dexterity Augmentation | DEX +3 | 15 | MCr5 |
| Endurance Augmentation | END +1 | 11 | Cr500,000 |
| Endurance Augmentation | END +2 | 12 | MCr1 |
| Endurance Augmentation | END +3 | 15 | MCr5 |
| Enhanced Vision | Binoculars, IR/Light Intensification | 13 | Cr25,000 |
| Neural Comm | Audio only | 10 | Cr1000 |
| Neural Comm | Audio and visual, Computer/0 | 12 | Cr5000 |
| Neural Comm | Multiple forms of data, Computer/1 | 14 | Cr20,000 |
| Skill Augmentation | Skill DM+1 | 12 | Cr50,000 |
| Strength Augmentation | STR +1 | 11 | Cr500,000 |
| Strength Augmentation | STR +2 | 12 | MCr1 |
| Strength Augmentation | STR +3 | 15 | MCr5 |
| Subdermal Armour | Armour +1 | 10 | Cr50,000 |
| Subdermal Armour | Armour +3 | 11 | Cr100,000 |
| Wafer Jack | Total storage capacity of rating/4 | 12 | Cr10,000 |
| Wafer Jack | Total storage capacity of rating/8 | 13 | Cr15,000 |

## Communication

| Radio Transceivers | Mass | Range | Cost |
| ------------------ | ---- | ----- | ---- |
| TL5 | 20 kg | 5 km | Cr225 |
| TL5 | 70 kg | 50 km | Cr750 |
| TL5 | 150 kg | 500 km | Cr1500 |
| TL5 | 300 kg | 5,000 km | Cr15,000 |
| TL8 | — | 50 km | Cr75 |
| TL9 | — | 500 km | Cr500 |
| TL9 (Computer/0) | — | 2,500 km | Cr5000 |
| TL10 (Computer/0) | — | 500 km | Cr250 |
| TL12 (Computer/0) | 1 kg | 10,000 km | Cr1000 |
| TL13 (Computer/1) | — | 1000 km | Cr250 |
| TL14 (Computer/1) | — | 3000 km | Cr500 |

| Laser Transceivers | Mass | Range | Cost |
| ------------------ | ---- | ----- | ---- |
| TL9 (Computer/0) | 1.5 kg | 500 km | Cr2500 |
| TL11 (Computer/0) | 0.5 kg | 500 km | Cr1500 |
| TL13 (Computer/1) | — | 500 km | Cr500 |

### Bug

| TL | Features | Cost |
| -- | -------- | ---- |
| TL5 | Audio only | Cr50 |
| TL7 | Audio or Visual | Cr100 |
| TL9 | Audio or Visual or Data | Cr200 |
| TL11 | Audio/Visual/Data | Cr300 |
| TL13 | Audio/Visual/Data/Bioscan | Cr400 |
| TL15 | Audio/Visual/Data/Bioscan/(Computer/1) | Cr500 |

- **Audio**: The bug records anything it hears.
- **Visual**: The bug records anything it sees.
- **Data**: If attached to a computer system, the bug can search and copy data from the computer. The bug cannot breach computer security on its own, but if a user accesses the computer in the bug's presence, the bug can read his data.
- **Bioscan**: The bug has a basic biological scanner, allowing it to sample the area for DNA traces, chemical taint and so forth.
- **Computer/1**: The bug has an onboard computer system rated Computer/1.

A bug can be active or passive. An active bug transmits data (either constantly, or when triggered). Passive bugs just record until activated.

A **Commdot** *(TL10) Cr10* is a tiny microphone/speaker and transmitter capable of interfacing with another communications device.

### Mobile Comm

| TL | Features | Cost |
| -- | -------- | ---- |
| TL6 | Audio only | Cr50 |
| TL8 | Audio and visual, Computer/0 | Cr150 |
| TL10 | Multiple forms of data, Computer/1 | Cr500 |

## Sensors

| Sensor | TL | Weight | Cost | About |
| ------ | -- | ------ | ---- | ----- |
| Binoculars | 3 | 1 kg | Cr75 | |
| Binoculars | 8 | 1 kg | Cr750 | Captures images; light-intensifying |
| Binoculars | 12 | 1 kg | Cr3500 | PRIS (Portable Radiation Imaging System) |
| Bioscanner | 15 | 3.5 kg | Cr350,000 | Chemical analysis |
| Densitometer | 14 | 5 kg | Cr20,000 | Measures density and creates thorough 3D image |
| EM Probe | 10 | 1 kg | Cr10,000 | Electronic diagnostic tool |
| Geiger Counter | 5 | 2 kg | Cr250 | Detects radiation |
| IR Goggles | 6 | — | Cr500 | Permits the user to see exothermic sources |
| Light Intensifier Goggles | 7 | 1 kg | Cr500 | Permits sight in less than total darkness |
| Light Intensifier Goggles | 9 | — | Cr1250 | Combined with IR goggles |
| NAS | 15 | 10 kg | Cr35,000 | Detects neural activity up to 500m away |

## Survival Gear and Supplies

| Survival Gear | TL | Weight | Cost | About |
| ------------- | --:| ------ | ---- | ----- |
| Artificial Gill | 8 | 4 kg | Cr4000 | Extracts oxygen from water allowing the wearer to breathe underwater. Only works on worlds with breathable atmospheres. |
| Breather Mask | 8 | — | Cr150 | Combines the filter and respirator into a single package. |
| Breather Mask | 10 | — | cr2000 | The more advanced filter is small enough to fit into the nose, or can even be a lung implant. |
| Environment Suit | 8 | 1 kg | Cr500 | Designed to protect the wearer from extreme cold or heat, the environment suit has a hood, gloves and boots, but leaves the face exposed in normal operations. A mask or rebreather can be attached, but truly hostile situations call for the heavy-duty hostile environment vacc suit. |
| Filter Mask | 7 | — | Cr100 | Filters are breathing masks that strip out harmful elements from the air inhaled by the character, such as dangerous gases or dust particles. |
| Grav Belt | 12 | 6 kg | Cr100,000 | A harness worn across the body, the grav belt is equipped with artificial gravity modules, allowing the Traveller to fly. The internal battery can be operated for a maximum of four hours before recharging. At TL15, the battery can operate for 12 hours before recharging. The grav belt allows the wearer to fly at Medium speed. |
| Habitat Module | 8 | 1000 kg | Cr10,000 | A modular unpressurised quarters for six people, capable of withstanding anything less than hurricane- force winds. Includes survival rations & battery power. Requires 12 man-hours to assemble, and can be attached to other modules to form a base. |
| Habitat Module | 10 | 500 kg | Cr20,000 | The TL10 module is pressurised, and includes life-support for six occupants for one week (1000 person/hours). |
| Portable Fusion Generator | 10 | 20 kg | Cr500,000 | This is a light-duty portable fusion generator, capable of recharging weapons and other equipment for up to one month of use. |
| Rescue Bubble | 9 | 2 kg | Cr600 | A large (2 metre diameter) plastic bubble. Piezoelectric layers in the bubble wall translate the user’s movements into electricity to recharge the bubble’s batteries and power its distress beacon, and a small oxygen tank both inflates the bubble and provides two person/hours of life support. A self-repairing plastic seal serves as an emergency airlock. Rescue bubbles are found on both space and sea vessels as emergency lifeboats. |
| Respirator | 6 | — | Cr100 | This rebreather concentrates inhaled oxygen, allowing a Traveller to breathe on worlds with a thin atmosphere. Respirators take the form of a face mask or mouthpiece initially. |
| Respirator | 10 | — | Cr2000 | The more advanced respirator is small enough to fit into the nose, or can even be a lung implant. |
| Tent | 3 | 6 kg | Cr200 | A basic tent provides shelter for two people against the weather. |
| Tent | 7 | 5 kg | Cr2000 | The TL7 version can be pressurised. There is no airlock – the tent is depressurised when opened. |

### Tent Options

- **Climate Controlled (TL10)**: Structures can be given the climate-control option, allowing their internal temperature and other conditions to be controlled for comfort. Costs Cr500.
- **Self-Assembling (TL11)**: The self-assembling upgrade can be given to tents, habitat modules and other basic structures. The structure is capable of expanding and assembling itself with only minimal aid, reducing the time needed to set up the shelter to a single man-hour. Costs Cr5000.
- **Self-Sealing (TL13)**: Structures can be made self- repairing and self-sealing for Cr2000. Small breaches and rips are automatically fixed in seconds.

| Toolkits | TL | Weight | Cost | About |
| -------- | --:| ------ | ---- | ----- |
| Electronics (specific skill) | 7 | 2 kg | Cr2000 | Required for performing repairs and installing new equipment. |
| Engineering (specific skill) | 12 | 12 kg | Cr4000 | Required for performing repairs and installing new equipment. |
| Forensics | 8 | 12 kg | Cr2000 | Required for investigating crime scenes and testing samples. |
| Mechanical | 5 | 12 kg | Cr1000 | Required for repairs and construction. |
| Scientific | 5 | 8 kg | Cr2000 | Required for scientific testing and analysis. |
| Surveying | 6 | 12 kg | Cr1000 | Required for planetary surveys or mapping. |
