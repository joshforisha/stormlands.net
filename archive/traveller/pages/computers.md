# Computers & Software

## Portable Computers

| TL | Processing | Mass | Cost |
| -- | ---------- | ---- | ---- |
| TL7 | Computer/0 | 5 kg | Cr500 |
| TL8 | Computer/1 | 2 kg | Cr250 |
| TL9 | Computer/1 | 1 kg | Cr100 |
| TL10 | Computer/2 | 0.5 kg | Cr500 |
| TL11 | Computer/2 | 0.5 kg | Cr300 |
| TL12 | Computer/3 | 0.5 kg | Cr1000 |
| TL13 | Computer/4 | 0.5 kg | Cr1500 |
| TL14 | Computer/5 | 0.5 kg | Cr5000 |

### Options:

- **Comms (TL8+)**: Any computer of TL8 or more can act as a comm unit or transceiver if this is specified at purchase. This does not increase the cost of the computer.
- **Data Display/Recorder (TL13)**: This headpiece provides a continuous heads-up display for the user, allowing him to view computer data from any linked system. DD/R headsets are commonly used by starship crews, to access information without changing their primary console displays. DD/Rs can display data from any system, not just computers—they can display vacc suit oxygen reserves, grav belt status, neural activity scanner results and so forth. Costs Cr5000.
- **Data Wafer (TL10)**: The standard medium of information storage in many universes is the data wafer, a rectangle of hardened plastic about the size of a credit card.  The interface for a data wafer is standardised, but the internal workings vary. A TL10 data wafer is memory diamond, with information encoded in structures of carbon atoms; more advanced wafers use more exotic means of data storage. Costs Cr5.
- **Specialised Computer**: A computer can be designed for a specific purpose, which gives it a Processing Score of +1 or +2 higher for that software only. The navigation hand computer used by an explorer, for example, might be only a Computer/1, but could run the Navigation/3 software because it is specially designed for that task. A specialised computer costs 25% more per added rating – so, a Computer/1, Navigation/3 portable computer costs 150% of the cost of a basic Computer/1. At TL9 this would be Cr150.

## Software

| Software | Bandwidth | TL | Cost | Effect |
| -------- | --------- | -- | ---- | ------ |
| Interface | 0 | TL7 | Included | Displays data. |
| Intelligent Interface | 1 | TL11 | Cr100 | Artificial intelligence allows voice control and displays data intelligently. Required for using Expert software. |
| Security | 0 | TL8 | Included | Security software packages defend against intrusion. Security/0 requires an Average (8+) Electronics (computers) check to successfully bypass. |
|          | 1 | TL10 | Cr200 | Difficult (10+) difficulty |
|          | 2 | TL11 | Cr1000 | Hard (12+) difficulty |
|          | 3 | TL12 | Cr20,000 | Formidable (14+) difficulty |
| Intrusion | 1 | TL10 | Cr1000 | Intrusion software packages aid hacking attempts, giving a bonus equal to their Bandwidth. Intrusion software is often illegal. |
|           | 2 | TL11 | Cr10,000 | |
|           | 3 | TL13 | Cr100,000 | |
|           | 4 | TL15 | MCr1 | |
| Expert | 1 | TL11 | Cr1000 | Expert software packages mimic skills. A Traveller using Expert software may make a skill check as if he had the skill at the software's Bandwidth -1. Only INT and EDU-based checks can be attempted. If a Traveller already has the skill, then Expert grants DM+1 to his check. |
|        | 2 | TL12 | Cr10,000 | |
|        | 3 | TL13 | Cr100,000 | |
| Translator | 0 | TL9 | Cr50 | Translators are specialised Expert packages that only have Language skills. The TL9 version provides a near-real-time translation. The TL10 works in real-time and has a much better understanding of the nuances of language. |
|            | 1 | TL10 | Cr500 | |
| Database | — | TL7 | Cr10–10,000 | A database is a large store of information on a topic that can be searched with an Electronics (computers) check or using an Agent. |
| Agent | 0 | TL11 | Cr500 | Agen packages have an Electronics (computers) skill equal to their Bandwidth, and can carry out tasks assigned to them with a modicum of intelligence. For example, an Agent package might be commanded to hack into an enemy computer system and steal a particular data file. They are effectively specialised combinations of Expert Electronics (computers) anc less capable Intellect software packages. |
|       | 1 | TL12 | Cr2000 | |
|       | 2 | TL13 | Cr100,000 | |
|       | 3 | TL14 | Cr250,000 | |
| Intellect | 1 | TL12 | Cr20,000 | Intellect is an improved Agent, which can use Expert systems. For example, a robot doctor might be running Intellect/1 and Expert Medic/3, giving it a Medic skill of 2. An Intellect program can simultaneously use a number of skills equal to its Bandwidth. |
|           | 2 | TL13 | Cr50,000 | |
|           | 3 | TL14 | — | |

## Computer Variants

A computer’s optimum Technology Level is the level at which it reaches standard production and is no longer considered a prototype or experiment. Most computing systems will be built at the optimal level or higher, to allow utilisation of higher TL programs. Computers may be built at lower than optimum Tech Levels (Prototech) or reduced Tech Levels (Retrotech).

Prototech: A computer type may be produced at up to 2 levels below its optimum TL as a prototype (or reverse engineered experiment), or as a substandard but cheaper model. The system has its Processing and TL at normal values, but cost and mass are multiplied by 10 for construction at one TL lower, and 100 for construction at two TL lower

For example, a TL10 Computer/2 salvaged from a mysterious alien wreck could be used to reverse engineer a Computer/2 by a TL8 society. It would run as a TL10 Computer/2, but would cost Cr50000 and mass 50 kg.

RetroTech: While computers may be built at, and rated as any TL above the optimum TL, a higher TL society may produce a lower rated TL system for reduced cost and weight. Any system may be built at any TL below the society’s current level, but not below the optimum TL for the model. Each reduced level halves the cost and weight of the basic model.

## Expert Skills

Having a tool or weapon with the appropriate Skill Expert program and an Intelligent Interface can grant a Traveller DM+1 to his checks. However, the program can only help if the difficulty of the task is less than a certain value.

| Software Package | Maximum Difficulty |
| ---------------- | ------------------ |
| Expert/1 | Difficult (10+) |
| Expert/2 | Very Difficult (12+) |
| Expert/3 | Formidable (14+) |
