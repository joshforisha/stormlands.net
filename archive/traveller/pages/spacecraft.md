# Spacecraft

## Landing

Requires a Routine (6+) Pilot check (1D x 10 seconds), but most pilots will take 1D minutes to perform a landing, and gain DM+2 on the task.

Landing in the wild (with landing gear) requires an Average (8+), Difficult (10+), or even Very Difficult (12+) Pilot check, depending on local terrain.

## Docking

Requires a Routine (6+) Pilot check (1D minutes).

## Power

- **Basic Ship Systems** require 20% of the total tonnage of the hull
- **Manoeuvre Drive** requires 10% of the total tonnage of the hull, multiplied by the maximum Thrust the drive is capable of
- **Jump Drive** requires 10% of the hull's total tonnage multiplied by the maximum Jump number

### Weapons and Systems

| System        | Power Required |
| ------------- | -------------- |
| Beam Laser    | 4              |
| Missile Rack  | 0              |
| Particle Beam | 8              |
| Pulse Laser   | 4              |
| Sandcaster    | 0              |
| Turret        | 1              |

## Fuel

- Refined fuel costs Cr500/ton
- Unrefined fuel costs Cr100/ton
- 1D hours to refuel
- Fuel scooping requires a Difficult (10+) Pilot check (1D hours)

## Jump Travel

(Core p148)
