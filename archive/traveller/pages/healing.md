# Healing

- **First Aid**: (Within one minute) restores Medic Effect points
- **Surgery**:
  - Required with three damaged characteristics
  - Requires hospital of sickbay
  - Failed check: patient takes 3+ Effect damage
- **Medical Care**:
  - Requires hospital or sickbay, and the Traveller on bed rest
  - Restores 3+ END DM + the doctor's Medic skill per day, divided evenly among all damaged characteristics
- **Augmentations**: Medic checks suffer a negative DM equal to the TL difference between the facility and the highest relevant implant
- **Mental**: Each mental characteristic heals one point per day
- **Natural Healing**:
  - An injured Traveller regains characteristic points equal to 1D + END DM per day of full rest
  - A Traveller who requires surgery only regains END DM per day
- **Unconsciousness**:
 - May make an END check after every minute
 - Each fail adds a cumulative DM+1
