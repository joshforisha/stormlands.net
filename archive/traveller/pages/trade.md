# Trade

## Passage and Freight Costs

| Parsecs | High Passage | Middle Passage | Basic Passage | Low Passage | Freight  |
| ------- | ------------:| --------------:| -------------:| -----------:| --------:|
| 1       |      Cr8,500 |        Cr6,200 |       Cr2,200 |       Cr700 |  Cr1,000 |
| 2       |     Cr12,000 |        Cr9,000 |       Cr2,900 |     Cr1,300 |  Cr1,600 |
| 3       |     Cr20,000 |       Cr15,000 |       Cr4,400 |     Cr2,200 |  Cr3,000 |
| 4       |     Cr41,000 |       Cr31,000 |       Cr8,600 |     Cr4,300 |  Cr7,000 |
| 5       |     Cr45,000 |       Cr34,000 |       Cr9,400 |    Cr13,000 |  Cr7,700 |
| 6       |    Cr470,000 |      Cr350,000 |      Cr93,000 |    Cr96,000 | Cr86,000 |

## Seeking Passengers

Roll 2D on *Passenger Traffic* four times, once each for Low, Basic, Middle, and High passengers, with the following modifiers.

- Effect of a Brokers, Carouse, or Streetwise check
- Chief Steward: DM+ highest Steward skill on ship
- Rolling for High Passengers: DM-4
- Rolling for Low Passengers: DM+1
- World Population 1 or less: DM-4
- World Population 6–7: DM+1
- World Population 8 or more: DM+3
- Starport A: DM+2
- Starport B: DM+1
- Starport E: DM-1
- Starport X: DM-3
- Amber Zone: DM+1
- Red Zone: DM-4

## Passenger Traffic

| 2D | Passengers |
| -- | ---------- |
| 1- | 0 |
| 2–3 | 1D |
| 4–6 | 2D |
| 7–10 | 3D |
| 11–13 | 4D |
| 14–15 | 5D |
| 16 | 6D |
| 17 | 7D |
| 18 | 8D |
| 19 | 9D |
| 20+ | 10D |

## Freight

- Major Cargo: 1Dx10 tons
- Minor Cargo: 1Dx5 tons
- Incidental Cargo: 1D tons
- Cargo is paid for *on delivery*
- Not on time: reduces pay by 1D+4 x 10%
- Roll 2D on *Freight Traffic* three times (Incidental, Minor, Major):
  - The Effect of a Broker or Streetwise check
  - Rolling for Major Cargo: DM-4
  - Rolling for Incidental Cargo: DM+2
  - World Population 1 or less: DM-4
  - World Population 6–7: DM+2
  - World Population 8 or more: DM+4
  - Starport A: DM+2
  - Starport B: DM+1
  - Starport E: DM-1
  - Starport X: DM-3
  - Tech Level 6 or less: DM-1
  - Tech Level 9 or more: DM+2
  - Amber Zone: DM-2
  - Red Zone: DM-6

### Freight Traffic

| 2D | Lots |
| -- | ---- |
| 1 or less | 0
| 2–3 | 1D |
| 4–5 | 2D |
| 6–8 | 3D |
| 9–11 | 4D |
| 12–14 | 5D |
| 15–16 | 6D |
| 17 | 7D |
| 18 | 8D |
| 19 | 9D |
| 20 or more | 10D |

## Mail

- 5 tons of space per container
- Cr25,000 per container
- 1D containers available; *take all or none*
- 12+ (Very Difficult) task
  - Freight Traffic DM-10 or less: DM-2
  - Freight Traffic DM-9 to -5: DM-1
  - Freight Traffic DM-4 to +4: DM+0
  - Freight Traffic DM 5 to 9: DM+1
  - Freight Traffic DM 10 or more: DM+2
  - Travellers' ship is armed: DM+2
  - + Travellers' highest Naval or Scout rank
  - + Travellers' highest Social Standing DM
  - World is TL of 5 or less: DM-4

## Modified Price

| Result     | Purchase Price | Sale Price |
| ---------- | -------------- | ---------- |
| -1 or less |           200% |        30% |
| 0          |           175% |        40% |
| 1          |           150% |        45% |
| 2          |           135% |        50% |
| 3          |           125% |        55% |
| 4          |           120% |        60% |
| 5          |           115% |        65% |
| 6          |           110% |        70% |
| 7          |           105% |        75% |
| 8          |           100% |        80% |
| 9          |            95% |        85% |
| 10         |            90% |        90% |
| 11         |            85% |       100% |
| 12         |            80% |       105% |
| 13         |            75% |       110% |
| 14         |            70% |       115% |
| 15         |            65% |       120% |
| 16         |            60% |       125% |
| 17         |            55% |       130% |
| 18         |            50% |       135% |
| 19         |            45% |       140% |
| 20         |            40% |       145% |
| 21         |            35% |       150% |
| 22         |            30% |       155% |
| 23+        |            25% |       160% |

## Speculative Trade & Smuggling

### 1. Find a supplier

* DM-1 per previous attempt in the same month
* Local guide: Broker @ 1D-2
  * DM+1 per 5% (10% black market) cut, max DM+4
* Class A Starport: DM+6
* Class B Starport: DM+4
* Class C Starport: DM+2
