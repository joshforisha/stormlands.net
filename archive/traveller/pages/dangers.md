# Dangers

## Diseases

| Disease | END check Difficulty | Damage | Interval |
| ------- | -------------------- | ------ | -------- |
| Anthrax | Very Difficult (12+) | 2D | 1D days |
| Biological Weapon | Formidable (14+) | 3D | 1D hours |
| Pnemonia | Average (8+) | 1D | 1D weeks |
| Regina Flu | Routine (6+) | 1D-2 | 1D days |

## Falling

- On 1g world, 1D damage per 2 metres
- High- or low-gravity modify by 1D for every 4 metres
- Can reduce calculated distance by Effect metres from Athletics check

## Fatigue

- Staying awake for hours greater than END + 18
- Performing heavy labour for hours greater than END
- Making a number of consecutive melee attacks greater than END (single combat)

A fatigued Traveller suffers a Bane to all checks until they rest.

## Poisons

| Poison | END check difficulty | Damage | Interval |
| ------ | -------------------- | ------ | -------- |
| Arsenic | Difficult (10+) | 2D | 1D minutes |
| Tranq Gas | Difficult (10+) | Unconscious | 1D seconds |
| Neurotoxin | Very Difficult (12+) | 1D INT | 1D seconds |

## Gravity

- **High Gravity**
  - DM-1 on all skill checks, until 1D weeks acclimation
  - Travellers with Athletics (dexterity) acclimatise automatically
- **Low Gravity**:
  - DM-1 on physical skill checks, until 1D weeks acclimation
  - Travellers with Athletics (dexterity) acclimatise automatically
- **Zero Gravity**:
  - On weapon usage, Average (8+) Athletics (dexterity) check; failure is automatic miss and spin out of control
  - Spinning Traveller can attempt Average (8+) Athletics (dexterity) check to regain control

## Radiation Effects

| Exposure | Immediate Effects | Cumulative Effects |
| -------- | ----------------- | ------------------ |
| 50 rads or less | None | None |
| 51–150 | 1D damage, Nausea (-1 to all checks until medical treatment received) | None |
| 151–300 rads | 2D damage | -1 END permanently |
| 301–500 rads | 4D damage, hair loss | -2 END permanently |
| 501–800 rads | 6D damage, sterile | -3 END permanently |
| 801 rads or more | 8D damage, internal bleeding | -4 END permanently |

## Radiation Exposure

| Radiation Source | Rads Received |
| ---------------- | ------------- |
| Minor reactor leak | 2D/hour |
| Serious reactor lead | 2D/20 minutes |
| Minor solar flare | 1D x 100/hour |
| Major solar flare | 3D x 100/hour |
| Radiation weapon | 2D x 20 |

## Temperature Table

| Heat | Effect | Cold | Effect |
| ---- | ------ | ---- | ------ |
| 50° (very hot desert) | 1D/hour | -25° (Arctic) | 1D/hour |
| 200° (~Mercury) | 1D/round | -50° (~Mars) | 2D/hour |
| 500° (~Venus) | 2D/round | -200° (~Pluto) | 1D/round |
| Burning Torch | 1D/round | Freezer Berth | 1D/round |
| Welding Torch | 2D/round | Liquid Nitrogen | 2D/round |
| Inferno | 3D/round | — | — |

## Miscellaneous

- **Suffocation**: Traveller suffers 1D damage per minute
- **Vacuum**:
  - Traveller suffers cumulative 1D damage per round
  - Absorbs 2D x 10 rads each round when in space itself
- **Weather**: High winds and torrential rain inflict DM-1 to all skill checks
