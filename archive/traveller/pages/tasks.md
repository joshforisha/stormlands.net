# Tasks

## Task Difficulty

| Difficulty | Target Number |
| ---------- | ------------- |
| Simple | 2+ |
| Easy | 4+ |
| Routine | 6+ |
| Average | 8+ |
| Difficult | 10+ |
| Very Difficult | 12+ |
| Formidable | 14+ |

## Effect Results

| Effects | Result |
| ------- | ------ |
| -6 or less | Exceptional Failure |
| -2 to -5 | Average Failure |
| -1 | Marginal Failure |
| 0 | Marginal Success |
| 1 to 5 | Average Success |
| 6 or more | Exceptional Success |

## Timeframes

| Timeframe | Increment | Example Action |
| --------- | --------- | -------------- |
| 1D Seconds | One second | Shooting, punching, jumping |
| 1D Combat Rounds | Combat round (six seconds) | Hurrying jump calculations |
| 1D &times; 10 Seconds | Ten seconds | Rerouting power, opening a comms channel |
| 1D Minutes | One minute | Applying first aid, basic technical tasks |
| 1D &times; 10 Minutes | Ten minutes | More complex technical tasks, searching an area thoroughly |
| 1D Hours | One hour | Building a shelter, moving through the wilderness |
| 1D &times; 4 Hours | Four hours | Researching a problem |
| 1D &times; 10 Hours | Ten hours | Repairing a damaged ship |
| 1D Days | One day | Combing a city for a missing person |

*Faster DM-2, Slower DM+2*

## Task Chain

| Previous Check | DM | Previous Check | DM |
| -------------- | -- | -------------- | -- |
| Failed With Effect -6 or less | -3 | Succeeded With Effect 0 | +0 |
| Failed With Effect -2 to -5 | -2 | Succeeded With Effect 1 to 5 | +1 |
| Failed With Effect -1 | -1 | Succeeded With Effect 6 or more | +2 |
