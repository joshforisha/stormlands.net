# Vehicles

## Speed Bands

| Speed Band | Number | Speed          |
| ---------- | ------:| --------------:|
| Stopped    |      0 |         0 km/h |
| Idle       |      1 |      1–20 km/h |
| Very Slow  |      2 |     20–50 km/h |
| Slow       |      3 |    50–100 km/h |
| Medium     |      4 |   100–200 km/h |
| High       |      5 |   200–300 km/h |
| Fast       |      6 |   300–500 km/h |
| Very Fast  |      7 |   500–800 km/h |
| Subsonic   |      8 |  800–1200 km/h |
| Supersonic |      9 | 1200–6000 km/h |
| Hypersonic |     10 |     6000+ km/h |

## Rough Terrain

Off-road ground vehicles suffer DM-2 to control and have their maximum speed reduced by two Speed Bands. Rough ground cannot be traversed in a ground vehicle.

Off-road capable and Drive (track) vehicles suffer no penalty off-road, and can traverse rough terrain with DM-2 to control and a reduced maximum speed of two Speed Bands.

## Airborne Movement

Aircraft can only fly on worlds with Size and Atmosphere types within 2 of their world of creation. They suffer DM-1 to control when not in the matching Atmosphere and/or Size. Aircraft require at least Atmosphere 1.

## Dogfight

- A vehicle within 1 km and within one Speed Band of another may initiate
- Both drivers make opposed skill checks using the vehicle skill (with Agility as normal)
  - Each additional enemy DM-1
- *Draw*: only turrets may attack
- *Winner* may choose:
  - to place opponent's vehicle within their fire arc
  - which of the opposing vehicle's firing arcs his own vehicle lies in
- *Winner* also gains DM+2 to attack rolls this round
- *Loser* suffers DM-2 to attack rolls
- Winner of previous round dogfight applies difference of previous check as DM to new check

## Evasive Action

- Driver makes vehicle skill check (with Agility)
- Result's Effect acts as DM- to all attacks against the vehicle or passengers
- Effect also acts as DM- to attacks *from* vehicle too
- Lasts until driver's next turn

## Ram

- Requires a significant action and vehicle skill check

## Weave

- Driver chooses a weaving score, between 1 and current Speed Band Number
- Vehicle skill check with DM-weaving
- **Fail**: crashes into obstacle
- **Succeed**: pursuers make weaving check with same difficulty

## Critical Hits Location

Damaging attack with Effect of 6+ results in a critical hit.

| 2D    | Location     |
| -----:| ------------ |
|   2–3 | Fuel         |
|     4 | Power Plant  |
|     5 | Weapon       |
|     6 | Armour       |
|     7 | Hull         |
|     8 | Cargo        |
|     9 | Occupants    |
|    10 | Drive System |
| 11–12 | Systems      |

*Severity* is equal to the damage the vehicle has taken by the attack, divided by ten (rounding up).

## Collisions

- Roll 1D for every Speed Band Number the vehicle was travelling (round up)
- Applied as damage to anything hit and, if solid enough, also to the vehicle
- Unsecured passengers take equal damage and are thrown 10m for every Speed Band Number

## Repairing Vehicles (p 136)

- Average (+8) Mechanic check (1D hours +1 dmg, INT or EDU) Cr500 dmg
- Critical hit: Average (8+) Mechanic check (1D hours, INT or EDU) with Severity as DM
- Weapons and equipment must be replaced

## Vehicle Weapons

| Weapon | TL | Range | Damage | Tons | Cost | Mag | Mag Cost | Traits |
| ------ | --:| ----- | ------ | ---- | ---- | --- | -------- | ------ |
| Cannon | 8 | 2 | 1DD | 2.5 | Cr400,000 | 30 | Cr5000 | Blast 10 |
| Fusion Gun | 14 | 5 | 3DD | 4 | MCr3 | — | — | AP 20, Blast 2-, Radiation |
| Heavy Machinegun | 6 | 1 | 4D | 0.1 | Cr4500 | 100 | Cr400 | Auto 3 |
| Laser Cannon | 9 | 2.5 | 1DD | 6 | Cr100,000 | — | — | AP 10 |
| Light Autocannon | 6 | 1 | 6D | 0.25 | Cr10,000 | 500 | Cr1000 | Auto 3 |

## Vehicle Models

### Air/Raft

An open-topped vehicle supported by anti-gravity technology. Air/Rafts are capable of reaching orbit, but passengers must wear vacc suits. They are ubiquitous, remarkably reliable and flexible vehicles.

- **TL**: 8
- **Skill**: Flyer (grav)
- **Agility**: +1
- **Speed (Cruise)**: High (Medium)
- **Range (Cruise)**: 1000 km (1500 km)
- **Crew**: 1
- **Passengers**: 5
- **Cargo**: 0.25 tons
- **Hull**: 16
- **Shipping**: 4 tons
- **Cost**: Cr250,000
- **Armour**:
  - **Front**: 2
  - **Sides**: 2
  - **Rear**: 2
- **Traits**:
  - Autopilot (Flyer 1)
  - Communications (TL8)
  - Computer/1 (Database)
  - Entertainment System
  - Navigation (basic)
  - Sensors (basic)

### Grav Car

- **TL**: 9
- **Skill**: Flyer (grav)
- **Agility**: +1
- **Speed (Cruise)**: Fast (High)
- **Range (Cruise)**: 2000 km (3000 km)
- **Crew**: 1
- **Passengers**: 3
- **Spaces**: 5
- **Hull**: 10
- **Shipping**: 2.5 tons
- **Cost**: Cr185,000
- **Armour**: 10/6/2
- **Traits**:
  - Ejection Seat

### Armored Motorbike

- **TL**: 9
- **Skill**: Drive (wheel)
- **Agility**: +1
- **Speed (Cruise)**: High (Medium)
- **Range (Cruise)**: 500 km (750 km)
- **Crew**: 1
- **Passengers**: 1
- **Cargo**: 0
- **Hull**: 6
- **Shipping**: 1.5 tons
- **Cost**: Cr6000
- **Armour**: 5/5/5
- **Traits**:
  - Open Frame
  - Open Vehicle

### G/bike

Much like its ground-based predecessors, the G/bike's speed and size make it a favourite with many Travellers needing to make their own way around a strange planet. It is also the focus of many subcultures and gangs across Charted Space.

- **TL**: 12
