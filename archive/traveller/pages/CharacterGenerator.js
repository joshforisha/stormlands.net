import * as Character from '~/data/character'
import * as React from 'react'
import Button from '~/view/Button'
import Slab from '~/view/Slab'
import styled from 'styled-components'

const History = styled.ul``

const Line = styled.div`
  display: flex;
  margin-top: 32px;

  & > ${Slab.componentId}:not(:first-of-type) {
    margin-left: 32px;
  }
`

const Npc = styled.div``

const Skill = styled.li``

const Skills = styled.ul``

export default function CharacterGenerator () {
  const [npc, setNpc] = React.useState(null)

  function generate () {
    setNpc(Character.generate())
  }

  React.useEffect(() => {
    generate()
  }, [])

  let npcView = null
  if (npc) {
    const history = npc.history.map(
      (note, i) => <li key={i}>{note}</li>
    )

    const skills = npc.skills.map(
      ([skill, score], i) => (
        <Skill key={i}>
          {Array.isArray(skill) ? `${skill[0]} (${skill[1]})` : skill} {score}
        </Skill>
      )
    )

    npcView = (
      <Npc>
        <Line>
          <Slab label='Age' value={npc.age} />
        </Line>
        <Line>
          <Slab label='STR' value={npc.strength} />
          <Slab label='DEX' value={npc.dexterity} />
          <Slab label='END' value={npc.endurance} />
          <Slab label='INT' value={npc.intellect} />
          <Slab label='EDU' value={npc.education} />
          <Slab label='SOC' value={npc.socialStanding} />
        </Line>
        <Skills>{skills}</Skills>
        <History>{history}</History>
      </Npc>
    )
  }

  return (
    <>
      <header>
        <Button onClick={generate}>Generate</Button>
      </header>
      {npcView}
    </>
  )
}
