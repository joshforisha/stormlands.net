# Weapons

## Melee Weapons

| Melee Weapon | TL  | Damage | Mass | Cost | Traits |
| ------------ | ---:| ------ | ---- | ---- | ------ |
| Blade        |   2 | 2D     | 2 kg   | Cr100 | —     |
| Broadsword   |   2 | 4D     | 8 kg   | Cr500 | Bulky |
| Club         |   1 | 2D     | 3 kg   | —     | —     |
| Cutlass      |   2 | 3D     | 4 kg   | Cr200 | —     |
| Dagger       |   1 | 1D+2   | 1 kg   | Cr10  | —     |
| Improvised   |   — | 2D-2   | Varies | —     | —     |
| Rapier       |   3 | 2D     | 2 kg   | Cr200 | +1 Parrying |
| Shield       |   1 | 1D     | 6 kg   | Cr150 | Melee +1 |
| Staff        |   1 | 2D     | 3 kg   | —     | —     |
| Stunstick    |   8 | 2D     | 1 kg   | Cr300 | Stun  |
| Unarmed      |   — | 1D     | —      | —     | —     |

## Slug Throwers

| Pistol | TL | Range | Damage | Mass | Cost | Mag Size | Mag Cost | Traits |
| ------ | --:| ----- | ------ | ---- | ---- | -------- | -------- | ------ |
| Antique Pistol        |  3 | 5m | 2D-3 | 1 kg | Cr100 | 1 | Cr5 | — |
| Autopistol            |  6 | 10m | 3D-3 | 1 kg | Cr200 | 15 | Cr10 | — |
| Body Pistol           |  8 | 5m | 2D | — | Cr500 | 6 | Cr10 | — |
| Gauss Pistol          | 13 | 20m | 3D | 1 kg | Cr500 | 40 | Cr20 | AP 3, Auto 2 |
| Revolver              |  5 | 10m | 3D-3 | 1 kg | Cr150 | 6 | Cr5 | — |
| Snub Pistol           |  8 | 5m | 3D-3 | — | Cr150 | 6 | Cr10 | Zero-G |

| Rifle | TL | Range | Damage | Mass | Cost | Mag Size | Mag Cost | Traits |
| ----- | --:| ----- | ------ | ---- | ---- | -------- | -------- | ------ |
| Accelerator Rifle     |  9 | 250m | 3D | 2 kg | Cr900 | 15 | Cr30 | Zero-G |
| Advanced Combat Rifle | 10 | 450m | 3D | 3 kg | Cr1000 | 40 | Cr15 | Auto 3, Scope |
| 40mm Grenade          |  — | 250m | Grenade | — | — | 1 | As Grenade | — |
| Antique Rifle         |  3 | 25m | 3D-3 | 6 kg | Cr150 | 1 | Cr10 | — |
| Assault Rifle         |  7 | 200m | 3D | 4 kg | Cr500 | 30 | Cr15 | Auto 2 |
| Autorifle             |  6 | 300m | 3D | 5 kg | Cr750 | 20 | Cr10 | Auto 2 |
| Gauss Rifle           | 12 | 600m | 4D | 4 kg | Cr1500 | 80 | Cr40 | AP 5, Auto 3, Scope |
| Rifle                 |  5 | 250m | 3D | 5 kg | Cr200 | 5 | Cr10 | — |
| Shotgun               |  4 | 50m | 4D | 4 kg | Cr200 | 6 | Cr10 | Bulky |
| Submachine Gun        |  6 | 25m | 3D | 3 kg | Cr400 | 20 | Cr10 | Auto 3 |

## Energy Weapons

| Weapon | TL | Range | Damage | Mass | Cost | Mag Size | PP Cost | Traits |
| ------ | --:| ----- | ------ | ---- | ---- | -------- | ------- | ------ |
| Laser Pistol | 9 | 20m | 3D | 3 kg | Cr2000 | 100 | Cr1000 | Zero-G |
| Laser Pistol | 11 | 30m | 3D+3 | 2 kg | Cr3000 | 100 | Cr3000 | Zero-G |
| Stunner | 8 | 5m | 2D | 0.5 kg | Cr500 | 100 | Cr200 | Stun, Zero-G |
| Stunner | 10 | 5m | 2D+3 | 0.5 kg | Cr750 | 100 | Cr200 | Stun, Zero-G |
| Stunner | 12 | 10m | 3D | 0.5 kg | Cr1000 | 100 | Cr200 | Stun, Zero-G |
| Laser Carbine | 9 | 150m | 4D | 4 kg | Cr2500 | 50 | Cr1000 | Zero-G |
| Laser Carbine | 11 | 200m | 4D+3 | 3 kg | Cr4000 | 50 | Cr3000 | Zero-G |
| Laser Rifle | 9 | 200m | 5D | 8 kg | Cr3500 | 100 | Cr1500 | Zero-G |
| Laser Rifle | 11 | 400m | 5D+3 | 5 kg | Cr8000 | 100 | Cr3500 | Zero-G |
| Laser Sniper Rifle | 12 | 600m | 5D+3 | 6 kg | Cr9000 | 6 | Cr2500 | Scope, Zero-G |
| Plasma Rifle | 16 | 300m | 6D | 6 kg | Cr100,000 | Unlimited | — | — |

## Grenade Weapons

| Grenade | TL | Range | Damage | Mass   | Cost | Traits        | Info |
| ------- | --:| ----- | ------ | ------ | ---- | ------------- | ---- |
| Aerosol |  9 | 20m   | —      | 0.5 kg | Cr15 | Blast 9       | Laser attack -10, no laser comm, dissipates 1D &times; 3 rounds |
| Frag    |  6 | 20m   | 5D     | 0.5 kg | Cr30 | Blast 9       | |
| Smoke   |  6 | 20m   | —      | 0.5 kg | Cr15 | Blast 9       | DM-2 attacks, IR vision avoids, dissipates 1D &times; 3 rounds |
| Stun    |  7 | 20m   | 3D     | 0.5 kg | Cr30 | Blast 9, Stun | |

## Heavy Weapons

| Weapon | TL | Range | Damage | Mass | Cost | Mag Size | Mag Cost | Traits |
| ------ | --:| ----- | ------ | ---- | ---- | -------- | -------- | ------ |
| FGMP | 14 | 450m | 2DD | 12 kg | Cr100000 | - | - | Radiation, Very Bulky |
| FGMP | 15 | 450m | 2DD | 12 kg | Cr400000 | - | - | Bulky, Radiation |
| FGMP | 16 | 450m | 2DD | 15 kg | MCr0.5   | - | - | Radiation |
| Grenade Launcher | 7 | 100m | As grenade | 6 kg | Cr400 | 6 | As grenades | Bulky |
| Machinegun | 6 | 500m | 3D | 12 kg | Cr1500 | 60 | Cr100 | Auto 4 |
| PGMP | 12 | 250m | 1DD | 10 kg | Cr20000 | - | - | Very Bulky |
| PGMP | 13 | 450m | 1DD | 10 kg | Cr65000 | - | - | Bulky |
| PGMP | 14 | 450m | 1DD | 10 kg | Cr100000 | - | - | |
| RAM Grenade Launcher | 8 | 250m | As grenade | 2 kg | Cr800 | 6 | As grenades | Auto 3, Bulky |
| Rocket Launcher | 6 | 120m | 4D 8 | Cr2000 | 1 kg | Cr300 | Blast 6 |
| Rocket Launcher | 7 | 150m | 4D+3 8 | Cr2000 | 1 kg | Cr400 | Blast 6, Smart |
| Rocket Launcher | 8 | 200m | 5D 8 | Cr2000 | 2 kg | Cr600 | Blast 6, Scope, Smart |
| Rocket Launcher | 9 | 250m | 5D+6 8 | Cr2000 | 2 kg | Cr800 | Blast 6, Scope, Smart |

## Explosives

| Weapon | TL | Range | Damage | Mass | Cost | Traits |
| ------ | --:| ----- | ------ | ---- | ---- | ------ |
| Plastic | 6 | — | 3D | — | Cr200 | Blast 9 |
| Pocket Nuke | 12 | — | 6DD | 4 kg | Cr250,000 | Blast 1000, Radiation |
| TDX | 12 | — | 4D | — | Cr1000 | Blast 15 |

## Weapon Options

- **Auxiliary Grenade Launcher (TL7)**:
  - An underslung grenade launcher can be added to any rifle weapon at the cost of Cr1000. This grenade launcher has a magazine of one grenade takes three minor actions to reload. Otherwise, it is identical to the grenade launcher on page 126.
- **Gyrostabiliser (TL9)**:
  - Stabilisers can be added to any Bulky weapon, reducing its recoil and removing the Bulky trait at a cost of Cr500. They cannot be added to any Destructive weapon.
- **Intelligent Weapon (TL11)**:
  - This adds Computer/0 to any weapon. Costs Cr1000.
  - **TL13**: This adds Computer/1 to any weapon. Costs Cr5000.
- **Laser Sight (TL8)**:
  - Integrated optics and laser sights give DM+1 to any attack made at less than 50 metres. Costs Cr200.
  - **TL10**: X-ray lasers and improved display technology removes the tell-tale "red dot’ of a vislight laser.
- **Scope (TL5)**:
  - A high-quality telescopic scope for attachment to a rifle or heavy weapon, allowing accurate shots to be made at extreme ranges. Any rifle or heavy weapon equipped with this gains the Scope trait. Costs Cr50.
  - **TL7**: Adds image enhancement and light intensification, allowing the scope to be used in low-light environments without penalty.
- **Secure Weapon (TL10)**:
  - A secure weapon requires authentication in some fashion (scanning the user’s DNA or iris patterns, entering a password, transmission of an unlocking code from a comm) before it can be fired. Costs Cr250.
- **Supressor (TL8)**:
  - A supressor can be added to any non-automatic slug thrower, masking the sound produced by firing. Costs Cr250.

## Weapon Traits

- **AP X**: This weapon has the ability to punch through armour through the use of specially shaped ammunition or high technology. It will ignore an amount of Armour equal to the AP score listed. Spacecraft Scale targets (see page 157) ignore the AP trait unless the weapon making the attack is also Spacecraft Scale.
- **Auto X**: These weapons fire multiple rounds with every pull of the trigger, filling the air with a hail of fire. A weapon with the Auto trait can make attacks in three fire modes: single, burst, and full auto.
  - *Single*: Attacks are made using the normal combat rules. Burst: Add the Auto score to damage. This uses a number of rounds equal to the Auto score.
  - *Full Auto*: Make a number of attacks equal to the Auto score. These attacks can be made against separate targets so long as they are all within six metres of one another. Full auto uses a number of rounds equal to three times the Auto score.
  - A weapon cannot use the Auto trait in the same action as the Scope trait or aiming action.
- **Blast X**: This weapon has an explosive component or is otherwise able to affect targets spread across a wide area. Upon a successful attack, damage is rolled against every target within the weapon's Blast score in metres. Dodge Reactions may not be made against a Blast weapon, but targets may dive for cover. Cover may be taken advantage of if it lies between a target and the centre of the weapon’s Blast.
- **Bulky**: A Bulky weapon has a powerful recoil or is simply extremely heavy – this makes it difficult to use effectively in combat by someone of a weak physical stature. A Traveller using a Bulky weapon must have STR 9 or higher to use it without penalty. Otherwise, all attack rolls will have a negative DM equal to the difference between their STR DM and +1.
- **Radiation**: When a Radiation weapon is fired, anyone close to the firer, target and the line of fire in-between the two will receive 2D x 20 rads, multiplied by 5 for Spacecraft scale weapons. This effect extends from the firer, target and line of fire a distance in metres equal to the number of dice the weapon rolls for damage. If the fusion weapon is Destructive, this distance becomes ten times the number of dice rolled for damage.
- **Scope**: The weapon has been fitted with vision- enhancing sights, allowing it to put shots on target from far greater ranges. A weapon with the Scope trait ignores the rule that limits all attacks made at a range greater than 100 metres are automatically Extreme Range, so long as the Traveller aims before shooting.
- **Smart**: This weapon has intelligent or semi-intelligent rounds that are able to guide themselves onto a target. They gain a DM to their attack rolls equal to the difference between their TL and that of the target, to a minimum of DM+1 and a maximum of DM+6.
- **Stun**: These weapons are designed to deal non-lethal damage, incapacitating a living target rather than killing it. Damage is only deducted from END, taking into account any armour. If the target’s END is reduced to 0, the target will be incapacitated and unable to perform any actions for a number of rounds by which the damage exceeded his END. Damage received from Stun weapons is completely healed by one hour of rest.
- **Very Bulky**: Some weapons are designed only for the strongest combatants. A Traveller using a Very Bulky weapon must have STR 12 or higher to use it without penalty. Otherwise, all attack rolls will have a negative DM equal to the difference between their STR DM and +2.
- **Zero-G**: This weapon has little or no recoil, allowing it to be used in low or zero gravity situations without requiring an Athletics (dexterity) check.
