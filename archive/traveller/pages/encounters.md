# Encounters

## Allies and Enemies

| D66 | Character | D66 | Character |
| ---:| --------- | ---:| --------- |
| 11 | Naval Officer | 41 | Bored Noble |
| 12 | Imperial Diplomat | 42 | Planetary Governor |
| 13 | Crooked Trader | 43 | Inveterate Gambler |
| 14 | Medical Doctor | 44 | Crusading Journalist |
| 15 | Eccentric Scientist | 45 | Doomsday Cultist |
| 16 | Mercenary | 46 | Corporate Agent |
| 21 | Famous Performer | 51 | Criminal Syndicate |
| 22 | Alien Thief | 52 | Military Governor |
| 23 | Free Trader | 53 | Army Quartermaster |
| 24 | Explorer | 54 | Private Investigator |
| 25 | Marine Captain | 55 | Starport Administrator |
| 26 | Corporate Executive | 56 | Retired Admiral |
| 31 | Researcher | 61 | Alien Ambassador |
| 32 | Cultural Attaché | 62 | Smuggler |
| 33 | Religious Leader | 63 | Weapons Inspector |
| 34 | Conspirator | 64 | Elder Statesman |
| 35 | Rich Noble | 65 | Planetary Warlord |
| 36 | Artificial Intelligence | 66 | Imperial Agent |

## Random Opposition

| D66 | Opposition | D66 | Opposition |
| ---:| ---------- | ---:| ---------- |
| 11 | Animals | 41 | Target is in deep space |
| 12 | Large Animal | 42 | Target is in orbit |
| 13 | Bandits & thieves | 43 | Hostile weather conditions |
| 14 | Fearful peasants | 44 | Dangerous organisms or radiation |
| 15 | Local authorities | 45 | Target is in a dangerous region |
| 16 | Local lord | 46 | Target is in a restricted area |
| 21 | Criminals – thugs or corsairs | 51 | Target is under electronic observation |
| 22 | Criminals – thieves or saboteurs | 52 | Hostile guard robots or ships |
| 23 | Police – ordinary security forces | 53 | Biometric identification required |
| 24 | Police – inspectors & detectives | 54 | Mechanical failure or computer hacking |
| 25 | Corporate – agents | 55 | Travellers are under surveillance |
| 26 | Corporate – legal | 56 | Out of fuel or ammunition |
| 31 | Starport security | 61 | Police investigation |
| 32 | Imperial marines | 62 | Legal barriers |
| 33 | Interstellar corporation | 63 | Nobility |
| 34 | Alien – private citizen or corporation | 64 | Government officials |
| 35 | Alien – government | 65 | Target is protected by a third party |
| 36 | Space travellers or rival ship | 66 | Hostages |

## Random Mission

| D66 | Mission | D66 | Mission |
| ---:| ------- | ---:| ------- |
| 11 | Assassinate a target | 41 | Investigate a crime |
| 12 | Frame a target | 42 | Investigate a theft |
| 13 | Destroy a target | 43 | Investigate a murder |
| 14 | Steal from a target | 44 | Investigate a mystery |
| 15 | Aid in a burglary | 45 | Investigate a target |
| 16 | Stop a burglary | 46 | Investigate an event |
| 21 | Retrieve data or an object from a secure facility | 51 | Join an expedition |
| 22 | Discredit a target | 52 | Survey a planet |
| 23 | Find a lost cargo | 53 | Explore a new system |
| 24 | Find a lost person | 54 | Explore a ruin |
| 25 | Deceive a target | 55 | Salvage a ship |
| 26 | Sabotage a target | 56 | Capture a creature |
| 31 | Transport goods | 61 | Hijack a ship |
| 32 | Transport a person | 62 | Entertain a noble |
| 33 | Transport data | 63 | Protect a target |
| 34 | Transport goods secretly | 64 | Save a target |
| 35 | Transport goods quickly | 65 | Aid a target |
| 36 | Transport dangerous goods | 66 | It's a trap – the Patron intends to betray the Traveller |

## Starport Encounters

| D66 | Encounter | D66 | Encounter |
| ---:| --------- | ---:| --------- |
| 11 | Maintenance robot at work | 41 | Traders offer spare parts and supplies at cut- price rates |
| 12 | Trade ship arrives or departs | 42 | Repair yard catches fire |
| 13 | Captain argues about fuel prices | 43 | Passenger liner arrives or departs |
| 14 | News report about pirate activity on a starport screen draws a crowd | 44 | Servant robot offers to guide Travellers around the spaceport |
| 15 | Bored clerk makes life difficult for the Travellers | 45 | Trader from a distant system selling strange curios |
| 16 | Local merchant with cargo to transport seeks a ship | 46 | Old crippled belter asks for spare change and complains about drones taking his job |
| 21 | Dissident tries to claim sanctuary from planetary authorities | 51 | Patron offers the Travellers a job |
| 22 | Traders from offworld argue with local brokers | 52 | Passenger looking for a ship |
| 23 | Technician repairing starport computer system | 53 | Religious pilgrims try to convert the Travellers |
| 24 | Reporter asks for news from offworld | 54 | Cargo hauler arrives or departs |
| 25 | Bizarre cultural performance | 55 | Scout ship arrives or departs |
| 26 | Patron argues with another group of Travellers | 56 | Illegal or dangerous goods are impounded |
| 31 | Military vessel arrives or departs | 61 | Pickpocket tries to steal from the Travellers |
| 32 | Demonstration outside starport | 62 | Drunken crew pick a fight |
| 33 | Escaped prisoners begs for passage offworld | 63 | Government officials investigate the characters |
| 34 | Impromptu bazaar of bizarre items | 64 | Random security sweep scans Travellers and their baggage |
| 35 | Security patrol | 65 | Starport is temporarily shut down for security reasons |
| 36 | Unusual alien | 66 | Damaged ship makes emergency docking |

## Rural Encounters

| D66 | Encounter | D66 | Encounter |
| ---:| --------- | ---:| --------- |
| 11 | Wild Animal | 41 | Wild Animal |
| 12 | Agricultural robots | 42 | Small community – quiet place to live |
| 13 | Crop sprayer drone flies overhead | 43 | Small community – on a trade route |
| 14 | Damaged agricultural robot being repaired | 44 | Small community – festival in progress |
| 15 | Small, isolationist community | 45 | Small community – in danger |
| 16 | Noble hunting party | 46 | Small community – not what it seems |
| 21 | Wild Animal | 51 | Wild Animal |
| 22 | Local landing field | 52 | Unusual weather |
| 23 | Lost child | 53 | Difficult terrain |
| 24 | Travelling merchant caravan | 54 | Unusual creature |
| 25 | Cargo convoy | 55 | Isolated homestead – welcoming |
| 26 | Police chase | 56 | Isolated homestead – unfriendly |
| 31 | Wild Animal | 61 | Wild Animal |
| 32 | Telecommunications black spot | 62 | Private villa |
| 33 | Security patrol | 63 | Monastery or retreat |
| 34 | Military facility | 64 | Experimental farm |
| 35 | Bar or waystation | 65 | Ruined structure |
| 36 | Grounded spacecraft | 66 | Research facility |

## Urban Encounters

| D66 | Encounter | D66 | Encounter |
| ---:| --------- | ---:| --------- |
| 11 | Street riot in progress | 41 | Security Patrol |
| 12 | Travellers pass a charming restaurant | 42 | Ancient building or archive |
| 13 | Trader in illegal goods | 43 | Festival |
| 14 | Public argument | 44 | Someone is following the characters |
| 15 | Sudden change of weather | 45 | Unusual cultural group or event |
| 16 | Travellers are asked for help | 46 | Planetary official |
| 21 | Travellers pass a bar or pub | 51 | Travellers spot someone they recognise |
| 22 | Travellers pass a theatre or other entertainment venue | 52 | Public demonstration |
| 23 | Curiosity Shop | 53 | Robot or other servant passes Travellers |
| 24 | Street market stall tries to sell the Travellers something | 54 | Prospective patron |
| 25 | Fire, dome breach or other emergency in progress | 55 | Crime such as robbery or attack in progress |
| 26 | Attempted robbery of Travellers | 56 | Street poacher rants at the Travellers |
| 31 | Vehicle accident involving the Travellers | 61 | News broadcast on public screens |
| 32 | Low-flying spacecraft flies overhead | 62 | Sudden curfew or other restriction on movement |
| 33 | Alien or other offworlder | 63 | Unusually empty or quiet street |
| 34 | Random character bumps into a Traveller | 64 | Public announcement |
| 35 | Pickpocket | 65 | Sports event |
| 36 | Media team or journalist | 66 | Imperial Dignitary |

## Range Band

| Range | Distance to Target |
| ----- | ------------------ |
| Close | Up to 5 metres |
| Short | 5–10 metres |
| Medium | 11–50 metres |
| Long | 51–250 metres |
| Very Long | 251–500 metres |
| Distant | 501–5000 metres |
| Very Distant | Over 5 kilometres |

## Encounter Distance

- Clear Terrain: DM+3
- Forest or Woods: DM-2
- Crowded Area: DM-2
- In Space: DM+4
- Target is a Vehicle: DM+2 for every 10 Hull or part of
- Travellers actively looking for danger: + highest Recon skill

| 2D | Range Band |
| -- | ---------- |
| 2 or less | Close |
| 3 | Short |
| 4–5 | Medium |
| 6–9 | Long |
| 10–11 | Very Long |
| 12 or more | Distant |

## In Space

Roll 1D every day. On a 6, the ship has encountered something—roll D66 twice, modifying only the first die:

- Highport: DM+3
- High-Traffic Space: DM+2
- Settled Space: DM+1
- Border Systems: DM+0
- Wild Space: DM-1
- Empty Space: DM-4

(Core p147)

| D66 | Encounter | D66 | Encounter |
| --- | --------- | --- | --------- |

Encounter distance depends on an *Electronics (sensors)* check.
