# Medical and Care Supplies

**Healing**: An injured Traveller who needs hospital care for a prolonged period will pay approximately Cr100 per month per Tech Level (at TL11+ doctors may just use Medicinal Slow in many cases). Surgery costs 1D &times; Cr50 &times; Tech Level.

**Replacements**: A Traveller whose injuries require cloning limbs or cybernetic replacement must pay Cr5000 per characteristic point lost.

## Cryoberth (TL10)

A cryoberth, or "icebox" is a coffin-like machine similar to the low or frozen berths used on some spacecraft. The main difference is that a cryoberth works much faster than a low berth, freezing and preserving its occupant almost instantly. A cryoberth can therefore be used to place an severely injured Traveller into stasis until he receives medical treatment. A cryoberth’s internal power system can function for up to one week on its own, but a berth is usually connected to a vehicle’s or ship’s power supply. Costs Cr50000 with a mass of 200 kg.

## Medikit (TL8+)

There are different types of medikit available at different technology levels. All medikits contain diagnostic devices and scanners, surgical tools and a panoply of drugs and antibiotics, allowing a medic to practise his art in the field. Higher-technology medikits do not give a bonus to basic treatment, but can help with more exotic problems or when treating augmented individuals. For example, a TL8 medikit can test blood pressure and temperature, while a TL14 kit has a medical densitometer to create a three-dimensional view of the patient’s body and can scan brain activity on the quantum level. All medikits mass 1 kg, except at TL14 where they have an effective mass of 0kg. TL8: Costs Cr1000.

**TL10**: Grants DM+1 on Medic checks performed for first aid. Costs Cr1500.
**TL12**: Grants DM+2 on Medic checks performed for first aid. Costs Cr5000.
**TL14**: Grants DM+3 on Medic checks performed for first aid. Costs Cr10000.

## Drugs

There are several drugs (or "meds") in standard use.

**Anagathics (TL15)**: These slow the user’s ageing process. Synthetic anagathics become possible at TL15, but there are natural spices and other rare compounds that have comparable effects. Anagathics are illegal or heavily controlled on many worlds. One dose must be taken each month to maintain the anti-aging effect. They cost Cr20000 per dose.

**Anti-rad (TL8)**: Anti-rad drugs must be administered before or immediately (within ten minutes) after radiation exposure. They absorb up to 100 rads per dose. A Traveller may only use anti-rad drugs once per day – taking any more causes permanent END damage of 1D per dose. Costs Cr1000 per dose.

**Combat Drugs (TL10)**: Combat drugs increase reaction times and improve a body’s responses to trauma. A Traveller using combat drugs gains DM+4 to all initiative rolls. He also gains a free reaction every round with no penalty applied, and reduces all damage sustained by –2 points. The drug kicks in around 20 seconds (three rounds) after ingestion or injection, and lasts for around ten minutes. When the drug wears off, the user is Fatigued (see page 76). Combat drugs cost Cr1000 per dose.

**Fast Drug (TL10)**: Also called Hibernation, this drug puts the user into a state akin to suspended animation, slowing his metabolic rate down to a ratio of 60 to 1 – a subjective day for the user is actually two months. Fast Drug is normally used to prolong life support reserves or as a cheap substitute for a cryoberth. Fast drug costs Cr200 per dose.

**Medicinal Drugs (TL5+)**: Includes vaccines, antitoxins and antibiotics. They range in cost from Cr5 to several thousand credits, depending on the rarity and complexity of the drug. Medicinal drugs require the Medic skill to use properly—using the wrong drug can be worse than doing nothing.

**Metabolic Accelerator (TL10)**: These boost the user’s reaction time to superhuman levels. To the user, everyone else appears to be moving much slower. A Traveller using a metabolic accelerator in combat gains DM+8 to all initiative rolls. He also gains two free reactions every round with no penalties applied for either. The drug kicks in 45 seconds after ingestion or injection, and lasts for around ten minutes. When the drug wears off, the user’s system crashes. He suffers 2D points of damage and is fatigued (see page 109). Metabolic accelerator costs Cr500 per dose.

**Panaceas (TL8+)**: These are wide-spectrum medicinal drugs specifically designed not to interact harmfully. They can therefore be used on any wound or illness and are guaranteed not to make things worse. A Traveller using panaceas may make a Medic check as if he had Medic 0 when treating an infection or disease. Panaceas cost Cr200 per dose.

**Slow Drug (TL11)**: This is a variant of the metabolic accelerator. It can only be applied safely in a medical facility where life-support and cryotechnology is available, as it increases the metabolism to around thirty times normal, allowing a patient to undergo a month of healing in a single day. Using Medicinal Slow outside of a hospital or sickbay is a messy and painful way to commit suicide, as the user will rapidly cook his internal organs and suffer massive brain damage. Medicinal slow costs Cr500 per dose.

**Stims (TL8)**: These remove fatigue, though at a cost. A Traveller who uses stims removes Fatigue (see page 76) but also sustains one point of damage. If stims are used repeatedly without natural sleep in between, the user suffers a cumulative additional point of damage every time (so, on the second use, two points of damage are sustained, on the third, three points, and so on). Costs Cr50 per dose.
