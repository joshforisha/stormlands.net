export function $(sel) {
  return document.querySelector(sel)
}

export function $$(sel) {
  return Array.from(document.querySelectorAll(sel))
}

export function h(elementType, children = []) {
  if (typeof children === 'string') children = [children]
  const element = document.createElement(elementType)
  for (const c of children) {
    if (!c) continue
    element.appendChild(typeof c === 'string' ? document.createTextNode(c) : c)
  }
  return element
}

export function list(items) {
  return h(
    'ul',
    items.filter(Boolean).map((x) => h('li', x))
  )
}

export function numberedList(items) {
  return h(
    'ol',
    items.filter(Boolean).map((x) => h('li', x))
  )
}

export function trait(name, value) {
  if (!value) return null
  if (typeof value === 'number') value = value.toString()
  return [h('b', [`${name}: `]), value]
}
