import { without } from './array.js'

export function dice (min, max) {
  return (num = 1) => {
    let total = 0
    for (let i = 0; i < num; i++) total += die(min, max)
    return total
  }
}

export function die (min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min)
}

export function roll (type, num, mod) {
  if (mod) window.alert(mod(type(num)))
  else window.alert(type(num))
}

export const d3 = dice(1, 3)
export const d4 = dice(1, 4)
export const d5 = dice(1, 5)
export const d6 = dice(1, 6)
export const d8 = dice(1, 8)
export const d10 = dice(1, 10)
export const d12 = dice(1, 12)
export const d20 = dice(1, 20)
export const d100 = dice(1, 100)

export function rand (min, max) {
  return Math.floor(min + Math.random() * (max - min + 1))
}

export function random (min, max) {
  return min + Math.random() * (max - min)
}

export function takeUniform (count, xs) {
  let ys = xs
  const results = []
  for (let i = 0; i < count; i++) {
    const index = rand(0, ys.length - 1)
    const y = ys[index]
    if (typeof y === 'function') results.push(y())
    else results.push(y)
    ys = without(index, ys)
  }
  return results
}

export function takeWeighted (count, xs) {
  let ys = xs
  const results = []
  for (let i = 0; i < count; i++) {
    const index = weightedIndex(ys)
    const y = ys[index][1]
    if (typeof y === 'function') results.push(y())
    else results.push(y)
    ys = without(index, ys)
  }
  return results
}

export function uniform (xs) {
  const x = xs[rand(0, xs.length - 1)]
  if (typeof x === 'function') return x()
  return x
}

export function weighted (xs, max) {
  const [, x] = xs[weightedIndex(xs, max)]
  if (typeof x === 'function') return x()
  return x
}

export function weightedIndex (xs, max) {
  const total = xs.reduce((t, [w]) => t + w, 0)
  const roll = rand(0, typeof max === 'number' ? max : total - 1)
  let y = 0
  for (let i = 0; i < xs.length; i++) {
    const [w] = xs[i]
    if (roll < y + w) return i
    y += w
  }
}
