import { alterations } from './technobabble/alterations.js'
import { draw } from './array.js'
import { h } from './dom.js'
import { nouns } from './technobabble/nouns.js'
import { problems } from './technobabble/problems.js'
import { qualifiers } from './technobabble/qualifiers.js'
import { relations } from './technobabble/relations.js'
import { scientifics } from './technobabble/scientifics.js'
import { solutions } from './technobabble/solutions.js'

const noun = () => draw(nouns)
const qualifier = () => draw(qualifiers)
const scientific = () => draw(scientifics)

function complexTech() {
  if (Math.random() < 0.5) {
    return `${qualifier()} ${scientific()} ${noun()} ${noun()}`
  }
  return `${qualifier()} ${scientific()} ${noun()}`
}

function generalTech() {
  return `${draw(qualifiers)} ${draw(nouns)}`
}

function simpleTech() {
  return `${scientific()} ${noun()}`
}

export const actions = {
  Technobabble: [
    '🤖',
    () => {
      return {
        title: 'Technobabble',
        content: [
          h('p', [
            h('b', 'Problem: '),
            `${draw(problems)} ${generalTech()}, ${draw(
              relations
            )} the ${complexTech()}`,
            h('br'),
            h('b', 'Solution: '),
            `${draw(solutions)} the ${complexTech()}, to ${draw(
              alterations
            )} the ${simpleTech()}`
          ])
        ]
      }
    }
  ]
}
