export function capital (str) {
  return str.substring(0, 1).toUpperCase() + str.substring(1)
}

export function indefinite (noun) {
  const vowels = ['a', 'e', 'i', 'o', 'u']
  const article = vowels.includes(noun[0]) ? 'an' : 'a'
  return article + ' ' + noun
}

export function listWords (words) {
  if (words.length === 1) return words[0]
  if (words.length === 2) return words.join(' and ')
  return [
    words.slice(0, -1).join(', '),
    words[words.length - 1]
  ].join(', and ')
}

export function plural (num, noun, pnoun) {
  return `${num} ${num === 1 ? noun : (pnoun || `${noun}s`)}`
}

export function withString (str) {
  if (str) return ` ${str}`
  return ''
}
