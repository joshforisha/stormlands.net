import weightedAmphibiansAndReptiles from './weighted-amphibians-and-reptiles.js'
import weightedAquaticAnimals from './weighted-aquatic-animals.js'
import weightedBirds from './weighted-birds.js'
import weightedFantasticAnimals from './weighted-fantastic-animals.js'
import weightedMammals from './weighted-mammals.js'
import { weighted } from '../random.js'

export default [
  [2, () => weighted(weightedAmphibiansAndReptiles)],
  [1, () => weighted(weightedAquaticAnimals)],
  [1, () => weighted(weightedBirds)],
  [4, () => weighted(weightedMammals)],
  // [1, () => weighted(weightedOtherAnimals)],
  // [1, () => weighted(weightedPlants)],
  [1, () => weighted(weightedFantasticAnimals)]
  // [1, () => weighted(weightedFantasticPeoples)],
]
