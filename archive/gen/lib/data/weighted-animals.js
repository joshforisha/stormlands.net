import weightedAmphibiansAndReptiles from './weighted-amphibians-and-reptiles.js'
import weightedAquaticAnimals from './weighted-aquatic-animals.js'
import weightedBirds from './weighted-birds.js'
import weightedMammals from './weighted-mammals.js'

export default [
  ...weightedAmphibiansAndReptiles,
  ...weightedAquaticAnimals,
  ...weightedBirds,
  ...weightedMammals
]
