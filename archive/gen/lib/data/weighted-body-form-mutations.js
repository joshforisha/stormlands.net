import extendedColors from './extended-colors.js'
import weightedBodyParts from './weighted-body-parts.js'
import weightedCreatures from './weighted-creatures.js'
import { capital, indefinite, listWords, plural } from '../text.js'
import { initialize } from '../array.js'
import {
  d3,
  d4,
  d6,
  d10,
  takeUniform,
  takeWeighted,
  uniform,
  weighted
} from '../random.js'

const bodyPart = () => weighted(weightedBodyParts)
const color = () => uniform(extendedColors)
const colors = (num) => listWords(takeUniform(num, extendedColors))
const creature = (max) => weighted(weightedCreatures, max)

export default [
  [
    1,
    [
      'Aesthetically Attractive',
      () => [
        'This mutant’s physical appearance exemplifies the properties its species considers visually pleasing, regardless of whether this inspires sexual desire or not. Other mutations it has may or may not spoil this effect.'
      ]
    ]
  ],
  [
    1,
    [
      'Alopecia',
      () => [
        'This mutant experiences uneven hair loss. If the hair grows back, there is a 1 in 3 chance that it has no color in it.',
        `Type of alopecia: ${weighted([
          [1, 'allergy-based hair loss'],
          [2, 'periodic bald patches'],
          [1, 'hereditary spot baldness'],
          [2, 'stress-induced hair loss']
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Alternate Locomotion',
      () => [
        'Instead of walking on two legs (or whatever is normal for its species), this mutant has some other form of locomotion. Its old method of travel is no longer effective.',
        `Method of travel: ${weighted([
          [3, () => `legs of ${indefinite(creature())}`],
          [4, 'quadraped'],
          [3, 'slithering'],
          [1, 'mass of tendrils or tentacles'],
          [1, 'pseduopods'],
          [3, () => `body of ${indefinite(creature())}, except for head`],
          [1, 'bouncing'],
          [1, 'fronds'],
          [2, 'hovering'],
          [1, 'teleporting']
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Amorphous',
      () => [
        'This mutant is a blob, probably without distinguishable limbs or extremities.',
        `Extent of movement: ${uniform([
          'can creep or slither',
          'can form temporary limbs to move',
          'can roll around',
          "can't move without help"
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Animal Feet',
      () => [
        'This mutant has the feet of some other creature.',
        `Type of animal feet: ${uniform([
          'bird feet',
          'hooves',
          'mammalian paws',
          'reptilian claws',
          'stumps',
          'tentacles'
        ])}.`
      ]
    ]
  ],
  [
    2,
    [
      'Antennae',
      () => [
        'This mutant has a set of two or more antennae extending from its head, which it can use to feel and possibly smell and taste or otherwise augment its various senses.',
        `Additional sense: ${uniform([
          'hearing and vibration sense',
          'smell and taste, and vibration sense',
          'vibration sense only',
          'vibration sense and visuals',
          'all mundane sense',
          'sense magic and the supernatural'
        ])}.`
      ]
    ]
  ],
  [
    2,
    [
      'Anthropomorphic Animal',
      () => [
        `This mutant appears to be an uplifted or anthropomorphic, talking mutant ${creature()}.`,
        `Anthropomorphic appearance: ${weighted([
          [3, 'body is half-human, facial features are all creature'],
          [1, 'human that resembles the creature'],
          [2, 'half creature, half human'],
          [
            2,
            'same as creature (ignore this result for one of either hands or pedalism)'
          ]
        ])}.`,
        `Hand development: ${uniform([
          'clumsy hands',
          'dexterous creature hands',
          'human hands',
          'same as creature'
        ])}.`,
        `Pedalism: ${weighted([
          [2, 'fully bipedal'],
          [2, 'half creature, half human'],
          [1, 'stooped bipedal'],
          [1, 'same as creature']
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Antlers',
      () => [
        'This mutant has antlers growing out of its head.',
        `Type of antlers: ${weighted([
          [1, 'one sindle antler-like horn'],
          [2, 'rack of dull antlers'],
          [3, 'rack of sharp antlers'],
          [2, 'rack of velvet-covered antlers']
        ])}.`
      ]
    ]
  ],
  [
    2,
    [
      'Armor',
      () => [
        'This mutant is protected by an armored exterior.',
        `Type of natural armor: ${uniform([
          'bone plates (harder)',
          'carapace (hard)',
          'hard shell (hardest)',
          'scales (soft)'
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Atrophied or Malformed Part',
      () => [
        'One or more parts of this mutant’s body are atrophied or malformed.',
        `Affected parts: ${takeWeighted(
          weighted([
            [3, 1],
            [1, 3],
            [2, 2]
          ]),
          [
            [2, 'arm'],
            [2, 'face or mouth'],
            [1, 'foot'],
            [1, 'genitals'],
            [1, 'hand'],
            [1, 'head'],
            [2, 'leg'],
            [1, 'tail or other'],
            [1, 'wing or other']
          ]
        ).join(', ')}.`
      ]
    ]
  ],
  [
    1,
    [
      'Backwards Parts',
      () => [
        'One or more parts of this mutant are attached backwards—the opposite way they are attached on others of the same species.',
        `Affected parts: ${takeWeighted(
          weighted([
            [3, 1],
            [1, 3],
            [2, 2]
          ]),
          [
            [2, 'arms'],
            [1, 'feet'],
            [1, 'genitals'],
            [1, 'hands'],
            [2, 'head'],
            [1, 'legs']
          ]
        ).join(', ')}.`
      ]
    ]
  ],
  [
    1,
    [
      'Beak',
      () => [
        'This mutant has a beak instaed of the mouth that is normal for its species. If it normally has a beak, now it has two.'
      ]
    ]
  ],
  [
    1,
    [
      'Bestial Face',
      () => [
        `This mutant has a face that perfectly or superficially resembles that of ${indefinite(
          creature(8)
        )}.`
      ]
    ]
  ],
  [
    1,
    [
      'Birthmark',
      () => [
        'This mutant has a prominent birthmark resembling a wine stain, or other appropriate discoloration.',
        `Most affected location: ${bodyPart()}.`
      ]
    ]
  ],
  [
    1,
    [
      'Body Barbs',
      () => [
        'Bone spurs grow out of this mutant’s skin, and can be use as weapons.',
        `Location of body barbs: ${weighted([
          [1, 'back'],
          [2, 'elbows and/or knees'],
          [2, 'hands and/or feet'],
          [1, 'head']
        ])}.`,
        `Shape of body barbs: ${uniform([
          'dull',
          'pointed',
          'sharp',
          'sharp and pointed'
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Boils and Sores',
      () => [
        'This mutant’s body is covered in painful and irritating boils and sores that leak fluids and are prone to infection.'
      ]
    ]
  ],
  [
    1,
    [
      'Bristles',
      () => [
        'This mutant has coarse bristly hair all over it that offers some small amount of protection.'
      ]
    ]
  ],
  [1, ['Bulging Eyes', () => ['This mutant’s eyes are large and bulbous.']]],
  [
    2,
    [
      'Cat’s Eyes',
      () => [
        'This mutant’s pupils are slit like a cat’s eyes, increasing its night vision.'
      ]
    ]
  ],
  [
    1,
    [
      'Changing Colors',
      () => [
        'This mutant physically changes color, based on temperament and temperature.',
        `Colors: ${takeUniform(d6(1), extendedColors).join(', ')}.`
      ]
    ]
  ],
  [
    1,
    [
      'Cilia',
      () => [
        'This mutant, or part of it, is covered in tiny, movable, hair-like appendages or tentacles that probably keep it free from dirt and constantly move food toward its mouth.',
        `Cilia coverage: ${weighted([
          [1, 'back'],
          [1, 'front'],
          [2, 'full body'],
          [2, () => takeWeighted(2, weightedBodyParts).join(' to ')]
        ])}.`
      ]
    ]
  ],
  [
    3,
    [
      'Claws',
      () => [
        'This mutant has claws that can be used as weapons.',
        `Type of claws: ${uniform([
          'bird-like talons',
          'claws made of scales',
          'retractable claws',
          'sharp, iron-hard nails'
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Clothes',
      () => [
        'This mutant appears to be wearing clothes, but this is actually just the appearance of its naked skin. It can still wear clothes over top of its garment-resembling skin, of course.'
      ]
    ]
  ],
  [
    2,
    [
      'Clubfoot',
      () => [
        'One of this mutant’s feet is deformed and gimpy. It is probably shorter than the other, impedes proper movement, and makes shoe purchasing more difficult.'
      ]
    ]
  ],
  [
    1,
    [
      'Complex Mouthparts',
      () => [
        'This mutant has additional complex parts around its mouth, like an arthropod. This can include external chelicerae, mandibles, maxillae, a proboscis, a radula, or other parts.'
      ]
    ]
  ],
  [
    1,
    [
      'Covered in Orifices',
      () => [
        'This mutant’s body is covered in orifices.',
        `Type of orifices: ${weighted([
          [1, 'anuses'],
          [2, 'cloacas/urethras'],
          [1, 'ears'],
          [2, 'eyes'],
          [1, 'large pores'],
          [2, 'mouths'],
          [1, 'nostrils'],
          [1, 'siphons or valves'],
          [1, 'vaginas']
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Crown',
      () => [
        'This mutant has a crown of bone or fleshy material encircling its head.',
        `Resemblance: ${weighted([
          [1, 'bone or cartilage'],
          [1, 'coral'],
          [1, 'ears'],
          [1, 'erectile tissue'],
          [1, 'eye stalks'],
          [1, 'fingers or toes'],
          [1, 'noses'],
          [2, 'other material'],
          [1, 'tongues']
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Diffused Organs',
      () => [
        'One or more of this mutant’s organs are diffused, spread out in a larger system within the mutant’s body, instead of concentrated in one space. This makes the organ(s) much more resistant to injury, as it can continue to operate when a part of it is wounded.',
        `Diffused organ(s): ${uniform([
          'bladder or kidneys',
          'brain',
          'heart and blood circulation',
          'intestines and stomach',
          'liver and pancreas',
          'lungs and respiratory system',
          'sense organs',
          'two organs or sets of organs'
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Doppelgänger',
      () => [
        'This mutant looks exactly like one or more other individuals it may or may not know.'
      ]
    ]
  ],
  [
    1,
    [
      'Double Mouth',
      () => [
        'This mutant has an interior mouth inside of an exterior mouth. The interior mouth may or may not be extensible. It cannot be seen when the exterior mouth is closed.'
      ]
    ]
  ],
  [
    1,
    [
      'Emotionally Attractive',
      () => [
        'This mutant’s physical appearance has a tendency to inspire feelings of empathy, sympathy, and emotional attachment in other members of its species. It might be cute, pathetic-looking, or just aesthetically pleasing in a way that puts others at ease.'
      ]
    ]
  ],
  [
    2,
    [
      'Enormously Fat',
      () => [
        'This mutant is so corpulent that it can no longer run and likely has trouble walking.'
      ]
    ]
  ],
  [
    2,
    [
      'Exotic Genitalia',
      () => [
        'This mutant’s sexual organs include unusual shapes and structures, and may include erectile tissue in various places all over the body or in strange locations.'
      ]
    ]
  ],
  [
    2,
    [
      'Extra Eyes',
      () => [
        'This mutant has more eyes than normal.',
        `Number of additional eyes: ${weighted([
          [1, () => `${d6(1) + 1} extra eyes`],
          [1, 'four extra eyes'],
          [3, 'one extra eye'],
          [1, 'two extra eyes'],
          [1, 'three extra eyes']
        ])}.`
      ]
    ]
  ],
  [
    2,
    [
      'Extra Fingers and Toes',
      () => [
        'This mutant has more finger and toes than is normal for its species.',
        `Number of extra digits: ${weighted([
          [1, () => `${d3(1) + 1} extra digits on each hand and foot`],
          [
            1,
            () =>
              `${d6(2)} extra digits, each randomly assigned to a hand or foot`
          ],
          [1, 'one extra digit on each foot'],
          [3, 'one extra digit on each hand'],
          [2, 'one extra digit on each hand and foot']
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Extra Joints',
      () => [
        'This mutant has one or more extra joints in its limbs.',
        `Limbs with extra joints: ${uniform([
          'arms only',
          'arms and legs',
          'legs only',
          'other limbs only'
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Extra Orifices',
      () => [
        'This mutant has one or more additional orifices of some kind.',
        ...initialize(
          weighted([
            [1, () => d4(1) + 1],
            [2, 1],
            [1, 2]
          ]),
          () =>
            `${capital(bodyPart())}: extra ${uniform([
              'anus or cloaca',
              'ear or valve',
              'eye',
              'mouth or siphon',
              'nose or nostril',
              'urethra or vagina'
            ])}.`
        )
      ]
    ]
  ],
  [
    2,
    [
      'Extremely Thin',
      () => [
        'This mutant can’t maintain a normal body weight for its species, and is very thin.'
      ]
    ]
  ],
  [
    1,
    [
      'Eye Stalks',
      () => [
        'This mutant’s eyes are on the end of stalks, which may or may not be semi-retractable.'
      ]
    ]
  ],
  [
    3,
    [
      'Fangs',
      () => [
        'This mutant has dangerous teeth.',
        `Type of fangs: ${uniform([
          'long canines',
          'needle-like teeth',
          'rat-like incisors',
          'retractable canines'
        ])}.`
      ]
    ]
  ],
  [
    2,
    ['Feathers', () => ['This mutant has feathers on part or all of its body.']]
  ],
  [
    1,
    [
      'Fin',
      () => [
        'This mutant has a fin protruding from its back or head, which helps it swim.'
      ]
    ]
  ],
  [
    1,
    [
      'Flaking Skin',
      () => ['This mutant has full-body dandruff due to dry and flaky skin.']
    ]
  ],
  [
    1,
    [
      'Flippers',
      () => [
        'This mutant has flippers instead of either arms and hands, or both arms as well as legs.'
      ]
    ]
  ],
  [
    5,
    [
      'Fur',
      () => [
        'This mutant has fur covering all or most of its body.',
        `Type of fur: ${uniform([
          'long fur',
          'short, bristly fur',
          'short, soft fur',
          'thick, curly hair'
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Fused Ribs',
      () => [
        'This mutant’s ribs are fused together, forming a bony plate across the upper torso.'
      ]
    ]
  ],
  [
    1,
    [
      'Gliding Membranes',
      () => [
        'This mutant has membranes or flaps of skin between its limbs that, along with hollow bones, allows it to glide after jumping from heights.'
      ]
    ]
  ],
  [
    2,
    [
      'Hairless',
      () => ['This mutant has no hair, fur, or feathers anywhere on its body.']
    ]
  ],
  [
    1,
    [
      'Headless',
      () => [
        'This mutant has no head. Its face is located on its chest or some other part of its body.'
      ]
    ]
  ],
  [
    1,
    [
      'Hemihypertrophy',
      () => [
        'One side of this mutant’s body is significantly larger than the other, resulting in an unattractive lack of symmetry. Limbs on one side are longer and stronger than those on the other side, and facial features are distorted.'
      ]
    ]
  ],
  [
    1,
    [
      'Hideous Appearance',
      () => [
        'This mutant’s appearance is deformed in some way that makes it universally repugnant to others of its species.'
      ]
    ]
  ],
  [
    1,
    [
      'Hooks',
      () => [
        'This mutant has hooks on its hands and/or feet, made of bone, cartilage, horn, or other material, that it can use as weapons, or to make climbing much easier.'
      ]
    ]
  ],
  [
    1,
    [
      'Hopper',
      () => [
        'This mutant has one large, powerful leg that it hops around on. Maybe two legs.'
      ]
    ]
  ],
  [
    2,
    [
      'Horns',
      () => [
        'This mutant has one or more horns growing out of its head.',
        `Type of horns: ${weighted([
          [1, 'mass of intertwined horns'],
          [2, 'one large horn'],
          [1, 'one small horn'],
          [1, 'three or more large horns'],
          [1, 'three or more small horns'],
          [3, 'two large, curved horns'],
          [1, 'two large, straight horns'],
          [2, 'two small horns']
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Huge Beard',
      () => ['This mutant has an enormous beard growing out of its face.']
    ]
  ],
  [
    2,
    [
      'Huge Head',
      () => [
        'This mutant’s head is larger than what is normal for its species.',
        `Head size: ${weighted([
          [2, 'two times normal'],
          [1, 'three times normal'],
          [1, 'inconsistently larger']
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Hump',
      () => [
        'This mutant has one or more humps on its back that store food and water, allowing the mutant to go without for extended periods of time.'
      ]
    ]
  ],
  [
    1,
    [
      'Hunchback',
      () => [
        'This mutant’s back is twisted and hunched, making it slightly more difficult to move.'
      ]
    ]
  ],
  [
    1,
    [
      'Internal Weapon',
      () => [
        'This mutant can produce a weapon from its own body, or transform part of its body into a weapon. The mutant can produce a single weapon, or one weapon per limb. These weapons can be hidden when not in use.',
        `Type of weapon: ${uniform([
          'club (blunt, jagged, or spiked)',
          'flail, lash, or whip',
          'knives or sword-like blades',
          'spear or piercing weapon',
          'explosive weapon',
          'ranged weapon'
        ])}.`
      ]
    ]
  ],
  [
    2,
    [
      'Large Ears',
      () => [
        `This mutant’s external ears are ${
          d10(1) + 1
        } times as large as normal for its species, or if its species does not have external ears, this mutant does. Because the ears direct sound, this can result in an increased ability to detect and identify specific noises.`
      ]
    ]
  ],
  [
    4,
    [
      'Large Size',
      () => [
        'This mutant is larger than normal for its species.',
        `Size increase: ${uniform([
          '50% larger',
          'double normal size',
          'triple normal size',
          'quadruple normal size'
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Leaves',
      () => [
        'This mutant is covered in leaves.',
        `Type of leaves: ${weighted([
          [1, 'barbed leaves'],
          [2, 'coniferous needles'],
          [2, 'deciduous leaves'],
          [1, 'fronds'],
          [2, 'grass'],
          [2, 'perpetually green leaves'],
          [1, 'razor-edged leaves'],
          [1, 'saw-edged leaves']
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Limb Loss',
      () => [
        'This mutant has fewer limbs than is normal for its species.',
        `Missing limb: ${uniform([
          'arm',
          'foot',
          'hand',
          'leg',
          'tail or other',
          'wings or other'
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Limb Transference',
      () => [
        'This mutant has limbs in places they normally are not.',
        ...takeWeighted(
          weighted([
            [1, () => d4(1) + 1],
            [4, 1],
            [1, 2]
          ]),
          [
            [2, 'Arm'],
            [1, 'Eyes'],
            [1, 'Head'],
            [2, 'Leg'],
            [1, 'Mouth'],
            [1, 'Other part']
          ]
        ).map(
          (part) =>
            `${part} transferred to ${weighted([
              [2, 'back'],
              [1, 'chest'],
              [1, 'elbow'],
              [1, 'foot'],
              [1, 'groin or hip'],
              [1, 'hand'],
              [1, 'head'],
              [1, 'knee'],
              [1, 'stomach']
            ])}.`
        )
      ]
    ]
  ],
  [
    1,
    [
      'Long Face',
      () => [
        'This mutant’s facial features are elongated to an extreme amount, giving it a long face.'
      ]
    ]
  ],
  [
    1,
    [
      'Long Legs',
      () => [
        'This mutant’s legs are freakisthly long, allowing it to move up to 50% faster.'
      ]
    ]
  ],
  [
    1,
    [
      'Long Neck',
      () => [
        `This mutant’s neck is at least ${
          d3(1) + 1
        } times as long as is normal for its species.`
      ]
    ]
  ],
  [
    1,
    [
      'Long Nose',
      () => [
        'This mutant’s nose sticks out much farther than is considered extreme for its species.'
      ]
    ]
  ],
  [
    1,
    [
      'Long Tongue',
      () => [
        `This mutant’s tongue is at least ${
          d4(1) + 1
        } times as long as is normal for its species.`
      ]
    ]
  ],
  [
    1,
    [
      'Loose Skin',
      () => [
        'This mutant’s loose, baggy skin hangs off in folds and rolls, almost appearing to be melting.'
      ]
    ]
  ],
  [
    1,
    [
      'Mane',
      () => [
        'This mutant’s head is framed by a mane of hair, flesh, or other organic material',
        `Type of mane: ${weighted([
          [1, 'mane of feathers'],
          [1, 'mane of fleshy tentacles'],
          [2, 'mane of hair like a horse'],
          [2, 'mane of hair like a lion'],
          [1, 'mane of barbs, quills, or spines'],
          [1, 'membranous mane like a lizard']
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Mostly Hairless',
      () => [
        'This mutant still has hair on the top of its head, but little or no hair on the rest of its body. It may or may not have eyebrows and/or a small patch of pubic hair, but cannot grow a beard.'
      ]
    ]
  ],
  [
    1,
    [
      'Multiple Arms',
      () => [
        'This mutant has more arms than is normal for its species.',
        `Number of extra arms: ${weighted([
          [1, () => plural(d6(1), 'extra arm')],
          [1, () => `${d6(2)} extra arms`],
          [2, 'four extra arms'],
          [2, 'one extra arm'],
          [1, 'three extra arms'],
          [3, 'two extra arms']
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Multiple Heads',
      () => [
        'This mutant has additional heads.',
        `Number of extra heads: ${weighted([
          [3, 'one extra head'],
          [1, 'three extra heads'],
          [2, 'two extra heads']
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Multiple Legs',
      () => [
        'This mutant has more legs than is normal for its species.',
        `Number of additional legs: ${weighted([
          [1, () => `${plural(d6(1), 'extra leg')}`],
          [1, () => `${d6(2)} extra legs`],
          [2, 'four extra legs'],
          [2, 'one extra leg'],
          [1, 'three extra legs'],
          [3, 'two extra legs']
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'No Ears',
      () => [
        'This mutant has no ears.',
        `Extent of earlessness: ${weighted([
          [2, 'no ears except vibration sensors'],
          [1, 'no ears whatsoever, internal or external'],
          [3, 'no external ears (just holes in head)']
        ])}.`
      ]
    ]
  ],
  [1, ['No Eyes', () => ['This mutant has no eyes in its face.']]],
  [
    1,
    [
      'No Nose',
      () => ['This mutant has slits for a nose insteat of any external organ.']
    ]
  ],
  [
    1,
    [
      'No Skin',
      () => [
        'This mutant has no skin protecting its fat, muscles, and internal organs from the environment. Either it has a hardy immune system or it uses some form of artificial skin.'
      ]
    ]
  ],
  [
    2,
    [
      'Oddly-Shaped Tongue',
      () => [
        'This mutant’s tongue is not the normal shape for its species.',
        `Type of tongue: ${uniform([
          'extremely thick',
          'extremely long',
          'extremely short',
          'forked or pincer-shaped',
          'hollow',
          'parasitic organism',
          'sharp and blade-like',
          'tentacle'
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'One Eye',
      () => [
        'This mutant has a single, cyclopean eye in the center of its face.'
      ]
    ]
  ],
  [
    1,
    [
      'Overgrown Body Part',
      () => [
        'One or more parts of this mutant’s body have grown to a larger size than normal.',
        `Overgrown body part: ${uniform([
          '1.5x',
          'double',
          'triple',
          'quadruple'
        ])} sized ${uniform([
          'arms',
          'feet',
          'hands',
          'head',
          'legs',
          'torso'
        ])}.`
      ]
    ]
  ],
  [
    4,
    [
      'Patterned Skin',
      () => [
        'This mutant’s skin (or fur) is patterned in multiple colors that are otherwise natural to the mutant’s species.',
        `Type of pattern: ${weighted([
          [2, 'banded'],
          [1, 'belted'],
          [2, 'camouflage pattern'],
          [3, 'different color face or head'],
          [3, 'different color hands and feet'],
          [4, 'small spots'],
          [1, 'patchy'],
          [3, 'striped'],
          [1, 'whorled']
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Physically Immature',
      () => [
        'This mutant looks like a child, and will always appear pre-pubescent no matter how old.'
      ]
    ]
  ],
  [
    2,
    [
      'Pigment Deficiency',
      () => [
        'This mutant has less pigmentation than others of its species. This can make the mutant acutely vulnerable to sunburn, and there is a 1 in 3 chance of severe eye conditions accompanying non-cosmetic albinism.',
        `Type of pigment deficiency: ${weighted([
          [
            1,
            'cosmetic albinism; pale skin, red eyes, and white-ish fur on hair'
          ],
          [
            1,
            'cyclical vitiligo; seasonal or stress-based partial loss of pigmentation'
          ],
          [1, 'non-segmental vitiligo; partial lack of pigmentation'],
          [1, 'ocular albinism; no pigment in the eyes and reduced eyesight'],
          [2, 'oculocutaneous albinism; no pigment and reduced eyesight']
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Pincers',
      () => [
        'This mutant has crab-like claws, with two digits each, for hands.'
      ]
    ]
  ],
  [
    1,
    [
      'Pinhead',
      () => [
        'This mutant has a shrunken, tiny head. This affects brain size and mental capacities.',
        `Impairment caused by pinhead: ${weighted([
          [2, 'minor impairment'],
          [3, 'serious impairment (mental capacity is halved)'],
          [
            1,
            'total impairment (mutant is barely able or completety unable to think)'
          ]
        ])}.`
      ]
    ]
  ],
  [
    2,
    ['Pointed Head', () => ['This mutant has a pointed or cone-shaped head.']]
  ],
  [
    1,
    [
      'Pouch',
      () => [
        'This mutant has a pouch of skin, like a marsupial, or a similar compartment in its body.'
      ]
    ]
  ],
  [
    1,
    [
      'Protective Eyelids',
      () => [
        'This mutant has additional, protective eyelids made of chitin, bone, or other material more durable than the mutant’s skin. They protect the mutant’s eyes from harm.'
      ]
    ]
  ],
  [
    1,
    [
      'Puny',
      () => [
        'This mutant is shrivelled and weak, its muscles wasted and withered and its body as small as one-half normal size. The mutant’s physical capabilities are reduced by at least one quarter.'
      ]
    ]
  ],
  [
    1,
    [
      'Quills, Spines, or Thorns',
      () => [
        `This mutant’s back (or other area) is covered in spiny quills that provide protection and can be used as weapons. There is a 1 in 3 chance the mutant will be able to project these quills, spines, or thorns ${d4(
          2
        )} times each week before they must be regrown.`
      ]
    ]
  ],
  [
    1,
    [
      'Radula',
      () => [
        'Instead of a tongue, this mutant has a toothed, chitinous ribbon like the radula of a mollusc, which can be used to scrape and cut food. It is still capable of speech, although its speech may sound quite different from that of other creatures.'
      ]
    ]
  ],
  [
    1,
    [
      'Re-Arranged Face',
      () => [
        'This mutant’s face is completely re-arranged. Even though it still has all the normal facial features, they are all in completely different locations than is normal.'
      ]
    ]
  ],
  [
    1,
    [
      'Redundant Vital Organs',
      () => [
        'This mutant has multiples of one or more vital organs, so that if one is damaged, the mutant is less aversely affected.',
        `Redundant organ: ${weighted([
          [1, 'all internal organs'],
          [2, 'heart'],
          [1, 'kidneys, liver, and pancreas'],
          [2, 'lungs'],
          [1, 'reproductive organs'],
          [1, 'stomach']
        ])}.`
      ]
    ]
  ],
  [
    3,
    [
      'Reverse Pedalism',
      () => [
        'If the mutant is normally bipedal, it becomes a quadruped. If the mutant is normally quadrupedal, it becomes a biped. If the mutant has a different form of locomotion, reverse the limbs normally used for movement and manipulation.'
      ]
    ]
  ],
  [
    1,
    [
      'Rigid Crest',
      () => [
        'This mutant has a crest on top of its head, made of rigid flesh, bone, cartilage, horn, scales, or other such materials.'
      ]
    ]
  ],
  [3, ['Scales', () => ['This mutant’s skin is covered with scales.']]],
  [
    1,
    [
      'Scars',
      () => [
        'This mutant bears prominent scars as a result of an over-production of scar tissue, a skin disease, or treatment of some other condition.'
      ]
    ]
  ],
  [
    1,
    [
      'Sexually Attractive',
      () => [
        'This mutant is considered attractive in ways that make it sexually desirable to other members of its species, regardless of whether it conforms to standards of aesthetic beauty or not. This mutant needs not be photogenic to be sexually attractive, either.'
      ]
    ]
  ],
  [
    1,
    [
      'Short Legs',
      () => [
        'This mutant’s legs are abnormally short, cutting its normal movement rate and jumping distance by at least half.'
      ]
    ]
  ],
  [
    1,
    [
      'Siamese Twin',
      () => [
        'This mutant has two bodies joined together at birth, which may or may not be two different people. Other mutations and powers may belong to both or either twin.',
        `Type of twin: ${weighted([
          [1, 'back-to-back conjoined twin'],
          [1, 'head-to-head conjoined twin'],
          [1, 'parasitic twin'],
          [3, 'side-to-side conjoined twin']
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Skull Face',
      () => [
        'This mutant’s face resembles a skull, with either no skin on it, or only a small amount of skin that does not prevent the skull from showing through.'
      ]
    ]
  ],
  [
    4,
    [
      'Small Size',
      () => [
        'This mutant is a miniature version of its normal race.',
        `New size: ${uniform([
          'a quarter smaller than normal',
          'half the normal size',
          'one third the normal size',
          'one quarter normal size'
        ])}.`
      ]
    ]
  ],
  [1, ['Snout', () => ['This mutant has an elongated, snout-like face.']]],
  [
    1,
    [
      'Soft Crest',
      () => [
        'This mutant has a crest on top of its head, made of flesh, flexible cartilage, feathers, loose scales, or other non-rigid materials.'
      ]
    ]
  ],
  [
    1,
    [
      'Steatopygia',
      () => [
        'This mutant’s ass, hind quarters, or abdominal segment is extraordinarily large.'
      ]
    ]
  ],
  [
    1,
    [
      'Stomach Orifice',
      () => [
        'This mutant has an extra orifice near its stomach that can consume and regurgitate food.'
      ]
    ]
  ],
  [1, ['Strange Ears', () => ['This mutant has odd-looking ears.']]],
  [
    1,
    [
      'Strange Interior Organs',
      () => [
        'The inside of this mutant’s body is completely different from that of others of its species, and its internal organs make no sense according to its exterior appearance.'
      ]
    ]
  ],
  [
    1,
    [
      'Strange Texture',
      () => [
        'This mutant’s skin feels like some other substance.',
        `Type of skin texture: ${uniform([
          'bark or plant material',
          'brick, dirt, or stone',
          'crystal',
          'foam',
          'geometric patterns',
          'glass',
          'hard or soft plastic',
          'metal',
          'rough fabric',
          'sand or sandpaper',
          'smooth and slippery',
          'soft fabric'
        ])}.`
      ]
    ]
  ],
  [
    4,
    [
      'Strangely-Colored Eyes',
      () => [
        'This mutant’s eyes are an odd color, and may not even have a visible iris or pupil. Roll on either the basic or extended color table to determine dominant color.'
      ]
    ]
  ],
  [
    4,
    [
      'Strangely-Colored Hair',
      () => [`This mutant’s hair is an odd color: ${color()}.`]
    ]
  ],
  [
    5,
    [
      'Strangely-Colored Skin',
      () => [
        'This mutant has skin that is patterned in one or more strange colors.',
        `Skin pattern: ${weighted([
          [3, () => `disruptive camouflage pattern; ${color()}`],
          [1, () => `front and back different colors; ${colors(2)}`],
          [1, () => `left and right different colors; ${colors(2)}`],
          [1, () => `limbs colored differently from body; ${color()}`],
          [1, () => `multi-colored lozenges; ${colors(d6(1) + 1)}`],
          [
            1,
            () =>
              `multi-colored dots, freckles, or splotches; ${colors(d6(1) + 1)}`
          ],
          [4, () => `single color; ${color()}`],
          [2, () => `single color spots or freckles; ${color()}`],
          [1, () => `single- or multi-colored squares; ${colors(d3())}`],
          [2, () => `tiger- or zebra-like stripes; ${color()}`],
          [1, 'transparent skin'],
          [1, 'unreflecting skin (looks black)'],
          [1, () => `zigzag stripes; ${colors(d3())}`]
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Stubby Digits',
      () => ['This mutnat has short and stubby fingers and toes.']
    ]
  ],
  [
    1,
    [
      'Sympathetic Biomorphism',
      () => [
        'The physical form of this mutant changes to comply with the individuals it associates with, even if that shape has more limbs and sense organs. After a certain time, depending on speed, its will resemble those around it in shape, though its cosmetic appearance will not change.',
        `Speed of biomorphic changes: ${uniform([
          'several days',
          'several hours',
          'several months',
          'several weeks'
        ])}.`
      ]
    ]
  ],
  [
    5,
    [
      'Tail',
      () => [
        'This mutant has a tail.',
        `Type of tail: ${weighted([
          [2, 'long tail'],
          [1, 'mace tail'],
          [1, 'prehensile or tentacle tail'],
          [1, 'scorpion tail (with venom)'],
          [2, 'short tail'],
          [1, 'snake or other legless creature']
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Taller',
      () => [
        `This mutant is ${
          d10(1) * 10
        }% taller than the normal maximum height for its species. The mutant’s other dimensions are not proportionally filled out.`
      ]
    ]
  ],
  [
    2,
    [
      'Tentacles',
      () => [
        'This mutant has tentacles.',
        `Location of tentacles: ${uniform([
          'tentacles instead of arms',
          'tentacles instead of hair',
          'tentacles instead of legs',
          'tentacles on arms and legs',
          'tentacles on back of body',
          'tentacles on front of body',
          'tentacles on head or face',
          `tentacles on two locations, ${listWords(
            takeUniform(2, [
              'instead of arms',
              'instead of hair',
              'instead of legs',
              'on arms and legs',
              'on back of body',
              'on front of body',
              'on head or face'
            ])
          )}`
        ])}.`,
        `Size and precision of tentacles: ${uniform([
          'large and clumsy',
          'large and dexterous',
          'small and clumsy',
          'small and dexterous'
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Thick Skin',
      () => [
        'This mutant’s skin is particularly thick and durable.',
        `What makes its skin so thick: ${uniform([
          'additional layer of durable material',
          'blubber',
          'hardened outer layer',
          'thick dermal layer'
        ])}.`
      ]
    ]
  ],
  [1, ['Tongueless', () => ['This mutant has no tongue.']]],
  [
    1,
    [
      'Toothed Skin',
      () => [
        'This mutant’s skin is covered in small scales that resemble sharp teeth, which can be used to cut and saw, and make it uncomfortable—even dangerous—for others to touch the mutant.'
      ]
    ]
  ],
  [
    3,
    [
      'Transparent Eyelids',
      () => [
        'This mutant’s eyes have an additional pair of transparent eyelids, such as nictating membranes, that protect the eyes from damage without unduly impairing vision. These eyelids can be tinted, to aid vision in bright light, or designed to filter out certain colors.'
      ]
    ]
  ],
  [
    1,
    [
      'Trunk',
      () => [
        'This mutant has a prehensile nose, like the trunk of an elephant.'
      ]
    ]
  ],
  [
    2,
    [
      'Tumors',
      () => ['This mutant suffers from large tumors that grow on its body.']
    ]
  ],
  [
    1,
    [
      'Tusks',
      () => ['This mutant has large teeth protruding from its lower jaw.']
    ]
  ],
  [
    1,
    [
      'Twisted Frame',
      () => [
        'This mutant’s body shape is twisted and bent, making normal movements and posture difficult or even impossible. This reduces agility, co-ordination, and reflexes by half.'
      ]
    ]
  ],
  [
    1,
    [
      'Vestigial Wings',
      () => [
        'A small, useless pair of wings sprouts from this mutant’s back or shoulders.'
      ]
    ]
  ],
  [
    1,
    [
      'Walking Head',
      () => [
        'This mutant has no appreciable torso between its head and limbs and so appears to be a walking head.'
      ]
    ]
  ],
  [
    1,
    ['Warty Skin', () => ['This mutant’s skin is covered in warty growths.']]
  ],
  [
    1,
    [
      'Weapon Hands',
      () => [
        'This mutant has hands formed in the shape of weapons.',
        `Type of weapon hands: ${weighted([
          [2, 'blades'],
          [1, 'blunt weapon'],
          [1, 'hook'],
          [2, 'pincers'],
          [1, 'scissors'],
          [1, 'stingers']
        ])}.`
      ]
    ]
  ],
  [
    3,
    [
      'Webbed Digits',
      () => [
        'This mutant’s hands and feet are webbed, allowing it to swim faster.'
      ]
    ]
  ],
  [
    2,
    [
      'Whiskers',
      () => [
        'This mutant has long whiskers on its face that it uses to aid perception.'
      ]
    ]
  ],
  [
    1,
    [
      'Wide',
      () => [
        `This mutant is ${
          d10(1) * 10
        }% wider than others of its species, but is normal-sized in all other respects.`
      ]
    ]
  ],
  [
    5,
    [
      'Wings',
      () => [
        'If this mutant is already armless, it has wings in addition to any other limbs.',
        `If the mutant has arms, they have: ${weighted([
          [1, 'flexible wings instead of arms that can function like hands'],
          [2, 'normal wings instead of arms'],
          [1, 'wings in addition to arms']
        ])}.`,
        `Types of wings: ${uniform([
          'alien wings',
          'avian wings',
          'bat wings',
          'dragonfly wings',
          'glider wings',
          'gossamer wings',
          'insect wings',
          'other kind of wings'
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Wrinkled Skin',
      () => [
        'This mutant’s skin is covered in thick, pruny wrinkles, lines, and creases.'
      ]
    ]
  ]
]
