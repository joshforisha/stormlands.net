import extendedColors from './extended-colors.js'
import weightedBodyParts from './weighted-body-parts.js'
import weightedCreatures from './weighted-creatures.js'
import { indefinite, listWords, plural } from '../text.js'
import { d4, d6, d10, takeUniform, uniform, weighted } from '../random.js'

const bodyPart = () => weighted(weightedBodyParts)
const color = () => uniform(extendedColors)
const creature = () => weighted(weightedCreatures)
const feet = (ft) => `${ft} feet (${Math.round(0.3048 * ft)}m)`

export default [
  [
    2,
    [
      'Adhesive Touch',
      () => [
        'This mutant has microscopic suckers and hooks on its hands and feet, and perhaps other areas of its body, which allow it to stick to walls and walk or crawl on ceilings.'
      ]
    ]
  ],
  [
    2,
    [
      'Allergies',
      () => [
        'This mutant is extremely allergic to some common substance or group of substances.',
        `Allergic to: ${weighted([
          [1, 'a specific food type'],
          [2, 'animal fur'],
          [1, 'dust'],
          [1, 'feathers'],
          [1, 'fruits'],
          [1, 'insects'],
          [1, 'iron or silver'],
          [1, 'latex'],
          [1, 'leather'],
          [1, 'most drugs'],
          [1, 'natural fabrics'],
          [1, 'nuts'],
          [1, 'paint and/or soap'],
          [1, 'perfume'],
          [1, 'petroleum products'],
          [1, 'plastics'],
          [2, 'pollen'],
          [1, 'synthetic fabrics']
        ])}.`
      ]
    ]
  ],
  [
    2,
    [
      'Ambidextrous',
      () => [
        'This mutant can use both sides of its body with the same ability as a right-handed person using their right hand (or leg).'
      ]
    ]
  ],
  [
    1,
    [
      'Anaerobic',
      () => [
        'This mutant does not require oxygen and does not breathe air or water.'
      ]
    ]
  ],
  [
    1,
    [
      'Aquatic Adaptation',
      () => [
        'This mutant’s body is adapted to living underwater. It may have gills or a blowhole, flippers, a tail, or any other feature that would help it function aquatically.'
      ]
    ]
  ],
  [
    1,
    [
      'Bad Breath',
      () => [
        'This mutant’s mouth exudes a horrifying stench, which it cannot mask or alleviate in any way.'
      ]
    ]
  ],
  [
    3,
    [
      'Bioluminescent',
      () => [
        `Part of this mutant’s body generates a cold, softly glowing light. Color: ${color()}. There is a 1 in 3 chance the mutant can change the color of its bioluminescence.`,
        `Bioluminescence trigger: ${weighted([
          [1, 'arousal/excitement'],
          [2, 'at will'],
          [2, 'glows in the dark'],
          [1, 'stress-induced']
        ])}.`
      ]
    ]
  ],
  [1, ['Blind', () => ['If this mutant has eyes, they do not work.']]],
  [
    1,
    [
      'Blood Substitution',
      () => [
        'Instead of blood, this mutant has some other substance in its veins.',
        `Type of substitution: ${uniform([
          'amoeba or protoplasm',
          'hemolymph fluid',
          'respiratory proteins',
          'acid',
          'ichor',
          'poisonous venom',
          'black bile',
          'electricity',
          'fire',
          'gasoline',
          'lava or molten metal',
          'leeches or maggots',
          'mercury',
          'noxious gas',
          'powder or sand',
          'spirits',
          'urine',
          'vomit',
          'water',
          'wine'
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Buoyancy',
      () => [
        'Because it has an air bladder it can inflate, or because of its cell structure, this mutant floats in water, even fresh water.'
      ]
    ]
  ],
  [
    2,
    [
      'Chameleoline Skin',
      () => [
        'This mutant’s skin can change color to mimic its surroundings, providing camouflage when it is not moving quickly.'
      ]
    ]
  ],
  [
    1,
    [
      'Chronic Pain',
      () => [
        'This mutant has pains in its body that never go away. It must take large quantities of drugs in order to get any relief, though some days the pain is worse than others.'
      ]
    ]
  ],
  [
    1,
    [
      'Cocoon',
      () => [
        'This mutant may create a cocoon out of secretions and whatever else it needs, and is then able to hibernate inside it.'
      ]
    ]
  ],
  [
    2,
    [
      'Cold Blooded',
      () => [
        'This mutant either suffers from poikilothermia, or it is naturally cold-blooded, like a reptile. It will be adversely affected by temperatures lower than 30°C (86°F).'
      ]
    ]
  ],
  [
    2,
    [
      'Color Blind',
      () => [
        'This mutant cannot distinguish between certain colors.',
        `Type of color blindness: ${weighted([
          [2, 'dichromacy (green and red look the same)'],
          [1, 'monochromacy (mutant sees in black and white)'],
          [
            1,
            'tritanopia (blue and green look the same; violet and yellow look the same)'
          ]
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Cracking Joints',
      () => [
        'This mutant’s joints are constantly cracking as it moves, making it hard to move silently. This is not painful to the mutant in any way, and does not wear down the joints.'
      ]
    ]
  ],
  [1, ['Deaf', () => ['This mutant is unable to hear.']]],
  [
    1,
    [
      'Decreased Agility',
      () => [
        'This mutant’s physical co-ordination, flexibility, and sense of balance are far lower than the average specimen of its type.',
        `Amount of decrease: ${uniform([
          'decreased to one quarter',
          'decreased by one half',
          'decreased by one third',
          'decreased by one quarter'
        ])}.`
      ]
    ]
  ],
  [
    2,
    [
      'Decreased Metabolism',
      () => [
        'This mutant’s reactions are slightly slower than normal. It may also suffer from a weakened immune system, an increased susceptibility to cold, brittle hair and nails, constipation, dry skin, irregular menstruation, poor memory, and unexplained weight gain.'
      ]
    ]
  ],
  [
    1,
    [
      'Decreased Physical Stamina',
      () => [
        'This mutant has lower than average stamina, endurance, and toughness.',
        `Amount of decrease: ${uniform([
          'decreased to one quarter',
          'decreased by one half',
          'decreased by one third',
          'decreased by one quarter'
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Decreased Physical Strength',
      () => [
        'This mutant’s muscles are weaker than what is normal for its species.',
        `Amount of decrease: ${uniform([
          'decreased to one quarter',
          'decreased by one half',
          'decreased by one third',
          'decreased by one quarter'
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Defensive Barbs',
      () => [
        'When threatened, this mutant can raise dangerous barbs, horns, spikes, or spines upon its skin. These function as weapons and protection, and are concealed when not in use.'
      ]
    ]
  ],
  [
    1,
    [
      'Diabetes',
      () => [
        'This mutant has a problem with high blood sugar levels, probably due to an inability to produce insulin or a resistance to it. Diabetes can cause blindness, heart disease, obesity, and other health problems, including diabetic ketoacidosis, which can be fatal if untreated.'
      ]
    ]
  ],
  [
    1,
    [
      'Disease Carrier',
      () => [
        'This mutant is the carrier of an infectious disease.',
        `Effect on mutant: ${uniform([
          'the mutant suffers minor symptoms',
          'the mutant is completely unaffected',
          'thu mutant suffers the full effects short of death'
        ])}.`,
        `Symptoms: ${uniform([
          'bone rot',
          'flesh rot',
          'inflammation',
          'neurological damage',
          'pulmonary infection',
          'sores and wounds',
          'tumors',
          'weakening'
        ])}.`,
        `Vector of spreading: ${weighted([
          [1, 'airborn'],
          [3, 'fluids/sexual contact'],
          [2, 'ingestion'],
          [2, 'touch']
        ])}.`
      ]
    ]
  ],
  [
    2,
    [
      'Double Jointed',
      () => [
        'This mutant is able to twist its limbs into surprising positions owing to a superior flexibility.'
      ]
    ]
  ],
  [
    2,
    [
      'Early Maturation',
      () => [
        'This mutant matures quickly, finishing puberty and reaching physical adulthood at half the age as is normal for its species.'
      ]
    ]
  ],
  [
    1,
    [
      'Efficient Digestion',
      () => [
        'This mutant digests biomass more efficiently than normal, allowing it to eat more types of organic material than is normal for its species, and cutting down on the amount of waste products it produces.'
      ]
    ]
  ],
  [
    2,
    [
      'Electrical Generation',
      () => [
        'This mutant is able to produce an electrical charge that shocks anyone touching it. This charge may be transmitted through water and other conductive materials.'
      ]
    ]
  ],
  [
    2,
    [
      'Electromagnetic Disruption',
      () => [
        'This mutant disrupts any electronic devices in its presence, unless those devices are specially shielded against such interference.',
        `Range of disruption: ${weighted([
          [2, () => feet(d10(1) * 10)],
          [1, 'a hundred yards (91m)'],
          [1, 'sight'],
          [2, 'touch']
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Enhanced Musculature',
      () => [
        'This mutant’s strength and reflexes are increased by half. Corresponding muscle functions, like grip and leaping distance, are likewise increased.'
      ]
    ]
  ],
  [
    2,
    [
      'Enlarging Mouth',
      () => [
        'This mutant’s mouth and throat can stretch, allowing it to swallow things its own size.'
      ]
    ]
  ],
  [
    1,
    [
      'Excretion',
      () => [
        'This mutant excretes a substance on its skin.',
        `Type of substance excreted: ${weighted([
          [1, 'acidic'],
          [1, 'lubricant'],
          [2, 'hallucigenic'],
          [1, 'numbing agent'],
          [1, 'other effect'],
          [1, 'pain-killer'],
          [1, 'paralytic'],
          [1, 'sticky'],
          [1, 'toxic']
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Explosive Death',
      () => [
        'When this mutant dies, it explodes, as do sizable parts of its body when they are removed.'
      ]
    ]
  ],
  [
    1,
    [
      'Extensible Limbs',
      () => [
        `Some of this mutant’s limbs can stretch and elongate, to ${
          d4(1) + 1
        } times their normal length.`,
        `Limbs able to extend: ${weighted([
          [1, 'arms'],
          [2, 'arms and legs'],
          [1, 'legs'],
          [2, 'neck'],
          [1, 'all limbs'],
          [1, 'wings']
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Fast Hair Growth',
      () => [
        `This mutant’s hair and nails grow ${d6(
          2
        )} times faster than is normal for its species. By extension, this applies to fur, feathers, horns, and other similar body parts.`
      ]
    ]
  ],
  [
    1,
    [
      'Fast Movement',
      () => [
        'This mutant moves much faster than others of its species.',
        `Amount of speed increase: ${uniform([
          'doubled',
          'increased by one half',
          'increased by one third',
          'increased by one quarter'
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Finesse',
      () => [
        'This mutant’s manual dexterity is much better than others of its species, and its fingers are deft and light.',
        `Amount of finesse increase: ${uniform([
          'doubled',
          'increased by one half',
          'increased by one third',
          'increased by one quarter'
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Fragrant',
      () => [
        'This mutant exudes a noticeably pleasant odour that most others of its species find attractive and/or relaxing.',
        `Fragrance: ${weighted([
          [1, 'bark or leaves'],
          [1, 'candy'],
          [1, 'cleanliness'],
          [2, 'flowers'],
          [1, 'food'],
          [2, 'fruit or seeds'],
          [1, 'honey'],
          [2, 'perfume'],
          [1, 'vanilla']
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Frog Tongue',
      () => [
        'This mutant has a very sticky tongue, which it can extend out of its mouth about twice its own length, and wrap around objects or drag them into its mouth.'
      ]
    ]
  ],
  [
    1,
    [
      'Fruit',
      () => [
        'This mutant produces a biological product in a form that can be safely harvested, such as fruit or resin.',
        `Type of fruit: ${uniform([
          'anti-radiation medication',
          'explosive',
          'immune system booster',
          'mental booster',
          'poison antidote',
          'radioactive',
          'sedative or tranquilizer',
          'tasty food',
          'toxic poison',
          'two properties or other effects'
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'G-Tolerance',
      () => [
        'This mutant has an easy time adapting to changes in gravity. It functions well in zero-G environments, and withstands high-gravity pressures much better than others of its species.'
      ]
    ]
  ],
  [
    1,
    [
      'Gas Bag',
      () => [
        'This mutant has a gas bag attached to the back or top of its body, which can be inflated enough to allow it to float through the air. It takes several moments to inflate, and will be of little use mitigating the effects of short falls, but can be inflated in time to prevent death from falls greater than a hundred feet (30m). The gas bag does not provide any way to move through the air, and the mutant may be at the mercy of wind currents.'
      ]
    ]
  ],
  [
    1,
    [
      'Gas Emission',
      () => [
        'This mutant can emit a cloud of gas that envelops it and affects anyone in its presence.',
        `Type of gas: ${uniform([
          'black and oily',
          'hallucinogenic',
          'irritant',
          'narcotic',
          'paralytic',
          'pleasant perfume',
          'stinking',
          'toxic poison'
        ])}.`
      ]
    ]
  ],
  [
    2,
    [
      'Gills and Lungs',
      () => [
        'This mutant has both gills and lungs, so it can breathe in water and on land.'
      ]
    ]
  ],
  [
    1,
    [
      'Hangover Resistance',
      () => [
        'This mutant never has hangovers, and quickly recovers once its liver has neutralized toxins.'
      ]
    ]
  ],
  [
    2,
    [
      'Heightened Hearing',
      () => [
        'This mutant’s hearing is extremely well-developed, allowing it to filter, pinpoint, and identify specific sounds with ease, and notice much quieter sounds than others of its species.'
      ]
    ]
  ],
  [
    2,
    [
      'Heightened Sense of Smell and Taste',
      () => [
        'This mutant can identify odours up to a hundred feet (30m) upwind and a hundred yards (91m) downwind, follow trails, identify individuals by smell, and sense contamination and poison in food when tasted. The mutant is also more sensitive to pheromones and toxic gases, but not to what would be considered “bad” smells.'
      ]
    ]
  ],
  [
    2,
    [
      'Heightened Sense of Touch',
      () => [
        'This mutant’s somatosensory system is so acute it can pinpoint the sources of faint vibrations, sense minute changes in temperature, and obtain more information about objects by touch than other members of its species.'
      ]
    ]
  ],
  [
    2,
    [
      'Hemophilia',
      () => [
        'This mutant’s blood lacks coagulating agents, and so any cuts will continue to bleed until the mutant is dead or the wounds are artificially sealed.'
      ]
    ]
  ],
  [
    1,
    [
      'Hermaphromorph',
      () => [
        'This mutant is able to change sex at will. This process does not necessarily affect the mutant’s gender identity.',
        `Time changes take: ${weighted([
          [2, 'a day'],
          [1, 'half a day'],
          [1, 'hours'],
          [2, 'several days'],
          [3, 'minutes'],
          [1, 'seconds']
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'High Body Temperature',
      () => [
        'This mutant’s body temperature is much higher than normal, and it can suffer from heat exhaustion in very warm environments.'
      ]
    ]
  ],
  [
    2,
    [
      'High Pain Threshold',
      () => [
        'This mutant is never stunned or incapacitated by pain. It can still feel pain, and is inconvenienced by it, but its mental and physical functions remain unimpeded.'
      ]
    ]
  ],
  [
    1,
    [
      'Hollow Bones',
      () => ['This mutant’s bones are hollow, being both lighter and weaker.']
    ]
  ],
  [
    1,
    [
      'Horrible Stench',
      () => [
        'This mutant smells incredibly bad according to almost all other living creatures. There is little or nothing the mutant can do to alleviate this smell, which resembles rotting meat, except perhaps to try drowning it out with other scents.'
      ]
    ]
  ],
  [
    1,
    [
      'Hyperadrenal Gland',
      () => [
        'When this mutant is subjected to stress, the adrenaline produced by its body is more effective than normal, and the boost to its physical effectiveness is very noticeable. After the adrenaline high wears off, the mutant experiences a period of weakness and shaking.'
      ]
    ]
  ],
  [
    1,
    [
      'Hyperopia',
      () => [
        'This mutant cannot see objects clearly when they are close enough to be within reach.'
      ]
    ]
  ],
  [
    1,
    [
      'Immune to Disease',
      () => [
        'This mutant is not susceptible to disease, either from bacteria, viruses, or latent genetic flaws.'
      ]
    ]
  ],
  [
    1,
    [
      'Immune to Poison',
      () => [
        'This mutant is immune to toxins and poisons, and any life-threatening drug.'
      ]
    ]
  ],
  [
    1,
    [
      'Increased Agility',
      () => [
        'This mutant’s physical co-ordination, flexibility, and sense of balance are far greater than the average specimen of its type.',
        `Amount of increase: ${uniform([
          'doubled',
          'increased by one half',
          'increased by one third',
          'increased by one quarter'
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Increased Appendix Size',
      () => [
        'This mutant’s appendix is able to neutralize normally-deadly poisons, but can rupture if too much pressure is applied to the abdomen.'
      ]
    ]
  ],
  [
    1,
    [
      'Increased Heart and Lung Size',
      () => [
        'This mutant’s stamina is doubled due to its powerful heart and lungs. The mutant can hold its breathe for at least ten minutes at a time.'
      ]
    ]
  ],
  [
    2,
    [
      'Increased Metabolism',
      () => [
        'This mutant requires more nourishment than others of its species, but this increase in appetite will never cause it to gain any significant amount of weight.'
      ]
    ]
  ],
  [
    1,
    [
      'Increased Physical Stamina',
      () => [
        'This mutant has higher than average stamina, endurance, and toughness.',
        `Amount of increase: ${uniform([
          'doubled',
          'increased by one half',
          'increased by one third',
          'increased by one quarter'
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Increased Physical Strength',
      () => [
        'This mutant’s muscles are much stronger and denser than what is normal for its species.',
        `Amount of increase: ${uniform([
          'doubled',
          'increased by one half',
          'increased by one third',
          'increased by one quarter'
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Increased Susceptibility to Illness',
      () => [
        'This mutant is exceedingly vulnerable to diseases, viruses, and bacterial infections.'
      ]
    ]
  ],
  [
    1,
    [
      'Independently Focusable Eyes',
      () => [
        'Each of this mutant’s eyes can focus independently without it being unduly distracted.'
      ]
    ]
  ],
  [
    1,
    [
      'Infected',
      () => [
        'This mutant is infected with a bacteria, fungus, or parasite of some kind.',
        `Symptoms: ${weighted([
          [1, 'full symptoms'],
          [1, 'mild symptoms'],
          [2, 'no detrimental symptoms'],
          [2, 'only visible symptoms']
        ])}.`,
        `Transmission vector: ${uniform([
          'airborn',
          'fluids/sexual contact',
          'ingestion',
          'not infectious',
          'prolonged exposure',
          'touch'
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Infertile',
      () => [
        'This mutant is either sterile, or has an incredibly low fertility rate.'
      ]
    ]
  ],
  [
    1,
    [
      'Inflation',
      () => [
        'This mutant is able to increase the appearance of its size, either by an expandable hood, frill, or ruff, or by puffing itself up, inflating loose, stretchy skin with air, gas, or liquid.'
      ]
    ]
  ],
  [
    1,
    [
      'Infravision',
      () => [
        'This mutant can see radiated heat, allowing it to differentiate between temperatures on sight, and function relatively normally in the dark.'
      ]
    ]
  ],
  [
    1,
    [
      'Ink',
      () => [
        'This mutant can produce a cloud or jet of blackish ink from its mouth or other orifice.'
      ]
    ]
  ],
  [
    1,
    [
      'Intersex',
      () => [
        'This mutant displays a mix of features considered typical of either male and female sexes, but not usually both, or is otherwise near the middle of the dimorphic spectrum.',
        `Type of intersexuality: ${weighted([
          [2, 'ambiguous genitalia'],
          [
            1,
            'internal and external sex characteristics of (what are usually) different sexes (or even the sex of a different species)'
          ],
          [1, 'no secondary sex characteristics'],
          [1, 'secondary sex characteristics but no reproductive organs'],
          [
            1,
            'true hermaphrodite, has fully functioning male and female sexual organs. If the mutant is normally asexual, it has the sex organs of another species.'
          ]
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Kidney Stones',
      () => [
        'This mutant is prone to passing stones that form in the kidneys and cause excruciating pain.'
      ]
    ]
  ],
  [
    1,
    [
      'Light Dependency',
      () => [
        'This mutant requires an external light source to live and will begin to die if left in total darkness. No amount of light will blind or otherwise hinder this mutant.'
      ]
    ]
  ],
  [
    1,
    [
      'Light Generation',
      () => [
        `This mutant can generate ${color()} light at will.`,
        `Type of light generates: ${weighted([
          [1, 'blinding flashes'],
          [1, 'directed beams'],
          [2, 'phosphorescence']
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Light Sensitivity',
      () => [
        'This mutant’s eyes are highly sensitive to light. It is blinded by normal daylight, but can see normally in dim light.'
      ]
    ]
  ],
  [
    1,
    [
      'Long Life',
      () => [
        'This mutant, unless killed, will live about twice as long as others of its species, and will not suffer the effects of aging until very late in its life.'
      ]
    ]
  ],
  [
    2,
    [
      'Low Pain Threshold',
      () => ['This mutant is extremely sensitive to pain.']
    ]
  ],
  [
    1,
    [
      'Low Sex Drive',
      () => [
        'This mutant has an unusually low libido, even if it is fertile (though it may not be). It has little or no desire to mate with members of the opposite sex or engage in recreational sexual activity of any sort, and will be bored when attempting to do so.'
      ]
    ]
  ],
  [
    1,
    [
      'Malleable Body',
      () => [
        'This mutant can bend, twist, and collapse its body, squeezing through small spaces and being comfortable in awkward positions. Any hard parts of the mutant’s anatomy, like skeleton or shell, are extremely flexible, but not fragile.'
      ]
    ]
  ],
  [
    1,
    [
      'Malleable Features',
      () => [
        'This mutant can reshape its own appearance by the application of pressure, using fingers and other implements to give itself a new face, move fat and muscles around, and reshape skin details. The mutant chooses when to be malleable, but must shape itself by hand.'
      ]
    ]
  ],
  [
    1,
    [
      'Microscopic Vision',
      () => ['This mutant can see tiny objects clearly, if they are close.']
    ]
  ],
  [
    1,
    [
      'Mute',
      () => [
        'This mutant has no vocal cords or speech-producing organs, and cannot talk.'
      ]
    ]
  ],
  [
    1,
    [
      'Myopia',
      () => [`This mutant cannot see clearly past ${feet(d10(1) + 10)}.`]
    ]
  ],
  [
    1,
    [
      'Nerve Cut-Off',
      () => [
        'This mutant can deaden its own nerves at will, so that it feels no pain or other sensations.'
      ]
    ]
  ],
  [
    1,
    [
      'No Pain Receptors',
      () => [
        'This mutant is unable to feel pain, though it retains a normal sense of touch otherwise.'
      ]
    ]
  ],
  [
    1,
    [
      'No Sense of Smell or Taste',
      () => [
        'This mutant has no olfactory senses, and cannot smell or taste anything. The mutant will still be affected by piquant food, poisons, and irritants, however.'
      ]
    ]
  ],
  [
    1,
    [
      'No Sense of Touch',
      () => [
        'This mutant is almost totally unable to sense by touch, but still feels pain.'
      ]
    ]
  ],
  [
    1,
    [
      'Oversexed',
      () => [
        'This mutant has an unusually high libido and desires sexual activity at all times. It may pass up activities necessary for survival in order to obtain sexual satisfaction. This increased sex drive is entirely biological in nature, not psychological.'
      ]
    ]
  ],
  [1, ['Paraplegic', () => ['This mutant is paralysed below the waist.']]],
  [
    2,
    [
      'Parasite Infestation',
      () => [
        'This mutant’s body is infested with one or more parasites.',
        `Type of infestation: ${uniform([
          'cloud of flies',
          'insect hive',
          'nest of crawling bugs',
          'nest of snakes',
          'tongue replaced by parasite',
          'worms',
          'alien creature',
          'demon or supernatural being',
          'ghost or other undead',
          'intelligent alien colonists'
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Parthenogenesis',
      () => [
        'This mutant may reproduce on its own, without requiring another member of its own species. The offspring will be a clone, the same as an identical twin. This mutation probably requires a womb or similar reproductive organs.'
      ]
    ]
  ],
  [
    1,
    [
      'Performance Enhancement',
      () => [
        'This mutant can enhance its own mental and physical performance for a short amount of time by concentrating and expending great amounts of energy. Speed, strength, and each of its senses can be enhanced, usually in order to reach a certain short-term objective, but this is extremely draining, and will leave the mutant tired and hungry.'
      ]
    ]
  ],
  [
    1,
    [
      'Permeable Skin',
      () => [
        'This mutant may absorb water and oxygen through its skin like an amphibian. This allows the mutant to breathe and drink underwater.'
      ]
    ]
  ],
  [
    2,
    [
      'Pheromones',
      () => [
        'This mutant exudes pheromones.',
        `Pheromone function: ${weighted([
          [1, 'attracts prey'],
          [1, 'causes disorientation'],
          [1, 'inspires aggression'],
          [1, 'inspires fear'],
          [2, 'inspires lust'],
          [2, 'marks territory or trails']
        ])}.`,
        `Pheromone trigger: ${uniform([
          'according to a biological cycle',
          'always on',
          'at will',
          'stress-induced'
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Photosynthetic',
      () => [
        'This mutant extracts nutrients from air, soil, and water by absorbing energy from sunlight.'
      ]
    ]
  ],
  [
    1,
    [
      'Poison Glands',
      () => [
        'This mutant has glands that excrete poison.',
        `Poison location: ${weighted([
          [2, 'bite'],
          [1, 'claws'],
          [1, 'injector'],
          [1, 'secretion'],
          [1, 'spit or spray'],
          [2, 'kiss']
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Poison Susceptibility',
      () => [
        'This mutant is exceedingly vulnerable to poisons and toxins, and levels that will only make others of the same species sick will kill this mutant.'
      ]
    ]
  ],
  [
    1,
    [
      'Poor Dexterity',
      () => [
        'This mutant’s manual dexterity is much worse than others of its species, and its fingers do not display finesse.',
        `Amount of decrease: ${uniform([
          'decreased to one quarter',
          'decreased by one half',
          'decreased by one third',
          'decreased by one quarter'
        ])}.`
      ]
    ]
  ],
  [
    2,
    [
      'Poor Respiration',
      () => [
        'This mutant’s respiratory system is weaker than normal. It must rest after even short periods of strenuous activity or pass out, and cannot hold its breathe for more than several seconds.'
      ]
    ]
  ],
  [
    1,
    [
      'Pouched Cheeks',
      () => [
        'This mutant’s cheeks are elastic and its mouth may be used to carry a great deal of food or other objects.'
      ]
    ]
  ],
  [
    1,
    [
      'Powerful Jaws',
      () => [
        'This mutant’s jaw and mouth muscles are extremely well-developed and dense. It can easily crush bone between its teeth.'
      ]
    ]
  ],
  [
    1,
    [
      'Powerful Legs',
      () => [
        'Because of its powerful leg muscles, this mutant can move at double normal speed and jump three times as high and far as normal.'
      ]
    ]
  ],
  [
    2,
    [
      'Prehensile Feet',
      () => [
        'This mutant’s feet are as dexterous as hands, with toes and possibly a thumb that are capable of fine manipulation and grasping.'
      ]
    ]
  ],
  [
    1,
    [
      'Prehensile Hair',
      () => [
        'This mutant’s hair can move and grasp objects at the mutant’s direction.'
      ]
    ]
  ],
  [
    2,
    [
      'Premature Aging',
      () => [
        'This mutant ages quicker than normal, appearing about twice as old as it actually is.'
      ]
    ]
  ],
  [
    1,
    [
      'Prey Scent',
      () => ['This mutant gives off a scent that attracts predators.']
    ]
  ],
  [
    1,
    [
      'Protected Senses',
      () => [
        'One or more of this mutant’s senses is protected from over-stimulation. Disorienting stimuli and attacks against this sense have no effect on the mutant.',
        `Protected senses: ${uniform([
          'balance/direction',
          'hearing',
          'pain/touch',
          'smell/taste',
          'vision',
          () =>
            listWords(
              takeUniform(2, [
                'balance/direction',
                'hearing',
                'pain/touch',
                'smell/taste',
                'vision'
              ])
            )
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Pseudopods',
      () => [
        'This mutant can project temporary tentacle-like appendages from its body. It appears otherwise normal when not employing any pseudopods.'
      ]
    ]
  ],
  [1, ['Quadriplegic', () => ['This mutant is paralysed below the neck.']]],
  [
    1,
    [
      'Quick Reflexes',
      () => [
        'This mutant’s reaction speed is far quicker than the average specimen of its type.',
        `Amount of increase: ${uniform([
          'doubled',
          'increased by one half',
          'increased by one third',
          'increased by one quarter'
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Radar',
      () => [
        'This mutant can sense and broadcast radio waves in order to determine the shape of its surroundings. These radio waves do not pass through solid objects or liquids.',
        `Range of radar: ${uniform([
          'a couple miles (3km)',
          'a hundred feet (30m)',
          'a hundred miles (160km)',
          'a hundred yards (91m)',
          'five hundred miles (800km)',
          'six hundred yards (550m)',
          'ten miles (16km)',
          'twenty miles (32km)'
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Reflective Surface',
      () => [
        'This mutant’s skin or other covering reflects heat, light, and electro-magnetic energy, including harmful laser blasts and nuclear energies. It may or may not be as shiny as a mirror.'
      ]
    ]
  ],
  [
    2,
    [
      'Regeneration',
      () => [
        'This mutant can heal faster and more effectively than others.',
        `Effectiveness: ${weighted([
          [
            2,
            'injuries heal several times faster than normal, and lost limbs grow back'
          ],
          [
            1,
            'injuries heal within days instead of months, and minutes instead of hours'
          ],
          [3, 'the mutant’s limbs will grow back is severed']
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Respiratory Filters',
      () => [
        'This mutant has filters in its lungs, esophagus, or at some other point in its respiratory system. It is immune to airborne bacteria, drugs, toxins, and viruses.'
      ]
    ]
  ],
  [
    1,
    [
      'Roots and Vines',
      () => [
        'This mutant has roots and vines that can burrow into the ground and absorb nutrients, as well as keep the mutant attached to solid objects.'
      ]
    ]
  ],
  [
    1,
    [
      'Rotating Body Part',
      () => [
        'One or more of this mutant’s body parts can rotate a full 360 degrees.',
        `Body part: ${uniform([
          'arms and legs',
          'hands and feet',
          'head',
          'waist'
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Rotting Flesh',
      () => [
        'This mutant’s flesh is prone to rotting—becoming putrid and foul-smelling, dripping fluids and falling apart easily. Without constant medical attention, the mutant will die.'
      ]
    ]
  ],
  [
    1,
    [
      'Rubber Bones',
      () => [
        'This mutant’s bones are soft and rubbery, making it harder to stand up straight and use its muscles properly. Strength is halved, but the mutant can fit into tight spaces easier. The mutant is injured less by blunt impacts, due to shock absorption, but suffers more from crushing and squeezing, since these bones provide less protection to internal organs.'
      ]
    ]
  ],
  [
    1,
    [
      'Rubbery Skin',
      () => [
        'This mutant’s skin is hard and rubbery and slows it down a little, especially in cold weather.'
      ]
    ]
  ],
  [
    1,
    [
      'Saliva Substitution',
      () => [
        'This mutant’s saliva has some additional property that it is immune to, but other are not.',
        `Substitution: ${uniform([
          'acidic',
          'analgesic',
          'dries as a solid mass',
          'hallucinogenic',
          'paralytic',
          'piquant',
          'poisonous',
          'webbing strands'
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Salt Drinker',
      () => [
        'This mutant suffers no ill effects for drinking salt water instead of fresh water.'
      ]
    ]
  ],
  [
    1,
    [
      'Sanitized Metabolism',
      () => [
        'This mutant has virtually no foreign bacteria or parasites in its digestive system, which produces natural enzymes to break down food in place of these organisms. Its immune system may be correspondingly weak, forcing it to live in a special or artificial environment.'
      ]
    ]
  ],
  [
    1,
    [
      'Shapechanger',
      () => [
        'This mutant can change its shape and appearance.',
        `Type of shape-changing: ${weighted([
          [1, 'the mutant can conceal its other mutations and appear normal'],
          [2, () => `the mutant can mimic ${indefinite(creature())}`],
          [1, 'the mutant has a multi-stage life cycle'],
          [
            3,
            () =>
              `the mutant has a second form (${creature()}) and can switch between at will`
          ],
          [
            2,
            () =>
              `the mutant is diurnal, with one form during the day and another at night (${creature()})`
          ],
          [1, 'the mutant suffers from uncontrollable shape changes']
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Shedding',
      () => [
        'This mutant sheds its skin.',
        `Type of shedding: ${uniform([
          'regular moulting of whole skin',
          'skin can be shed as necessary',
          'skin constantly flakes off',
          'skin constantly peeling'
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Short Life',
      () => [
        'This mutant will only live half as long as others of its species, but it will not age prematurely.'
      ]
    ]
  ],
  [
    2,
    [
      'Silent Movement',
      () => [
        'Due to soft pads on hands and feet, and a modified physiognomy, this mutant makes almost no sound when it moves.'
      ]
    ]
  ],
  [
    1,
    [
      'Silk Production',
      () => [
        'This mutant can produce strands of very strong silk, like that of a spider or silkworm. These can be smooth or sticky, and can be used to climb, form a web, or bind prey.'
      ]
    ]
  ],
  [
    2,
    [
      'Slow Healer',
      () => [
        'This mutant’s ability to heal is impaired, for whatever reason. It takes twice as long as is normal for its species to recover from injuries, fatigue, and illness.'
      ]
    ]
  ],
  [
    1,
    [
      'Slow Movement Speed',
      () => [
        'This mutant cannot travel on its limbs as fast as others of its species.',
        `Amount of decrease: ${uniform([
          'decreased to one quarter',
          'decreased by one half',
          'decreased by one third',
          'decreased by one quarter'
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Slow Reaction Time',
      () => [
        'This mutant’s reaction speed is far lower than the average specimen of its type.',
        `Amount of decrease: ${uniform([
          'decreased to one quarter',
          'decreased by one half',
          'decreased by one third',
          'decreased by one quarter'
        ])}.`
      ]
    ]
  ],
  [
    2,
    [
      'Sonar',
      () => [
        'This mutant emits high-frequency sound and listens to the reflections in order to navigate its surroundings. It can operate normally in darkness, and knows the shape of its surroundings within a hundred feet (30m). Creatures and devices able to detect high frequencies will be alerted to the mutant’s presence when it uses sonar.'
      ]
    ]
  ],
  [
    1,
    [
      'Sonic Blast',
      () => [
        'This mutant can project a concentrated blast of sound that stuns and deafens those targeted. This mutant can also be very loud.'
      ]
    ]
  ],
  [
    1,
    [
      'Sound Mimicry',
      () => ['This mutant can closely mimic any sound it hears.']
    ]
  ],
  [
    2,
    [
      'Speech Impediment',
      () => [
        'This mutant’s mouth has an awkward shape—perhaps because of a cleft lip and/or palate, too many teeth, oddly-shaped teeth or bones, or gaps where teeth failed to develop—giving it a distinctively thick speech impediment.'
      ]
    ]
  ],
  [
    1,
    [
      'Spinning',
      () => [
        'This mutant can spin at high speeds without becoming dizzy or disoriented. While spinning, the mutant’s senses are impaired but still functional.'
      ]
    ]
  ],
  [
    1,
    [
      'Spore Cloud',
      () => [
        'This mutant can emit a cloud of spores that irritate the eyes, skin, and throat of those caught within it, and possibly helps it reproduce.'
      ]
    ]
  ],
  [
    1,
    [
      'Sprint',
      () => [
        'This mutant may sprint at incredibly fast speeds over short distances, up to five times as fast as normal for its species.'
      ]
    ]
  ],
  [
    1,
    [
      'Stiff Joints',
      () => [
        'This mutant’s limbs and joints are stiff and hard to bend. The mutant moves in a jerky, ungraceful manner, cannot run at full speed, and has trouble with manual dexterity.'
      ]
    ]
  ],
  [
    1,
    [
      'Strong Skeleton',
      () => [
        'This mutant has extremely hard bones that can withstand as much kinetic force as steel.'
      ]
    ]
  ],
  [
    1,
    [
      'Structural Weakness',
      () => [
        'This mutant’s bones and other hard parts are structurally deficient and more prone to breaking and fracturing than normal.'
      ]
    ]
  ],
  [
    1,
    [
      'Suckers',
      () => [
        'This mutant has visible suckers that help it grip and hold things.',
        `Location of suckers: ${weighted([
          [1, 'almost all over'],
          [3, 'hands and feet'],
          [2, 'tips of fingers and toes'],
          [2, 'underside of body']
        ])}.`
      ]
    ]
  ],
  [
    2,
    [
      'Telescopic Vision',
      () => [
        'This mutant can see clearly for up to two miles (3km) and still distinguish individuals.'
      ]
    ]
  ],
  [
    2,
    [
      'Temperature Resistance',
      () => [
        'This mutant can function normally in temperatures that others of its species find too hot or cold. It takes longer to freeze to death or suffer heat exhaustion, but cannot withstand temperatures that boil water or freeze flesh.'
      ]
    ]
  ],
  [
    2,
    [
      'Temperature Sensitivity',
      () => [
        'This mutant is acutely vulnerable to extremes of heat and cold, and will become weak and sluggish before others do.'
      ]
    ]
  ],
  [
    1,
    [
      'Thin Skin',
      () => [
        'This mutant’s skin is thin and brittle, making it easier to penetrate or tear.'
      ]
    ]
  ],
  [
    1,
    [
      'Total Healing',
      () => [
        'This mutant has the ability to replace all its cells rapidly, which takes a span of one day for every hundred pounds, and requires the mutant to eat constantly throughout. This process sheds skin, regrows lost limbs, and purges the body of radiation poisoning and toxins. When the process is finished, the mutant is weak and exhausted, and must rest for several days.'
      ]
    ]
  ],
  [
    1,
    [
      'Toxic',
      () => [
        'This mutant’s flesh and organs are highly toxic to most other animals.'
      ]
    ]
  ],
  [
    1,
    [
      'Toxin Resistance',
      () => [
        'This mutant is unaffected by normally-lethal levels of most or all toxins, poisons, and drugs. This is not total immunity, but much higher doses are required for effect.'
      ]
    ]
  ],
  [
    1,
    [
      'Trail of Slime',
      () => ['This mutant secretes slime as it moves along the ground.']
    ]
  ],
  [
    1,
    [
      'Tremor Sense',
      () => [
        'This mutant can sense movement through vibrations in the ground and other solid matter.',
        `Range of tremor sense: ${weighted([
          [2, () => feet(d10(1) * 10)],
          [1, 'a couple miles (3km)'],
          [2, 'a hundred yards (91m)'],
          [1, 'half a mile (800m)']
        ])}.`
      ]
    ]
  ],
  [
    1,
    ['Tunnel Vision', () => ['This mutant his little or no peripheral vision.']]
  ],
  [
    1,
    [
      'Ultravision',
      () => [
        'This mutant can see into the ultraviolet range, which makes radiation and electro-magnetic energy visible. The mutant is immune to welder’s flash, but will be almost blind in space, due to the abundance of vacuum ultraviolet.'
      ]
    ]
  ],
  [
    1,
    [
      'Uncontrollable Flatulence',
      () => [
        'This mutant has intestinal difficulties that cause it to fart a lot.'
      ]
    ]
  ],
  [
    2,
    [
      'Venomous Bite',
      () => [
        'This mutant’s bite is enhanced by venom.',
        `Type of venom: ${uniform([
          'necrotic venom',
          'paralytic venom',
          'toxic poison'
        ])}.`
      ]
    ]
  ],
  [
    1,
    [
      'Ventriloquist',
      () => [
        'This mutant can throw its voice, without moving its lips or appearing to make noise at all.'
      ]
    ]
  ],
  [
    1,
    [
      'Vents',
      () => [
        'This mutant has vents in its skin that release air, steam, or other gases.'
      ]
    ]
  ],
  [
    1,
    [
      'Voluminous Lungs',
      () => [
        'Due to its expansive lung capacity and other adaptations, this mutant can hold its breath for up to an hour.'
      ]
    ]
  ],
  [
    1,
    [
      'Vomits at Will',
      () => [
        'This mutant may projectile vomit. There is a 1 in 3 chance this vomit is harmful enough to be used as a deadly weapon.'
      ]
    ]
  ],
  [
    1,
    [
      'Water Dependency',
      () => [
        `This mutant’s skin must be kept moist by immersion in water at least once every ${plural(
          d4(),
          'hour'
        )}. If its skin dries up, it will crack and split painfully, becoming vulnerable to infection.`
      ]
    ]
  ],
  [
    1,
    [
      'Water-Soluble Skin',
      () => [
        'This mutant’s skin lacks cohesion and will dissolve if immersed in water.'
      ]
    ]
  ],
  [
    1,
    [
      'Weak Spot',
      () => [
        'This mutant has a particular weak spot. If this spot is struck at all, the mutant will be stunned, paralysed, or even killed.',
        `Weak spot: ${bodyPart()}.`
      ]
    ]
  ],
  [
    1,
    [
      'Wet Skin',
      () => [
        'This mutant’s skin is coated with a layer of fluids, secreted by special glands.'
      ]
    ]
  ],
  [
    1,
    [
      'Wounding Genitals',
      () => [
        'This mutant’s genitals cause wounds, either because of barbs, coarse texture, grinding parts, stingers, teeth, or other features.'
      ]
    ]
  ]
]
