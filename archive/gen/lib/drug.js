import colors from './data/colors.js'
import { capital } from './text.js'
import { draw } from './array.js'
import { h } from './dom.js'
import { die } from './random.js'

const categories = [
  'Anti-inflammatory',
  'Anticonvulsant',
  'Antiemetic (treats nausea and vomiting)',
  'Antihistamine',
  'Antipsychotic (major tranquilizer)',
  'Antipyretic (reduces fever)',
  'Barbiturate (major sedative)',
  'Benzodiazepine (minor sedative)',
  'Cough suppressant (reflex)',
  'Cytotoxin',
  'Decongestant',
  'Diuretic',
  'Expectorant',
  'Immunosuppressive',
  'Laxative',
  'Muscle relaxant',
  'Narcotic analgesic (severely reduces pain)',
  'Non-narcotic analgesic (mildly reduces pain)',
  'Vitamin'
]

const liquidAdministrations = [
  'Injected',
  'Nasal',
  'Optical',
  'Oral',
  'Smoked',
  'Swallowed',
  'Vaporized'
]

const solidAdministrations = [
  'Chewed',
  'Eaten',
  'Nasal',
  'Optical',
  'Oral',
  'Smoked',
  'Vaporized'
]

const forms = [
  ['cream', false],
  ['leaves', false],
  ['liquid', true],
  ['powder', false],
  ['produce', false],
  ['root', false]
]

const sideEffects = [
  'Blurred vision',
  'Confusion',
  'Diarrhea',
  'Loss of sense',
  'Nausea',
  'Nose-bleed',
  'Pupil dilation',
  'Tremors',
  'Weakness'
]

export const actions = {
  Drug: [
    '💊',
    () => {
      const [form, isLiquid] = draw(forms)
      const administered = draw(
        isLiquid ? liquidAdministrations : solidAdministrations
      )

      return {
        title: 'Drug',
        content: [
          h('p', [
            `Category: ${draw(categories)}`,
            h('br'),
            `Form: ${capital(draw(colors))} ${form}`,
            h('br'),
            `Administration: ${administered}`,
            h('br'),
            `Side Effect: ${draw(sideEffects)}`,
            h('br'),
            `Potency: ${die(1, 100)}%`
          ])
        ]
      }
    }
  ]
}
