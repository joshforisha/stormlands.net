import weightedBodyFormMutations from './data/weighted-body-form-mutations.js'
import weightedBodyFunctionMutations from './data/weighted-body-function-mutations.js'
import { h } from './dom.js'
import { weighted } from './random.js'

export const actions = {
  'Mutation: Body Form': [
    '💪',
    () => {
      const [formName, makeFormInfo] = weighted(weightedBodyFormMutations)
      const formInfo = makeFormInfo()
      return {
        title: `Body Form: ${formName}`,
        content: formInfo.map((i) => h('p', [i]))
      }
    }
  ],
  'Mutation: Body Function': [
    '✍️',
    () => {
      const [functionName, makeFunctionInfo] = weighted(
        weightedBodyFunctionMutations
      )
      const functionInfo = makeFunctionInfo()
      return {
        title: `Body Function: ${functionName}`,
        content: functionInfo.map((i) => h('p', [i]))
      }
    }
  ]
}
