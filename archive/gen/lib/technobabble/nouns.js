export const nouns = [
  'actuator',
  'adjustor',
  'aligner',
  'array',
  'assembly',
  'balancer',
  'battery',
  'bay',
  'buffer',
  'bus',
  'bypass',
  'calibrator',
  'capacitor',
  'carrier',
  'cell',
  'chamber',
  'circuit',
  'cluster',
  'coil',
  'collar',
  'collector',
  'compensator',
  'computer',
  'conditioner',
  'conduit',
  'configurator',
  'console',
  'controller',
  'cooler',
  'core',
  'coupling',
  'crystal',
  'dampener',
  'deflector',
  'diffusor',
  'device',
  'differential',
  'discriminator',
  'display',
  'dissipater',
  'disturbance',
  'drive',
  'effect',
  'ejector',
  'emitter',
  'enhancer',
  'enlarger',
  'extender',
  'fabricator',
  'fastener',
  'field',
  'filament',
  'flow',
  'gateway',
  'generator',
  'gradient',
  'grid',
  'gyroscope',
  'harmonizer',
  'inducer',
  'inductor',
  'infuser',
  'inhibitor',
  'injector',
  'interface',
  'inverter',
  'isolator',
  'manifold',
  'manipulator',
  'mask',
  'matrix',
  'module',
  'network',
  'nutation',
  'plate',
  'platform',
  'projector',
  'protector',
  'reactor',
  'recorder',
  'reducer',
  'regulator',
  'relay',
  'repulsor',
  're-router',
  'resistor',
  'resonator',
  'scanner',
  'shell',
  'shunt',
  'stream',
  'structure',
  'sustainer',
  'synthesizer',
  'terminal',
  'thrusters',
  'transformer',
  'unit',
  'vector',
  'vent'
]
