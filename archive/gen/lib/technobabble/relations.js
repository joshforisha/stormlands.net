export const relations = [
  'affixed to',
  'connected to',
  'feeding into',
  'inside of',
  'preempting',
  'surrounding'
]
