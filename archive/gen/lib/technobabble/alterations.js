export const alterations = [
  'amplify',
  'bypass',
  'compensate for',
  'constrain',
  'neutralize',
  'normalize',
  'nullify',
  'parameterize',
  'reinforce',
  'replicate',
  'void'
]
