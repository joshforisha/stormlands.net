export function draw (xs) {
  return xs[Math.floor(Math.random() * xs.length)]
}

export function initialize (num, value = (i) => i) {
  const xs = []
  for (let i = 0; i < num; i++) {
    xs.push(typeof value === 'function' ? value(i) : value)
  }
  return xs
}

export function intersperse (xs, y) {
  const lastIndex = xs.length - 1
  return xs.reduce((zs, x, i) => {
    zs.push(x)
    if (i < lastIndex) zs.push(typeof y === 'function' ? y() : y)
    return zs
  }, [])
}

export function make (count, fn) {
  const items = []
  for (let i = 0; i < count; i++) items.push(fn(i))
  return items
}

export function shuffle (xs) {
  const ys = [...xs]
  let i = xs.length - 1
  for (i; i > 0; i--) {
    const j = Math.floor(Math.random() * i)
    const y = ys[i]
    ys[i] = ys[j]
    ys[j] = y
  }
  return ys
}

export function without (index, xs) {
  return xs.filter((_, i) => i !== index)
}
