import { draw } from './array.js'
import { h } from './dom.js'
import careers from './data/character-careers.js'
import flaws from './data/character-flaws.js'
import bodyLanguages from './data/character-body-language.js'

export const actions = {
  Character: [
    '👨',
    () => {
      return {
        title: 'Character',
        content: [
          h('p', [h('strong', 'Career: '), draw(careers)]),
          h('p', [h('strong', 'Flaw: '), draw(flaws)]),
          h('p', [h('strong', 'Body Language: '), draw(bodyLanguages)])
        ]
      }
    }
  ]
}
