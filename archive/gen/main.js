import * as Character from './lib/character.js'
import * as Concept from './lib/concept.js'
import * as Drug from './lib/drug.js'
import * as Mutations from './lib/mutations.js'
import * as Technobabble from './lib/technobabble.js'
import { $, h } from './lib/dom.js'

const actions = {
  ...Character.actions,
  ...Concept.actions,
  ...Drug.actions,
  ...Mutations.actions,
  ...Technobabble.actions
}

const dialog = $('dialog')
const dialogContent = $('dialog .content')
const dialogTitle = $('dialog .title')
const main = $('main')
const regenButton = $('dialog button')

let currentAction = null

function selectAction(action) {
  dialogTitle.textContent = ''
  dialogContent.innerHTML = ''

  const { title, content } = action()
  dialogTitle.textContent = title
  for (const child of content) dialogContent.appendChild(child)

  if (!dialog.hasAttribute('open')) {
    currentAction = action
    dialog.showModal()
  }
}

dialog.addEventListener('click', (event) => {
  if (event.target.closest('div') === null) {
    dialog.close()
    currentAction = null
  }
})

regenButton.addEventListener('click', () => {
  if (currentAction) selectAction(currentAction)
})

Object.entries(actions).forEach(([name, [iconString, action]]) => {
  const icon = h('span')
  icon.classList.add('icon')
  icon.textContent = iconString
  const button = h('button', [icon, name])
  button.addEventListener('click', () => {
    selectAction(action)
  })
  main.appendChild(button)
})
