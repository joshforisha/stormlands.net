import * as Color from './lib/Colors'
import * as Icon from './lib/Icons'
import allegianceHexes from './allegiance-hexes.json'
import bahamutSectorData from './bahamut-sector.csv'
import { svg } from './lib/Utils'

const allegianceColors = {
  Bahamut: `${Color.white}12`,
  Cojiu: `${Color.orange}18`,
  Elram: `${Color.green}18`,
  Hesa: `${Color.darkRed}26`,
  Idiju: `${Color.yellow}20`,
  Imperium: `${Color.darkTeal}30`,
  Jumei: `${Color.darkPink}1C`,
  Laue: `${Color.red}14`,
  Lufhoibe: `${Color.darkYellow}14`,
  Roeci: `${Color.green}15`,
  Sanaco: `${Color.darkCyan}22`,
  Selti: `${Color.yellow}10`,
  Zefle: `${Color.darkPurple}24`,
  Zucai: `${Color.orange}22`
}

const map = document.getElementById('Map')

function drawHex (x, y, coord) {
  const hex = svg('g', {
    class: 'hex',
    id: `Hex${coord}`,
    transform: `translate(${x} ${y})`,
    x,
    y
  })

  hex.appendChild(
    hexagon({
      'stroke-width': 4,
      fill: Color.black,
      stroke: Color.darkGray,
      x,
      y
    })
  )
  map.appendChild(hex)

  const allegiance = Object.keys(allegianceHexes).find(
    a => allegianceHexes[a].indexOf(coord) > -1
  )
  if (allegiance) {
    const allegianceHex = hexagon({
      fill: allegianceColors[allegiance],
      stroke: 'transparent',
      transform: `translate(${x + 3} ${y + 3}), scale(0.98)`
    })
    map.appendChild(allegianceHex)
  }

  const coordinateText = svg('text', {
    'font-size': 28,
    fill: Color.white,
    opacity: 0.5,
    x: x + 150,
    y: y + 50
  })
  coordinateText.textContent = coord
  map.appendChild(coordinateText)
}

function drawWorld (world) {
  const hex = map.getElementById(`Hex${world.hex}`)
  const [x, y] = [
    parseInt(hex.getAttribute('x')),
    parseInt(hex.getAttribute('y'))
  ]

  if (world.allegiance) {
    const allegianceHex = hexagon({
      fill: allegianceColors[world.allegiance],
      stroke: 'transparent',
      transform: `translate(${x + 3} ${y + 3}), scale(0.98)`
    })
    map.appendChild(allegianceHex)
  }

  const g = svg('g', {
    transform: `translate(${x}, ${y})`
  })

  if (world.bases.includes('N')) {
    g.appendChild(Icon.navyBase())
  }

  if (world.bases.includes('R')) {
    g.appendChild(Icon.researchBase())
  }

  if (world.gasGiants > 0) {
    g.appendChild(Icon.gasGiant())

    const gasGiantsText = svg('text', {
      'font-size': 24,
      'font-weight': 700,
      fill: Color.black,
      x: 150,
      y: 89
    })
    gasGiantsText.textContent = world.gasGiants.toString()
    g.appendChild(gasGiantsText)
  }

  if (world.bases.includes('S')) {
    g.appendChild(Icon.scoutBase())
  }

  if (world.bases.includes('T')) {
    g.appendChild(Icon.tasHostel())
  }

  const tradeCodesText = svg('text', {
    'font-size': 22,
    fill: Color.white,
    opacity: 0.4,
    x: 150,
    y: 125
  })
  tradeCodesText.textContent = world.tradeCodes
  g.appendChild(tradeCodesText)

  const nameText = svg('text', {
    'font-size': 30,
    fill: Color.white,
    x: 151,
    y: 161
  })
  nameText.textContent = world.name.toUpperCase()
  g.appendChild(nameText)

  /*
  const popText = svg('text', {
    'font-size': 60,
    fill: Color.white,
    opacity: 0.25,
    x: 85,
    y: 224
  })
  popText.textContent = world.upp[4]
  hex.appendChild(popText)
  // */

  const r = 26 + parseInt(world.upp[1], 16)
  if (world.tradeCodes.includes('As')) {
    g.appendChild(Icon.asteroidWorld(r))
  } else {
    const hydro = parseInt(world.upp[3], 16)
    if (hydro < 4) g.appendChild(Icon.dryWorld(r))
    else if (hydro < 8) g.appendChild(Icon.terrestrialWorld(r))
    else g.appendChild(Icon.wetWorld(r))
  }

  const uppText = svg('text', {
    'font-size': 28,
    fill: Color.white,
    x: 150,
    y: 270
  })
  uppText.textContent = world.upp
  g.appendChild(uppText)

  map.appendChild(g)
}

function drawZone (x, y, code) {
  const outline = hexagon()
  outline.setAttribute('transform', `translate(${x} ${y})`)
  outline.setAttribute('fill', 'transparent')
  outline.setAttribute('stroke-width', 5)

  if (code === 'Amber') {
    outline.setAttribute('stroke', Color.darkYellow)
  } else if (code === 'Red') {
    outline.setAttribute('stroke', Color.red)
  }

  map.appendChild(outline)
}

function hexagon (attrs = {}) {
  return svg(
    'polygon',
    Object.assign(
      {
        points: '300,150 225,280 75,280 0,150 75,20 225,20'
      },
      attrs
    )
  )
}

function render (worlds) {
  map.innerHTML = ''

  const travelHexes = []

  for (let ssr = 0; ssr < 4; ssr++) {
    for (let ssc = 0; ssc < 4; ssc++) {
      for (let row = 0; row < 10; row++) {
        for (let col = 0; col < 8; col++) {
          //* Tight
          const x = 28 + ssc * 1860 + col * 225
          const y = 2 + ssr * 2680 + row * 260 + (col % 2 > 0 ? 130 : 0)
          // */

          /* Spaced out
          const x = 28 + ssc * 1920 + col * 225;
          const y = 2 + ssr * 2800 + row * 260 + (col % 2 > 0 ? 130 : 0);
          // */

          const [cx, cy] = [ssc * 8 + col + 1, ssr * 10 + row + 1]
          const coordX = cx < 10 ? `0${cx}` : cx.toString()
          const coordY = cy < 10 ? `0${cy}` : cy.toString()
          const coord = `${coordX}${coordY}`
          const world = worlds.filter(w => w.hex === coord)[0]

          drawHex(x, y, coord, world)

          if (typeof world !== 'undefined') {
            drawWorld(world)

            if (world.travelCode !== '') {
              travelHexes.push({ travelCode: world.travelCode, x, y })
            }
          }
        }
      }
    }
  }

  travelHexes.forEach(({ x, y, travelCode }) => {
    drawZone(x, y, travelCode)
  })
}

window.downloadMap = function () {
  const mapBlob = new window.Blob([map.outerHTML], { type: 'image/svg+xml' })
  const mapURL = window.URL.createObjectURL(mapBlob)

  const canvas = document.createElement('canvas')
  canvas.setAttribute('height', map.getAttribute('height'))
  canvas.setAttribute('width', map.getAttribute('width'))

  const ctx = canvas.getContext('2d')

  const img = new window.Image()
  img.onload = () => {
    ctx.drawImage(img, 0, 0)
    window.URL.revokeObjectURL(mapURL)
    const w = window.open()
    w.document.body.append(canvas)
  }
  img.src = mapURL
}

window
  .fetch(bahamutSectorData)
  .then(data => data.text())
  .then(text => {
    const textData = text.split('\n')

    const keys = textData[0]
      .split(',')
      .map(t => t[0].toLowerCase() + t.slice(1).replace(' ', ''))

    const worlds = textData.slice(1).map(line => {
      const values = line.split(',')
      const world = {}

      keys.forEach((k, i) => {
        world[k] = values[i]
      })

      return world
    })

    render(worlds)
  })
