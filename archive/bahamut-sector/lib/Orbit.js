import { Enum } from '/lib/Utils'

const Orbit = Enum(['Cold', 'Empty', 'Hot', 'Hospitable', 'Inhospitable'])

export default Orbit
