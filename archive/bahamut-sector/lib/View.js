import Color from '/lib/Color'
import gasGiantIcon from '/images/gas-giant.svg'
import navyBaseIcon from '/images/navy-base.svg'
import researchBaseIcon from '/images/research-base.svg'
import scoutBaseIcon from '/images/scout-base.svg'
import tasHostelIcon from '/images/tas-hostel.svg'

const gasGiantImage = svg(gasGiantIcon)
const navyBaseImage = svg(navyBaseIcon)
const researchBaseImage = svg(researchBaseIcon)
const scoutBaseImage = svg(scoutBaseIcon)
const tasHostelImage = svg(tasHostelIcon)

export function drawCircle ({ color, ctx, width, x, y }) {
  ctx.beginPath()
  ctx.ellipse(x, y, width, width, 45 * Math.PI / 180, 0, 2 * Math.PI)
  ctx.fillStyle = color
  ctx.fill()
}

export function drawHexagon ({ color, ctx, width, x, y }) {
  ctx.beginPath()
  ctx.moveTo(x + 100 * Math.cos(0), y + 100 * Math.sin(0))
  for (let side = 0; side < 7; side++) {
    ctx.lineTo(
      x + 100 * Math.cos(side * 2 * Math.PI / 6),
      y + 100 * Math.sin(side * 2 * Math.PI / 6)
    )
  }
  ctx.strokeStyle = color
  ctx.lineWidth = width
  ctx.stroke()
}

export function drawSubsector ({ ctx, subsector, x, y }) {
  subsector.hexes.forEach((hexes, column) => {
    hexes.forEach((hex, row) => {
      drawTile({
        ctx,
        hex,
        x: x + column * 150,
        y: y + (column % 2 > 0 ? 186.25 : 100) + row * 172.5
      })
    })
  })

  ctx.font = '200 48px sans-serif'
  ctx.fillStyle = Color.LightestGray
  ctx.textAlign = 'center'
  ctx.fillText(subsector.name, x + 525, y + 1910)
}

export function drawTile ({ ctx, hex, scale, x, y }) {
  drawHexagon({ color: Color.DarkGray, ctx, width: 2, x, y })

  if (hex.system !== null) {
    ctx.font = '200 18px sans-serif'
    ctx.fillStyle = Color.LightGray
    ctx.textAlign = 'center'
    ctx.fillText(hex.label, x, y - 66)

    /*
    ctx.font = '300 23px sans-serif'
    ctx.fillStyle = Color.White
    ctx.textAlign = 'center'
    ctx.fillText(hex.world.name, x, y + 8)

    const color = hex.world.hydrographic === 0 ? Color.White : Color.Blue
    drawCircle(ctx, x, y, 12 + hex.world.size, color)

    if (scale >= 0.8) {
      if (hex.world.hasGasGiant) {
        ctx.drawImage(gasGiantImage, x - 80, y - 12, 28, 28)
      }

      if (isin(hex.world.bases, 'Naval')) {
        ctx.drawImage(navyBaseImage, x - 50, y + 3, 18, 18)
      }

      if (isin(hex.world.bases, 'Research')) {
        ctx.drawImage(researchBaseImage, x - 48, y - 22, 16, 20)
      }

      if (isin(hex.world.bases, 'Scout')) {
        ctx.drawImage(scoutBaseImage, x + 40, y - 23, 26, 26)
      }

      if (isin(hex.world.bases, 'TAS')) {
        ctx.drawImage(tasHostelImage, x + 42, y + 4, 24, 20)
      }

      ctx.font = '200 17px sans-serif'
      ctx.fillStyle = Color.DarkGray
      ctx.fillText(hex.world.classificationString, x, y - 42)

      const tradeCodes = hex.world.tradeCodes
      if (tradeCodes.length > 0) {
        ctx.font = '200 italic 15px sans-serif'
        ctx.fillStyle = Color.DarkGray
        ctx.fillText(tradeCodes.join(' '), x, y - 22)
      }
    } else {
      const classText = `${hex.world.starport}–${hex.world.techLevel
        .toString(16)
        .toUpperCase()}`
      ctx.font = '200 23px sans-serif'
      ctx.fillStyle = Color.LightGray
      ctx.textAlign = 'center'
      ctx.fillText(classText, x, y - 32)
    }
    */
  }
}

export function render (model) {
  const { center, ctx, scale } = model

  ctx.clearRect(0, 0, window.innerWidth, window.innerHeight)

  ctx.canvas.setAttribute('height', window.innerHeight)
  ctx.canvas.setAttribute('width', window.innerWidth)

  ctx.transform(
    scale,
    0,
    0,
    scale,
    scale * center.x + center.x,
    scale * center.y + center.y
  )

  model.subsectors.forEach((subs, column) => {
    subs.forEach((subsector, row) => {
      drawSubsector({
        ctx,
        subsector,
        x: 100 + 1400 * column,
        y: 2000 * row
      })
    })
  })

  /*
  model.hexes
    .filter(hex => hex.world && hex.world.travelCode !== null)
    .forEach(({ world, x, y }) => {
      let color = Color.Green
      if (world.travelCode === 'amber') color = Color.Yellow
      else if (world.travelCode === 'red') color = Color.Red
      drawHexagon(ctx, color, 3, x, y)
    })
  */
}

function svg (path) {
  const image = document.createElement('img')
  image.setAttribute('src', path)
  image.setAttribute('type', 'image/svg')
  return image
}
