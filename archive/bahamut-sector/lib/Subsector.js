import Hex from '/lib/Hex'
import StarSystem from '/lib/StarSystem'
import { Enum, initialize, roll, zeroPad } from '/lib/Utils'

export const SubsectorType = Enum([
  'Dense',
  'Rift',
  'Scattered',
  'Sparse',
  'Standard'
])

function systemPresent (subsectorType) {
  switch (subsectorType) {
    case SubsectorType.Dense:
      return roll(1, 3)
    case SubsectorType.Rift:
      return roll(2, 12)
    case SubsectorType.Scattered:
      return roll(1, 5)
    case SubsectorType.Sparse:
      return roll(1, 6)
    case SubsectorType.Standard:
      return roll(1, 4)
  }
}

export default class Subsector {
  constructor ({ column, row, type }) {
    this.type = type

    this.hexes = initialize(8, hexColumn =>
      initialize(10, hexRow => {
        const columnText = zeroPad(2, column * 8 + hexColumn + 1)
        const rowText = zeroPad(2, row * 10 + hexRow + 1)

        return new Hex({
          label: `${columnText}${rowText}`,
          system: systemPresent(type) ? new StarSystem() : null
        })
      })
    )

    // TODO
    this.name = `Subsector ${column + 1}–${row + 1}`
  }
}
