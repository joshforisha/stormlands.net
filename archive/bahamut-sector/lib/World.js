import * as Word from '/lib/Word'
import { capital, chance, draw } from '/lib/Utils'

export default class World {
  constructor () {
    const syllablePattern = draw(['CV', 'CVN?', 'NVS?'])
    const numSyllables = chance(0.67) ? 2 : 3
    const syllables = []
    for (let i = 0; i < numSyllables; i++) {
      syllables.push(Word.generate(syllablePattern))
    }
    this.name = capital(syllables.join(''))
  }

  toClassificationString () {
    return ''
  }

  toString () {
    return `${this.name} (${this.toClassificationString()})`
  }
}
