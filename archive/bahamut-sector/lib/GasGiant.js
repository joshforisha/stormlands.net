import World from '/lib/World'
import { Enum, d } from '/lib/Utils'

export const Size = Enum(['Large', 'Small'])

export default class GasGiant extends World {
  constructor () {
    super()

    this.size = d() >= 4 ? Size.Small : Size.Large
  }

  toClassificationString () {
    return this.size === Size.Small ? 'SGG' : 'LGG'
  }
}
