import GasGiant from '/lib/GasGiant'
import Orbit from '/lib/Orbit'
import Terrestrial from '/lib/Terrestrial'
import { CompanionStar, PrimaryStar, SpectralType, starZones } from '/lib/Star'
import {
  Enum,
  constrain,
  d,
  d2,
  filterMap,
  initialize,
  isin,
  range,
  select
} from '/lib/Utils'

function emptyableZone (orbit, index) {
  if (
    typeof orbit === 'undefined' ||
    isin([Orbit.Inhospitable, Orbit.Hot, Orbit.Hospitable, Orbit.Cold], orbit)
  ) {
    return index
  }
}

function gasGiantZone (orbit, index) {
  if (isin([Orbit.Hospitable, Orbit.Cold], orbit)) return index
}

function genNumStars () {
  const res = d2()
  if (res < 8) return 1
  if (res < 12) return 2
  return 3
}

function planetoidZone (orbit, index) {
  if (isin([Orbit.Cold, Orbit.Hospitable, Orbit.Hot], orbit)) {
    return index
  }
}

function terrestrialZone (orbit) {
  return (
    typeof orbit === 'undefined' ||
    isin([Orbit.Cold, Orbit.Hospitable, Orbit.Hot], orbit)
  )
}

export default class StarSystem {
  constructor () {
    const numStars = genNumStars()
    this.stars = []
    const primaryStar = new PrimaryStar()
    this.stars.push(primaryStar)
    while (this.stars.length < numStars) {
      this.stars.push(new CompanionStar(primaryStar, this.stars.length))
    }

    const orbits = initialize(primaryStar.maximumOrbit + 1, () => {})
    this.stars
      .filter(s => typeof s.orbit === 'number')
      .filter(s => orbits.length > s.orbit)
      .filter(s => typeof orbits[s.orbit] !== 'undefined')
      .forEach(star => {
        orbits[star.orbit] = star

        const closerIndex = Math.floor(star.orbit / 2)
        orbits.slice(closerIndex, star.orbit).forEach((orbit, i) => {
          if (typeof orbit === 'undefined') orbits[closerIndex + i] = null
        })

        if (orbits.length > star.orbit + 1) orbits[star.orbit + 1] = null
      })

    const orbitZones = starZones(primaryStar)
    this.orbits = orbits.map((orbit, i) => {
      if (typeof orbit !== 'undefined') return orbit

      switch (orbitZones[i]) {
        case ' ':
        case '.':
          return null

        case '-':
          return Orbit.Inhospitable

        case 'I':
          return Orbit.Hot

        case 'H':
          return Orbit.Hospitable

        case 'O':
        default:
          return Orbit.Cold
      }
    })

    const emptyDM = isin(
      [SpectralType.B, SpectralType.A],
      primaryStar.spectralType
    )
      ? 1
      : 0
    const emptyOrbits =
      d() + emptyDM >= 5
        ? select(d() + emptyDM, [[[1, 2], 1], [[3], 2], [[4, 5, 6, 7], 3]])
        : 0
    for (let i = 0; i < emptyOrbits; i++) {
      const emptyableOrbits = filterMap(this.orbits, emptyableZone)
      if (emptyableOrbits.length === 0) break
      const index = Math.floor(Math.random() * emptyableOrbits.length)
      this.orbits[index] = Orbit.Empty
    }

    // TODO: Define captured planets

    let gasGiantSlots = filterMap(this.orbits, gasGiantZone)
    let numGasGiants =
      d2() <= 9
        ? Math.min(
          gasGiantSlots.length,
          select(d(), [
            [[1, 2, 3], 1],
            [[4, 5], 2],
            [[6, 7], 3],
            [[8, 9, 10], 4],
            [[11, 12], 5]
          ])
        )
        : 0
    if (numGasGiants > 0) {
      if (gasGiantSlots.length === 0) {
        this.orbits.push(new GasGiant())
        numGasGiants += 1
      } else {
        for (let i = 0; i < numGasGiants; i++) {
          const gi = Math.floor(Math.random() * gasGiantSlots.length)
          this.orbits[gasGiantSlots[gi]] = new GasGiant()
          gasGiantSlots = filterMap(this.orbits, gasGiantZone)
        }
      }
    }

    const planetoidRes = constrain(0, 12, d2() - numGasGiants)
    if (planetoidRes < 7) {
      const numPlanetoids = select(constrain(0, 12, d2() - numGasGiants), [
        [[0], 3],
        [range(1, 6), 2],
        [range(7, 12), 1]
      ])
      for (let i = 0; i < numPlanetoids; i++) {
        const slots = filterMap(this.orbits, planetoidZone)
        if (slots.length === 0) break
        const si = Math.floor(Math.random() * slots.length)
        this.orbits[slots[si]] = 'Planetoid'
      }
    }

    const habitableIndex = this.orbits.indexOf(Orbit.Habitable)
    this.orbits = this.orbits.map(
      (orbit, index) =>
        terrestrialZone(orbit)
          ? new Terrestrial({
            habitableOffset:
                habitableIndex === -1 ? null : index - habitableIndex,
            orbit,
            primaryStar
          })
          : orbit
    )

    console.log(this.toString())

    // 5. Determine number of satellites per planet, or gas giant
    // Planets: 1D-3
    // Small gas giants: 2D-4
    // Large gas giants: 2D
    // 6. Generate satellites within system
    // Size, orbit, atmosphere, hydrographics, population
    // 7. Designate main world and determine additional characteristics
    // Government, law level, starport type, tech level
    // Trade classifications, naval and scout bases
    // Communication routes
    // 8. Determine additional planet and satellite characteristics
    // Subordinate government
    // Subordinate law level
    // Subordinate facilities
    // Subordinate tech level
    // Spaceport type
    // 9. Record statistics and data
    // Map data on subsector grid
    // Note main world data on subsector data form
    // Note complete system data on system data form
  }

  toString () {
    const items = []

    this.stars
      .filter(s => s instanceof PrimaryStar || s.orbit === 'Close')
      .forEach(s => {
        items.push(s.toString())
      })

    this.orbits.forEach((o, i) => {
      if (o !== null && o.hasOwnProperty('toString')) {
        return items.push(`${i}: ${o.toString()}`)
      }

      return items.push(`${i}: ${o}`)
    })

    this.stars.filter(s => s.orbit === 'Far').forEach(s => {
      items.push(s.toString())
    })

    return items.join(', ')
  }
}
