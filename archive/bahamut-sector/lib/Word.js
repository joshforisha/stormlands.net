import { chance, draw } from '/lib/Utils'

const nonStridents = [
  'b',
  'd',
  'f',
  'g',
  'h',
  'k',
  'm',
  'n',
  'p',
  'r',
  't',
  'v',
  'w',
  'x',
  'z'
]

const stridents = ['j', 'l', 'r', 's', 'sh', 'th', 'y', 'z']

const diphthongs = [
  'ae',
  'ai',
  'ao',
  'au',
  'ea',
  'ei',
  'eo',
  'eu',
  'ia',
  'ie',
  'io',
  'iu',
  'oa',
  'oe',
  'oi',
  'ou',
  'ua',
  'ue',
  'ui',
  'uo'
]

const monophthongs = ['a', 'e', 'i', 'o', 'u']

const consonant = () => draw(chance(0.75) ? nonStridents : stridents)

const diphthong = () => draw(diphthongs)

const monophthong = () => draw(monophthongs)

const nonStrident = () => draw(nonStridents)

const strident = () => draw(stridents)

const vowel = () => draw(chance(0.75) ? monophthongs : diphthongs)

export function generate (pattern, rng = Math.random) {
  return pattern
    .replace(/'C\?/g, () => (chance(0.5, rng) ? `’${consonant(rng)}` : ''))
    .replace(/C\?/g, () => (chance(0.5, rng) ? consonant(rng) : ''))
    .replace(/C/g, () => consonant(rng))
    .replace(/'D\?/g, () => (chance(0.5, rng) ? `’${diphthong(rng)}` : ''))
    .replace(/D\?/g, () => (chance(0.5, rng) ? diphthong(rng) : ''))
    .replace(/D/g, () => diphthong(rng))
    .replace(/'M\?/g, () => (chance(0.5, rng) ? `’${monophthong(rng)}` : ''))
    .replace(/M\?/g, () => (chance(0.5, rng) ? monophthong(rng) : ''))
    .replace(/M/g, () => monophthong(rng))
    .replace(/'N\?/g, () => (chance(0.5, rng) ? `’${nonStrident(rng)}` : ''))
    .replace(/N\?/g, () => (chance(0.5, rng) ? nonStrident(rng) : ''))
    .replace(/N/g, () => nonStrident(rng))
    .replace(/'S\?/g, () => (chance(0.5, rng) ? `’${strident(rng)}` : ''))
    .replace(/S\?/g, () => (chance(0.5, rng) ? strident(rng) : ''))
    .replace(/S/g, () => strident(rng))
    .replace(/'V\?/g, () => (chance(0.5, rng) ? `’${vowel(rng)}` : ''))
    .replace(/V\?/g, () => (chance(0.5, rng) ? vowel(rng) : ''))
    .replace(/V/g, () => vowel(rng))
    .replace(/'\?/g, () => (chance(0.5, rng) ? '’' : ''))
    .replace(/'/g, () => '’')
}
