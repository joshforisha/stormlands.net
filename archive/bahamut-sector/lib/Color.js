const Color = {
  Black: '#181818',
  Blue: '#7CAFC2',
  Brown: '#A16946',
  Cyan: '#86C1B9',
  DarkGray: '#585858',
  DarkerGray: '#383838',
  DarkestGray: '#282828',
  Green: '#A1B56C',
  LightGray: '#B8B8B8',
  LighterGray: '#D8D8D8',
  LightestGray: '#E8E8E8',
  Magenta: '#BA8BAF',
  Orange: '#DC9656',
  Red: '#AB4642',
  White: '#F8F8F8',
  Yellow: '#F7CA88'
}

export default Color
