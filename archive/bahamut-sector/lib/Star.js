import { Enum, constrain, d, d2, range, select } from '/lib/Utils'

export const SpectralType = Enum(['O', 'B', 'A', 'F', 'G', 'K', 'M'])
export const Size = Enum(['Ia', 'Ib', 'II', 'III', 'IV', 'V', 'VI', 'D'])

function brightSupergiantZones (type, dec) {
  switch (type) {
    case SpectralType.B:
      if (dec < 5) return ' -------IIIIIHO'
      return ' ------IIIIIHOO'

    case SpectralType.A:
      return ' .-----IIIIIHOO'

    case SpectralType.F:
      if (dec < 5) return ' ..---IIIIIIHOO'
      return ' ..---IIIIIHOOO'

    case SpectralType.G:
      if (dec < 5) return ' ...---IIIIIHOO'
      return ' ....--IIIIIHOO'

    case SpectralType.K:
      return ' .....-IIIIIHOO'

    case SpectralType.M:
      if (dec < 5) return ' ......IIIIIHOO'
      return ' .......IIIIHOO'
  }
}

function brightGiantZones (type, dec) {
  switch (type) {
    case SpectralType.B:
      if (dec < 5) return ' ------IIIIIHO'
      return ' ----IIIIIIHOO'

    case SpectralType.A:
      if (dec < 5) return ' --IIIIIIHOOOO'
      return ' -IIIIIIHOOOOO'

    case SpectralType.F:
      return ' -IIIIIIHOOOOO'

    case SpectralType.G:
      return ' -IIIIIIHOOOOO'

    case SpectralType.K:
      if (dec < 5) return ' -IIIIIIIHOOOO'
      return ' .-IIIIIIHOOOO'

    case SpectralType.M:
      if (dec < 5) return ' ...IIIIIIHOOO'
      if (dec < 9) return ' .....IIIIIHOO'
      return ' .....IIIIIHOO'
  }
}

function giantZones (type, dec) {
  switch (type) {
    case SpectralType.B:
      if (dec < 5) return ' ------IIIIIHO'
      return ' ----IIIIIHOOO'

    case SpectralType.A:
      if (dec < 5) return ' IIIIIIIHOOOOO'
      return ' IIIIIIHOOOOOO'

    case SpectralType.F:
      if (dec < 5) return ' IIIIIHOOOOOOO'
      return ' IIIIIHOOOOOOO'

    case SpectralType.G:
      if (dec < 5) return ' IIIIIHOOOOOOO'
      return ' IIIIIIHOOOOOO'

    case SpectralType.K:
      if (dec < 5) return ' IIIIIIHOOOOOO'
      return ' IIIIIIIHOOOOO'

    case SpectralType.M:
      if (dec < 5) return ' -IIIIIIHOOOOO'
      if (dec < 9) return ' ...IIIIIHOOOO'
      return ' ....IIIIHOOOO'
  }
}

function mainSequenceZones (type, dec) {
  switch (type) {
    case SpectralType.B:
      if (dec < 5) return '------IIIIIIHO'
      return '---IIIIIIHOOOO'

    case SpectralType.A:
      if (dec < 5) return 'IIIIIIIHOOOOOO'
      return 'IIIIIIHOOOOOOO'

    case SpectralType.F:
      if (dec < 5) return 'IIIIIHOOOOOOOO'
      return 'IIIIHOOOOOOOOO'

    case SpectralType.G:
      if (dec < 5) return 'IIIHOOOOOOOOOO'
      return 'IIHOOOOOOOOOOO'

    case SpectralType.K:
      if (dec < 5) return 'IIHOOOOOOOOOOO'
      return 'HOOOOOOOOOOOOO'

    case SpectralType.M:
      if (dec < 5) return 'HOOOOOOOOOOOOO'
      return 'OOOOOOOOOOOOOO'
  }
}

export function starZones (star) {
  switch (star.size) {
    case Size.Ia:
      return brightSupergiantZones(star.spectralType, star.spectralDecimal)
    case Size.Ib:
      return weakerSupergiantZones(star.spectralType, star.spectralDecimal)
    case Size.II:
      return brightGiantZones(star.spectralType, star.spectralDecimal)
    case Size.III:
      return giantZones(star.spectralType, star.spectralDecimal)
    case Size.IV:
      return subgiantZones(star.spectralType, star.spectralDecimal)
    case Size.V:
      return mainSequenceZones(star.spectralType, star.spectralDecimal)
    case Size.VI:
      return subDwarfZones(star.spectralType, star.spectralDecimal)
    case Size.D:
      return whiteDwarfZones(star.spectralType)
  }
}

function subDwarfZones (type, dec) {
  switch (type) {
    case SpectralType.F:
      return 'IIIHO'

    case SpectralType.G:
      if (dec < 5) return 'IIHOO'
      return 'IHOOO'

    case SpectralType.K:
      if (dec < 5) return 'IHOOO'
      return 'OOOOO'

    case SpectralType.M:
      return 'OOOOO'
  }
}

function subgiantZones (type, dec) {
  switch (type) {
    case SpectralType.B:
      if (dec < 5) return '-------IIIIIHO'
      return '---IIIIIIHOOOO'

    case SpectralType.A:
      if (dec < 5) return '-IIIIIIHOOOOOO'
      return 'IIIIIIHOOOOOOO'

    case SpectralType.F:
      if (dec < 5) return 'IIIIIIHOOOOOOO'
      return 'IIIIIHOOOOOOOO'

    case SpectralType.G:
      return 'IIIIIHOOOOOOOO'

    case SpectralType.K:
      return 'IIIIHOOOOOOOOO'
  }
}

function weakerSupergiantZones (type, dec) {
  switch (type) {
    case SpectralType.B:
      if (dec < 5) return ' -------IIIIIHO'
      return ' -----IIIIIHOOO'

    case SpectralType.A:
      if (dec < 5) return ' ----IIIIIIHOOO'
      return ' ----IIIIIHOOOO'

    case SpectralType.F:
      if (dec < 5) return ' ----IIIIIHOOOO'
      return ' ---IIIIIIHOOOO'

    case SpectralType.G:
      if (dec < 5) return ' ---IIIIIIHOOOO'
      return ' .---IIIIIHOOOO'

    case SpectralType.K:
      if (dec < 5) return ' ...-IIIIIHOOOO'
      return ' ....-IIIIIHOOO'

    case SpectralType.M:
      if (dec < 5) return ' .....IIIIIHOOO'
      if (dec < 9) return ' ......IIIIIHOO'
      return ' .......IIIIHOO'
  }
}

function whiteDwarfZones (type) {
  switch (type) {
    case SpectralType.B:
      return 'HOOOO'

    default:
      return 'OOOOO'
  }
}

class Star {
  constructor () {
    this.spectralType = ''
    this.spectralDecimal = ''
    this.size = ''
  }

  toString () {
    return `${this.spectralType}${this.spectralDecimal}-${this.size}`
  }
}

export class CompanionStar extends Star {
  constructor (primaryStar, companionNumber) {
    super()

    this.spectralType = select(
      constrain(1, 12, d2() + primaryStar.spectralTypeRoll),
      [
        [[1], SpectralType.B],
        [[2], SpectralType.A],
        [[3, 4], SpectralType.F],
        [[5, 6], SpectralType.G],
        [[7, 8], SpectralType.K],
        [range(9, 12), SpectralType.M]
      ]
    )

    this.spectralDecimal = Math.floor(Math.random() * 10)

    this.size = select(d2(), [
      [[0], Size.Ia],
      [[1], Size.Ib],
      [[2], Size.II],
      [[3], Size.III],
      [[4], Size.IV],
      [[5, 6, 10, 11, 12], Size.D],
      [[7, 8], Size.V],
      [[9], Size.VI]
    ])

    if (
      this.size === Size.IV &&
      ((this.spectralType === SpectralType.K && this.spectralDecimal >= 5) ||
        this.spectralType === SpectralType.M)
    ) {
      this.size = Size.V
    }

    if (
      this.size === Size.VI &&
      (this.spectralType === SpectralType.B ||
        this.spectralType === SpectralType.A ||
        (this.spectralType === SpectralType.F && this.spectralDecimal < 5))
    ) {
      this.size = Size.V
    }

    this.orbit = select(
      constrain(0, 12, companionNumber === 1 ? d2() : d2() + 4),
      [
        [range(0, 3), 'Close'],
        [[4], 1],
        [[5], 2],
        [[6], 3],
        [[7], () => 4 + d()],
        [[8], () => 5 + d()],
        [[9], () => 6 + d()],
        [[10], () => 7 + d()],
        [[11], () => 8 + d()],
        [[12], () => `Far (${d()}000 AU)`]
      ]
    )
  }
}

export class PrimaryStar extends Star {
  constructor () {
    super()

    this.spectralTypeRoll = d2()

    this.spectralType = select(this.spectralTypeRoll, [
      [[0, 1], SpectralType.B],
      [[2], SpectralType.A],
      [range(3, 7), SpectralType.M],
      [[8], SpectralType.K],
      [[9], SpectralType.G],
      [[10, 11, 12], SpectralType.F]
    ])

    this.spectralDecimal = Math.floor(Math.random() * 10)

    this.size = select(d2(), [
      [[0], Size.Ia],
      [[1], Size.Ib],
      [[2], Size.II],
      [[3], Size.III],
      [[4], Size.IV],
      [range(5, 10), Size.V],
      [[11], Size.VI],
      [[12], Size.D]
    ])

    if (
      this.size === Size.IV &&
      ((this.spectralType === SpectralType.K && this.spectralDecimal >= 5) ||
        this.spectralType === SpectralType.M)
    ) {
      this.size = Size.V
    }

    if (
      this.size === Size.VI &&
      (this.spectralType === SpectralType.B ||
        this.spectralType === SpectralType.A ||
        (this.spectralType === SpectralType.F && this.spectralDecimal < 5))
    ) {
      this.size = Size.V
    }

    let orbitDM = 0
    if (this.size === Size.III) orbitDM += 4
    else if (
      this.size === Size.Ia ||
      this.size === Size.Ib ||
      this.size === Size.II
    ) {
      orbitDM += 8
    }
    if (this.spectralType === SpectralType.M) orbitDM -= 4
    else if (this.spectralType === SpectralType.D) orbitDM -= 2
    this.maximumOrbit = Math.max(0, d2() + orbitDM)
  }
}
