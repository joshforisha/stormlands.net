import Orbit from '/lib/Orbit'
import World from '/lib/World'
import { SpectralType } from '/lib/Star'
import { constrain, d2, isin } from '/lib/Utils'

export default class Terrestrial extends World {
  constructor ({ habitableOffset, orbit, primaryStar }) {
    super()

    let sizeDM = 0
    if (orbit === 0) sizeDM -= 5
    else if (orbit === 1) sizeDM -= 4
    else if (orbit === 2) sizeDM -= 3
    if (primaryStar.spectralType === SpectralType.M) sizeDM -= 2
    this.size = constrain(0, 10, d2() - 2 + sizeDM)

    let atmosphereDM = 0
    if (orbit === Orbit.Hot) atmosphereDM -= 2
    else if (orbit === Orbit.Cold) atmosphereDM -= 4
    if (this.size === 0) this.atmosphere = 0
    else if (habitableOffset >= 2 && d2() === 12) this.atmosphere = 10
    else this.atmosphere = constrain(0, 15, d2() - 7 + this.size + atmosphereDM)

    let hydroDM = 0
    if (orbit === Orbit.Cold) hydroDM -= 2
    if (this.atmosphere <= 1 || this.atmosphere >= 10) hydroDM -= 4
    if (this.size <= 1 || orbit === Orbit.Hot) this.hydrographics = 0
    else this.hydrographics = constrain(0, 10, d2() - 7 + this.size + hydroDM)

    let populationDM = 0
    if (orbit === Orbit.Hot) populationDM -= 5
    else if (orbit === Orbit.Cold) populationDM -= 3
    if (!isin([0, 5, 6, 8], this.atmosphere)) populationDM -= 2
    this.population = constrain(0, 10, d2() - 2 + populationDM)
  }

  toClassificationString () {
    const size = this.size === 0 ? 'S' : this.size.toString(16)
    const atmosphere = this.atmosphere.toString(16)
    const hydrographics = this.hydrographics.toString(16)
    const population = this.population.toString(16)

    return `${size}${atmosphere}${hydrographics}${population}`
  }
}
