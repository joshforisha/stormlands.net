export default class Hex {
  constructor ({ label, system }) {
    this.label = label
    this.system = system
  }
}
