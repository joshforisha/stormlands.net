export function Enum (values) {
  const enumObject = {}
  values.forEach(value => {
    enumObject[value] = value
  })
  return enumObject
}

export function capital (string) {
  return string[0].toUpperCase() + string.substring(1)
}

export function chance (pct) {
  return Math.random() < pct
}

export function cond (obj) {
  return Object.keys(obj).filter(
    k => (typeof obj[k] === 'function' ? obj[k]() === true : obj[k] === true)
  )
}

export function constrain (min, max, x) {
  return Math.max(min, Math.min(max, x))
}

export function d () {
  return Math.ceil(Math.random() * 6)
}

export function d2 () {
  return Math.ceil(Math.random() * 6) + Math.ceil(Math.random() * 6)
}

export function draw (items) {
  return items[Math.floor(Math.random() * items.length)]
}

export function d66 (count = 1) {
  let total = 0
  for (let i = 0; i < count; i++) {
    total += d() * 10 + d()
  }
  return total
}

export function filterMap (xs, fn) {
  return xs
    .map((x, i) => {
      const value = fn(x, i)
      return [value, typeof value !== 'undefined']
    })
    .filter(([, pass]) => pass)
    .map(([value]) => value)
}

export function initialize (count, fn) {
  const values = []
  for (let i = 0; i < count; i++) {
    values.push(fn(i))
  }
  return values
}

export function isin (xs, y) {
  return xs.some(x => y === x)
}

export function pad (char, length, num) {
  let string = num.toString(10)
  while (string.length < length) string = `${char}${string}`
  return string
}

export function range (a, b) {
  const vals = []
  for (let i = 0; i <= b - a; i++) {
    vals.push(a + i)
  }
  return vals
}

export function roll (numDice, target = 8) {
  let total = 0
  for (let i = 0; i < numDice; i++) {
    total += d()
  }
  return total >= target
}

export function select (value, matches) {
  for (let i = 0; i < matches.length; i++) {
    if (matches[i][0].some(x => value === x)) {
      if (typeof matches[i][1] === 'function') {
        return matches[i][1]()
      }
      return matches[i][1]
    }
  }
}

export function svg (el, attributes = {}, nsAttributes = {}, children = []) {
  const e = document.createElementNS('http://www.w3.org/2000/svg', el)

  Object.keys(attributes).forEach(k => {
    e.setAttribute(k, attributes[k])
  })

  Object.keys(nsAttributes).forEach(k => {
    e.setAttributeNS('http://www.w3.org/1999/xlink', k, nsAttributes[k])
  })

  children.forEach(child => {
    e.appendChild(child)
  })

  return e
}

export function zeroPad (length, num) {
  return pad('0', length, num)
}
