import Demeanor from '~/lib/Demeanor'

export const friendlyBearings = [
  'alliance',
  'comfort',
  'gratitude',
  'shelter',
  'happiness',
  'support',
  'promise',
  'delight',
  'aid',
  'celebration'
]

export const hostileBearings = [
  'death',
  'capture',
  'judgement',
  'combat',
  'surrender',
  'rage',
  'resentment',
  'submission',
  'injury',
  'destruction'
]

export const inquisitiveBearings = [
  'questions',
  'investigation',
  'interest',
  'demand',
  'suspicion',
  'request',
  'curiosity',
  'skepticism',
  'command',
  'petition'
]

export const insaneBearings = [
  'madness',
  'fear',
  'accident',
  'chaos',
  'idiocy',
  'illusion',
  'turmoil',
  'confusion',
  'façade',
  'bewilderment'
]

export const knowingBearings = [
  'report',
  'effects',
  'examination',
  'records',
  'account',
  'news',
  'history',
  'telling',
  'discourse',
  'speech'
]

export const mysteriousBearings = [
  'rumor',
  'uncertainty',
  'secrets',
  'misdirection',
  'whispers',
  'lies',
  'shadows',
  'enigma',
  'obscurity',
  'conundrum'
]

export const prejudicedBearings = [
  'reputation',
  'doubt',
  'bias',
  'dislike',
  'partiality',
  'belief',
  'view',
  'discrimination',
  'assessment',
  'difference'
]

export const schemingBearings = [
  'intent',
  'bargain',
  'means',
  'proposition',
  'plan',
  'compromise',
  'agenda',
  'arrangement',
  'negotiation',
  'plot'
]

export function bearingsFor (demeanor) {
  switch (demeanor) {
    case Demeanor.Friendly:
      return friendlyBearings
    case Demeanor.Hostile:
      return hostileBearings
    case Demeanor.Inquisitive:
      return inquisitiveBearings
    case Demeanor.Insane:
      return insaneBearings
    case Demeanor.Knowing:
      return knowingBearings
    case Demeanor.Mysterious:
      return mysteriousBearings
    case Demeanor.Prejudiced:
      return prejudicedBearings
    case Demeanor.Scheming:
      return schemingBearings
  }
}
