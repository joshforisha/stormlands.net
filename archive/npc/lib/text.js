const vowels = ['a', 'e', 'i', 'o', 'u']

export function capital (word) {
  return [word[0].toUpperCase(), word.slice(1)].join('')
}

export function indef (word, include = false) {
  const lowerWord = word.toLowerCase()

  let article = 'a'
  if (vowels.some(l => lowerWord.startsWith(l))) {
    article = 'an'
  }

  if (include) return `${article} ${word}`
  return article
}

export function presentTense (verb) {
  if (verb.endsWith('s')) return `${verb}es`
  return `${verb}s`
}
