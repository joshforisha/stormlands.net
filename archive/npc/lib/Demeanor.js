export default {
  Friendly: 'friendly',
  Hostile: 'hostile',
  Inquisitive: 'inquisitive',
  Insane: 'insane',
  Knowing: 'knowing',
  Mysterious: 'mysterious',
  Prejudiced: 'prejudiced',
  Scheming: 'scheming'
}
