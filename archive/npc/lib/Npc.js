import Demeanor from '~/lib/Demeanor'
import focuses from '~/lib/focuses'
import modifiers from '~/lib/modifiers'
import motivationNouns from '~/lib/motivation-nouns'
import motivationVerbs from '~/lib/motivation-verbs'
import nouns from '~/lib/nouns'
import waylayModifiers from '~/lib/waylay-modifiers'
import waylayNouns from '~/lib/waylay-nouns'
import waylaySolutions from '~/lib/waylay-solutions'
import { bearingsFor } from '~/lib/bearings'
import { draw, initialize, takeRandom, zip } from '~/lib/array'
import { randomInt } from '~/lib/number'

function generateBearing (demeanor) {
  return draw(bearingsFor(demeanor))
}

function generateMood () {
  const roll = randomInt(1, 100)
  if (roll < 6) return 'withdrawn'
  if (roll < 16) return 'guarded'
  if (roll < 31) return 'cautious'
  if (roll < 61) return 'neutral'
  if (roll < 86) return 'sociable'
  if (roll < 96) return 'helpful'
  return 'forthcoming'
}

function generateMotivations () {
  return zip(
    [draw(motivationVerbs), draw(motivationVerbs), draw(motivationVerbs)],
    takeRandom(3, motivationNouns)
  ).map(([verb, noun]) => `${verb} ${noun}`)
}

function generatePowerLevel () {
  const roll = randomInt(1, 100)
  if (roll < 6) return 'much weaker than'
  if (roll < 21) return 'slightly weaker than'
  if (roll < 81) return 'comparable to'
  if (roll < 96) return 'slightly stronger than'
  return 'much stronger than'
}

function generateWaylay (withSolution) {
  const modifier = draw(waylayModifiers)
  const noun = draw(waylayNouns)
  let waylay = `${modifier} ${noun}`
  if (withSolution) {
    const solution = draw(waylaySolutions)
    waylay += ` overcome by ${solution}`
  }
  return waylay
}

export function generate () {
  const demeanor = draw(Object.values(Demeanor))

  return {
    bearing: generateBearing(demeanor),
    demeanor,
    focus: draw(focuses),
    modifier: draw(modifiers),
    mood: generateMood(),
    motivations: generateMotivations(),
    nestedWaylays: initialize(3, () => generateWaylay(true)),
    noun: draw(nouns),
    powerLevel: generatePowerLevel(),
    themeWaylay: generateWaylay(false)
  }
}
