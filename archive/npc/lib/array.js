export function draw (xs) {
  return xs[Math.floor(Math.random() * xs.length)]
}

export function initialize (count, init) {
  const xs = []
  for (let i = 0; i < count; i++) xs.push(init(i))
  return xs
}

export function omit (xs, index) {
  return [...xs.slice(0, index), ...xs.slice(index + 1)]
}

export function takeRandom (count, xs) {
  if (count === 0) return []
  if (count === 1) return [draw(xs)]

  const index = Math.floor(Math.random() * xs.length)
  return [xs[index], ...takeRandom(count - 1, omit(xs, index))]
}

export function zip (...xss) {
  if (xss.length === 0) return []
  if (xss.length === 1) return xss

  return xss[0].map((_, i) => xss.map((x) => x[i]))
}
