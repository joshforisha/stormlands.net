import styled from 'styled-components'

export default styled.button`
  background-color: var(--black);
  border-width: 0px;
  color: var(--white);
  cursor: pointer;
  font-size: 1.25em;
  padding: var(--medium) var(--large);
  transition: background-color 32ms ease-out;

  &:hover {
    background-color: var(--purple);
  }
`
