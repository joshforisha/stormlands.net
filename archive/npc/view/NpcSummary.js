import * as React from 'react'
import styled from 'styled-components'
import { capital, indef } from '~/lib/text'

const Container = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  font-size: 1.2em;
`

const List = styled.ul`
  margin-top: 0px;
`

const ListHeader = styled.p`
  margin-bottom: 0px;
  text-align: center;
`

const Sentence = styled.p`
  text-align: center;
`

export default function NpcSummary ({ npc }) {
  return (
    <Container>
      <Sentence>
        {capital(indef(npc.modifier))} {npc.modifier} {npc.noun},{' '}
        {npc.powerLevel} the party,
        <br />
        {npc.motivations[0]}, {npc.motivations[1]}, and {npc.motivations[2]}.
      </Sentence>
      <Sentence>
        They’re {npc.mood} and {npc.demeanor}, and speaks of {npc.bearing}{' '}
        regarding {npc.focus}.
      </Sentence>
      <ListHeader>Waylay: {npc.themeWaylay}</ListHeader>
      <List>
        <li>Exposition: {npc.nestedWaylays[0]}</li>
        <li>Conflict: {npc.nestedWaylays[1]}</li>
        <li>Climax: {npc.nestedWaylays[2]}</li>
      </List>
    </Container>
  )
}
