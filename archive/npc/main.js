import * as Npc from '~/lib/Npc'
import * as React from 'react'
import Button from '~/view/Button'
import NpcSummary from '~/view/NpcSummary'
import styled from 'styled-components'
import { render } from 'react-dom'

const Container = styled.div`
  margin: 0px auto;
  max-width: 1200px;
  padding: var(--medium);
`

const Header = styled.header`
  border-bottom: 1px solid var(--black);
  padding: var(--large) 0px;
  text-align: center;
`

function Root () {
  const [npc, setNpc] = React.useState(null)

  function generateNpc () {
    setNpc(Npc.generate())
  }

  React.useEffect(() => {
    if (npc === null) {
      setNpc(Npc.generate())
    }
  }, [npc, setNpc])

  let npcSummary = null
  if (npc) npcSummary = <NpcSummary npc={npc} />

  return (
    <Container>
      <Header>
        <Button onClick={generateNpc}>Generate</Button>
      </Header>
      {npcSummary}
    </Container>
  )
}

render(<Root />, document.getElementById('Root'))
