# Enterprise Moves

> #### ACQUISITION (+Cargo)
> When you demand important services or assets from a market ablet to supply those demands, Roll+0. If you offer a cargo unit as part of the deal, Roll+ that cargo's Class.
>
> <u>On a 13+</u>, the deal goes through; you get what you asked for.
> <u>On a 10–12</u>, the deal goes through if the seller/market is amiably disposed toward the deal. Otherwise, as 7–9.
> <u>On a 7–9</u>, the deal will only go through if you accept a cost, a task or alesser asset/service instead of what you asked for.
> <u>On a 6–</u>, the deal will only go through if you call in a Favor.

> #### BARTER (+Cargo)
> When you exchange a foreign unit of cargo for local trade goods, Roll+ the Class of the cargo unit.
>
> <u>On a 13+</u>, you attract the attention of a faction or individual with a unique item or service to trade.
> <u>On a 10–12</u>, you get a higher Class cargo of local goods in exchange, to a max of Class 4.
> <u>On a 7–9</u>, you get a higher Class cargo, to a max of Class 4, and the GM chooses one flaw:
> - The negotiations take many days to complete.
> - The goods need special care (fragile, hazardous, etc.).
> - The goods are very odd, distasteful or bizarre.
> - The provenance or legality of the goods is dubious.

> #### CRAMPED QUARTERS
> When you've been trapped in cramped quarters with the same people for a significant amount of time (a leg of an interstellar journey, etc.), choose a character trapped here with you and Roll 2d6.
>
> <u>On a 10+</u>, describe how the two of you bonded over the past few days.
> <u>On a 7–9</u>, reveal/discover the answer to their question about an aspect of yourself or your past.
> <u>On a 6-</u>, describe what caused the newest hurt feelings or bad blood between you.

> #### SHIELDS UP
> When one of the starship sections would suffer damage from outside, the pilot Rolls 2d6. If a character is at the shield station, they Roll instaed, and add their +Interface.
>
> <u>On a 10+</u>, the severity of the damage is reduced by two.
> <u>On a 7–9</u>, the severity of the damage is reduced by one.
> <u>On a 6-</u>, shields down! The section suffers damage, and the shields need to be repaired or recharged before they can be used again.

> #### WILD JUMP
> When you force your ship to make a Wild Jump, Roll 2d6.
>
> <u>On a 10+</u>, the crew only suffers nausea, headaches and other minor effects. You reach a point within a week's travel of your destination, or choose from the list below.
> <u>On a 7–9</u>, the illness and hallucinations are pronounced. The GM chooses one from the list below:
> - You find an uncharted world, ready for exploration.
> - You find exploitable resources, there for the taking.
> - You discover a scientifically-interesting phenomenon.
> - You discover wreckage or ruins of unknown origin.
> - You find a new path to a well-known destination.
> - You encounter a faction or culture that is new to you.
>
> <u>On a 6-</u>, the GM will describe the ugly, debilitating, terrifying consequences. It's full of stars.
