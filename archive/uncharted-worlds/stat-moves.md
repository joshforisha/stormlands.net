# Stat Moves

> #### ACCESS (+Interface)
> When you spend several minutes accessing a locked, protected system or network with the appropriate tools, Roll+Interface.
>
> <u>On a 10+</u>, credentials verified, access granted. The system is now open to Interface-based Moves.
> <u>On a 7–9</u>, as above, but your breach is detected. The owners of the system will likely retaliate soon, either electronically, legally or physically.

> #### BRACE FOR IMPACT (+Armor)
> When you would suffer harm, the GM will tell you the Severity (Minor, Major, Severe, Critical or Fatal). Roll+Armor.
>
> <u>On a 13+</u>, the severity is reduced by two levels.
> <u>On a 10–12</u>, the severity is reduced by one level.
> <u>On a 7–9</u>, you suffer an injury of that severity.
> <u>On a 6-</u>, you also suffer debilities, incur costs/troubles, or suffer an injury of a graeter severity, at the GM's discretion.

> #### COMMAND (+Influence)
> When you issue a command to a group that is inclined to follow your orders, Roll+Influence.
>
> <u>On a 10+</u>, they follow those orders to the best of their ability, though there may be costs in time, resources or personnel.
> <u>On a 7–9</u>, as above, but their disposition or effectiveness has been significantly impacted in some way. This crew will not accept a new Command until those issues have been dealt with.

> #### LAUNCH ASSAULT (+Physique)
> When you engage enemy forces in chaotic close-quarters combat, describe your tactics and primary targets, then Roll+Physique.
>
> <u>On a 10+</u>, you win this engagement. Your targets are dead, injured, incapacitated, retreating, pinned, surrendering, etc.
> <u>On a 7–9</u>, as above, but the GM will choose 1 or more of the following consequences:
> - You suffer harm during the exchange.
> - The exchange causes undesirable collateral damage.
> - The battle shifts, changing threats or adding new ones.
> - The targets actually suffer a lesser fate (GM chooses).

> #### OPEN FIRE (+Mettle)
> When you engage enemy forces in long ranged, cover-to-cover firefights, describe your tactics, primary targets and Roll+Mettle.
>
> <u>On a 10+</u>, you win this engagement. Your targets are dead, injured, incapacitated, retreating, pinned, surrendering, etc.
> <u>On a 7–9</u>, as above, but the GM will choose 1 or more of the following consequences:
> - You suffer harm during the exchange.
> - The exchange causes undesirable collateral damage.
> - The battle shifts, changing threats or adding new ones.
> - The targets actually suffer a lesser fate (GM chooses).

> #### PATCH UP (+Expertise)
> When using appropriate medical supplies/tools to repair harm to people or machinery, Roll+Expertise.
>
> <u>On a 10+</u>, choose 1 from the list below.
> <u>On a 7–9</u>, choose 1, but you've reached the limit of what you can do; you cannot re-attempt to Patch Up the subject for now.
> - Treat a single minor, major or severe injury/damage
> - Treat a malfunction or minor debility
> - Stabilize a major debility
> - Perform a medical/technical procedure
