# Common Moves

> #### ASSESSMENT (+Stat)
> When you collect critical information about an important, dangerous or mysterious subject using...
> ...stealth, focus or cunning, Roll+Mettle.
> ...research and experimentation, Roll+Expertise.
> ...exploration, labor or strenuous activity, Roll+Physique.
> ...informants, interviews or gossip, Roll+Influence.
> ...the SectorNet or open networks, Roll+Interface.
>
> <u>On a 10+</u>, you gain significant information about the subject, and earn a Data Point about it as well.
> <u>On a 7–9</u>, the GM will reveal interesting, potentially useful information about the subject. Or they might ask you to do so.
> <u>On a 6-</u>, the GM will reveal facts about the subject you probably wish were not true.

> #### FACE ADVERSITY (+Stat)
> When you overcome opposition or danger using...
> ...stealth, piloting, accuracy or discipline, Roll+Mettle.
> ...knowledge, mechanics or first aid, Roll+Expertise.
> ...athletics, endurance, strength or health, Roll+Physique.
> ...charm, diplomacy, bargaining or lies, Roll+Influence.
> ...open computer systems and networks, Roll+Interface.
>
> <u>On a 10+</u> you overcome the opposition or danger, just as you described.
> <u>On a 7–9</u>, the danger is overcome, but at a price; the GM will offer you a cost or a hard choice.

> #### GET INVOLVED (+Stat)
> When an ally makes a Move and you affect the result using...
> ...stealth, piloting, accuracy, or bravery, Roll+Mettle.
> ...education, mechanics or first aid, Roll+Expertise.
> ...athletics, endurance, strength or health, Roll+Physique.
> ...charm, diplomacy, bargaining or lies, Roll+Influence.
> ...open computer systems and networks, Roll+Interface.
>
> <u>On a 10+</u> Choose 1
> - Turn a failure (6-) into a partial success (7–9).
> - Turn a partial success (7–9) into a full success (10+).
> - Turn a full success (10+) into a partial success (7–9).
> - Turn a partial success (7–9) into a failure (6-).
>
> <u>On a 7–9</u>, as above, but you incur a cost, complication or hard choice in order to get involved.
