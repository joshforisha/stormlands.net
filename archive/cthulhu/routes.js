import * as React from 'react'
import GenerateNpc from '~/pages/GenerateNpc'
import Madness from '~/pages/Madness'
import Page from '~/view/Page'
import equipmentCosts from '~/content/equipment-costs.md'
import skillDetails from '~/content/skill-details.md'
import skillList from '~/content/skill-list.md'
import travelCosts from '~/content/travel-costs.md'
import weapons from '~/content/weapons.md'

const routes = {
  '#/': () => null,
  '#/equipment-costs': () => <Page content={equipmentCosts} />,
  '#/generate-npc': () => <GenerateNpc />,
  '#/madness': () => <Madness />,
  '#/skill-details': () => <Page content={skillDetails} />,
  '#/skill-list': () => <Page content={skillList} />,
  '#/travel-costs': () => <Page content={travelCosts} />,
  '#/weapons': () => <Page content={weapons} />
}

export default routes
