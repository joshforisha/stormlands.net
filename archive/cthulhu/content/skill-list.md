# Skill List

- Accounting (05%)
- Animal Handling (05%) *Uncommon*
- Anthropology (01%)
- Appraise (05%)
- Archaeology (01%)
- Art and Craft (Acting) (05%)
- Art and Craft (Fine Art) (05%)
- Art and Craft (Forgery) (05%)
- Art and Craft (Photography) (05%)
- Artillery (01%) *Uncommon*
- Charm (15%)
- Climb (20%)
- Credit Rating (00%)
- Cthulhu Mythos (00%)
- Demolitions (01%) *Uncommon*
- Disguise (05%)
- Diving (01%) *Uncommon*
- Dodge (half DEX)
- Drive Auto (20%)
- Electrical Repair (10%)
- Fast Talk (05%)
- Fighting (Axe) (15%)
- Fighting (Brawl) (25%)
- Fighting (Flail) (10%)
- Fighting (Garrote) (15%)
- Fighting (Spear) (20%)
- Fighting (Sword) (20%)
- Fighting (Whip) (05%)
- Firearms (Bow) (15%)
- Firearms (Flamethrower) (10%)
- Firearms (Handgun) (20%)
- Firearms (Heavy Weapons) (10%)
- Firearms (Machine Gun) (10%)
- Firearms (Rifle/Shotgun) (25%)
- Firearms (Submachine Gun) (15%)
- First Aid (30%)
- History (05%)
- Hypnosis (01%) *Uncommon*
- Intimidate (15%)
- Jump (20%)
- Language (Other) (01%) *Group*
- Language (Own) (EDU)
- Law (05%)
- Library Use (20%)
- Listen (20%)
- Locksmith (01%)
- Lore (01%) *Group, Uncommon*
- Mechanical Repair (10%)
- Medicine (01%)
- Natural World (10%)
- Navigate (10%)
- Occult (05%)
- Operate Heavy Machinery (01%)
- Persuade (10%)
- Pilot (01%) G
- Psychoanalysis (01%)
- Psychology (10%)
- Read Lips (01%) *Uncommon*
- Ride (05%)
- Science (Astronomy) (01%)
- Science (Biology) (01%)
- Science (Botany) (01%)
- Science (Chemistry) (01%)
- Science (Cryptology) (01%)
- Science (Engineering) (01%)
- Science (Forensics) (01%)
- Science (Geology) (01%)
- Science (Mathematics) (10%)
- Science (Meteorology) (01%)
- Science (Pharmacy) (01%)
- Science (Physics) (01%)
- Science (Zoology) (01%)
- Sleight of Hand (10%)
- Spot Hidden (25%)
- Stealth (20%)
- Survival (10%) *Group*
- Swim (20%)
- Throw (20%)
- Track (10%)
