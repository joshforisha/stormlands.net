# Equipment Costs – 1920s

All costs are for an average item unless stated otherwise. Prices vary due to scarcity and demand, and Keepers may decrease or increase costs accordingly.

## Men's Clothing

- **Worsted Wool Dress Suit** *$17.95*
- **Cashmere Dress Suit** *$18.50*
- **Suit, mohair** *$13.85*
- **Corduroy Norfolk Suit** *$9.95*
- **Union suit, Forest Mills** *69¢*
- **Outdoor Coat** *$9.95–$35.00*
- **Dog Fur Overcoat** *$37.50*
- **Chesterfield Overcoat** *$19.95*
- **Oxford Dress Shoes** *$6.95*
- **Leather Work Shoes** *$4.95*
- **Slacks, white flannel** *$8.00*
- **Lace-bottom Breeches** *$4.95*
- **Shirt, percale** *79¢–$1.25*
- **Broadcloth Dress Shirt** *$1.95*
- **Shaker Sweater** *$7.69*
- **Felt Fedora** *$4.95*
- **Wool Golf Cap** *79¢*
- **Straw Hat** *$1.95*
- **Padded Leather Football Helmet** *$3.65*
- **Sweatshirt** *98¢*
- **Sealskin Fur Cap** *$16.95*
- **Silk Four-in-hand Tie** *96¢*
- **Necktie, silk** *50¢*
- **Batwing Bow Tie** *55¢*
- **Sock Garters** *39¢*
- **Cotton Union Suit** *$1.48*
- **Cufflinks** *40¢*
- **Leather Belt** *$1.35*
- **Suspenders** *79¢*
- **Hiking Boots** *$7.25*
- **Shoes with cleats** *$5.25*
- **Bathing Suit** *$3.45*
- **Canvas Bathing Shoes** *75¢*

## Women's Clothing

- **Chic Designer Dress** *$90.00+*
- **Silk Crepe Dress** *$13.95*
- **Silk Taffeta Dress** *$10.95*
- **Satin Charmeuse** *$10.95*
- **Gingham Dress** *$2.59*
- **French Repp Dress** *$10.95*
- **Pleated Skirt, silk** *7.95*
- **Blouse, cotton** *$1.98*
- **Worsted Wool Sweater** *$9.48*
- **Cotton Crepe Negligee** *88¢*
- **Spike-heeled Parisian Shoes** *$4.45*
- **Leather One-strap Slippers** *98¢*
- **Snug Velour Hat** *$4.44*
- **Satin Turban-style Hat** *$3.69*
- **Rayon Elastic Corset** *$2.59*
- **Embroidered Costume Slip** *$1.59*
- **Silk Hose (3 pairs)** *$2.25*
- **Bloomers, silk** *$3.98–4.98*
- **Tweed Jacket, fully lined** *$3.95*
- **Velour Coat with Fur Trim** *$39.75*
- **Brown Fox Fur Coat** *$198.00*
- **Belted Rain Coat, cotton** *$3.98*
- **Velted Rain Coat, Schappe silk** *$8.98*
- **Silk Handbag** *$4.98*
- **Dress Hair Comb** *98¢*

#### Outdoor Shirt:

- **Khaki Jean Material** *$1.79*
- **All Wool Tweed or Linen** *$2.98*

#### Outdoor Knee-Length Knickers:

- **Khaki Jean Material** *$1.79*
- **Grey Wool Tweed** *$2.98*
- **White Linen** *$2.98*
- **Khaki Leggings (Ankle to knee)** *98¢*
- **Outdoor Boots** *$2.59*
- **Bathing Suit** *$4.95*
- **Bathing Cap** *40¢*
- **Canvas Bathing Shoes** *54¢*
- **Shoes, Pumps** *$1.29*

## Personal Care

- **Make-up Kit** *$4.98*
- **Men's Toilet Set (10 pieces)** *$9.98*
- **Women's Toilet Set (15 pieces)** *$22.95*
- **Hair Colorator** *79¢*
- **Curling Iron, Wavette** *$2.19*
- **Hair Brush** *89¢*
- **Hair Net, Zephyr (4)** *25¢*
- **Mouthwash, Listerine** *79¢*
- **Shampoo, Coconut Oil** *50¢*
- **Soap (12 Cakes)** *$1.39*
- **Talcum powder** *19¢*
- **Toothpaste, Pepsodent** *39¢*

## Food & Beverages

- **Bacon, sugar cured** *25¢/lb*
- **Beef, Pot Roast** *10¢/lb*
- **Bologna & Frankfurters** *19¢/lb*
- **Butter, Brookfield Creamery** *43¢/lb*
- **Carrots** *10¢/bunch*
- **Cookies, Fig Bars** *25¢/lb*
- **Corn** *10¢*
- **Eggs** *13¢/dozen*
- **Ham, smoked calf** *13¢/lb*
- **Lamb, Leg** *40¢/lb*
- **Lard, Swift's Pure** *17¢/lb*
- **Lettuce** *10¢*
- **Oil, Olive, Imported** *$2.25/half gallon*
- **Oranges** *50¢/dozen*
- **Peaches** *15¢/basket*
- **Peas** *25¢/2 qt*
- **Potatoes** *45¢/4 qt*
- **Prunes, California** *24¢/2 lb*
- **Rice** *20¢/3 lb*
- **Tomatoes** *10¢/can*

## Meals Out

- **Chicken, Dinner** *$2.50/person*
- **Breakfast** *45¢*
- **Lunch** *65¢*
- **Dinner** *$1.25*

## Speakeasy Prices

- **Rotgut Gin (Shot)** *10¢*
- **Cocktail** *25¢*
- **Wine (Glass)** *75¢*
- **Beer (Glass)** *20¢*
- **Whiskey (Glass)** *25¢*
- **Coca-Cola (12 oz)** *5¢*

## Furniture

- **Bedroom Set, walnut finish** *$199.00*
- **Arm Chair, French split cane** *$25.00*
- **Rocking Chair** *$25.00*
- **Desk** *$80.00*
- **Dining Room Set, walnut finish** *$295.00*
- **Living Room Set, upholstered mohair** *$395.00*
- **Rug, oriental** *$20.00–$50.00*

## Household Goods

- **Fan, "GE Whiz"** *$10.00*
- **Dinnerware, 52 pieces** *$19.95*
- **Lamp, Glass Shade** *$5.00*
- **Gas Range** *$77.00*
- **Refrigerator** *$49.50*
- **Vacuum Cleaner** *$48.00*
- **Washing Machine** *$125.00*

## Medical Equipment

- **Aspirin (12 pills)** *10¢*
- **Epsom Salts** *9¢/lb*
- **Indigestion Medicine** *25¢*
- **Laxative, Nature's Remedy** *25¢*
- **Medical Case** *$10.45*
- **Forceps** *$3.59*
- **Scalpel Set** *$1.39*
- **Hypodermic Syringes** *$12.50*
- **Atomizer** *$1.39*
- **Gauze Bandages (5 yd)** *69¢*
- **Clinical Thermometer** *69¢*
- **Alcohol (half gallon)** *20¢*
- **Hard Rubber Syringe** *69¢*
- **Bed Pan** *$1.79*
- **Wheel Chair** *$39.95*
- **Maple Crutches** *$1.59*
- **Adhesive Plaster** *29¢*
- **Metal Arch Supports** *$1.98*
- **Leather Ankle Supports (pair)** *98¢*

## Outdoor & Travel Gear

- **Cooking Kit** *$8.98*
- **Camp Stove** *$6.10*
- **Vacuum Bottle** *89¢*
- **Folding Bathtub** *$6.79*
- **Waterproof Blanket (58x96")** *$5.06*
- **Folding Camp Bed** *$3.65*
- **Carbide Lamp (300' Beam)** *$2.59*
- **Can of Carbide (2 lb)** *25¢*
- **Searchlight** *$5.95*
- **Gasoline Lantern (built-in pump)** *$6.59*
- **Kerosene Lantern** *$1.39*
- **Dark Lantern** *$1.68*
- **Electric Torch** *$1.35–$2.25*
- **Batteries** *60¢*
- **Pen Light** *$1.00*
- **Flare (Disposable)** *27¢*
- **Telescope** *$3.45*
- **Field Glasses (3x to 6x)** *$6.00–$23.00*
- **Binoculars** *$28.50*
- **Jeweled Compass** *$3.25*
- **Compass with Lid** *$2.85*
- **Hunting Knife** *#2.35*
- **Heavy 2-Blade Pocket Knife** *$1.20*
- **Hand Axe** *98¢*
- **Small Live Animal Trap** *$2.48*
- **Coil Spring Animal Trap** *$5.98*
- **Bear Trap** *$11.43*
- **Collapsible Fishing Rod and Tackle Set** *$9.35*
- **Hemp Twine** *27¢*
- **Pedometer** *$1.70*
- **Heavy Canvas Shoulder Bag** *$3.45*
- **Fifteen Hour Candles (dozen)** *62¢*
- **Waterproof Match Case** *48¢*
- **Steel Row Boat (4 seats)** *$35.20*
- **2 HP Motor for Row Boats** *$79.95*
- **Canvas and Wood Canoe** *$75.00*

## Luggage

- **Handle Bag (8 lb)** *$7.45*
- **Suitcase (15 lb)** *$9.95*
- **Steamer Trunk (55 lb)** *$12.00*
- **Wardrobe Trunk (95 lb)** *$54.95*
- **Wardrobe Trunk (115 lb)** *$79.95*
- **Luggage, black patent leather** *$12.50*

## Tents

- **7 x 7 foot Tent** *$11.48*
- **12 x 16 foot Tent** *$28.15*
- **16 x 24 foot Tent** *$53.48*
- **24 x 36 foot Tarpaulin** *$39.35*
- **7 x 7 foot Car Tent** *$12.80*
- **13.5" Iron Tent Stakes (dozen)** *$1.15*
- **Auto Bed** *$8.95*
- **Canteen (1 qt)** *$1.69*
- **Insulated Tank (5 gal)** *$3.98*
- **Water Bag (1 gal)** *80¢*
- **Water Bag (2 gal)** *$1.04*
- **Water Bag (5 gal)** *$2.06*

## Tools

- **Tool Outfit (20 tools)** *$14.90*
- **Hand Drill (Plus 8 Bits)** *$6.15*
- **Large Steel Pulley** *$1.75*
- **Padlock** *95¢*
- **Rope (50 ft)** *$8.60*
- **Light Chain** *10¢/ft*
- **Watchmaker's Tool Kit** *$7.74*
- **Crowbar** *$2.25*
- **Handsaw** *$1.65*
- **Gasoline Blowtorch** *$4.45*
- **Electricians Gloves** *$1.98*
- **Lineman's Tool Belt & Safety Strap** *$3.33*
- **Lineman's Climbers** *$2.52*
- **Jewelers 48-Piece Tool Set** *$15.98*
- **Rotary Tool Grinder** *$6.90*
- **Shovel** *95¢*
- **Home Tool Set in Box** *$14.90*

## Investigator Tools

- **Handcuffs** *$3.35*
- **Extra Handcuff Key** *28¢*
- **Police Whistle** *30¢*
- **Dictaphone** *$39.95*
- **Wire Recorder** *$129.95*
- **Wristwatch** *$5.95*
- **Gold Pocket Watch** *$35.10*
- **Self-filling Fountain Pen** *$1.80*
- **Mechanical Pencil** *85¢*
- **Writing Tablet** *20¢*
- **Straightjacket** *$9.50*
- **Sketch Pad** *25¢*
- **Complete Diving Suit** *$1,200.00*
- **Remington Typewriter** *$40.00*
- **Harris Typewriter** *$66.75*
- **Pocket Microscope** *58¢*
- **100x Desk Microscope** *$17.50*
- **Floor Safe (3 ft high, 950 lb)** *$62.50*
- **Umbrella** *$1.79*
- **Turkish Water Pipe** *99¢*
- **Cigarettes, 1 pack** *10¢*
- **Box of Cigars** *$2.29*
- **Unabridged Dictionary** *$6.75*
- **10-volume Encyclopedia** *$49.00*
- **Wet Sponge Respirator** *$1.95*
- **3-Lens Pocket Magnifying Glass (7x to 30x)** *$1.68*
- **Bible** *$3.98*
- **Briefcase** *$1.48*
- **Globe on Stand** *$9.95*
- **Folding Writing Desk** *$16.65*
- **Glass-Door Oak Bookcase (200 books)** *$24.65*
- **Baby Buggy** *$34.45*
- **Chemical Fire Extinguisher** *$13.85*
- **Watchmaker's Eye Glass** *45¢*

## Communications

- **Telegram (12 words)** *25¢, 2¢/add. word*
- **International Telegram** *$1.25/word*
- **Postage** *2¢/oz*
- **Postcard** *5¢–20¢*
- **Console Radio Receiver** *$49.95*
- **Desk Phone (Bridging Type)** *$15.75*
- **Telegraph Outfit** *$4.25*
- **Newspaper** *5¢*

## Entertainment

- **Movie Ticket, Seated** *15¢*
- **Movie Ticket, Nickelodeon** *5¢*
- **Professional Baseball Ticket** *$1.00*
- **Concert Hall, Public Seating** *$4.00*
- **Concert Hall, Box** *$10.00*
- **4-string Jazz Banjo** *$7.45*
- **Brass Saxophone** *$69.75*
- **Cabinet Phonograph** *$98.00*
- **Phonograph Records** *75¢*
- **Box Brownie Camera** *$2.29–$4.49*
- **Film, 24 Exposures** *38¢*
- **Film Developing Kit** *$4.95*
- **Kodak Folding No. 1 Camera** *$4.25–$28.00*
- **Eastman Commercial Camera** *$140.00*
- **16mm Movie Camera & Projector** *$335.00*
- **Portable Radio Receiver** *$65.00*
- **Accordion** *$8.95*
- **Ukulele (Kit)** *$2.75*
- **Guitar (Kit)** *$9.95*
- **Violin (Kit)** *$14.95*
- **Army Bugle** *$3.45*
- **Parlor Organ** *$127.00*
- **Player Piano** *$447.00*

## Sports & Games

- **150 Clay Marbles** *15¢*
- **25 Glass Marbles** *33¢*
- **Baseball Mitt** *$5.45*
- **Baseball Bat** *$1.30*
- **Baseball Catcher's Pads & Mask Set** *$14.10*
- **Baseball** *55¢*
- **Basketball** *$6.75*
- **Rugby Football** *$4.15*
- **Roller Skates** *$1.65*
- **Tennis Racket & Balls (3-pack)** *$4.82*
- **Beginner Golf Set w/ Bag** *$9.25*
- **Pro Steel Golf Club** *$6.15*
- **Golf Bag** *$5.95*
- **Boxing Gloves** *$3.75*
- **Dumbbells (5 lb, pair)** *$1.68*
- **Bamboo Vaulting Pole, 12 ft** *$7.40*
- **Playing Cards** *59¢*
- **Ouija Board** *98¢*
- **Dominoes** *59¢*
- **Chess Set** *$1.39*
- **Bicycle** *$23.95*
- **Croquet Set** *$2.30*
- **Billiard Cue** *$1.99*
- **Mahjong Set** *$1.80*
