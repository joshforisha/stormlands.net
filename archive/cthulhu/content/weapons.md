# Weapons — 1920s

## Melee Weapons Costs

- **Rapier** *$12.50*
- **Bayonet** *$3.75*
- **Dagger** *$2.50*
- **Straight Razor** *65¢–$5.25*
- **Brass Knuckles** *$1.00*
- **Billy Club (12")** *$1.98*
- **Horsewhip** *60¢*
- **4-pound Wood Axe** *$1.95*
- **16-foot Bullwhip** *$1.75*

## Hand-to-Hand Weapons

| Name | Skill | Damage | Base Range | Uses/round | Bullets | Cost | Malfunction |
| ---- | ----- | ------ | ---------- | ---------- | ------- | ----:| ----------- |
| Bow and Arrows | Firearms (Bow) | 1D6+half DB | 30 yd | 1 | 1 | $7 | 97 |
| Brass Knuckles | Fighting (Brawl) | 1D3+1+DB | Touch | 1 | – | $1 | – |
| Bullwhip | Fighting (Whip) | 1D3+half DB | 10 ft | 1 | – | $5 | – |
| Burning Torch | Fighting (Brawl) | 1D6+Burn | Touch | 1 | – | 5¢ | – |
| Blackjack | Fighting (Brawl) | 1D8+DB | Touch | 1 | – | $2 | – |
| Club, large | Fighting (Brawl) | 1D8+DB | Touch | 1 | – | $3 | – |
| Club, small | Fighting (Brawl) | 1D6+DB | Touch | 1 | – | $3 | – |
| Crossbow *(i)* | Firearms (Bow) | 1D8+2 | 50 yd | 1/2 | 1 | $10 | 96 |
| Garrote *(i)* | Fighting (Garrote) | 1D6+DB | Touch | 1 | – | 50¢ | – |
| Hatchet/Sickle *(i)* | Fighting (Axe) | 1D6+1+DB | Touch | 1 | – | $3 | – |
| Knife, large *(i)* | Fighting (Brawl) | 1D8+DB | Touch | 1 | – | $4 | – |
| Knife, medium *(i)*| Fighting (Brawl) | 1D4+2+DB | Touch | 1 | – | $2 | – |
| Knife, small *(i)* | Fighting (Brawl) | 1D4+DB | Touch | 1 | – | $2 | – |
| Mace Spray | Fighting (Brawl) | Stun | 6 ft | 1 | 25 Squirts | – | – |
| Nunchaku | Fighting (Flail) | 1D8+DB | Touch | 1 | – | $1 | – |
| Rock, thrown | Throw | 1D4+half DB | STR / 5 yd | 1 | – | – | – |
| Shuriken *(i)* | Throw | 1D3+half DB | STR / 5 yd | 2 | One use | 50¢ | 100 |
| Spear *(i)* | Fighting (Spear) | 1D8+1 | Touch | 1 | – | $25 | – |
| Spear, thrown *(i)* | Throw | 1D8+half DB | STR / 5 yd | 1 | – | $1 | – |
| Sword, heavy | Fighting (Sword) | 1D8+1+DB | Touch | 1 | – | $30 | – |
| Sword, medium *(i)* | Fighting (Sword) | 1D6+1+DB | Touch | 1 | – | $15 | – |
| Sword, light *(i)* | Fighting (Sword) | 1D6+DB | Touch | 1 | – | $25 | – |
| Wood Axe *(i)* | Fighting (Axe) | 1D8+2+DB | Touch | 1 | – | $5 | – |

## Firearm Ammunition Costs

- **.22 Long Rifle (Box of 100)** *54¢*
- **.22 Hollow Rifle (Box of 100)** *53¢*
- **.22 Rimfire (Box of 100)** *$1.34*
- **.30-06 Gov't (Box of 100)** *$7.63*
- **.32 Special (Box of 100)** *$5.95*
- **.32-20 Repeater (Box of 100)** *$2.97*
- **.38 Short Round (Box of 100)** *$1.75*
- **.38-55 Repeater (Box of 100)** *$6.60*
- **.44 Hi-Power (Box of 100)** *$4.49*
- **.45 Automatic (Box of 100)** *$4.43*
- **10-Gauge Shell (Box of 25)** *$1.00*
- **10-Gauge Shell (Box of 100)** *$3.91*
- **12-Gauge Shell (Box of 25)** *93¢*
- **12-Gauge Shell (Box of 100)** *$3.63*
- **16-Gauge Shell (Box of 25)** *86¢*
- **16-Gauge Shell (Box of 100)** *$3.34*
- **20-Gauge Shell (Box of 25)** *85¢*
- **20-Gauge Shell (Box of 100)** *$3.30*
- **12-Gauge Single-barrel Shotgun Kit (30")** *$9.20*
- **12-Gauge Double-barrel Shotgun Kit (30")** *$21.35*
- **Extra Magazine for Pistol** *$1.90*

## Handguns *(i)*

| Name | Skill | Damage | Base Range | Uses/round | Bullets | Cost | Malfunction |
| ---- | ----- | ------ | ---------- | ---------- | ------- | ----:| ----------- |
| Flintlock | Firearms (Handgun) | 1D6+1 | 10 yd | 1/4 | 1 | $30 | 95 |
| .22 Short Automatic | Firearms (Handgun) | 1D6 | 10 yd | 1 (3) | 6 | $25 | 100 |
| .25 Derringer (1B) | Firearms (Handgun) | 1D6 | 3 yd | 1 | 1 | $12 | 100 |
| .32 or 7.65mm Revolver | Firearms (Handgun) | 1D8 | 15 yd | 1 (3) | 6 | $15 | 100 |
| .32 or 7.65mm Automatic | Firearms (Handgun) | 1D8 | 15 yd | 1 (3) | 8 | $20 | 99 |
| .38 or 9mm Revolver | Firearms (Handgun) | 1D10 | 15 yd | 1 (3) | 6 | $25 | 100 |
| .38 Automatic | Firearms (Handgun) | 1D10 | 15 yd | 1 (3) | 8 | $30 | 99 |
| Model P08 Luger | Firearms (Handgun) | 1D10 | 15 yd | 1 (3) | 8 | $75 | 99 |
| .41 Revolver | Firearms (Handgun) | 1D10 | 15 yd | 1 (3) | 8 | $30 | 100 |
| .45 Revolver | Firearms (Handgun) | 1D10+2 | 15 yd | 1 (3) | 6 | $30 | 100 |
| .45 Revolver | Firearms (Handgun) | 1D10+2 | 15 yd | 1 (3) | 7 | $40 | 100 |

## Rifles *(i)*

| Name | Skill | Damage | Base Range | Uses/round | Bullets | Cost | Malfunction |
| ---- | ----- | ------ | ---------- | ---------- | ------- | ----:| ----------- |
| .58 Springfield Rifle Musket | Firearms (Rifle) | 1D10+4 | 60 yd | 1/4 | 1 | $25 | 95 |
| .22 Bolt-Action Rifle | Firearms (Rifle) | 1D6+1 | 30 yd | 1 | 6 | $13 | 99 |
| .30 Lever-Action Carbine | Firearms (Rifle) | 2D6 | 50 yd | 1 | 6 | $19 | 98 |
| .45 Martini-Henry Rifle | Firearms (Rifle) | 1D8+1D6+3 | 80 yd | 1/3 | 1 | $20 | 100 |
| Col. Moran's Air Rifle | Firearms (Rifle) | 2D6+1 | 20 yd | 1/3 | 1 | $200 | 88 |
| .303 Lee-Enfield | Firearms (Rifle) | 2D6+4 | 110 yd | 1 | 10 | $50 | 100 |
| .30-06 Bolt-Action Rifle | Firearms (Rifle) | 2D6+4 | 110 yd | 1 | 5 | $75 | 100 |
| Elephant Gun (2B) | Firearms (Rifle) | 3D6+4 | 100 yd | 1 or 2 | 2 | $400 | 100 |

## Shotguns

| Name | Skill | Damage | Base Range | Uses/round | Bullets | Cost | Malfunction |
| ---- | ----- | ------ | ---------- | ---------- | ------- | ----:| ----------- |
| 20-Gauge Shotgun (2B) | Firearms (Shotgun) | 2D6/1D6/1D3 | 10/20/50 yd | 1 or 2 | 2 | $35 | 100 |
| 16-Gauge Shotgun (2B) | Firearms (Shotgun) | 2D6+2/1D6+1/1D4 | 10/20/50 yd | 1 or 2 | 2 | $40 | 100 |
| 12-Gauge Shotgun (2B) | Firearms (Shotgun) | 4D6/2D6/1D6 | 10/20/50 yd | 1 or 2 | 2 | $40 | 100 |
| 12-Gauge Shotgun (2B sawed off) | Firearms (Shotgun) | 4D6/1D6 | 5/10 yd | 1 or 2 | 2 | N/A | 100 |
| 10-Gauge Shotgun (2B) | Firearms (Shotgun) | 4D6+2/2D6+1/1D4 | 10/20/50 yd | 1 or 2 | 2 | Rare | 100 |

## Submachine Guns *(i)*

| Name | Skill | Damage | Base Range | Uses/round | Bullets | Cost | Malfunction |
| ---- | ----- | ------ | ---------- | ---------- | ------- | ----:| ----------- |
| Bergmann MP181/MP2811 | Firearms (SMG) | 1D10 | 20 yd | 1 (2) or full auto | 20/30/32 | $1,000 | 96 |
| Thompson | Firearms (SMG) | 1D10+2 | 20 yd | 1 or full auto | 20/30/50 | $200+ | 96

## Machine Guns *(i)*

| Name | Skill | Damage | Base Range | Uses/round | Bullets | Cost | Malfunction |
| ---- | ----- | ------ | ---------- | ---------- | ------- | ----:| ----------- |
| Model 1882 Gatling Gun | Firearms (MG) | 2D6+4 | 100 yd | Full auto | 200 | $2,000 (Rare) | 96 |
| Browning Auto Rifle M1918 | Firearms (MG) | 2D6+4 | 90 yd | 1 (2) or full auto | 20 | $800 | 100 |
| .30 Browning M1917A1 | Firearms (MG) | 2D6+4 | 150 yd | Full auto | 250 | $3,000 | 96 |
| Bren Gun | Firearms (MG) | 2D6+4 | 110 yd | 1 or full auto | 30/100 | $3,000 | 96 |
| Mark I Lewis Gun | Firearms (MG) | 2D6+4 | 110 yd | Full auto | 47/97 | $3,000 | 96 |
| Vickers .303 Machine Gun | Firearms (MG) | 2D6+4 | 110 yd | Full auto | 250 | N/A | 99 |

## Explosives, Heavy Weapons, Misc. *(i)*

| Name | Skill | Damage | Base Range | Uses/round | Bullets | Cost | Malfunction |
| ---- | ----- | ------ | ---------- | ---------- | ------- | ----:| ----------- |
| Molotov Cocktail | Throw | 2D6+Burn | STR/5 yd | 1/2 | 1 only | N/A | 95 |
| Signal Handgun (Flare gun) | Firearms (HG) | 1D10+1D3 Burn | 10 | 1/2 | 1 | $15 | 100 |
| Dynamite Stick | Throw | 4D10/ 3 yd | STR/5 yd | 1/2 | 1 only | $2 | 99 |
| Blasting Cap | Electrical Repair | 2D10/ 1 yd | N/A | N/A | One use | $1 | 100 |
| Pipe Bomb | Demolitions | 1D10/ 3 yd | In place | One Use | 1 only | N/A | 95 |
| Hand Grenade | Throw | 4D10/ 3 yd | STR/5 yd | 1/2 | 1 only | N/A | 99 |
| 75mm Field Gun | Artillery | 10D10/ 2 yd | 500 yd | 1/4 | Separate | $1,500 | 99 |
| Anti-Personnel Mine | Demolitions | 4D10/ 5 yd | In place | In place | One use | N/A | 99 |
| Flamethrower | Firearms (Flamethrower) | 2D6+Burn | 25 yd | 1 | At least 10 | N/A | 93 |
