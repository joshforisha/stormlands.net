# Travel Costs – 1920s

## Lodging

- **Average Hotel** *$4.50/night*
- **Average Hotel (with service)** *$10.00/week*
- **Fleabag Hotel** *75¢/night*
- **Good Hotel** *$9.00/night*
- **Good Hotel (with service)** *$24.00/week*
- **Luxury Hotel** *$30.00+/night*
- **YMCA, Furnished Room** *$5.00/night*
- **House, Large** *$1,000.00/year*
- **House, Medium** *$55.00/month*
- **Summer House** *$350.00/season*
- **Flat** *$12.50/week*
- **Apartment, Average** *$10.00/week*
- **Apartment, Good** *$40.00/week*

## Real Estate

- **Country House (13 rooms, 2 barns)** *$20,000.00+*
- **Large House (10 rooms)** *$7,000.00+*
- **Town House (6 rooms)** *$4,000.00–$8,000.00*
- **Average House (8 rooms)** *$2,900.00+*
- **Bungalow (4 rooms)** *$3,100.00*

#### Pre-Fabricated Houses:

- **The Clyde (6 rooms)** *$1,175.00*
- **The Columbine (8 rooms)** *$2,135.00*
- **The Honor (9 rooms)** *$3,278.00*
- **The Atlanta (24 apartment rooms)** *$4,492.00*

## Education

- **College Tuition (Semester)** *$275.00–$480.00*
- **Student Room & Board (Year)** *$350.00–$520.00*
- **Text Books (Semester)** *$30.00*

## Motor Vehicles

- **Norton Motorcycle** *$95.00*
- **Buick Model D-45** *$1,020.00*
- **Cadillac Type 55** *$2,240.00*
- **Chevrolet Capitol** *$695.00*
- **Chrysler Model F-58** *$1,045.00*
- **Dodge Model S/1** *$985.00*
- **Duesenberg J** *$20,000.00*
- **Ford Model T** *$360.00*
- **Ford Model A** *$450.00*
- **Hudson Super Six Series J** *$1,750.00*
- **Oldsmobile 43-AT** *$1,345.00*
- **Packard Twin Six Touring** *$2,950.00*
- **Pierce Arrow** *$6,000.00*
- **Pontiac 6-28 Sedan** *$745.00*
- **Studebaker Standard/Dictator** *$995.00*
- **Chevrolet, Roadster** *$570.00*
- **Hudston, Coach (7 passenger)** *$1,450.00*
- **Studebaker, Touring (5 passenger)** *$995.00*
- **1920 Chevrolet, F.B. Coupe (Used)** *$300.00*
- **1917 Buick (Used)** *$75.00*
- **Chevrolet Pickup Truck** *$545.00*
- **Dodge 1/2 Ton Truck** *$1,085.00*
- **Ford Model TT Truck** *$490.00*

#### Non-U.S. Vehicles

- **Bentley 3-Litre, England** *$9,000.00*
- **BMW Dixi, Germany** *$1,225.00*
- **Citroen C3, France** *$800.00*
- **Hispano-Suiza Alfonso, Spain** *$4,000.00*
- **Lancia Lambda 214, Italy** *$4,050.00*
- **Mercedes-Benz SS, Germany** *$7,750.00*
- **Renault AX, France** *$500.00*
- **Rolls-Royce Silver Ghost, England** *$6,750.00*
- **Rolls-Royce Phantom I, England** *$10,800.00*

## Vehicle Accessories

- **Tire** *$10.95*
- **Tire Repair Kit** *32¢*
- **Tire Snow Chains** *$4.95*
- **Jack** *$1.00*
- **Auto Battery** *$14.15*
- **Radiator** *$8.69*
- **Replacement Headlamp** *30¢*
- **Portable Air Pump** *$3.25*
- **Auto Spot Light** *$2.95*
- **Auto Luggage Carrier** *$1.35*

## Air Travel

- **Standard Ticket** *$2.00/10 mi*
- **International Ticket** *$18.00/100 mi*
- **Surplus Trainer Biplane** *$300.00*
- **Travel Air 2000 Biplane** *$3,000.00*

## Train Fares

- **50 Miles** *$2.00*
- **100 Miles** *$3.00*
- **500 Miles** *$6.00*

## Sea Voyage (U.S. / England)

- **First Class (One-way)** *$120.00*
- **First Class (Round-trip)** *$200.00*
- **Steerage** *$35.00*
- **4-Man Hot Air Balloon** *$1,800.00+*
- **Streetcar Fare** *10¢*
- **Bus Fare** *5¢*
