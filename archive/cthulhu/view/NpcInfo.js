import * as React from 'react'
import Slab from '~/view/Slab'
import { Gender, importantSkills, startingSkills } from '~/lib/NPC'

export default function NpcInfo ({ npc }) {
  const requiredSkills = importantSkills.reduce(
    (xs, skill, i) => {
      xs.push(
        <Slab key={skill} label={skill} rating value={npc.skills[skill]} />
      )
      if (i === 7) xs.push(<br key='break' />)
      return xs
    },
    []
  )

  const improvedSkills = Object.entries(npc.skills)
    .filter(
      ([skill, value]) =>
        !importantSkills.includes(skill) && value !== startingSkills[skill]
    )
    .map(
      ([skill, value]) => (
        <Slab key={skill} label={skill} rating value={value} />
      )
    )

  return (
    <>
      <Slab label='Name' value={npc.name} />
      <Slab label='Age' value={npc.age} />
      <Slab label='Gender' value={Gender[npc.gender]} />
      <Slab label='Occupation' value={npc.occupation} />
      <br />
      <Slab label='HP' value={npc.hitPoints} />
      <Slab label='Damage' value={npc.damageBonus} />
      <Slab label='Build' value={npc.build} />
      <Slab label='MOV' value={npc.moveRate} />
      <Slab label='Motivation' value={npc.motivation} />
      <br />
      <Slab label='STR' rating value={npc.strength} />
      <Slab label='DEX' rating value={npc.dexterity} />
      <Slab label='POW' rating value={npc.power} />
      <Slab label='CON' rating value={npc.constitution} />
      <Slab label='APP' rating value={npc.appearance} />
      <Slab label='EDU' rating value={npc.education} />
      <Slab label='SIZ' rating value={npc.size} />
      <Slab label='INT' rating value={npc.intelligence} />
      <Slab label='Luck' value={npc.luck} />
      <hr />
      {requiredSkills}
      <hr />
      {improvedSkills}
    </>
  )
}
