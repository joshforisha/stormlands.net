import * as React from 'react'
import styled, { css } from 'styled-components'

const Theme = {
  Extreme: 'Extreme',
  Hard: 'Hard',
  Regular: 'Regular'
}

const Container = styled.div`
  display: inline-block;
  margin: var(--medium);
`

const Label = styled.label`
  display: block;
  font-size: 0.95em;
  font-weight: 700;
  line-height: 1;
`

const Rating = styled.span`
  display: block;
`

const Ratings = styled.span`
  display: inline-block;
  font-size: 0.8em;
  line-height: 1.1;
  margin-left: var(--tiny);
`

const Value = styled.span`
  font-size: 2em;
  line-height: 1.3;

  ${({ theme }) =>
    theme === Theme.Extreme &&
    css`
      color: var(--red);
    `}

  ${({ theme }) =>
    theme === Theme.Hard &&
    css`
      color: var(--orange);
    `}
`

export default function Slab ({ label, rating, value }) {
  let ratings = null
  let theme = Theme.Regular
  if (rating && typeof value === 'number') {
    if (value >= 90) theme = Theme.Extreme
    else if (value >= 50) theme = Theme.Hard

    ratings = (
      <Ratings>
        <Rating>{Math.floor(value / 2)}</Rating>
        <Rating>{Math.floor(value / 5)}</Rating>
      </Ratings>
    )
  }

  return (
    <Container>
      <Value theme={theme}>{value}</Value>
      {ratings}
      <Label>{label}</Label>
    </Container>
  )
}
