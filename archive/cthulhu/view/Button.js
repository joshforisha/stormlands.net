import styled from 'styled-components'

const Button = styled.button`
  background-color: var(--light);
  border: 1px solid var(--shadow);
  border-radius: var(--tiny);
  cursor: pointer;
  font-size: 1em;
  padding: var(--small) var(--medium);
  transition: background-color 100ms ease-out;

  &:hover {
    background-color: var(--white);
  }
`

export default Button
