import * as React from 'react'
import styled, { css } from 'styled-components'

const Link = styled.a`
  border-bottom: 1px solid var(--chalk);
  color: var(--light);
  display: block;
  padding: var(--small);
  text-decoration: none;
  transition: background-color 50ms ease-out, color 50ms ease-out;

  ${({ current }) =>
    current &&
    css`
      background-color: var(--parchment);
      color: var(--dark);
    `}
`

export default function NavLink ({ children, href }) {
  return (
    <Link current={window.location.hash === href ? 1 : 0} href={href}>
      {children}
    </Link>
  )
}
