import * as React from 'react'
import NavLink from '~/view/NavLink'
import styled from 'styled-components'

const Container = styled.nav`
  background-color: var(--dark);

  @media (min-width: 900px) {
    -webkit-overflow-scrolling: touch;
    height: 100vh;
    overflow-y: scroll;
    position: fixed;
    width: 250px;
  }
`

export default function Nav () {
  return (
    <Container>
      <NavLink href='#/equipment-costs'>Equipment Costs</NavLink>
      <NavLink href='#/generate-npc'>Generate NPC</NavLink>
      <NavLink href='#/madness'>Madness</NavLink>
      <NavLink href='#/skill-details'>Skill Details</NavLink>
      <NavLink href='#/skill-list'>Skill List</NavLink>
      <NavLink href='#/travel-costs'>Travel Costs</NavLink>
      <NavLink href='#/weapons'>Weapons</NavLink>
    </Container>
  )
}
