import manias from '~/lib/manias'
import phobias from '~/lib/phobias'
import { d10 } from '~/lib/number'
import { draw } from '~/lib/array'

export const general = {
  Amnesia: () =>
    'The investigator comes to their senses in some unfamiliar place with no memory of who they are. Their memories will slowly return to them over time.',
  Battered: () =>
    `The investigator comes to their senses ${d10()} hours later to find themselves battered and bruised. Hit points are reduced to half of what they were before going insane, though this does not cause a Major wound. They have not been robbed. How the damage was sustained is up to the Keeper.`,
  'Ideology/Beliefs': () =>
    'Review the investigator’s backstory entry for Ideology and Beliefs. The investigator manifests one of these in an extreme, crazed, and demonstrative manner. For example, a religious person might be found later, preaching the gospel loudly on the subway.',
  Institutionalized: () =>
    'The investigator comes to their senses in a psychiatric ward or police cell. They may slowly recall the events that led them there.',
  Phobia: () => {
    const phobiaName = draw(Object.keys(phobias))

    return `The investigator gains a new phobia: ${phobiaName} (${
      phobias[phobiaName]
    }). The investigator comes to their senses ${d10()} hours later, having taken every precaution to avoid their new phobia.`
  },
  Robbed: () =>
    `The investigator comes to their senses ${d10()} hours later, having been robbed. They are unharmed. If they were carrying a Treasured Possession (see investigator backstory), make a Luck roll to see if it was stolen. Everything else of value is automatically missing.`,
  Violence: () =>
    'The investigator explodes in a spree of violence and destruction. When the investigator comes to their senses, their actions may or may not be apparent or remembered. Who or what the investigator has inflicted violence upon and whether they have killed or simply inflicted harm is up to the Keeper.',
  'Significant People': () =>
    `Consult the investigator’s backstory entry for Significant People and why the relationship is so important. In the time that passes (${d10()} hours or more) the investigator has done their best to get close to that person and act upon their relationship in some way.`,
  'Flee in panic': () =>
    'When the investigator comes to their senses they are far away, perhaps lost in the wilderness or on a train or long-distance bus.',
  Mania: () => {
    const maniaName = draw(Object.keys(manias))
    return `The investigator gains a new mania: ${maniaName} (${
      manias[maniaName]
    }). The investigator comes to their senses ${d10()} hours later. During this bout of madness, the investigator will have been fully indulging in their new mania. Whether this is apparent to other people is up to the Keeper and player.`
  }
}

export const realTime = {
  Amnesia: () =>
    `The investigator has no memory of events that have taken place since they were last in a place of safety. This lasts for ${d10()} rounds.`,
  'Psychosomatic Disability': () =>
    `The investigator suffers psychosomatic blindness, deafness, or loss of the use of a limb or limbs for ${d10()} rounds.`,
  Violence: () =>
    `A red mist descends on the investigator and they explode in a spree of uncontrolled violence and destruction directed at their surroundings, allies or foes alike for ${d10()} rounds.`,
  Paranoia: () =>
    `The investigator suffers severe paranoia for ${d10()} rounds; everyone is out to get them; no one can be trusted; they are being spied on; someone has betrayed them; what they are seeing is a trick.`,
  'Significant Person': () =>
    `Review the investigator's backstory entry for Significant People. The investigator mistakes another person in the scene for their Significant Person. Consider the nature of the relationship: the investigator acts upon it. This lasts ${d10()} rounds.`,
  Faint: () =>
    `The investigator faints. They recover after ${d10()} rounds.`,
  'Flee in Panic': () =>
    `The investigator is compelled to get as far away as possible by whatever means are available, even if it means taking the only vehicle and leaving everyone else behind. They travel for ${d10()} rounds.`,
  'Physical Hysterics': () =>
    `The investigator is incapicated from laughing, crying, screaming, etc. for ${d10()} rounds.`,
  Phobia: () => {
    const phobiaName = draw(Object.keys(phobias))

    return `The investigator gains a new phobia: ${phobiaName} (${
      phobias[phobiaName]
    }). Even if the source is not present, the investigator imagines it is there for ${d10()} rounds.`
  },
  Mania: () => {
    const maniaName = draw(Object.keys(manias))

    return `The investigator gains a new mania: ${maniaName} (${
      manias[maniaName]
    }). The investigator seeks to indulge in their new mania for ${d10()} rounds.`
  }
}
