import { useState } from 'react'

export default function useHashRoutes (routes) {
  const [result, setResult] = useState(
    window.location.hash in routes ? routes[window.location.hash] : null
  )

  window.addEventListener('hashchange', () => {
    setResult(
      window.location.hash in routes ? routes[window.location.hash] : null
    )
  })

  if (typeof result === 'function') return result()
  return result
}
