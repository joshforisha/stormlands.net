export function draw (items) {
  if (items.length === 0) return null
  if (items.length === 1) return items[0]
  return items[Math.floor(Math.random() * items.length)]
}

export function initialize (length, init) {
  const array = []
  if (typeof init === 'function') {
    for (let i = 0; i < length; i++) array.push(init(i))
  } else {
    while (array.length < length) array.push(init)
  }
  return array
}

export function omit (items, index) {
  return [...items.slice(0, index), ...items.slice(index + 1)]
}

export function take (count, items) {
  if (count === 0 || items.length === 0) return []

  const index = Math.floor(Math.random() * items.length)
  if (count === 1) return [items[index]]

  return [items[index], ...take(count - 1, omit(items, index))]
}

export function zip (...xs) {
  if (xs.length < 1) return []
  if (xs.length === 1) return xs
  return xs[0].map((_, c) => xs.map((r) => r[c]))
}
