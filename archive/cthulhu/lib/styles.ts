export enum Breakpoint {
  Tablet = "800px"
}

export enum Color {
  Black = "rgb(24, 24, 24)",
  Chalk = "rgba(240, 230, 220, 0.25)",
  Dark = "rgba(24, 24, 24, 0.9)",
  Light = "rgba(248, 248, 248, 0.9)",
  Orange = "rgb(200, 120, 0)",
  Parchment = "rgb(236, 233, 230)",
  Red = "rgb(200, 0, 0)",
  Shadow = "rgba(24, 24, 24, 0.2)",
  White = "rgb(248, 248, 248)"
}

export enum Size {
  Huge = "4rem",
  Large = "2rem",
  Medium = "1rem",
  Small = "0.5rem",
  Tiny = "0.25rem"
}

export const contentWidth = "800px";
