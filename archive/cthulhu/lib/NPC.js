import occupations from '~/lib/occupations'
import { capital } from '~/lib/text'
import { d6, d10, d100 } from '~/lib/number'
import { generate as generateMotivation } from '~/lib/motivations'
import { draw, initialize, zip } from '~/lib/array'

export const Gender = {
  Female: 'Female',
  Male: 'Male'
}

export const importantSkills = [
  'Charm',
  'Climb',
  'Credit Rating',
  'Dodge',
  'Drive Auto',
  'Fast Talk',
  'Fighting (Brawl)',
  'Intimidate',
  'Jump',
  'Listen',
  'Persuade',
  'Psychology',
  'Spot Hidden',
  'Stealth',
  'Swim',
  'Track'
]

export const startingSkills = {
  Accounting: 5,
  'Animal Handling': 5,
  Anthropology: 1,
  Appraise: 5,
  Archaeology: 1,
  'Art and Craft (Acting)': 5,
  'Art and Craft (Fine Art)': 5,
  'Art and Craft (Forgery)': 5,
  'Art and Craft (Photography)': 5,
  Artillery: 1,
  Charm: 15,
  Climb: 20,
  'Credit Rating': 0,
  'Cthulhu Mythos': 0,
  Demolitions: 1,
  Disguise: 5,
  Diving: 1,
  Dodge: ({ dexterity }) => Math.floor(dexterity / 2),
  'Drive Auto': 20,
  'Electrical Repair': 10,
  'Fast Talk': 5,
  'Fighting (Axe)': 15,
  'Fighting (Brawl)': 25,
  'Fighting (Flail)': 10,
  'Fighting (Garrote)': 15,
  'Fighting (Spear)': 20,
  'Fighting (Sword)': 20,
  'Fighting (Whip)': 5,
  'Firearms (Bow)': 15,
  'Firearms (Flamethrower)': 10,
  'Firearms (Handgun)': 20,
  'Firearms (Heavy Weapons)': 10,
  'Firearms (Machine Gun)': 10,
  'Firearms (Rifle/Shotgun)': 25,
  'Firearms (Submachine Gun)': 15,
  'First Aid': 30,
  History: 5,
  Hypnosis: 1,
  Intimidate: 15,
  Jump: 20,
  'Language (Other)': 1,
  'Language (Own)': ({ education }) => education,
  Law: 5,
  'Library Use': 20,
  Listen: 20,
  Locksmith: 1,
  Lore: 1,
  'Mechanical Repair': 10,
  Medicine: 1,
  'Natural World': 10,
  Navigate: 10,
  Occult: 5,
  'Operate Heavy Machinery': 1,
  Persuade: 10,
  Pilot: 1,
  'Pilot (Boat)': 1,
  'Pilot (Aircraft)': 1,
  Psychoanalysis: 1,
  Psychology: 10,
  'Read Lips': 1,
  Ride: 5,
  'Science (Astronomy)': 1,
  'Science (Biology)': 1,
  'Science (Botany)': 1,
  'Science (Chemistry)': 1,
  'Science (Cryptology)': 1,
  'Science (Engineering)': 1,
  'Science (Forensics)': 1,
  'Science (Geology)': 1,
  'Science (Mathematics)': 10,
  'Science (Meteorology)': 1,
  'Science (Pharmacy)': 1,
  'Science (Physics)': 1,
  'Science (Zoology)': 1,
  'Sleight of Hand': 10,
  'Spot Hidden': 25,
  Stealth: 20,
  Survival: 10,
  Swim: 20,
  Throw: 20,
  Track: 10
}

function distribute (points, values) {
  const newValues = [...values]
  let index
  for (let i = 0; i < Math.abs(points); i++) {
    index = Math.floor(Math.random() * values.length)
    newValues[index] = points > 0 ? newValues[index] + 1 : newValues[index] - 1
  }
  return newValues
}

function improve (value, times = 1) {
  let newValue = value
  for (let i = 0; i < times; i++) {
    if (d100() > newValue) newValue += d10()
  }
  return newValue
}

export function generate (init = {}) {
  let appearance = d6(3) * 5
  let constitution = d6(3) * 5
  let dexterity = d6(3) * 5
  let education = (d6(2) + 6) * 5
  const intelligence = (d6(2) + 6) * 5
  let luck = d6(3) * 5
  const power = d6(3) * 5
  let size = (d6(2) + 6) * 5
  let strength = d6(3) * 5

  let moveRate
  if (dexterity < size && strength < size) moveRate = 7
  else if (dexterity > size && strength > size) moveRate = 9
  else moveRate = 8

  const age = init.age || 15 + Math.floor(Math.random() * 76)

  if (age < 20) {
    [strength, size] = distribute(-5, [strength, size])
    education -= 5
    luck = Math.max(luck, d6(3) * 5)
  } else if (age < 40) {
    education = improve(education, 1)
  } else if (age < 50) {
    [strength, constitution, dexterity] = distribute(-5, [
      strength,
      constitution,
      dexterity
    ])
    appearance -= 5
    education = improve(education, 2)
    moveRate -= 1
  } else if (age < 60) {
    [strength, constitution, dexterity] = distribute(-10, [
      strength,
      constitution,
      dexterity
    ])
    appearance -= 10
    education = improve(education, 3)
    moveRate -= 2
  } else if (age < 70) {
    [strength, constitution, dexterity] = distribute(-20, [
      strength,
      constitution,
      dexterity
    ])
    appearance -= 15
    education = improve(education, 4)
    moveRate -= 3
  } else if (age < 80) {
    [strength, constitution, dexterity] = distribute(-40, [
      strength,
      constitution,
      dexterity
    ])
    appearance -= 20
    education = improve(education, 4)
    moveRate -= 4
  } else {
    [strength, constitution, dexterity] = distribute(-80, [
      strength,
      constitution,
      dexterity
    ])
    appearance -= 25
    education = improve(education, 4)
    moveRate -= 5
  }

  const dbb = strength + size
  let damageBonus, build
  if (dbb < 65) [damageBonus, build] = ['-2', -2]
  else if (dbb < 85) [damageBonus, build] = ['-1', -1]
  else if (dbb < 125) [damageBonus, build] = ['None', 0]
  else if (dbb < 165) [damageBonus, build] = ['+1D4', 1]
  else if (dbb < 205) [damageBonus, build] = ['+1D6', 2]
  else if (dbb < 285) [damageBonus, build] = ['+2D6', 3]
  else if (dbb < 365) [damageBonus, build] = ['+3D6', 4]
  else if (dbb < 445) [damageBonus, build] = ['+4D6', 5]
  else [damageBonus, build] = ['+5D6', 6]

  const hitPoints = Math.floor((constitution + size) / 10)

  const gender =
    init.gender || (Math.random() < 0.5 ? Gender.Female : Gender.Male)

  return window
    .fetch(
      `https://www.behindthename.com/api/random.json?gender=${
        gender === Gender.Female ? 'f' : 'm'
      }&key=jo843748017&number=1&randomsurname=yes`
    )
    .then(res => res.json())
    .then(data => data.names.join(' '))
    .then(name => {
      const occupationName = init.occupation || draw(Object.keys(occupations))
      const occupation = occupations[occupationName]
      const occupationSkillPoints = occupation.skillPoints({
        app: appearance,
        con: constitution,
        dex: dexterity,
        edu: education,
        int: intelligence,
        pow: power,
        str: strength
      })

      const creditRating = Math.floor(
        occupation.creditRating[0] +
          Math.random() * (occupation.creditRating[1] - occupation.creditRating[0])
      )

      const skillChoices = occupation.skills()

      const occupationSkills = zip(
        skillChoices,
        distribute(
          occupationSkillPoints - creditRating,
          initialize(skillChoices.length, i => {
            const skillName = skillChoices[i]

            const startingValue =
              skillName in startingSkills ? startingSkills[skillName] : 1

            if (typeof startingValue === 'number') return startingValue
            return startingValue({ dexterity, education })
          }),
          {
            max: 100
          }
        )
      )
        .concat([['Credit Rating', creditRating]])
        .reduce((occs, [skill, value]) => {
          occs[skill] = value
          return occs
        }, {})

      const skills = Object.entries(startingSkills)
        .concat(
          Object.entries(occupationSkills).filter(
            ([skill]) => !(skill in startingSkills)
          )
        )
        .sort(([a], [b]) => (a < b ? -1 : 1))
        .reduce((all, [skill, startingValue]) => {
          if (skill in occupationSkills) {
            all[skill] = occupationSkills[skill]
          } else {
            all[skill] =
              typeof startingValue === 'function'
                ? startingValue({ dexterity, education })
                : startingValue
          }
          return all
        }, {})

      const motivation = capital(generateMotivation())

      return {
        age,
        appearance,
        build,
        constitution,
        damageBonus,
        dexterity,
        education,
        gender,
        hitPoints,
        intelligence,
        luck,
        motivation,
        moveRate,
        name,
        occupation: occupationName,
        power,
        size,
        skills,
        strength
      }
    })
}
