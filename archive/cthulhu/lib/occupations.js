import { randomInt } from '~/lib/number'
import { take } from '~/lib/array'
import { unique } from '~/lib/filters'

const anyOf = xs => () => take(randomInt(0, xs.length), xs)

const oneOf = xs => () => take(1, xs)

const threeOf = xs => () => take(3, xs)

const twoOf = xs => () => take(2, xs)

function foldWhile (pred, fn, xs) {
  let ys = [...xs]
  while (pred(ys)) ys = ys.map(fn).flat()
  return ys
}

const sk = (...skills) => () =>
  foldWhile(
    xs => xs.some(x => typeof x === 'function'),
    x => (typeof x === 'function' ? x() : x),
    skills
  ).filter(unique)

const sp = (...chars) =>
  chars
    .map(char => (Array.isArray(char) ? Math.max(...char) * 2 : char * 2))
    .reduce((total, x) => total + x, 0)

const artsAndCrafts = [
  'Art and Craft (Acting)',
  'Art and Craft (Fine Art)',
  'Art and Craft (Forgery)',
  'Art and Craft (Photography)'
]

const entertaining = [
  'Art and Craft (Acting)',
  'Art and Craft (Comedy)',
  'Art and Craft (Singing)'
]

const fighting = [
  'Fighting (Axe)',
  'Fighting (Brawl)',
  'Fighting (Flail)',
  'Fighting (Garrote)',
  'Fighting (Spear)',
  'Fighting (Sword)',
  'Fighting (Whip)',
  'Firearms (Bow)'
]

const firearms = [
  'Firearms (Bow)',
  'Firearms (Flamethrower)',
  'Firearms (Handgun)',
  'Firearms (Heavy Weapons)',
  'Firearms (Machine Gun)',
  'Firearms (Rifle/Shotgun)',
  'Firearms (Submachine Gun)'
]

const sciences = [
  'Science (Astronomy)',
  'Science (Biology)',
  'Science (Botany)',
  'Science (Chemistry)',
  'Science (Cryptology)',
  'Science (Engineering)',
  'Science (Forensics)',
  'Science (Geology)',
  'Science (Mathematics)',
  'Science (Meteorology)',
  'Science (Pharmacy)',
  'Science (Physics)',
  'Science (Zoology)'
]

const academics = [
  'Anthropology',
  'Archaeology',
  'Art and Craft (Acting)',
  'History',
  'Language (Other)',
  'Language (Own)',
  'Law',
  'Lore',
  'Medicine',
  'Natural World',
  'Psychology',
  ...sciences
]

const social = ['Charm', 'Fast Talk', 'Intimidate', 'Persuade']

const occupations = {
  Accountant: {
    creditRating: [30, 70],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Accounting',
      'Law',
      'Library Use',
      'Listen',
      'Persuade',
      'Spot Hidden'
    )
  },
  Acrobat: {
    creditRating: [9, 20],
    skillPoints: ({ dex, edu }) => sp(edu, dex),
    skills: sk('Climb', 'Dodge', 'Jump', 'Throw', 'Spot Hidden', 'Swim')
  },
  'Agency Detective': {
    creditRating: [20, 45],
    skillPoints: ({ dex, edu, str }) => sp(edu, [str, dex]),
    skills: sk(
      oneOf(social),
      'Fighting (Brawl)',
      oneOf(firearms),
      'Law',
      'Library Use',
      'Psychology',
      'Stealth',
      'Track'
    )
  },
  Alienist: {
    creditRating: [10, 60],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Law',
      'Listen',
      'Medicine',
      'Language (Other)',
      'Psychoanalysis',
      'Psychology',
      'Science (Biology)',
      'Science (Chemistry)'
    )
  },
  'Animal Trainer': {
    creditRating: [10, 40],
    skillPoints: ({ app, edu, pow }) => sp(edu, [app, pow]),
    skills: sk(
      'Animal Handling',
      'Jump',
      'Listen',
      'Natural World',
      'Science (Zoology)',
      'Stealth',
      'Track'
    )
  },
  Antiquarian: {
    creditRating: [30, 70],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Appraise',
      oneOf(artsAndCrafts),
      'History',
      'Library Use',
      'Language (Other)',
      oneOf(social),
      'Spot Hidden'
    )
  },
  'Antique Dealer': {
    creditRating: [30, 50],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Accounting',
      'Appraise',
      'Drive Auto',
      twoOf(social),
      'History',
      'Library Use',
      'Navigate'
    )
  },
  Archaeologist: {
    creditRating: [10, 40],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Appraise',
      'Archaeology',
      'History',
      'Language (Other)',
      'Library Use',
      'Spot Hidden',
      'Mechanical Repair',
      'Navigate',
      oneOf(sciences)
    )
  },
  Architect: {
    creditRating: [30, 70],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Accounting',
      'Art and Craft (Technical Drawing)',
      'Law',
      'Language (Own)',
      'Library Use',
      'Persuade',
      'Psychology',
      'Science (Mathematics)'
    )
  },
  Artist: {
    creditRating: [9, 50],
    skillPoints: ({ dex, edu, pow }) => sp(edu, [dex, pow]),
    skills: sk(
      anyOf(artsAndCrafts),
      oneOf(['History', 'Natural World']),
      oneOf(social),
      'Language (Other)',
      'Psychology',
      'Spot Hidden'
    )
  },
  Assassin: {
    creditRating: [30, 60],
    skillPoints: ({ dex, edu, str }) => sp(edu, [dex, str]),
    skills: sk(
      'Disguise',
      'Electrical Repair',
      anyOf(fighting),
      anyOf(firearms),
      'Locksmith',
      'Mechanical Repair',
      'Stealth',
      'Psychology'
    )
  },
  'Asylum Attendant': {
    creditRating: [8, 20],
    skillPoints: ({ dex, edu, str }) => sp(edu, [str, dex]),
    skills: sk(
      'Dodge',
      'Fighting (Brawl)',
      'First Aid',
      twoOf(social),
      'Listen',
      'Psychology',
      'Stealth'
    )
  },
  Athlete: {
    creditRating: [9, 70],
    skillPoints: ({ dex, edu, str }) => sp(edu, [dex, str]),
    skills: sk(
      'Climb',
      'Jump',
      'Fighting (Brawl)',
      'Ride',
      oneOf(social),
      'Swim',
      'Throw'
    )
  },
  Author: {
    creditRating: [9, 30],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Art (Literature)',
      'History',
      'Library Use',
      oneOf(['Natural World', 'Occult']),
      'Language (Other)',
      'Language (Own)',
      'Psychology'
    )
  },
  Aviator: {
    creditRating: [30, 60],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Accounting',
      'Electrical Repair',
      'Listen',
      'Mechanical Repair',
      'Navigate',
      'Pilot (Aircraft)',
      'Spot Hidden'
    )
  },
  'Bank Robber': {
    creditRating: [5, 75],
    skillPoints: ({ dex, edu, str }) => sp(edu, [str, dex]),
    skills: sk(
      'Drive Auto',
      oneOf(['Electrical Repair', 'Mechanical Repair']),
      anyOf(fighting),
      anyOf(firearms),
      'Intimidate',
      'Locksmith',
      'Operate Heavy Machinery'
    )
  },
  Bartender: {
    creditRating: [8, 25],
    skillPoints: ({ app, edu }) => sp(edu, app),
    skills: sk(
      'Accounting',
      twoOf(social),
      'Fighting (Brawl)',
      'Listen',
      'Psychology',
      'Spot Hidden'
    )
  },
  'Big Game Hunter': {
    creditRating: [20, 50],
    skillPoints: ({ dex, edu, str }) => sp(edu, [dex, str]),
    skills: sk(
      oneOf(firearms),
      oneOf(['Listen', 'Spot Hidden']),
      'Natural World',
      'Navigate',
      oneOf(['Language (Own)', 'Survival']),
      oneOf(['Science (Biology)', 'Science (Botany)', 'Science (Zoology)']),
      'Stealth',
      'Track'
    )
  },
  'Book Dealer': {
    creditRating: [20, 40],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Accounting',
      'Appraise',
      'Drive Auto',
      'History',
      'Library Use',
      'Language (Other)',
      'Language (Own)',
      oneOf(social)
    )
  },
  'Bootlegger/Thug': {
    creditRating: [5, 30],
    skillPoints: ({ edu, str }) => sp(edu, str),
    skills: sk(
      'Drive Auto',
      anyOf(fighting),
      anyOf(firearms),
      twoOf(social),
      'Psychology',
      'Stealth',
      'Spot Hidden'
    )
  },
  'Bounty Hunter': {
    creditRating: [9, 30],
    skillPoints: ({ dex, edu, str }) => sp(edu, [dex, str]),
    skills: sk(
      'Drive Auto',
      'Electrical Repair',
      oneOf(fighting),
      oneOf(firearms),
      oneOf(social),
      'Law',
      'Psychology',
      'Track',
      'Stealth'
    )
  },
  'Boxer/Wrestler': {
    creditRating: [9, 60],
    skillPoints: ({ edu, str }) => sp(edu, str),
    skills: sk(
      'Dodge',
      'Fighting (Brawl)',
      'Intimidate',
      'Jump',
      'Psychology',
      'Spot Hidden'
    )
  },
  Burglar: {
    creditRating: [5, 40],
    skillPoints: ({ dex, edu }) => sp(edu, dex),
    skills: sk(
      'Appraise',
      'Climb',
      oneOf(['Electrical Repair', 'Mechanical Repair']),
      'Listen',
      'Locksmith',
      'Sleight of Hand',
      'Stealth',
      'Spot Hidden'
    )
  },
  'Butler/Valet/Maid': {
    creditRating: [9, 40],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      oneOf(['Accounting', 'Appraise']),
      anyOf([
        'Art and Craft (Cook)',
        'Art and Craft (Tailor)',
        'Art and Craft (Barber)',
        ...artsAndCrafts
      ]),
      'First Aid',
      'Listen',
      'Psychology',
      'Spot Hidden'
    )
  },
  Chauffeur: {
    creditRating: [10, 40],
    skillPoints: ({ dex, edu }) => sp(edu, dex),
    skills: sk(
      'Drive Auto',
      twoOf(social),
      'Listen',
      'Mechanical Repair',
      'Navigate',
      'Spot Hidden'
    )
  },
  Clergy: {
    creditRating: [9, 60],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Accounting',
      'History',
      'Library Use',
      'Listen',
      'Language (Other)',
      oneOf(social),
      'Psychology'
    )
  },
  'Clerk/Executive': {
    creditRating: [9, 20],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Accounting',
      oneOf(['Language (Other)', 'Language (Own)']),
      'Law',
      'Library Use',
      'Listen',
      oneOf(social)
    )
  },
  Conman: {
    creditRating: [10, 65],
    skillPoints: ({ app, edu }) => sp(edu, app),
    skills: sk(
      'Appraise',
      'Art and Craft (Acting)',
      oneOf(['Law', 'Language (Other)']),
      'Listen',
      twoOf(social),
      'Psychology',
      'Sleight of Hand'
    )
  },
  'Cowboy/girl': {
    creditRating: [9, 20],
    skillPoints: ({ dex, edu, str }) => sp(edu, [dex, str]),
    skills: sk(
      'Dodge',
      oneOf(fighting),
      oneOf(firearms),
      oneOf(['First Aid', 'Natural World']),
      'Jump',
      'Ride',
      'Survival',
      'Throw',
      'Track'
    )
  },
  Craftsperson: {
    creditRating: [10, 40],
    skillPoints: ({ dex, edu }) => sp(edu, dex),
    skills: sk(
      'Accounting',
      twoOf(artsAndCrafts),
      'Mechanical Repair',
      'Natural World',
      'Spot Hidden'
    )
  },
  'Criminal (freelance/solo)': {
    creditRating: [5, 65],
    skillPoints: ({ app, dex, edu }) => sp(edu, [dex, app]),
    skills: sk(
      oneOf(['Art and Craft (Acting)', 'Disguise']),
      'Appraise',
      oneOf(social),
      oneOf(fighting),
      oneOf(firearms),
      oneOf(['Locksmith', 'Mechanical Repair']),
      'Stealth',
      'Psychology',
      'Spot Hidden'
    )
  },
  'Cult Leader': {
    creditRating: [30, 60],
    skillPoints: ({ app, edu }) => sp(edu, app),
    skills: sk(
      'Accounting',
      twoOf(social),
      'Occult',
      'Psychology',
      'Spot Hidden'
    )
  },
  Designer: {
    creditRating: [20, 60],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Accounting',
      'Art and Craft (Photography)',
      anyOf(artsAndCrafts),
      'Library Use',
      'Mechanical Repair',
      'Psychology',
      'Spot Hidden'
    )
  },
  Dilettante: {
    creditRating: [50, 99],
    skillPoints: ({ app, edu }) => sp(edu, app),
    skills: sk(
      anyOf(artsAndCrafts),
      oneOf(firearms),
      'Language (Other)',
      'Ride',
      oneOf(social)
    )
  },
  Diver: {
    creditRating: [9, 30],
    skillPoints: ({ dex, edu }) => sp(edu, dex),
    skills: sk(
      'Diving',
      'First Aid',
      'Mechanical Repair',
      'Pilot (Boat)',
      'Science (Biology)',
      'Spot Hidden',
      'Swim'
    )
  },
  'Doctor of Medicine': {
    creditRating: [30, 80],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'First Aid',
      'Medicine',
      'Language (Latin)',
      'Psychology',
      'Science (Biology)',
      'Science (Pharmacy)'
    )
  },
  Drifter: {
    creditRating: [0, 5],
    skillPoints: ({ app, dex, edu, str }) => sp(edu, [app, dex, str]),
    skills: sk('Climb', 'Jump', 'Listen', 'Navigate', oneOf(social), 'Stealth')
  },
  Driver: {
    creditRating: [9, 20],
    skillPoints: ({ dex, edu, str }) => sp(edu, [dex, str]),
    skills: sk(
      'Accounting',
      'Drive Auto',
      'Listen',
      oneOf(social),
      'Mechanical Repair',
      'Navigate',
      'Psychology'
    )
  },
  Editor: {
    creditRating: [10, 30],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Accounting',
      'History',
      'Language (Own)',
      twoOf(social),
      'Psychology',
      'Spot Hidden'
    )
  },
  'Elected Official': {
    creditRating: [50, 90],
    skillPoints: ({ app, edu }) => sp(edu, app),
    skills: sk(
      'Charm',
      'History',
      'Intimidate',
      'Fast Talk',
      'Listen',
      'Language (Own)',
      'Persuade',
      'Psychology'
    )
  },
  Engineer: {
    creditRating: [30, 60],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Art and Craft (Technical Drawing)',
      'Electrical Repair',
      'Library Use',
      'Mechanical Repair',
      'Operate Heavy Machinery',
      'Science (Engineering)',
      'Science (Physics'
    )
  },
  Entertainer: {
    creditRating: [9, 70],
    skillPoints: ({ app, edu }) => sp(edu, app),
    skills: sk(
      oneOf(entertaining),
      'Disguise',
      twoOf(social),
      'Listen',
      'Psychology'
    )
  },
  Explorer: {
    creditRating: [55, 80],
    skillPoints: ({ app, dex, edu, str }) => sp(edu, [app, dex, str]),
    skills: sk(
      oneOf(['Climb', 'Swim']),
      oneOf(firearms),
      'History',
      'Jump',
      'Natural World',
      'Navigate',
      'Language (Other)',
      'Survival'
    )
  },
  Farmer: {
    creditRating: [9, 30],
    skillPoints: ({ dex, edu, str }) => sp(edu, [dex, str]),
    skills: sk(
      'Art and Craft (Farming)',
      'Drive Auto',
      oneOf(social),
      'Mechanical Repair',
      'Natural World',
      'Operate Heavy Machinery',
      'Track'
    )
  },
  'Federal Agent': {
    creditRating: [20, 40],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Drive Auto',
      'Fighting (Brawl)',
      oneOf(firearms),
      'Law',
      'Persuade',
      'Stealth',
      'Spot Hidden'
    )
  },
  Fence: {
    creditRating: [20, 40],
    skillPoints: ({ app, edu }) => sp(edu, app),
    skills: sk(
      'Accounting',
      'Appraise',
      'Art and Craft (Forgery)',
      'History',
      oneOf(social),
      'Library Use',
      'Spot Hidden'
    )
  },
  'Film Actor': {
    creditRating: [20, 90],
    skillPoints: ({ app, edu }) => sp(edu, app),
    skills: sk(
      'Art and Craft (Acting)',
      'Disguise',
      'Drive Auto',
      twoOf(social),
      'Psychology'
    )
  },
  Firefighter: {
    creditRating: [9, 30],
    skillPoints: ({ dex, edu, str }) => sp(edu, [dex, str]),
    skills: sk(
      'Climb',
      'Dodge',
      'Drive Auto',
      'First Aid',
      'Jump',
      'Mechanical Repair',
      'Operate Heavy Machinery',
      'Throw'
    )
  },
  'Foreign Correspondent': {
    creditRating: [10, 40],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'History',
      'Language (Other)',
      'Language (Own)',
      'Listen',
      twoOf(social),
      'Psychology'
    )
  },
  'Forensic Surgeon': {
    creditRating: [40, 60],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Language (Latin)',
      'Library Use',
      'Medicine',
      'Persuade',
      'Science (Biology)',
      'Science (Forensics)',
      'Science (Pharmacy)',
      'Spot Hidden'
    )
  },
  'Forger/Counterfeiter': {
    creditRating: [20, 60],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Accounting',
      'Appraise',
      'Art and Craft (Forgery)',
      'History',
      'Library Use',
      'Spot Hidden',
      'Sleight of Hand'
    )
  },
  Gambler: {
    creditRating: [8, 50],
    skillPoints: ({ app, dex, edu }) => sp(edu, [app, dex]),
    skills: sk(
      'Accounting',
      'Art and Craft (Acting)',
      twoOf(social),
      'Listen',
      'Psychology',
      'Sleight of Hand',
      'Spot Hidden'
    )
  },
  'Gangster Boss': {
    creditRating: [60, 95],
    skillPoints: ({ app, edu }) => sp(edu, app),
    skills: sk(
      anyOf(fighting),
      anyOf(firearms),
      'Law',
      'Listen',
      twoOf(social),
      'Psychology',
      'Spot Hidden'
    )
  },
  'Gangster Underling': {
    creditRating: [9, 20],
    skillPoints: ({ dex, edu, str }) => sp(edu, [dex, str]),
    skills: sk(
      'Drive Auto',
      anyOf(fighting),
      anyOf(firearms),
      twoOf(social),
      'Psychology'
    )
  },
  'Gentleman/Lady': {
    creditRating: [40, 90],
    skillPoints: ({ app, edu }) => sp(edu, app),
    skills: sk(
      anyOf(artsAndCrafts),
      twoOf(social),
      'Firearms (Rifle/Shotgun)',
      'History',
      'Language (Other)',
      'Navigate',
      'Ride'
    )
  },
  'Gun Moll': {
    creditRating: [10, 80],
    skillPoints: ({ app, edu }) => sp(edu, app),
    skills: sk(
      anyOf(artsAndCrafts),
      twoOf(social),
      oneOf(['Fighting (Brawl)', 'Firearms (Handgun)']),
      'DriveAuto',
      'Listen',
      'Stealth'
    )
  },
  Hobo: {
    creditRating: [0, 5],
    skillPoints: ({ app, dex, edu }) => sp(edu, [app, dex]),
    skills: sk(
      anyOf(artsAndCrafts),
      'Climb',
      'Jump',
      'Listen',
      oneOf(['Locksmith', 'Sleight of Hand']),
      'Navigate',
      'Stealth'
    )
  },
  'Hospital Orderly': {
    creditRating: [6, 15],
    skillPoints: ({ edu, str }) => sp(edu, str),
    skills: sk(
      'Electrical Repair',
      oneOf(social),
      'Fighting (Brawl)',
      'First Aid',
      'Listen',
      'Mechanical Repair',
      'Psychology',
      'Stealth'
    )
  },
  'Investigative Journalist': {
    creditRating: [9, 30],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      oneOf(['Art and Craft (Art)', 'Art and Craft (Photography)']),
      oneOf(social),
      'History',
      'Library Use',
      'Language (Own)',
      'Psychology'
    )
  },
  Judge: {
    creditRating: [50, 80],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'History',
      'Intimidate',
      'Law',
      'Library Use',
      'Listen',
      'Language (Own)',
      'Persuade',
      'Psychology'
    )
  },
  'Laboratory Assistant': {
    creditRating: [10, 30],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Library Use',
      'Electrical Repair',
      'Language (Other)',
      'Science (Chemistry)',
      twoOf(sciences),
      'Spot Hidden'
    )
  },
  'Laborer, Unskilled': {
    creditRating: [9, 30],
    skillPoints: ({ dex, edu, str }) => sp(edu, [dex, str]),
    skills: sk(
      'Drive Auto',
      'Electrical Repair',
      oneOf(fighting),
      'First Aid',
      'Mechanical Repair',
      'Operate Heavy Machinery',
      'Throw'
    )
  },
  Lawyer: {
    creditRating: [30, 80],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk('Accounting', 'Law', 'Library Use', twoOf(social), 'Psychology')
  },
  Librarian: {
    creditRating: [9, 35],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Accounting',
      'Library Use',
      'Language (Other)',
      'Language (Own)'
    )
  },
  Lumberjack: {
    creditRating: [9, 30],
    skillPoints: ({ dex, edu, str }) => sp(edu, [dex, str]),
    skills: sk(
      'Climb',
      'Dodge',
      'Fighting (Axe)',
      'First Aid',
      'Jump',
      'Mechanical Repair',
      oneOf([
        'Natural World',
        oneOf(['Science (Biology)', 'Science (Botany)'])
      ]),
      'Throw'
    )
  },
  Mechanic: {
    creditRating: [9, 40],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      oneOf([
        'Art and Craft (Carpentry)',
        'Art and Craft (Welding)',
        'Art and Craft (Plumbing)'
      ]),
      'Climb',
      'Drive Auto',
      'Electrical Repair',
      'Mechanical Repair',
      'Operate Heavy Machinery'
    )
  },
  'Middle/Senior Manager': {
    creditRating: [20, 80],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Accounting',
      'Language (Other)',
      'Law',
      twoOf(social),
      'Psychology'
    )
  },
  'Military Officer': {
    creditRating: [20, 70],
    skillPoints: ({ dex, edu, str }) => sp(edu, [dex, str]),
    skills: sk(
      'Accounting',
      anyOf(firearms),
      'Navigate',
      'First Aid',
      twoOf(social),
      'Psychology'
    )
  },
  Miner: {
    creditRating: [9, 30],
    skillPoints: ({ dex, edu, str }) => sp(edu, [dex, str]),
    skills: sk(
      'Climb',
      'Geology',
      'Jump',
      'Mechanical Repair',
      'Operate Heavy Machinery',
      'Stealth',
      'Spot Hidden'
    )
  },
  Missionary: {
    creditRating: [0, 30],
    skillPoints: ({ app, edu }) => sp(edu, app),
    skills: sk(
      oneOf(artsAndCrafts),
      'First Aid',
      'Mechanical Repair',
      'Medicine',
      'Natural World',
      oneOf(social)
    )
  },
  'Mountain Climber': {
    creditRating: [30, 60],
    skillPoints: ({ dex, edu, str }) => sp(edu, [dex, str]),
    skills: sk(
      'Climb',
      'First Aid',
      'Jump',
      'Listen',
      'Navigate',
      'Language (Other)',
      'Survival',
      'Track'
    )
  },
  'Museum Curator': {
    creditRating: [10, 30],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Accounting',
      'Appraise',
      'Archaeology',
      'History',
      'Library Use',
      'Occult',
      'Language (Other)',
      'Spot Hidden'
    )
  },
  Musician: {
    creditRating: [9, 30],
    skillPoints: ({ app, dex, edu }) => sp(edu, [app, dex]),
    skills: sk(
      'Art and Craft (Instrument)',
      oneOf(social),
      'Listen',
      'Psychology'
    )
  },
  Nurse: {
    creditRating: [9, 30],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'First Aid',
      'Listen',
      'Medicine',
      oneOf(social),
      'Psychology',
      'Science (Biology)',
      'Science (Chemistry)',
      'Spot Hidden'
    )
  },
  Occultist: {
    creditRating: [9, 65],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Anthropology',
      'History',
      'Library Use',
      oneOf(social),
      'Occult',
      'Language (Other)',
      'Science (Astronomy)'
    )
  },
  'Outdoorsman/woman': {
    creditRating: [5, 20],
    skillPoints: ({ dex, edu, str }) => sp(edu, [dex, str]),
    skills: sk(
      anyOf(firearms),
      'First Aid',
      'Listen',
      'Natural World',
      'Navigate',
      'Spot Hidden',
      'Survival',
      'Track'
    )
  },
  Parapsychologist: {
    creditRating: [9, 30],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Anthropology',
      'Art and Craft (Photography)',
      'History',
      'Library Use',
      'Occult',
      'Language (Other)',
      'Psychology'
    )
  },
  Pharmacist: {
    creditRating: [35, 75],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Accounting',
      'First Aid',
      'Language (Latin)',
      'Library Use',
      oneOf(social),
      'Psychology',
      'Science (Pharmacy)',
      'Science (Chemistry)'
    )
  },
  Photographer: {
    creditRating: [9, 30],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Art and Craft (Photography)',
      oneOf(social),
      'Psychology',
      'Science (Chemistry)',
      'Stealth',
      'Spot Hidden'
    )
  },
  Photojournalist: {
    creditRating: [10, 30],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Art and Craft (Photography)',
      'Climb',
      oneOf(social),
      'Language (Other)',
      'Psychology',
      'Science (Chemistry)'
    )
  },
  Pilot: {
    creditRating: [20, 70],
    skillPoints: ({ dex, edu }) => sp(edu, dex),
    skills: sk(
      'Electrical Repair',
      'Mechanical Repair',
      'Navigate',
      'Operate Heavy Machinery',
      'Pilot (Aircraft)',
      'Science (Astronomy)'
    )
  },
  'Police Detective': {
    creditRating: [20, 50],
    skillPoints: ({ dex, edu, str }) => sp(edu, [dex, str]),
    skills: sk(
      oneOf(['Art and Craft (Acting)', 'Disguise']),
      oneOf(firearms),
      'Law',
      'Listen',
      oneOf(social),
      'Psychology',
      'Spot Hidden'
    )
  },
  'Police Officer': {
    creditRating: [9, 30],
    skillPoints: ({ dex, edu, str }) => sp(edu, [dex, str]),
    skills: sk(
      'Fighting (Brawl)',
      oneOf(firearms),
      'First Aid',
      oneOf(social),
      'Law',
      'Psychology',
      'Spot Hidden'
    )
  },
  'Private Investigator': {
    creditRating: [9, 30],
    skillPoints: ({ dex, edu, str }) => sp(edu, [dex, str]),
    skills: sk(
      'Art and Craft (Photography)',
      'Disguise',
      'Law',
      'Library Use',
      oneOf(social),
      'Psychology',
      'Spot Hidden'
    )
  },
  Professor: {
    creditRating: [20, 70],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Library Use',
      'Language (Other)',
      'Language (Own)',
      'Psychology',
      twoOf(academics)
    )
  },
  Prospector: {
    creditRating: [0, 10],
    skillPoints: ({ dex, edu, str }) => sp(edu, [dex, str]),
    skills: sk(
      'Climb',
      'First Aid',
      'History',
      'Mechanical Repair',
      'Navigate',
      'Science (Geology)',
      'Spot Hidden'
    )
  },
  Prostitute: {
    creditRating: [5, 50],
    skillPoints: ({ app, edu }) => sp(edu, app),
    skills: sk(
      oneOf(artsAndCrafts),
      twoOf(social),
      'Dodge',
      'Psychology',
      'Sleight of Hand',
      'Stealth'
    )
  },
  Psychiatrist: {
    creditRating: [30, 80],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Language (Other)',
      'Listen',
      'Medicine',
      'Persuade',
      'Psychoanalysis',
      'Psychology',
      'Science (Biology)',
      'Science (Chemistry)'
    )
  },
  'Psychologist/Psychoanalyst': {
    creditRating: [10, 40],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Accounting',
      'Library Use',
      'Listen',
      'Persuade',
      'Psychoanalysis',
      'Psychology',
      oneOf(academics)
    )
  },
  Reporter: {
    creditRating: [9, 30],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Art and Craft (Acting)',
      'History',
      'Listen',
      'Language (Own)',
      oneOf(social),
      'Psychology',
      'Stealth',
      'Spot Hidden'
    )
  },
  Researcher: {
    creditRating: [9, 30],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'History',
      'Library Use',
      oneOf(social),
      'Language (Other)',
      'Spot Hidden',
      threeOf(academics)
    )
  },
  'Sailor, Commercial': {
    creditRating: [20, 40],
    skillPoints: ({ dex, edu, str }) => sp(edu, [dex, str]),
    skills: sk(
      'First Aid',
      'Mechanical Repair',
      'Natural World',
      'Navigate',
      oneOf(social),
      'Pilot (Boat)',
      'Spot Hidden',
      'Swim'
    )
  },
  'Sailor, Naval': {
    creditRating: [9, 30],
    skillPoints: ({ dex, edu, str }) => sp(edu, [dex, str]),
    skills: sk(
      oneOf(['Electrical Repair', 'Mechanical Repair']),
      'Fighting (Brawl)',
      anyOf(firearms),
      'First Aid',
      'Navigate',
      'Pilot (Boat)',
      'Survival',
      'Swim'
    )
  },
  Salesperson: {
    creditRating: [9, 40],
    skillPoints: ({ app, edu }) => sp(edu, app),
    skills: sk(
      'Accounting',
      twoOf(social),
      'Drive Auto',
      'Listen',
      'Psychology',
      oneOf(['Stealth', 'Sleight of Hand'])
    )
  },
  Scientist: {
    creditRating: [9, 50],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      threeOf(sciences),
      'Library Use',
      'Language (Other)',
      'Language (Own)',
      oneOf(social),
      'Spot Hidden'
    )
  },
  Secretary: {
    creditRating: [9, 30],
    skillPoints: ({ app, dex, edu }) => sp(edu, [dex, app]),
    skills: sk(
      'Accounting',
      oneOf(['Art and Craft (Typing)', 'Art and Craft (Short Hand)']),
      twoOf(social),
      'Language (Own)',
      'Library Use',
      'Psychology'
    )
  },
  Shopkeeper: {
    creditRating: [20, 40],
    skillPoints: ({ app, dex, edu }) => sp(edu, [app, dex]),
    skills: sk(
      'Accounting',
      twoOf(social),
      'Electrical Repair',
      'Listen',
      'Mechanical Repair',
      'Psychology',
      'Spot Hidden'
    )
  },
  Smuggler: {
    creditRating: [20, 60],
    skillPoints: ({ app, dex, edu }) => sp(edu, [app, dex]),
    skills: sk(
      oneOf(firearms),
      'Listen',
      'Navigate',
      oneOf(social),
      oneOf(['Drive Auto', oneOf(['Pilot (Aircraft)', 'Pilot (Boat)'])]),
      'Psychology',
      'Sleight of Hand',
      'Spot Hidden'
    )
  },
  'Soldier/Marine': {
    creditRating: [9, 30],
    skillPoints: ({ dex, edu, str }) => sp(edu, [dex, str]),
    skills: sk(
      oneOf(['Climb', 'Swim']),
      'Dodge',
      'Fighting (Brawl)',
      twoOf(firearms),
      'Stealth',
      'Survival',
      twoOf(['First Aid', 'Mechanical Repair', 'Language (Other)'])
    )
  },
  Spy: {
    creditRating: [20, 60],
    skillPoints: ({ app, dex, edu }) => sp(edu, [app, dex]),
    skills: sk(
      oneOf(['Art and Craft (Acting)', 'Disguise']),
      oneOf(firearms),
      'Listen',
      'Language (Other)',
      oneOf(social),
      'Psychology',
      'Sleight of Hand',
      'Stealth'
    )
  },
  'Stage Actor': {
    creditRating: [9, 40],
    skillPoints: ({ app, edu }) => sp(edu, app),
    skills: sk(
      'Art and Craft (Acting)',
      'Disguise',
      oneOf(fighting),
      'History',
      twoOf(social),
      'Psychology'
    )
  },
  'Street Punk': {
    creditRating: [3, 10],
    skillPoints: ({ dex, edu, str }) => sp(edu, [dex, str]),
    skills: sk(
      'Climb',
      oneOf(social),
      anyOf(fighting),
      'Firearms',
      'Jump',
      'Sleight of Hand',
      'Stealth',
      'Throw'
    )
  },
  'Student/Intern': {
    creditRating: [5, 10],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      oneOf(['Language (Own)', 'Language (Other)']),
      'Library Use',
      'Listen',
      threeOf(academics)
    )
  },
  Stuntman: {
    creditRating: [10, 50],
    skillPoints: ({ dex, edu, str }) => sp(edu, [dex, str]),
    skills: sk(
      'Climb',
      'Dodge',
      oneOf(['Electrical Repair', 'Mechanical Repair']),
      'Fighting (Brawl)',
      'First Aid',
      'Jump',
      'Swim',
      oneOf(['Diving', 'Drive Auto', 'Pilot (any)', 'Ride'])
    )
  },
  'Taxi Driver': {
    creditRating: [9, 30],
    skillPoints: ({ dex, edu }) => sp(edu, dex),
    skills: sk(
      'Accounting',
      'Drive Auto',
      'Electrical Repair',
      'Fast Talk',
      'Mechanical Repair',
      'Navigate',
      'Spot Hidden'
    )
  },
  'Tribe Member': {
    creditRating: [0, 15],
    skillPoints: ({ dex, edu, str }) => sp(edu, [str, dex]),
    skills: sk(
      'Climb',
      oneOf(['Fighting (Brawl)', 'Throw']),
      'Listen',
      'Natural World',
      'Occult',
      'Spot Hidden',
      'Swim',
      'Survival'
    )
  },
  Undertaker: {
    creditRating: [20, 40],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Accounting',
      'Drive Auto',
      oneOf(social),
      'History',
      'Occult',
      'Psychology',
      'Science (Biology)',
      'Science (Chemistry)'
    )
  },
  'Union Activist': {
    creditRating: [5, 30],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Accounting',
      twoOf(social),
      'Fighting (Brawl)',
      'Law',
      'Listen',
      'Operate Heavy Machinery',
      'Psychology'
    )
  },
  'Waitress/Waiter': {
    creditRating: [9, 20],
    skillPoints: ({ app, dex, edu }) => sp(edu, [app, dex]),
    skills: sk(
      'Accounting',
      oneOf(artsAndCrafts),
      'Dodge',
      'Listen',
      twoOf(social),
      'Psychology'
    )
  },
  Zealot: {
    creditRating: [0, 30],
    skillPoints: ({ app, edu, pow }) => sp(edu, [app, pow]),
    skill: sk('History', twoOf(social), 'Psychology', 'Stealth')
  },
  Zookeeper: {
    creditRating: [9, 40],
    skillPoints: ({ edu }) => edu * 4,
    skills: sk(
      'Animal Handling',
      'Accounting',
      'Dodge',
      'First Aid',
      'Natural World',
      'Medicine',
      'Science (Pharmacy)',
      'Science (Zoology)'
    )
  }
}

export default occupations
