export function roll (size, num = 1) {
  let total = 0
  for (let i = 0; i < num; i++) {
    total += Math.ceil(Math.random() * size)
  }
  return total
}

export const d6 = (num) => roll(6, num)
export const d10 = (num) => roll(10, num)
export const d100 = (num) => roll(100, num)

export function randomInt (min, max) {
  return Math.floor(Math.random() * (max - min) + min + 1)
}
