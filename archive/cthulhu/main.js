import * as React from 'react'
import Nav from '~/view/Nav'
import routes from '~/routes'
import styled from 'styled-components'
import useHashRoutes from '~/lib/useHashRoutes'
import { render } from 'react-dom'

const Main = styled.main`
  margin: 0px auto;
  padding: var(--large) var(--medium);

  @media (min-width: 900px) {
    margin-left: 250px;
  }
`

function App () {
  const page = useHashRoutes(routes)

  return (
    <>
      <Nav />
      <Main>{page}</Main>
    </>
  )
}

render(<App />, document.getElementById('Root'))
