import * as React from 'react'
import * as boutsOfMadness from '~/lib/bouts-of-madness'
import Button from '~/view/Button'
import { draw } from '~/lib/array'

export default function Madness () {
  const [generalBoutName, setGeneralBoutName] = React.useState()
  const [realTimeBoutName, setRealTimeBoutName] = React.useState()

  function generate () {
    setGeneralBoutName(draw(Object.keys(boutsOfMadness.general)))
    setRealTimeBoutName(draw(Object.keys(boutsOfMadness.realTime)))
  }

  return (
    <>
      <Button onClick={generate}>Generate</Button>
      <hr />
      {generalBoutName && (
        <>
          <h2>Bout of Madness (General): {generalBoutName}</h2>
          <p>{boutsOfMadness.general[generalBoutName]()}</p>
        </>
      )}
      {realTimeBoutName && (
        <>
          <h2>Bout of Madness (Real-time): {realTimeBoutName}</h2>
          <p>{boutsOfMadness.realTime[realTimeBoutName]()}</p>
        </>
      )}
    </>
  )
}
