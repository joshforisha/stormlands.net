import * as React from 'react'
import Button from '~/view/Button'
import NpcInfo from '~/view/NpcInfo'
import occupations from '~/lib/occupations'
import styled from 'styled-components'
import { Gender, generate as generateNpc } from '~/lib/NPC'
import { Size } from '~/lib/styles'

const Label = styled.label`
  margin-left: ${Size.Small};
`

const Select = styled.select`
  font-size: 1em;
`

export default function GenerateNpc () {
  const [age, setAge] = React.useState()
  const [gender, setGender] = React.useState()
  const [generating, setGenerating] = React.useState(false)
  const [npc, setNpc] = React.useState(null)
  const [occupation, setOccupation] = React.useState()

  function generate () {
    if (generating) return
    setGenerating(true)
    generateNpc({ age, gender, occupation })
      .then(newNpc => {
        setNpc(newNpc)
      })
      .finally(() => {
        setGenerating(false)
      })
  }

  function selectGender (event) {
    setGender(event.target.value || undefined)
  }

  function selectOccupation (event) {
    setOccupation(event.target.value || undefined)
  }

  function updateAge (event) {
    const value = Number(event.target.value)
    setAge(Number.isNaN(value) ? undefined : value)
  }

  const npcInfo = npc
    ? <NpcInfo npc={npc} />
    : generating
      ? <h3>Generating&hellip;</h3>
      : null

  const occupationOptions = Object.keys(occupations).map(
    occupation => (
      <option key={occupation} value={occupation}>
        {occupation}
      </option>
    )
  )

  return (
    <>
      <Button disabled={generating} onClick={generate}>
        Generate NPC
      </Button>
      <Label>
        Age:&nbsp;
        <input
          max='90'
          min='15'
          onChange={updateAge}
          step='1'
          type='number'
          value={age === undefined ? '' : String(age)}
        />
      </Label>
      <Label>
        Gender:&nbsp;
        <Select onChange={selectGender} value={gender}>
          <option value=''>Either</option>
          <option value={Gender.Female}>{Gender.Female}</option>
          <option value={Gender.Male}>{Gender.Male}</option>
        </Select>
      </Label>
      <Label>
        Occupation:&nbsp;
        <Select onChange={selectOccupation} value={occupation}>
          <option value=''>Any</option>
          {occupationOptions}
        </Select>
      </Label>
      <hr />
      {npcInfo}
    </>
  )
}
