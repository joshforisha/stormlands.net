import BeastMaker from '~/view/BeastMaker'
import BuildABot from '~/view/BuildABot'
import PassenGen from '~/view/PassenGen'
import React from 'react'
import ThingMaker from '~/view/ThingMaker'
import { FaRobot } from 'react-icons/fa'
import { GiAnimalSkull } from 'react-icons/gi'
import { IoMdCube } from 'react-icons/io'
import { MdAirlineSeatReclineExtra } from 'react-icons/md'

export default [
  {
    disabled: true,
    link: <><GiAnimalSkull />BeastMaker</>,
    path: '#beast-maker',
    view: <BeastMaker />
  },
  {
    link: <><FaRobot />Build-A-Bot</>,
    path: '#build-a-bot',
    view: <BuildABot />
  },
  {
    disabled: true,
    link: <><MdAirlineSeatReclineExtra />PassenGen</>,
    path: '#passengen',
    view: <PassenGen />
  },
  {
    disabled: true,
    link: <><IoMdCube />ThingMaker</>,
    path: '#thing-maker',
    view: <ThingMaker />
  }
]
