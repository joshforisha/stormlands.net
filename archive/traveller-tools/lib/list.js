export function draw (xs) {
  if (xs.length < 1) return
  return xs[Math.floor(Math.random() * xs.length)]
}

export function intersperse (y, xs) {
  if (xs.length < 2) return xs

  const zs = []
  const lastIndex = xs.length - 1
  xs.forEach(
    (x, i) => {
      zs.push(x)
      if (i < lastIndex) zs.push(y(i))
    }
  )

  return zs
}

export function weighted (xs) {
  const total = xs.reduce((tw, [w]) => tw + w, 0)
  const roll = Math.random() * total
  let max = 0
  for (let i = 0; i < xs.length; i++) {
    max += xs[i][0]
    if (roll < max) return xs[i][1]
  }
}
