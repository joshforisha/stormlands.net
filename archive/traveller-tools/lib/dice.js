const Dice = {
  D3: 'D3',
  One: '1D',
  Two: '2D',
  Three: '3D',
  Four: '4D',
  Five: '5D',
  Six: '6D',
  Seven: '7D',
  Eight: '8D'
}
export default Dice

export function higherRoll (dice) {
  switch (dice) {
    case Dice.D3: return Dice.One
    case Dice.One: return Dice.Two
    case Dice.Two: return Dice.Three
    case Dice.Three: return Dice.Four
    case Dice.Four: return Dice.Five
    case Dice.Five: return Dice.Six
    case Dice.Six: return Dice.Seven
    case Dice.Seven: return Dice.Eight
    default: return dice
  }
}

export function lowerRoll (dice) {
  switch (dice) {
    case Dice.One: return Dice.D3
    case Dice.Two: return Dice.One
    case Dice.Three: return Dice.Two
    case Dice.Four: return Dice.Three
    case Dice.Five: return Dice.Four
    case Dice.Six: return Dice.Five
    case Dice.Seven: return Dice.Six
    case Dice.Eight: return Dice.Seven
    default: return dice
  }
}
