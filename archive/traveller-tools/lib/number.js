export function randomFloat (min, max) {
  return min + Math.floor(Math.random() * (max - min + 1))
}

export function randomInt (min, max) {
  return Math.floor(min + Math.random() * (max - min + 1))
}
