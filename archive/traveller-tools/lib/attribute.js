export function handle (fn, t, def) {
  if (typeof fn === 'function') return fn(t)
  if (typeof fn !== 'undefined') return fn
  return def
}
