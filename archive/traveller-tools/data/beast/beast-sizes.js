import Dice from '~/lib/dice'

export default [
  {
    animal: 'Mouse or Rat',
    damage: '1',
    hits: [1, 2],
    traits: ['Small (-4)']
  },
  {
    animal: 'Cat or Racoon',
    damage: Dice.D3,
    hits: [3, 5],
    traits: ['Small (-3)']
  },
  {
    animal: 'Badger or Dog',
    damage: Dice.D3,
    hits: [6, 7],
    traits: ['Small (-2)']
  },
  {
    animal: 'Chimpanzee or Goat',
    damage: Dice.One,
    hits: [8, 13],
    traits: ['Small (-1)']
  },
  {
    animal: 'Human',
    damage: Dice.One,
    hits: [],
    traits: ['']
  },
  {
    animal: '',
    damage: '',
    hits: [],
    traits: ['Large (+1)']
  },
  {
    animal: '',
    damage: '',
    hits: [],
    traits: ['Large (+2)']
  },
  {
    animal: '',
    damage: '',
    hits: [],
    traits: ['Large (+3)']
  },
  {
    animal: '',
    damage: '',
    hits: [],
    traits: ['Large (+4)']
  },
  {
    animal: '',
    damage: '',
    hits: [],
    traits: ['Large (+5)']
  },
  {
    animal: '',
    damage: '',
    hits: [],
    traits: ['Large (+6)']
  }
]
