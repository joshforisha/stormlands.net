const frameTraits = [
  {
    name: 'Aerodynamic',
    complexity: 1,
    speed: 1,
    techLevel: 7,
    traits: ['Aerodynamic']
  },
  {
    name: 'Arm',
    complexity: 2,
    attacks: ['Arm (1D)'],
    multiple: true,
    skills: ['Athletics (dexterity) 1'],
    techLevel: 7,
    traits: ['Arm'],
    resources: {
      'Raw Materials': (brm) => Math.ceil(0.2 * brm)
    },
    requires: {
      slots: 1
    }
  },
  {
    name: 'Armour (1)',
    complexity: 1,
    armour: 1,
    techLevel: 6,
    traits: ['Armour (1)'],
    uniqueAmong: 'armour',
    resources: {
      'Raw Materials': (brm) => Math.ceil(0.05 * brm)
    }
  },
  {
    name: 'Armour (2)',
    complexity: 1,
    armour: 2,
    techLevel: 6,
    traits: ['Armour (2)'],
    uniqueAmong: 'armour',
    resources: {
      'Raw Materials': (brm) => Math.ceil(0.1 * brm)
    }
  },
  {
    name: 'Armour (3)',
    complexity: 1,
    armour: 3,
    techLevel: 6,
    traits: ['Armour (3)'],
    uniqueAmong: 'armour',
    resources: {
      'Raw Materials': (brm) => Math.ceil(0.15 * brm)
    }
  },
  {
    name: 'Armour (4)',
    complexity: 1,
    armour: 4,
    techLevel: 7,
    traits: ['Armour (4)'],
    uniqueAmong: 'armour',
    resources: {
      'Raw Materials': (brm) => Math.ceil(0.2 * brm)
    }
  },
  {
    name: 'Armour (5)',
    complexity: 1,
    armour: 5,
    techLevel: 7,
    traits: ['Armour (5)'],
    uniqueAmong: 'armour',
    resources: {
      'Raw Materials': (brm) => Math.ceil(0.25 * brm)
    }
  },
  {
    name: 'Armour (6)',
    complexity: 2,
    armour: 6,
    techLevel: 7,
    traits: ['Armour (6)'],
    uniqueAmong: 'armour',
    resources: {
      'Raw Materials': (brm) => Math.ceil(0.3 * brm)
    }
  },
  {
    name: 'Armour (7)',
    complexity: 2,
    armour: 7,
    techLevel: 7,
    traits: ['Armour (7)'],
    uniqueAmong: 'armour',
    resources: {
      'Raw Materials': (brm) => Math.ceil(0.35 * brm)
    }
  },
  {
    name: 'Armour (8)',
    complexity: 2,
    armour: 8,
    techLevel: 7,
    traits: ['Armour (8)'],
    uniqueAmong: 'armour',
    resources: {
      'Raw Materials': (brm) => Math.ceil(0.4 * brm)
    }
  },
  {
    name: 'Armour (9)',
    complexity: 2,
    armour: 9,
    techLevel: 7,
    traits: ['Armour (9)'],
    uniqueAmong: 'armour',
    resources: {
      'Raw Materials': (brm) => Math.ceil(0.45 * brm)
    }
  },
  {
    name: 'Armour (10)',
    complexity: 2,
    armour: 10,
    techLevel: 7,
    traits: ['Armour (10)'],
    uniqueAmong: 'armour',
    resources: {
      'Raw Materials': (brm) => Math.ceil(0.5 * brm)
    }
  },
  {
    name: 'Auto-Repair (Embedded)',
    complexity: 10,
    techLevel: 12,
    traits: ['Auto-Repair'],
    uniqueAmong: 'auto-repair',
    resources: {
      'Advanced Machine Parts': brm => Math.ceil(0.025 * brm),
      Polymers: brm => Math.ceil(0.025 * brm)
    }
  },
  {
    name: 'Decorative',
    complexity: 1,
    description: 'DM+1 to social checks',
    techLevel: 6,
    traits: ['Decorative']
  },
  {
    name: 'Divider',
    complexity: 2,
    description: 'Can split into multiple smaller bots',
    techLevel: 8,
    traits: ['Divider'],
    resources: {
      'Raw Materials': (brm) => Math.ceil(0.25 * brm)
    }
  },
  {
    name: 'Hardwearing',
    complexity: 3,
    description: 'Withstands environmental effects',
    techLevel: 7,
    traits: ['Hardwearing'],
    hits: ({ frame: { size } }) => size * 2,
    resources: {
      'Raw Materials': (brm) => Math.ceil(0.1 * brm)
    }
  },
  {
    name: 'Lightweight',
    complexity: 2,
    reducedMass: 0.25,
    speed: 2,
    techLevel: 7,
    traits: ['Lightweight'],
    requires: {
      reducedHits: 2,
      mobile: true
    }
  },
  {
    name: 'Sealed',
    complexity: 1,
    description: 'Prevents water and gas from getting inside',
    techLevel: 7,
    traits: ['Sealed'],
    resources: {
      'Raw Materials': (brm) => Math.ceil(0.1 * brm)
    }
  },
  {
    name: 'Strengthened',
    complexity: 2,
    description: 'Hold/carry up to 75% mass',
    hits: ({ frame: { size } }) => size * 2,
    skills: ['Athletics (endurance) 1'],
    techLevel: 7,
    traits: ['Strengthened'],
    resources: {
      'Raw Materials': (brm) => Math.ceil(0.25 * brm)
    }
  },
  {
    name: 'Thinmeld Armour',
    complexity: 3,
    armour: 14,
    techLevel: 12,
    traits: ['Armour (14)'],
    uniqueAmong: 'armour',
    resources: {
      'Raw Materials': (brm) => Math.ceil(0.2 * brm),
      'Advanced Machine Parts': (brm) => Math.ceil(0.1 * brm),
      Polymers: (brm) => Math.ceil(0.05 * brm)
    }
  }
]

export default frameTraits
