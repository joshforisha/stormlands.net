export default [
  {
    baseRawMaterials: 5,
    complexity: 1,
    hits: 1,
    scale: 'Mouse',
    size: 1,
    slots: 1,
    traits: ['Small (-4)']
  },
  {
    baseRawMaterials: 20,
    complexity: 2,
    hits: 3,
    scale: 'Cat',
    size: 2,
    slots: 2,
    traits: ['Small (-3)']
  },
  {
    baseRawMaterials: 40,
    complexity: 3,
    hits: 6,
    scale: 'Dog',
    size: 3,
    slots: 3,
    traits: ['Small (-2)']
  },
  {
    baseRawMaterials: 60,
    complexity: 4,
    hits: 8,
    scale: 'Chimpanzee',
    size: 4,
    slots: 5,
    traits: ['Small (-1)']
  },
  {
    baseRawMaterials: 100,
    complexity: 5,
    hits: 14,
    scale: 'Human',
    size: 5,
    slots: 8,
    traits: []
  },
  {
    baseRawMaterials: 200,
    complexity: 6,
    hits: 29,
    scale: 'Horse',
    size: 6,
    slots: 12,
    traits: ['Large (+1)']
  },
  {
    baseRawMaterials: 400,
    complexity: 7,
    hits: 36,
    scale: 'Moose',
    size: 7,
    slots: 16,
    traits: ['Large (+2)']
  },
  {
    baseRawMaterials: 1000,
    complexity: 8,
    hits: 50,
    scale: 'Rhino',
    size: 8,
    slots: 20,
    traits: ['Large (+3)']
  },
  {
    baseRawMaterials: 2000,
    complexity: 9,
    hits: 71,
    scale: 'Elephant',
    size: 9,
    slots: 24,
    traits: ['Large (+4)']
  },
  {
    baseRawMaterials: 5000,
    complexity: 10,
    hits: 91,
    scale: 'Carnasaur',
    size: 10,
    slots: 28,
    traits: ['Large (+5)']
  },
  {
    baseRawMaterials: 10000,
    complexity: 11,
    hits: 126,
    scale: 'Sauropod',
    size: 11,
    slots: 32,
    traits: ['Large (+6)']
  }
]
