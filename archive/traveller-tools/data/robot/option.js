import { handle } from '~/lib/attribute'
import { units } from '~/lib/text'

export function formatGains (option, record) {
  const output = []
  if ('description' in option) output.push(option.description)
  if ('armour' in option) output.push(`+${option.armour} Armour`)
  if ('attacks' in option) output.push(...handle(option.attacks, record))
  if ('hits' in option) output.push(`+${handle(option.hits, record)} Hits`)
  if ('reducedMass' in option) {
    output.push(
      `-${Math.round(100 * handle(option.reducedMass, record))}% Mass`
    )
  }
  if ('speed' in option) output.push(`+${handle(option.speed, record)} Speed`)

  if (output.length > 0) return output.join(', ')
}

export function formatRequirements (
  option,
  baseRawMaterials
) {
  const output = []

  if ('requires' in option) {
    const req = option.requires

    if ('minimumSize' in req) output.push(`Size ${req.minimumSize}+`)
    if ('mobile' in req) {
      if (req.mobile) output.push('Mobile')
      else output.push('Immobile')
    }
    if ('reduceHits' in req) output.push(`-${req.reduceHits} Hits`)
    if ('slots' in req) {
      output.push(`${req.slots} Slot${req.slots === 1 ? '' : 's'}`)
    }
  }

  for (const name in option.resources) {
    output.push(
      `${units(handle(option.resources[name], baseRawMaterials))} ${name}`
    )
  }

  if (output.length > 0) return output.join(', ')
}

export function validate (option, robot) {
  if (
    !option.multiple &&
    robot.options.some(o => o.name === option.name)
  ) {
    return false
  }

  if (
    'uniqueAmong' in option &&
    robot.options.some(o => o.uniqueAmong === option.uniqueAmong)
  ) {
    return false
  }

  if ('requires' in option) {
    if (
      'minimumSize' in option.requires &&
      robot.frame.size < option.requires.minimumSize
    ) { return false }

    if ('mobile' in option.requires && robot.speed === 0) return false

    if (
      'reduceHits' in option.requires &&
      robot.hits <= option.requires.reduceHits
    ) { return false }

    if (
      'slots' in option.requires &&
      robot.availableSlots < option.requires.slots
    ) { return false }
  }

  return true
}
