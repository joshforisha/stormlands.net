const outputModules = [
  {
    name: 'Holographic Projector (Advanced)',
    isEquipment: true,
    complexity: 5,
    techLevel: 10,
    resources: {
      'Advanced Machine Parts': 5
    },
    requires: { slots: 1, techLevel: 12 }
  },
  {
    name: 'Holographic Projector (Basic)',
    isEquipment: true,
    complexity: 7,
    techLevel: 9,
    resources: {
      'Advanced Machine Parts': 3
    },
    requires: { slots: 1 }
  },
  {
    name: 'Laser (Regional)',
    isEquipment: true,
    complexity: 4,
    techLevel: 7,
    resources: {
      'Advanced Machine Parts': 4
    },
    requires: { minimumSize: 3, slots: 1 }
  },
  {
    name: 'Loud Speaker',
    isEquipment: true,
    complexity: 1,
    techLevel: 6,
    resources: {
      'Raw Materials': 4
    },
    requires: { slots: 1 }
  },
  {
    name: 'Maser (Regional)',
    isEquipment: true,
    complexity: 7,
    techLevel: 7,
    resources: {
      'Advanced Machine Parts': 2
    },
    requires: { minimumSize: 4, slots: 1 }
  },
  {
    name: 'Meson (Continental)',
    isEquipment: true,
    complexity: 10,
    techLevel: 8,
    resources: {
      'Advanced Machine Parts': 100
    },
    requires: { minimumSize: 6, slots: 1 }
  },
  {
    name: 'Odour Emitter',
    isEquipment: true,
    complexity: 4,
    techLevel: 7,
    resources: {
      'Advanced Machine Parts': 2
    },
    requires: { slots: 1 }
  },
  {
    name: 'Pheromone Emitter',
    isEquipment: true,
    complexity: 4,
    techLevel: 7,
    resources: {
      'Advanced Machine Parts': 2
    },
    requires: { slots: 1 }
  },
  {
    name: 'Radio Transceiver (Continental)',
    isEquipment: true,
    complexity: 5,
    techLevel: 8,
    resources: {
      'Advanced Machine Parts': 4
    },
    requires: { minimumSize: 6, slots: 1 }
  },
  {
    name: 'Radio Transceiver (Distant)',
    isEquipment: true,
    complexity: 2,
    techLevel: 6,
    resources: {
      'Raw Materials': 2
    },
    requires: { minimumSize: 3, slots: 1 }
  },
  {
    name: 'Radio Transceiver (Regional)',
    isEquipment: true,
    complexity: 4,
    techLevel: 7,
    resources: {
      'Advanced Machine Parts': 2
    },
    requires: { minimumSize: 5, slots: 1 }
  },
  {
    name: 'Radio Transceiver (Very Distant)',
    isEquipment: true,
    complexity: 3,
    techLevel: 7,
    resources: {
      'Raw Materials': 2
    },
    requires: { minimumSize: 4, slots: 1 }
  },
  {
    name: 'Ultrasonic Emitter',
    isEquipment: true,
    complexity: 1,
    techLevel: 7,
    resources: {
      'Raw Materials': 4
    },
    requires: { slots: 1 }
  },
  {
    name: 'Vocoder (Advanced)',
    isEquipment: true,
    complexity: 3,
    techLevel: 7,
    resources: {
      'Advanced Machine Parts': 1
    },
    requires: { slots: 1 }
  },
  {
    name: 'Vocoder (Basic)',
    isEquipment: true,
    complexity: 1,
    techLevel: 6,
    resources: {
      'Raw Materials': 1
    },
    requires: { slots: 1 }
  }
]

export default outputModules
