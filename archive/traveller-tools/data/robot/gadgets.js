const gadgets = [
  {
    name: 'Auto-Repair',
    isEquipment: true,
    complexity: 8,
    techLevel: 10,
    uniqueAmong: 'auto-repair',
    resources: {
      'Advanced Machine Parts': brm => Math.ceil(0.05 * brm)
    },
    requires: { slots: 1 }
  },
  {
    name: 'Bio-Cover',
    isEquipment: true,
    complexity: 6,
    techLevel: 11,
    resources: {
      'Advanced Machine Parts': brm => Math.ceil(0.025 * brm),
      Biochemicals: brm => Math.ceil(0.025 * brm)
    },
    requires: { slots: 1 }
  },
  {
    name: 'Black Box',
    isEquipment: true,
    complexity: 2,
    techLevel: 7,
    resources: {
      'Raw Materials': brm => Math.ceil(0.05 * brm)
    },
    requires: { slots: 1 }
  },
  {
    name: 'Blades/Spikes',
    isEquipment: true,
    complexity: 1,
    techLevel: 6,
    attacks: ['Blades/Spikes (3D)'],
    resources: {
      'Raw Materials': brm => Math.ceil(0.05 * brm)
    },
    requires: { slots: 1 }
  },
  {
    name: 'Blow Dryer',
    isEquipment: true,
    complexity: 2,
    techLevel: 6,
    resources: {
      'Raw Materials': 3
    },
    requires: { minimumSize: 2, slots: 1 }
  },
  {
    name: 'Camouflage',
    isEquipment: true,
    complexity: 4,
    techLevel: 7,
    resources: {
      'Raw Materials': brm => Math.ceil(0.03 * brm)
    },
    requires: { slots: 1 }
  },
  {
    name: 'Chameleon (IR)',
    isEquipment: true,
    complexity: 6,
    techLevel: 11,
    resources: {
      'Advanced Machine Parts': brm => Math.ceil(0.1 * brm),
      'Raw Materials': brm => Math.ceil(0.05 * brm)
    },
    requires: { slots: 1 }
  },
  {
    name: 'Chameleon (Visible)',
    isEquipment: true,
    complexity: 8,
    techLevel: 12,
    resources: {
      'Advanced Machine Parts': brm => Math.ceil(0.1 * brm),
      'Raw Materials': brm => Math.ceil(0.05 * brm)
    },
    requires: { slots: 1 }
  },
  {
    name: 'Circuit Protection',
    isEquipment: true,
    complexity: 2,
    techLevel: 7,
    resources: {
      'Raw Materials': brm => Math.ceil(0.05 * brm)
    },
    requires: { slots: 1 }
  },
  {
    name: 'Cooler',
    isEquipment: true,
    complexity: 2,
    techLevel: 6,
    resources: {
      'Raw Materials': brm => Math.ceil(0.04 * brm)
    },
    requires: { minimumSize: 2, slots: 1 }
  },
  {
    name: 'Energy Shield',
    isEquipment: true,
    complexity: 10,
    techLevel: 14,
    resources: {
      'Advanced Machine Parts': brm => Math.ceil(0.15 * brm)
    },
    requires: { slots: 1 }
  },
  {
    name: 'Fire Extinguisher',
    isEquipment: true,
    complexity: 4,
    techLevel: 7,
    resources: {
      'Raw Materials': 2
    },
    requires: { minimumSize: 3, slots: 1 }
  },
  {
    name: 'GPS (inertial)',
    isEquipment: true,
    complexity: 8,
    techLevel: 12,
    resources: {
      'Advanced Machine Parts': 5,
      'Raw Materials': 2
    },
    requires: { slots: 1 }
  },
  {
    name: 'GPS (satellite)',
    isEquipment: true,
    complexity: 2,
    techLevel: 7,
    resources: {
      'Raw Materials': 4
    },
    requires: { slots: 1 }
  },
  {
    name: 'Gyroscope',
    isEquipment: true,
    complexity: 4,
    techLevel: 8,
    resources: {
      'Advanced Machine Parts': 3
    },
    requires: { slots: 1 }
  },
  {
    name: 'Hacking Device',
    isEquipment: true,
    complexity: 2,
    techLevel: 9,
    resources: {
      'Advanced Machine Parts': 3
    },
    requires: { slots: 1 }
  },
  {
    name: 'Heater',
    isEquipment: true,
    complexity: 1,
    techLevel: 6,
    resources: {
      Biochemicals: 1,
      'Raw Materials': brm => Math.ceil(0.1 * brm)
    },
    requires: { slots: 1 }
  },
  {
    name: 'Hooks',
    isEquipment: true,
    complexity: 1,
    techLevel: 7,
    resources: {
      'Raw Materials': brm => Math.ceil(0.08 * brm)
    },
    requires: { slots: 1 }
  },
  {
    name: 'Household Cleaner',
    isEquipment: true,
    complexity: 1,
    techLevel: 6,
    resources: {
      Biochemicals: 1,
      'Raw Materials': brm => Math.ceil(0.1 * brm)
    },
    requires: { slots: 1 }
  },
  {
    name: 'Jamming Device (Large)',
    isEquipment: true,
    complexity: 2,
    techLevel: 8,
    resources: {
      'Advanced Machine Parts': brm => Math.ceil(0.05 * brm),
      'Raw Materials': brm => Math.ceil(0.05 * brm)
    },
    requires: { minimumSize: 4, slots: 1 }
  },
  {
    name: 'Jamming Device (Small)',
    isEquipment: true,
    complexity: 1,
    techLevel: 8,
    resources: {
      'Advanced Machine Parts': brm => Math.ceil(0.1 * brm)
    },
    requires: { slots: 1 }
  },
  {
    name: 'Lightbar',
    isEquipment: true,
    complexity: 1,
    techLevel: 7,
    resources: {
      'Raw Materials': brm => Math.ceil(0.04 * brm)
    },
    requires: { slots: 1 }
  },
  {
    name: 'Lockpick (Advanced)',
    isEquipment: true,
    complexity: 4,
    techLevel: 8,
    resources: {
      'Advanced Machine Parts': 1,
      'Raw Materials': 1
    },
    requires: { slots: 1 }
  },
  {
    name: 'Lockpick (Basic)',
    isEquipment: true,
    complexity: 1,
    techLevel: 7,
    resources: {
      'Raw Materials': 1
    },
    requires: { slots: 1 }
  },
  {
    name: 'Magnetic Grapples',
    isEquipment: true,
    complexity: 1,
    techLevel: 8,
    resources: {
      'Raw Materials': brm => Math.ceil(0.04 * brm)
    },
    requires: { slots: 1 }
  },
  {
    name: 'Medical Scanner',
    isEquipment: true,
    complexity: 6,
    textLevel: 11,
    resources: {
      'Advanced Machine Parts': 5,
      Biochemicals: 5
    },
    requires: { slots: 1 }
  },
  {
    name: 'Microwave Oven',
    isEquipment: true,
    complexity: 2,
    techLevel: 6,
    resources: {
      'Raw Materials': 4
    },
    requires: { minimumSize: 3, slots: 1 }
  },
  {
    name: 'Mini-Fridge',
    isEquipment: true,
    complexity: 3,
    techLevel: 7,
    resources: {
      Biochemicals: brm => Math.ceil(0.025 * brm),
      'Raw Materials': brm => Math.ceil(0.15 * brm)
    },
    requires: { minimumSize: 2, slots: 1 }
  },
  {
    name: 'Oxy-Fuel Cutter',
    isEquipment: true,
    complexity: 2,
    techLevel: 9,
    resources: {
      'Raw Materials': brm => Math.ceil(0.06 * brm),
      Biochemicals: brm => Math.ceil(0.05 * brm)
    },
    requires: { slots: 1 }
  },
  {
    name: 'Safe',
    isEquipment: true,
    complexity: 1,
    techLevel: 6,
    resources: {
      'Raw Materials': 30
    },
    requires: { minimumSize: 3, slots: 1 }
  },
  {
    name: 'Self-Destruct (Nuclear)',
    isEquipment: true,
    complexity: 10,
    techLevel: 11,
    resources: {
      'Advanced Machine Parts': brm => Math.ceil(0.15 * brm),
      'Advanced Weapons': brm => Math.ceil(0.15 * brm),
      Biochemicals: brm => Math.ceil(0.05 * brm)
    },
    requires: { slots: 1 }
  },
  {
    name: 'Self-Destruct (Personal)',
    isEquipment: true,
    complexity: 3,
    techLevel: 10,
    resources: {
      'Raw Materials': brm => Math.ceil(0.1 * brm),
      Biochemicals: brm => Math.ceil(0.05 * brm)
    },
    requires: { slots: 1 }
  },
  {
    name: 'Self-Destruct (Tactical)',
    isEquipment: true,
    complexity: 10,
    techLevel: 11,
    resources: {
      'Advanced Machine Parts': brm => Math.ceil(0.2 * brm),
      Biochemicals: brm => Math.ceil(0.05 * brm)
    },
    requires: { slots: 1 }
  },
  {
    name: 'Spotlight',
    isEquipment: true,
    complexity: 1,
    techLevel: 6,
    resources: {
      'Raw Materials': brm => Math.ceil(0.1 * brm)
    },
    requires: { slots: 1 }
  },
  {
    name: 'Timer',
    isEquipment: true,
    complexity: 2,
    techLevel: 7,
    resources: {
      'Raw Materials': 1
    },
    requires: { slots: 1 }
  },
  {
    name: 'Video Display',
    isEquipment: true,
    complexity: 2,
    techLevel: 7,
    resources: {
      'Raw Materials': brm => Math.ceil(0.1 * brm)
    },
    requires: { slots: 1 }
  }
]

export default gadgets
