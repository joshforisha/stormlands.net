import { sizeOffset } from '~/data/robot/frame'

function speedFrom (max) {
  return ({ frame }) => max - sizeOffset(frame)
}

const mobilityOptions = [
  {
    name: 'Anti-Grav',
    complexity: 3,
    speed: speedFrom(9),
    skills: ['Athletics 0', 'Flyer (grav) 1'],
    techLevel: 9,
    traits: ['Anti-Grav'],
    resources: {
      'Advanced Machine Parts': brm => Math.ceil(0.25 * brm)
    }
  },
  {
    name: 'Legs',
    complexity: 1,
    attacks: ({ frame: { size } }) => (size > 3 ? ['Legs (1D)'] : []),
    skills: ['Athletics 0'],
    speed: speedFrom(5),
    techLevel: 7,
    traits: ['Legs'],
    resources: {
      'Raw Materials': brm => Math.ceil(0.15 * brm)
    }
  },
  {
    name: 'None',
    complexity: 0,
    techLevel: 6,
    traits: ['Stationary']
  },
  {
    name: 'Rotors',
    complexity: 2,
    attacks: ({ frame: { size } }) => [`Rotor Blades (${size}D)`],
    skills: ['Athletics 0', 'Flyer (rotor) 1'],
    speed: speedFrom(7),
    techLevel: 7,
    traits: ['Rotors'],
    resources: {
      'Raw Materials': brm => Math.ceil(0.2 * brm)
    }
  },
  {
    name: 'Sprawling Legs',
    complexity: 2,
    hits: 1,
    skills: ['Athletics 0'],
    speed: speedFrom(6),
    techLevel: 7,
    traits: ['Sprawling Legs'],
    attacks: ({ frame: { size } }) => (size > 3 ? ['Legs (2D)'] : []),
    resources: {
      'Raw Materials': brm => Math.ceil(0.2 * brm)
    }
  },
  {
    name: 'Tracks',
    complexity: 1,
    hits: 2,
    skills: ['Athletics 0'],
    speed: speedFrom(5),
    techLevel: 6,
    traits: ['Tracks'],
    resources: {
      'Raw Materials': brm => Math.ceil(0.25 * brm)
    }
  },
  {
    name: 'Wheels',
    complexity: 1,
    skills: ['Athletics 0'],
    speed: speedFrom(8),
    techLevel: 6,
    traits: ['Wheels'],
    resources: {
      'Raw Materials': brm => Math.ceil(0.15 * brm)
    }
  },
  {
    name: 'Enhanced Anti-Grav',
    complexity: 5,
    skills: ['Athletics 0', 'Flyer (grav) 1'],
    speed: speedFrom(10),
    techLevel: 10,
    traits: ['Enhanced Anti-Grav'],
    resources: {
      'Advanced Machine Parts': brm => Math.ceil(0.45 * brm)
    }
  },
  {
    name: 'Enhanced Legs',
    complexity: 3,
    skills: ['Athletics 0'],
    speed: speedFrom(7),
    techLevel: 8,
    traits: ['Enhanced Legs'],
    attacks: ({ frame: { size } }) => (size > 3 ? ['Legs (2D)'] : []),
    resources: {
      'Advanced Machine Parts': brm => Math.ceil(0.2 * brm),
      'Raw Materials': brm => Math.ceil(0.15 * brm)
    }
  },
  {
    name: 'Enhanced Rotors',
    complexity: 4,
    skills: ['Athletics 0', 'Flyer (rotor) 1'],
    speed: speedFrom(9),
    techLevel: 8,
    traits: ['Enhanced Rotors'],
    attacks: ({ frame: { size } }) => [`Rotor Blades (${size + 1}D)`],
    resources: {
      'Advanced Machine Parts': brm => Math.ceil(0.2 * brm),
      'Raw Materials': brm => Math.ceil(0.2 * brm)
    }
  },
  {
    name: 'Enhanced Sprawling Legs',
    complexity: 4,
    hits: 1,
    skills: ['Athletics 0'],
    speed: speedFrom(8),
    techLevel: 8,
    traits: ['Enhanced Sprawling Legs'],
    attacks: ({ frame: { size } }) => (size > 3 ? ['Legs (3D)'] : []),
    resources: {
      'Advanced Machine Parts': brm => Math.ceil(0.2 * brm),
      'Raw Materials': brm => Math.ceil(0.2 * brm)
    }
  },
  {
    name: 'Enhanced Tracks',
    complexity: 3,
    hits: 2,
    skills: ['Athletics 0'],
    speed: speedFrom(7),
    techLevel: 7,
    traits: ['Enhanced Tracks'],
    resources: {
      'Advanced Machine Parts': brm => Math.ceil(0.2 * brm),
      'Raw Materials': brm => Math.ceil(0.25 * brm)
    }
  },
  {
    name: 'Enhanced Wheels',
    complexity: 3,
    skills: ['Athletics 0'],
    speed: speedFrom(10),
    techLevel: 7,
    traits: ['Enhanced Wheels'],
    resources: {
      'Advanced Machine Parts': brm => Math.ceil(0.2 * brm),
      'Raw Materials': brm => Math.ceil(0.15 * brm)
    }
  }
]

export default mobilityOptions
