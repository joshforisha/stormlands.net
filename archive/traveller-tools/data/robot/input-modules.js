const inputModules = [
  {
    name: 'Audio Sensor',
    isEquipment: true,
    complexity: 1,
    techLevel: 6,
    requires: { slots: 1 },
    resources: {
      'Raw Materials': 1
    }
  },
  {
    name: 'Audio Sensor (Enhanced)',
    isEquipment: true,
    complexity: 2,
    techLevel: 7,
    requires: { slots: 1 },
    resources: {
      'Raw Materials': 1,
      'Advanced Machine Parts': 1
    }
  },
  {
    name: 'Holographic Recorder',
    isEquipment: true,
    complexity: 4,
    techLevel: 8,
    requires: { slots: 1, techLevel: 11 },
    resources: {
      'Advanced Machine Parts': 4
    }
  },
  {
    name: 'Infrared Vision',
    isEquipment: true,
    complexity: 1,
    techLevel: 6,
    resources: {
      'Raw Materials': 4
    },
    requires: { slots: 1 }
  },
  {
    name: 'Low-Light Vision',
    isEquipment: true,
    complexity: 1,
    techLevel: 6,
    resources: {
      'Raw Materials': 2
    },
    requires: { slots: 1 }
  },
  {
    name: 'Medical Scanner',
    isEquipment: true,
    complexity: 4,
    techLevel: 7,
    resources: {
      'Advanced Machine Parts': 6
    },
    requires: { slots: 1 }
  },
  {
    name: 'Microscope',
    isEquipment: true,
    complexity: 1,
    techLevel: 7,
    resources: {
      'Raw Materials': 2
    },
    requires: { slots: 1 }
  },
  {
    name: 'Motion Detector',
    isEquipment: true,
    complexity: 1,
    techLevel: 7,
    resources: {
      'Raw Materials': 4
    },
    requires: { slots: 1 }
  },
  {
    name: 'Neural Activity Sensor',
    isEquipment: true,
    complexity: 8,
    techLevel: 9,
    resources: {
      'Advanced Machine Parts': 20,
      Biochemicals: 10
    },
    requires: { slots: 1, techLevel: 14 }
  },
  {
    name: 'Optics (Advanced)',
    isEquipment: true,
    complexity: 3,
    techLevel: 8,
    resources: {
      'Raw Materials': 1
    },
    requires: { slots: 1 }
  },
  {
    name: 'Optics (Basic)',
    isEquipment: true,
    complexity: 1,
    techLevel: 7,
    resources: {
      'Raw Materials': 1
    },
    requires: { slots: 1 }
  },
  {
    name: 'Parabolic Microphone',
    isEquipment: true,
    complexity: 1,
    techLevel: 7,
    resources: {
      'Raw Materials': 2
    },
    requires: { slots: 1 }
  },
  {
    name: 'Radar',
    isEquipment: true,
    complexity: 4,
    techLevel: 7,
    resources: {
      'Raw Materials': 10
    },
    requires: { slots: 1 }
  },
  {
    name: 'Radiation Sensor',
    isEquipment: true,
    complexity: 1,
    techLevel: 7,
    resources: {
      'Raw Materials': 4
    },
    requires: { slots: 1 }
  },
  {
    name: 'Smell Detector',
    isEquipment: true,
    complexity: 2,
    techLevel: 7,
    resources: {
      'Raw Materials': 1
    },
    requires: { slots: 1 }
  },
  {
    name: 'Smell Detector (Enhanced)',
    isEquipment: true,
    complexity: 3,
    techLevel: 8,
    resources: {
      'Raw Materials': 1,
      'Advanced Machine Parts': 2
    },
    requires: { slots: 1 }
  },
  {
    name: 'Smoke Detector',
    isEquipment: true,
    complexity: 1,
    techLevel: 6,
    resources: {
      'Raw Materials': 1
    },
    requires: { slots: 1 }
  },
  {
    name: 'Sonar',
    isEquipment: true,
    complexity: 4,
    techLevel: 6,
    resources: {
      'Raw Materials': 10
    },
    requires: { slots: 1 }
  },
  {
    name: 'Tactile Sensor',
    isEquipment: true,
    complexity: 2,
    techLevel: 6,
    resources: {
      'Raw Materials': 1
    },
    requires: { slots: 1 }
  },
  {
    name: 'Tactile Sensor (Enhanced)',
    isEquipment: true,
    complexity: 3,
    techLevel: 7,
    resources: {
      'Raw Materials': 1,
      'Advanced Machine Parts': 2
    },
    requires: { slots: 1 }
  },
  {
    name: 'Taste Sensor',
    isEquipment: true,
    complexity: 2,
    techLevel: 7,
    resources: {
      'Raw Materials': 1
    },
    requires: { slots: 1 }
  },
  {
    name: 'Taste Sensor (Enhanced)',
    isEquipment: true,
    complexity: 3,
    techLevel: 8,
    resources: {
      'Raw Materials': 1,
      'Advanced Machine Parts': 2
    },
    requires: { slots: 1 }
  },
  {
    name: 'Wireless Connection',
    isEquipment: true,
    complexity: 1,
    techLevel: 7,
    resources: {
      'Raw Materials': 1
    },
    requires: { slots: 1 }
  },
  {
    name: 'X-Ray Vision',
    isEquipment: true,
    complexity: 2,
    techLevel: 8,
    resources: {
      'Raw Materials': 6
    },
    requires: { slots: 1 }
  }
]

export default inputModules
