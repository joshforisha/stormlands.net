export function sizeOffset ({ size }) {
  return Math.abs(4 - size)
}
