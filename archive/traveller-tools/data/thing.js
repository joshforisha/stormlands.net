import { draw } from '~/lib/list'
import { randomInt } from '~/lib/number'

const functions = [
  'Analog',
  'Cultural',
  'Enabler',
  'Enhancer',
  'Entangler',
  'Protector',
  'Resolver',
  'Value Item'
]

export function generate () {
  const techLevel = randomInt(0, 16)

  return {
    function: draw(functions),
    techLevel
  }
}
