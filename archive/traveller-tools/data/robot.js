import { handle } from '~/lib/attribute'

export function create ({
  designation,
  frame,
  mobility,
  mountedItems,
  options
}) {
  let armour = 0
  let complexity = 0
  let hits = 0
  let sizeString = '—'
  let speed = 0
  let takenSlots = 0
  let techLevel = 0
  let totalSlots = 0

  const attacks = []
  const skills = []
  const resources = {}
  const traits = []

  if (frame !== null) {
    complexity = frame.complexity
    hits = frame.hits
    resources['Raw Materials'] = frame.baseRawMaterials
    totalSlots = frame.slots
    sizeString = `${frame.size} (${frame.scale})`
    traits.push(...frame.traits)
  }

  function parseOption (option) {
    complexity += option.complexity

    if ('armour' in option) {
      armour += handle(option.armour, { frame, mobility })
    }

    if ('attacks' in option) {
      attacks.push(...handle(option.attacks, { frame, mobility }))
    }

    if ('hits' in option) hits += handle(option.hits, { frame, mobility })

    if ('skills' in option) {
      skills.push(...handle(option.skills, { frame, mobility }))
    }

    if ('speed' in option) speed += handle(option.speed, { frame, mobility })

    techLevel = Math.max(techLevel, option.techLevel)

    if ('traits' in option) {
      traits.push(...handle(option.traits, { frame, mobility }))
    }

    if ('requires' in option) {
      if ('slots' in option.requires) {
        takenSlots += option.requires.slots
      }
    }

    if ('resources' in option) {
      for (const res in option.resources) {
        resources[res] =
          (res in resources ? resources[res] : 0) +
          handle(option.resources[res], frame.baseRawMaterials)
      }
    }
  }

  if (mobility !== null) {
    parseOption(mobility)
  }

  options.forEach(option => {
    parseOption(option)
  })

  takenSlots += mountedItems.length

  const mountingMass = mountedItems.length

  if ('Raw Materials' in resources) {
    resources['Raw Materials'] += mountingMass
  } else {
    resources['Raw Materials'] = mountingMass
  }
  complexity += mountedItems.length

  const availableSlots = totalSlots - takenSlots

  const frameMass = frame?.baseRawMaterials || 0

  const mobilityMass =
    mobility !== null
      ? Object.values(mobility.resources || []).reduce(
          (total, m) => handle(m, frame.baseRawMaterials) + total,
          0
        )
      : 0

  const baseMass = frameMass + mobilityMass

  const mountedItemsMass = mountedItems.reduce(
    (total, { weight }) => total + weight,
    0
  )

  const maximumMountWeight = Math.round(0.5 * baseMass) - mountedItemsMass

  const totalMassReduction = options
    .filter(o => 'reducedMass' in o)
    .reduce((total, { reducedMass }) => total + reducedMass, 0.0)

  const optionsMass = options.reduce(
    (total, option) =>
      total +
      ('resources' in option
        ? Object.values(option.resources).reduce(
            (res, r) =>
              res + handle(r, frame ? frame.baseRawMaterials : 0),
            0
          )
        : 0),
    0
  )

  const onboardMass = baseMass + optionsMass + mountingMass

  const totalMass =
    Math.ceil((1.0 - totalMassReduction) * onboardMass) + mountedItemsMass

  return {
    armour,
    attacks,
    availableSlots,
    complexity,
    designation,
    frame,
    hits,
    maximumMountWeight,
    mobility,
    mountedItems,
    onboardMass,
    options,
    resources,
    sizeString,
    skills: skills.filter((s, i) => skills.indexOf(s) === i).sort(),
    speed,
    takenSlots,
    techLevel,
    totalMass,
    totalSlots,
    traits
  }
}
