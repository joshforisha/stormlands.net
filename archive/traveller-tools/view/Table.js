import React from 'react'
import styled, { css } from 'styled-components'

const Container = styled.table`
  border-spacing: 0px;
  min-width: 100%;

  td {
    padding: var(--small);
    vertical-align: middle;

    &:not(:first-child) {
      border-left: 1px solid var(--gray);
    }
  }

  th {
    background-color: var(--white);
    border-bottom: 1px solid var(--gray);
    padding: var(--small);
    position: sticky;
    text-align: ${({ align }) => align};
    top: 0px;
  }
`

const Td = styled.td`
  ${({ align }) =>
    align &&
    css`
      text-align: ${align};
    `}
`

const Tr = styled.tr`
  &:hover > td {
    background-color: var(--gray);
  }

  ${({ valid }) =>
    !valid &&
    css`
      opacity: 0.4;
    `}

  ${({ onClick }) =>
    onClick &&
    css`
      cursor: pointer;

      &:hover > td {
        background-color: var(--red);
        color: var(--white);
      }
    `}
`

export const Align = {
  Center: 'center',
  Left: 'left',
  Right: 'right'
}

export default function Table ({
  columns,
  data,
  onRowClick,
  validateRow
}) {
  const headerCells = columns.map(
    c => <th align={c.align || Align.Left} key={c.title}>{c.title}</th>
  )

  const bodyRows = data.map(
    (record, i) => {
      const cells = columns.map(
        (c, ci) => {
          let view = null
          if ('view' in c) view = c.view(record)
          else if ('key' in c) view = record[c.key]

          return (
            <Td align={c.align} key={ci}>
              {view}
            </Td>
          )
        }
      )

      let valid = true
      if (validateRow) {
        valid = validateRow(record)
      }

      let onClick = null
      if (valid && onRowClick) {
        onClick = () => onRowClick(record)
      }

      return (
        <Tr key={i} onClick={onClick} valid={valid}>
          {cells}
        </Tr>
      )
    }
  )

  return (
    <Container>
      <thead>
        <tr>{headerCells}</tr>
      </thead>
      <tbody>{bodyRows}</tbody>
    </Container>
  )
}
