import BuildSteps from '~/view/build-a-bot/BuildSteps'
import Button from '~/view/Button'
import FrameSelection from '~/view/build-a-bot/FrameSelection'
import MobilitySelection from '~/view/build-a-bot/MobilitySelection'
import OptionSelection from '~/view/build-a-bot/OptionSelection'
import PageHeader from '~/view/PageHeader'
import PageTitle from '~/view/PageTitle'
import React from 'react'
import Summary from '~/view/build-a-bot/Summary'
import frameOptions from '~data/robot/frame-options'
import frameTraits from '~data/robot/frame-traits'
import gadgets from '~data/robot/gadgets'
import inputModules from '~data/robot/input-modules'
import mobilityOptions from '~data/robot/mobility-options'
import outputModules from '~data/robot/output-modules'
import styled from 'styled-components'
import { BiReset } from 'react-icons/bi'
import { HiClipboardCheck, HiClipboardCopy } from 'react-icons/hi'
import { create as createRobot } from '~/data/robot'

const Builder = styled.div`
  padding-top: var(--large);
`

const Container = styled.main`
  padding: var(--medium);
`

const Content = styled.div`
  padding-top: var(--medium);
`

const BuildStep = {
  Frame: 'Choose Frame',
  Mobility: 'Choose Mobility',
  Options: 'Select Options'
}

const allOptions = [...frameTraits, ...gadgets, ...inputModules, ...outputModules]

export default function BuildABot () {
  const [designation, setDesignation] = React.useState('')
  const [frame, setFrame] = React.useState(null)
  const [mobility, setMobility] = React.useState(null)
  const [options, setOptions] = React.useState([])
  const [mountedItems, setMountedItems] = React.useState([])

  function confirmClear () {
    if (!window.confirm('Are you sure?')) return
    setDesignation('')
    setMountedItems([])
    setOptions([])
    setMobility(null)
    setFrame(null)
  }

  function copyRobot () {
    const hash = JSON.stringify([
      designation,
      frame?.size,
      mobility?.name,
      options.map(o => o.name),
      mountedItems.map(i => [i.name, i.weight])
    ])
    navigator.clipboard.writeText(window.btoa(hash))
    window.alert('Copied to clipboard!')
  }

  function pasteRobot () {
    const hash = window.prompt('Paste robot hash:')
    if (!hash) return
    let robo
    try {
      robo = JSON.parse(window.atob(hash))
    } catch (error) {
      console.error(error)
      return window.alert('Invalid robot!')
    }
    if (!Array.isArray(robo) || robo.length < 5 || typeof robo[0] !== 'string') return window.alert('Invalid robot!')
    setDesignation(robo[0])
    setFrame(frameOptions.find(o => o.size === robo[1]))
    setMobility(mobilityOptions.find(o => o.name === robo[2]))
    setOptions(robo[3].map(oName => allOptions.find(o => o.name === oName)))
    setMountedItems(robo[4].map(([name, weight]) => ({ name, weight })))
  }

  const dirty = designation !== '' ||
    frame !== null ||
    mobility !== null ||
    options.length > 0 ||
    mountedItems.length > 0

  const robot = createRobot({
    designation,
    frame,
    mobility,
    mountedItems,
    options
  })

  let buildStep
  let content = null
  if (frame === null) {
    buildStep = BuildStep.Frame
    content = (
      <>
        <FrameSelection onSelect={setFrame} />
      </>
    )
  } else if (mobility === null) {
    buildStep = BuildStep.Mobility
    content = (
      <>
        <MobilitySelection frame={frame} onSelect={setMobility} />
      </>
    )
  } else {
    buildStep = BuildStep.Options
    content = (
      <>
        <OptionSelection
          robot={robot}
          onMount={(item) => setMountedItems([...mountedItems, item])}
          onSelect={(option) => setOptions([...options, option])}
        />
      </>
    )
  }

  const exportable = designation.trim().length > 0 &&
    frame !== null &&
    mobility !== null

  return (
    <Container>
      <PageHeader>
        <PageTitle>Build-A-Bot™</PageTitle>
        <div>
          <Button disabled={!exportable} onClick={copyRobot}><HiClipboardCopy />Copy</Button>
          <Button onClick={pasteRobot}><HiClipboardCheck />Paste</Button>
          <Button disabled={!dirty} onClick={confirmClear}>
            <BiReset />Reset
          </Button>
        </div>
      </PageHeader>
      <Summary
        onChangeDesignation={setDesignation}
        onRemoveMount={(itemName) => {
          const index = mountedItems.findIndex(
            i => i.name === itemName
          )

          if (index > -1) {
            setMountedItems([
              ...mountedItems.slice(0, index),
              ...mountedItems.slice(index + 1)
            ])
          }
        }}
        onRemoveOption={(optionName) => {
          const index = options.findIndex(
            o => o.name === optionName
          )

          if (index > -1) {
            setOptions([
              ...options.slice(0, index),
              ...options.slice(index + 1)
            ])
          }
        }}
        robot={robot}
      />
      <Builder>
        <BuildSteps
          currentStep={buildStep}
          steps={Object.values(BuildStep)}
        />
        <Content>
          {content}
        </Content>
      </Builder>
    </Container>
  )
}
