import styled from 'styled-components'

export default styled.h1`
  color: var(--red);
  font-size: 2rem;
`
