import React from 'react'
import OptionGroup, { Arrow, Details, Option, Title } from '~/view/OptionGroup'
import Slab from '~/view/Slab'
import frameOptions from '~/data/robot/frame-options'
import { units } from '~/lib/text'

function FrameOption ({ frameOption, onClick }) {
  return (
    <Option onClick={onClick}>
      <Title>Size {frameOption.size} ({frameOption.scale})</Title>
      <Details flex={1}>{(frameOption.traits || []).join(', ')}</Details>
      <Slab
        label='Hits'
        value={frameOption.hits}
      />
      <Slab
        label='Slots'
        value={frameOption.slots}
      />
      <Slab
        label='Base Raw Materials'
        value={units(frameOption.baseRawMaterials)}
      />
      <Slab
        label='Complexity'
        value={`+${frameOption.complexity}`}
      />
      <Arrow />
    </Option>
  )
}

export default function BotFrameSelection ({ onSelect }) {
  const options = frameOptions.map(frameOption =>
    <FrameOption
      frameOption={frameOption}
      key={frameOption.scale}
      onClick={() => onSelect(frameOption)}
    />
  )

  return (
    <OptionGroup>
      {options}
    </OptionGroup>
  )
}
