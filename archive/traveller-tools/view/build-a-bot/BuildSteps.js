import React from 'react'
import styled, { css } from 'styled-components'

const Label = styled.span``

const Number = styled.span`
  opacity: 0.5;
  padding-right: var(--small);
`

const Step = styled.div`
  align-items: center;
  background-color: ${({ current }) => (current ? 'var(--black)' : 'var(--gray)')};
  border-bottom: 4px solid ${({ current }) => (current ? 'var(--red)' : 'var(--gray)')};
  color: var(--white);
  cursor: normal;
  display: flex;
  flex: 1;
  opacity: ${({ active }) => (active ? '1' : '0.25')};
  padding: var(--small) var(--small) var(--tiny);

  &:not(:first-child) {
    margin-left: 2px;
  }

  ${({ current }) => !current && css`
    & > ${Label} {
      opacity: 0.25;
    }
  `}
`

const Steps = styled.div`
  background-color: var(--white);
  display: flex;
  position: sticky;
  top: 0px;
  z-index: 1;
`

export default function BuildSteps ({ currentStep, steps }) {
  const currentIndex = steps.findIndex(s => s === currentStep)
  const buildSteps = steps
    .map((step, index) => (
      <Step
        active={index <= currentIndex}
        current={index === currentIndex}
        key={index}
      >
        <Number>{index + 1}.</Number>
        <Label>{step}</Label>
      </Step>
    ))

  return (
    <Steps>
      {buildSteps}
    </Steps>
  )
}
