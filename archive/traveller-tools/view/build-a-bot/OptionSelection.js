import Button from '~/view/Button'
import Input from '~/view/Input'
import React from 'react'
import Slab from '~/view/Slab'
import Switcher from '~/view/Switcher'
import frameTraits from '~/data/robot/frame-traits'
import gadgets from '~/data/robot/gadgets'
import inputModules from '~/data/robot/input-modules'
import outputModules from '~/data/robot/output-modules'
import styled from 'styled-components'
import { GrConnect } from 'react-icons/gr'
import { formatGains, formatRequirements, validate } from '~/data/robot/option'
import OptionGroup, {
  Details,
  DetailsGroup,
  Option,
  Plus,
  Title
} from '~/view/OptionGroup'

const ConnectIcon = styled(GrConnect)`
  margin-right: var(--small) !important;

  & > path {
    stroke: var(--white);
  }
`

const Content = styled.div`
  margin-top: 16px;
`

const Quiet = styled.span`
  font-style: italic;
  opacity: 0.5;
`

const View = {
  FrameTraits: 'Frame Traits',
  InputModules: 'Input Modules',
  OutputModules: 'Output Modules',
  Gadgets: 'Gadgets',
  MountEquipment: 'Mount Equipment'
}

function EquipmentTable ({ equipment, onSelect, robot }) {
  const brm = robot.frame.baseRawMaterials

  const traitOptions = equipment.map((item, i) => (
    <Option
      disabled={!validate(item, robot)}
      key={i}
      onClick={() => onSelect(item)}
    >
      <Plus />
      <Title>{item.name}</Title>
      <Details>Requires {formatRequirements(item, brm)}</Details>
      <Slab label='Tech Level' value={item.techLevel} />
      <Slab label='Complexity' value={`+${item.complexity}`} />
    </Option>
  ))

  return (
    <OptionGroup>
      {traitOptions}
    </OptionGroup>
  )
}

function FrameTraitsGroup ({ onSelect, robot }) {
  const brm = robot.frame.baseRawMaterials

  const traitOptions = frameTraits.map((trait, i) => {
    const requirements = formatRequirements(trait, brm)
    const requirementDetails = requirements
      ? <>Requires {requirements}</>
      : null

    return (
      <Option
        disabled={!validate(trait, robot)}
        key={i}
        onClick={() => onSelect(trait)}
      >
        <Plus />
        <Title>{trait.name}</Title>
        <DetailsGroup>
          <Details>Gain {formatGains(trait, robot)}</Details>
          <Details>{requirementDetails}</Details>
        </DetailsGroup>
        <Slab label='Tech Level' value={trait.techLevel} />
        <Slab label='Complexity' value={`+${trait.complexity}`} />
      </Option>
    )
  })

  return (
    <OptionGroup>
      {traitOptions}
    </OptionGroup>
  )
}

function MountedEquipment ({ onMount, robot }) {
  const [name, setName] = React.useState('')
  const [weight, setWeight] = React.useState(0)

  function mount () {
    onMount({ name, weight })
    setName('')
    setWeight(0)
  }

  const slotsFilled = robot.availableSlots === 0

  const weightLabel = (
    <>
      Weight (kg) <Quiet>Max: {robot.maximumMountWeight}</Quiet>
    </>
  )

  const valid = !slotsFilled && name.trim().length > 0

  return (
    <>
      <p>
        Each mounted item will require 1 Slot and 1 kg Raw Materials. The total
        mass of mounted items cannot exceed 50% of the model’s base mass.
      </p>
      <Input
        disabled={slotsFilled}
        label='Item Name'
        onChange={e => setName(e.target.value)}
        placeholder='Enter name of mounted item'
        type='text'
        value={name}
      />
      <Input
        disabled={slotsFilled}
        label={weightLabel}
        max={robot.maximumMountWeight}
        min={0}
        onChange={e => setWeight(parseInt(e.target.value, 10))}
        step={1}
        type='number'
        value={weight || 0}
      />
      <Button disabled={!valid} onClick={mount}>
        <ConnectIcon />Mount Item
      </Button>
    </>
  )
}

export default function OptionSelection (props) {
  const [view, setView] = React.useState(View.FrameTraits)

  let content = null
  switch (view) {
    case View.FrameTraits:
      content = <FrameTraitsGroup {...props} />
      break
    case View.InputModules:
      content = <EquipmentTable equipment={inputModules} {...props} />
      break
    case View.OutputModules:
      content = <EquipmentTable equipment={outputModules} {...props} />
      break
    case View.MountEquipment:
      content = <MountedEquipment {...props} />
      break
    case View.Gadgets:
      content = <EquipmentTable equipment={gadgets} {...props} />
      break
  }

  return (
    <>
      <Switcher
        current={view}
        onSelect={setView}
        options={Object.values(View)}
      />
      <Content>{content}</Content>
    </>
  )
}
