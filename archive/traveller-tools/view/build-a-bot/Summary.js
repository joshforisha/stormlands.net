import React from 'react'
import Tag from '~/view/Tag'
import styled from 'styled-components'
import { intersperse } from '~/lib/list'
import { units } from '~/lib/text'

const Container = styled.div``

const DesignationInput = styled.input`
  background-color: transparent;
  border: 2px solid transparent;
  color: var(--red);
  font-size: 1.25em;
  font-weight: 500;
  outline: none;
  padding: 0px var(--small);
  text-transform: uppercase;
  width: 100%;

  ::placeholder {
    color: var(--gray);
    font-style: italic;
    font-weight: 300;
    opacity: 0.5;
    text-transform: none;
  }

  &:active, &:focus {
    border-color: var(--red);
  }
`

const Grid = styled.div``

const GridLabel = styled.div`
  border-right: 2px solid var(--black);
  font-weight: 700;
  padding: var(--small);
  width: 8rem;
`

const SpecialLabel = styled(GridLabel)`
  color: var(--red);
`

const Value = styled.div`
  &:last-of-type {
    flex-grow: 1;
  }

  &:not(:last-of-type) > * {
    padding-right: 1.5em;
  }
`

const ValueLabel = styled.div`
  border-bottom: 1px solid var(--shade-light);
  font-weight: 700;
  height: var(--large);
  padding: var(--small);
`

const Values = styled.div`
  display: flex;
  width: 100%;
`

const ValueText = styled.div`
  padding: var(--small);
`

const GridRow = styled.section`
  display: flex;

  &:not(:last-of-type) > ${Values} {
    border-bottom: 1px solid var(--black);
  }
`

export default function Summary ({
  onChangeDesignation,
  onRemoveMount,
  onRemoveOption,
  robot
}) {
  function mountedTag (item, index) {
    return (
      <Tag key={index} onRemove={() => onRemoveMount(item.name)}>
        {item.name} [{item.weight} kg]
      </Tag>
    )
  }

  function optionTag (option, index) {
    return (
      <Tag key={index} onRemove={() => onRemoveOption(option.name)}>
        {option.name}
      </Tag>
    )
  }

  let armour = null
  if (robot.armour > 0) {
    armour = (
      <Value>
        <ValueLabel>Armour</ValueLabel>
        <ValueText>{robot.armour}</ValueText>
      </Value>
    )
  }

  let buildInfo = '—'
  if (robot.complexity > 0) {
    let difficulty = ''
    if (robot.complexity < 5) difficulty = 'Simple (2+)'
    else if (robot.complexity < 10) difficulty = 'Easy (4+)'
    else if (robot.complexity < 15) difficulty = 'Routine (6+)'
    else if (robot.complexity < 20) difficulty = 'Average (8+)'
    else if (robot.complexity < 25) difficulty = 'Difficult (10+)'
    else if (robot.complexity < 30) difficulty = 'Very Difficult (12+)'
    else difficulty = 'Formidable (14+)'

    buildInfo = <>{difficulty} Science (robotics) check (1D &times; 4 hours, INT or EDU)</>
  }

  let materialInfo = '—'
  if (robot.complexity > 0 && Object.keys(robot.resources).length > 0) {
    materialInfo = []
    for (const name in robot.resources) {
      materialInfo.push(`${units(robot.resources[name])} ${name}`)
    }
    materialInfo = materialInfo.join(', ')
  }

  let complexityText = '—'
  if (robot.complexity > 0) {
    complexityText = String(robot.complexity)
  }

  let hitsText = '—'
  if (robot.hits > 0) {
    hitsText = String(robot.hits)
  }

  let massText = '—'
  if (robot.totalMass > 0) {
    massText = `${robot.totalMass} kg`
  }

  let recoveryText = '—'
  if (robot.onboardMass > 0) {
    recoveryText = (
      <>
        <p>On success: 2D+Effect &times; {units(Math.ceil(0.02 * robot.onboardMass))} Raw Materials
        </p>
        <p>On failure: 2D+Effect &times; {units(Math.ceil(0.1 * robot.onboardMass))} Raw Materials</p>
      </>
    )
  }

  let slotsText = '—'
  if (robot.totalSlots > 0) {
    slotsText = `${robot.takenSlots}/${robot.totalSlots}`
  }

  let speedText = '—'
  if (robot.speed > 0) {
    speedText = String(robot.speed)
  }

  let techLevelText = '—'
  if (robot.techLevel > 0) {
    techLevelText = String(robot.techLevel)
  }

  const equipmentTags = robot.options
    .filter(o => o.isEquipment)
    .sort((a, b) => (a.name < b.name ? -1 : 1))
    .map(optionTag)

  const mountedTags = robot.mountedItems
    .sort((a, b) => (a.name < b.name ? -1 : 1))
    .map(mountedTag)

  const optionTags = robot.options
    .filter((o) => !o.isEquipment)
    .sort((a, b) => (a.name < b.name ? -1 : 1))
    .map(optionTag)

  return (
    <Container>
      <Grid>
        <GridRow>
          <GridLabel>Designation</GridLabel>
          <Values>
            <DesignationInput
              onChange={e => onChangeDesignation(e.target.value)}
              placeholder='Enter designation'
              value={robot.designation}
            />
          </Values>
        </GridRow>
        <GridRow>
          <GridLabel />
          <Values>
            {armour}
            <Value>
              <ValueLabel>Hits</ValueLabel>
              <ValueText>{hitsText}</ValueText>
            </Value>
            <Value>
              <ValueLabel>Mass</ValueLabel>
              <ValueText>{massText}</ValueText>
            </Value>
            <Value>
              <ValueLabel>Size</ValueLabel>
              <ValueText>{robot.sizeString}</ValueText>
            </Value>
            <Value>
              <ValueLabel>Slots</ValueLabel>
              <ValueText>{slotsText}</ValueText>
            </Value>
            <Value>
              <ValueLabel>Speed</ValueLabel>
              <ValueText>{speedText}</ValueText>
            </Value>
            <Value>
              <ValueLabel>TL</ValueLabel>
              <ValueText>{techLevelText}</ValueText>
            </Value>
            <Value>
              <ValueLabel>Complexity</ValueLabel>
              <ValueText>{complexityText}</ValueText>
            </Value>
          </Values>
        </GridRow>
        <GridRow>
          <GridLabel>Skills</GridLabel>
          <Values>
            <ValueText>
              {robot.skills.length > 0 ? robot.skills.join(', ') : 'None'}
            </ValueText>
          </Values>
        </GridRow>
        <GridRow>
          <GridLabel>Attacks</GridLabel>
          <Values>
            <ValueText>
              {robot.attacks.length > 0 ? robot.attacks.join(', ') : 'None'}
            </ValueText>
          </Values>
        </GridRow>
        <GridRow>
          <GridLabel>Traits</GridLabel>
          <Values>
            <ValueText>
              {robot.traits.length > 0 ? robot.traits.join(', ') : 'None'}
            </ValueText>
          </Values>
        </GridRow>
        <GridRow>
          <GridLabel>Build</GridLabel>
          <Values>
            <ValueText>
              {buildInfo}
            </ValueText>
          </Values>
        </GridRow>
        <GridRow>
          <GridLabel>Materials</GridLabel>
          <Values>
            <ValueText>
              {materialInfo}
            </ValueText>
          </Values>
        </GridRow>
        <GridRow>
          <GridLabel>Recover</GridLabel>
          <Values>
            <ValueText>{recoveryText}</ValueText>
          </Values>
        </GridRow>
        <GridRow>
          <SpecialLabel>Equipment</SpecialLabel>
          <Values>
            <ValueText>
              {equipmentTags.length > 0
                ? intersperse(
                    i => <span key={`i${i}`}>, </span>,
                    equipmentTags
                  )
                : '—'}
            </ValueText>
          </Values>
        </GridRow>
        <GridRow>
          <SpecialLabel>Mounts</SpecialLabel>
          <Values>
            <ValueText>
              {mountedTags.length > 0
                ? intersperse(
                    i => <span key={`i${i}`}>, </span>,
                    mountedTags
                  )
                : '—'}
            </ValueText>
          </Values>
        </GridRow>
        <GridRow>
          <SpecialLabel>Options</SpecialLabel>
          <Values>
            <ValueText>
              {optionTags.length > 0
                ? intersperse(
                    i => <span key={`i${i}`}>, </span>,
                    optionTags
                  )
                : '—'}
            </ValueText>
          </Values>
        </GridRow>
      </Grid>
    </Container>
  )
}
