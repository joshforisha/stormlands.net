import React from 'react'
import Slab from '~/view/Slab'
import mobilityOptions from '~/data/robot/mobility-options'
import { formatGains, formatRequirements } from '~/data/robot/option'
import { handle } from '~/lib/attribute'
import OptionGroup, {
  Arrow, Details, DetailsGroup, Option, Title
} from '~/view/OptionGroup'

function MobilityOption ({ frame, mobilityOption, onSelect }) {
  const { complexity, name, speed, techLevel } = mobilityOption

  const gains = formatGains(mobilityOption, { frame })
  const gainDetails = gains
    ? <>Gain {gains}</>
    : null

  const requirements = formatRequirements(mobilityOption, frame.baseRawMaterials)
  const requirementDetails = requirements
    ? <>Requires {requirements}</>
    : null

  return (
    <Option onClick={() => onSelect(mobilityOption)}>
      <Title>{name}</Title>
      <DetailsGroup>
        <Details>{gainDetails}</Details>
        <Details>{requirementDetails}</Details>
      </DetailsGroup>
      <Slab label='Speed' value={handle(speed, { frame }, 0)} />
      <Slab label='Tech Level' value={techLevel} />
      <Slab label='Complexity' value={`+${complexity}`} />
      <Arrow />
    </Option>
  )
}

export default function MobilitySelection ({ frame, onSelect }) {
  const options = mobilityOptions
    .map(mobilityOption => (
      <MobilityOption
        frame={frame}
        key={mobilityOption.name}
        mobilityOption={mobilityOption}
        onSelect={onSelect}
      />
    ))

  return <OptionGroup>{options}</OptionGroup>
}
