import React from 'react'
import styled from 'styled-components'
import { ImPlus } from 'react-icons/im'

const ArrowWrapper = styled.div`
  align-items: center;
  color: var(--red);
  display: flex;
  justify-content: center;
  padding-left: var(--medium);
`
export const Arrow = styled(() => <ArrowWrapper>&rang;</ArrowWrapper>)``

export const Details = styled.span`
  flex: ${({ flex = 1.5 }) => flex};
  font-size: 0.9em;
  min-height: 1em;
  text-align: left;
`

export const DetailsGroup = styled.div`
  display: flex;
  flex: 1.5;
  flex-direction: column;
  justify-content: flex-start;

  & > ${Details} {
    flex: 0;
  }
`

const MinusWrapper = styled.div``
export const Minus = styled(() => <MinusWrapper>-</MinusWrapper>)``

const PlusWrapper = styled.div`
  align-items: center;
  background-color: var(--red);
  border-radius: 0.7rem;
  color: var(--white);
  display: flex;
  height: 1.4rem;
  justify-content: center;
  margin-right: var(--small);
  padding: var(--small) var(--tiny);
  width: 1.4rem;
`
export const Plus = styled(() => <PlusWrapper><ImPlus /></PlusWrapper>)``

export const Title = styled.span`
  color: var(--red);
  flex: 1;
  font-size: 1.1em;
  padding-bottom: var(--tiny);
  text-align: left;
`

export const Option = styled.button`
  align-items: center;
  background-color: var(--white);
  border-bottom: none;
  border-left: 1px solid var(--gray);
  border-right: 1px solid var(--gray);
  border-top: 1px solid var(--gray);
  display: flex;
  padding: var(--small);
  width: 100%;

  &:disabled {
    cursor: not-allowed;

    & > * {
      opacity: 0.5;
    }

    & ${PlusWrapper} {
      opacity: 0;
    }
  }

  &:not(:disabled):hover {
    background-color: var(--red);

    & * {
      color: var(--white);
    }

    & ${PlusWrapper} {
      color: var(--red);
      opacity: 0.5;
    }
  }

  &:last-of-type {
    border-bottom: 1px solid var(--gray);
  }
`

export default styled.div``
