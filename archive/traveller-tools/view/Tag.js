import React from 'react'
import styled from 'styled-components'

const Container = styled.span`
  cursor: pointer;
  display: inline-block;

  &:hover {
    color: var(--red);
    text-decoration: line-through;
  }
`

export default function Tag ({ children, onRemove }) {
  return <Container onClick={onRemove}>{children}</Container>
}
