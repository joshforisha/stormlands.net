import React from 'react'
import styled from 'styled-components'
import Button from '~/view/Button'
import PageHeader from '~/view/PageHeader'
import PageTitle from '~/view/PageTitle'

const Page = styled.div`
  padding: var(--medium);
`

export default function BeastMaker () {
  return (
    <Page>
      <PageHeader>
        <PageTitle>BeastMaker</PageTitle>
        <div>
          <Button>Generate</Button>
          <Button>Reset</Button>
        </div>
      </PageHeader>
    </Page>
  )
}
