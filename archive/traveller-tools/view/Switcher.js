import React from 'react'
import styled, { css } from 'styled-components'

const Option = styled.div`
  background-color: var(--red);
  border: 2px solid ${({ current }) => (current ? 'var(--white)' : 'var(--red)')};
  color: var(--white);
  cursor: pointer;
  flex: 1;
  padding: calc(var(--small) - 2px);
  text-align: center;

  &:not(:last-of-type) {
    margin-right: var(--small);
  }

  ${({ current }) => !current && css`
    & > span {
      opacity: 0.4;
    }
  `}
`

const Container = styled.div`
  background-color: var(--black);
  display: flex;
  padding: var(--small);
  position: sticky;
  top: 2.4rem;
  z-index: 1;
`

export default function Switcher ({
  current,
  onSelect,
  options
}) {
  const optionButtons = options.map(
    (option, i) => (
      <Option
        current={current === option}
        key={i}
        onClick={() => onSelect(option)}
      >
        <span>{option}</span>
      </Option>
    )
  )

  return <Container>{optionButtons}</Container>
}
