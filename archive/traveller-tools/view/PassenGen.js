import Button from '~/view/Button'
import PageTitle from '~/view/PageTitle'
import React from 'react'
import styled from 'styled-components'
import { GoGear } from 'react-icons/go'

const Header = styled.header`
  display: flex;
  justify-content: space-between;
  padding-bottom: var(--large);
`

const Page = styled.div`
  padding: var(--medium);
`

export default function PassenGen () {
  return (
    <Page>
      <Header>
        <PageTitle>PassenGen</PageTitle>
        <Button><GoGear />Generate</Button>
      </Header>
    </Page>
  )
}
