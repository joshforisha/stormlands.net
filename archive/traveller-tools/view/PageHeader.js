import styled from 'styled-components'

export default styled.header`
  display: flex;
  justify-content: space-between;
  padding-bottom: var(--large);

  & > div {
    display: flex;

    & > * {
      margin-left: var(--small);
    }
  }
`
