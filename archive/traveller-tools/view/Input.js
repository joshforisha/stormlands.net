import React from 'react'
import styled from 'styled-components'

const Container = styled.label`
  display: block;
  padding: var(--small) 0px;
`

const LabelText = styled.span`
  display: block;
`

const SubInput = styled.input`
  border: 2px solid var(--gray);
  font-size: 1em;
  outline: none;
  padding: var(--small);
  width: 100%;

  &:focus {
    border-color: var(--red);
  }
`

export default function Input (props) {
  return (
    <Container className={props.className}>
      <LabelText>{props.label}</LabelText>
      <SubInput {...props} />
    </Container>
  )
}
