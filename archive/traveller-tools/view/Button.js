import styled from 'styled-components'

export default styled.button`
  align-items: center;
  background-color: var(--red);
  border-width: 0px;
  color: var(--white);
  cursor: pointer;
  display: flex;
  font-size: 1em;
  font-weight: 500;
  padding: var(--small) var(--medium);

  &:disabled {
    background-color: var(--gray);
    cursor: not-allowed;
    opacity: 0.5;
  }

  & > svg {
    height: 1.1em;
    margin-right: var(--tiny);
    opacity: 0.6;
    width: 1.1em;
  }
`
