import Button from '~/view/Button'
import PageTitle from '~/view/PageTitle'
import React from 'react'
import Slab from '~/view/Slab'
import styled from 'styled-components'
import { GoGear } from 'react-icons/go'
import { generate as generateThing } from '~/data/thing'

const Container = styled.div`
  padding: var(--medium);
`

const Content = styled.div`
  display: flex;
`

const Header = styled.div`
  display: flex;
  justify-content: space-between;
  padding-bottom: var(--large);
`

export default function ThingMaker () {
  const [thing, setThing] = React.useState(generateThing())

  const generate = () => setThing(generateThing())

  return (
    <Container>
      <Header>
        <PageTitle>ThingMaker</PageTitle>
        <Button onClick={generate}><GoGear />Generate</Button>
      </Header>
      <Content>
        <Slab label='Function' value={thing.function} />
        <Slab label='Tech Level' value={thing.techLevel} />
      </Content>
    </Container>
  )
}
