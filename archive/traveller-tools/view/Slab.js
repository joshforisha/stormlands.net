import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
  align-items: center;
  display: flex;
  flex-direction: column;
  padding: 0px var(--medium);
`

const Label = styled.label`
  cursor: inherit;
  font-size: 0.9em;
  opacity: 0.7;
`

const Value = styled.div`
  font-size: 1.4em;
  font-weight: 300;
`

export default function Slab ({ className, label, value }) {
  return (
    <Container>
      <Value>{value}</Value>
      <Label>{label}</Label>
    </Container>
  )
}
