import React from 'react'
import routes from '~/routes'
import styled, { css } from 'styled-components'
import { RiSpaceShipFill } from 'react-icons/ri'

const NavButton = styled.button`
  align-items: center;
  background-color: var(--gray);
  border-style: none;
  color: var(--white);
  display: flex;
  margin-top: var(--small);
  padding: var(--small);
  text-decoration: none;
  width: 100%;

  ${({ current }) => current && css`
    background-color: var(--white);
    color: var(--red);
    cursor: default;

    & > svg {
      opacity: 1;
    }
  `}

  ${({ current }) => !current && css`
    &:not(:disabled):hover {
      background-color: var(--red);
    }
  `}

  &:disabled {
    color: rgba(255, 255, 254, 0.25);
    cursor: not-allowed;
    opacity: 0.5;
  }

  & > svg {
    height: 1.25em;
    margin-right: var(--small);
    opacity: 0.6;
    width: 1.25em;
  }
`

const Nav = styled.nav`
  background-color: var(--black);
  color: var(--white);
  grid-area: navigation;
  height: 100vh;
  overflow-y: scroll;
  padding: var(--small);
  position: fixed;
  width: 16rem;
`

const Title = styled.h1`
  align-items: center;
  color: var(--red);
  display: flex;
  font-style: italic;
  font-weight: 400;
  justify-content: center;
  letter-spacing: -1px;
  padding: var(--small);

  & > svg {
    background-color: var(--red);
    border-radius: 0.6em;
    fill: var(--black);
    height: 1.2em;
    margin-right: var(--small);
    padding: var(--tiny);
    width: 1.2em;
  }
`

export default function Navigation ({ currentPath }) {
  function goto (path) {
    window.location = path
  }

  const links = routes.map(({ disabled, link, path }) =>
    <NavButton
      current={currentPath === path}
      disabled={disabled}
      onClick={() => goto(path)}
      key={path}
    >{link}
    </NavButton>
  )

  return (
    <Nav>
      <Title><RiSpaceShipFill />Traveller Tools</Title>
      {links}
    </Nav>
  )
}
