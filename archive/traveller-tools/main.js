import Navigation from '~/view/Navigation'
import React from 'react'
import routes from '~/routes'
import styled from 'styled-components'
import { render } from 'react-dom'

const Main = styled.main`
  grid-area: main;
`

const AppRoot = styled.div`
  display: grid;
  grid-template-columns: 16rem 1fr;
  grid-template-rows: 1fr;
  grid-template-areas: "navigation main";
  height: 100vh;
`

function router (path) {
  if (path === '') return null
  const route = routes.find(route => route.path === path)
  if (route) return route.view
  return <h2>Path not found!</h2>
}

function App () {
  const [currentPath, setCurrentPath] = React.useState(window.location.hash)

  React.useEffect(() => {
    const updatePath = () => setCurrentPath(window.location.hash)
    window.addEventListener('hashchange', updatePath)
    return () => {
      window.removeEventListener('hashchange', updatePath)
    }
  }, [])

  return (
    <AppRoot>
      <Navigation currentPath={currentPath} />
      <Main>
        {router(currentPath)}
      </Main>
    </AppRoot>
  )
}

render(<App />, document.getElementById('Root'))
