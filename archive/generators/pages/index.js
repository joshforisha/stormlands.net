import * as Adventure from './adventure'
import * as BodyMutation from './body-mutation'
import * as Drug from './drug'
import * as FoodIngredient from './food-ingredient'
import * as IndoorSetting from './indoor-setting'
import * as NPC from './npc'
import * as OutdoorSetting from './outdoor-setting'

export default {
  Adventure: Adventure.view,
  Drug: Drug.view,
  'Food Ingredient': FoodIngredient.view,
  'Indoor Setting': IndoorSetting.view,
  'Mutate Body': BodyMutation.view,
  NPC: NPC.view,
  'Outdoor Setting': OutdoorSetting.view
}
