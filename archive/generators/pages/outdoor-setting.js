import * as Outdoor from '~/data/outdoor-setting'
import h from '~/lib/h'
import { capital } from '~/lib/text'
import { draw, shuffle } from '~/lib/random'

export function view () {
  const [a, b, c] = shuffle([
    Outdoor.determiners,
    Outdoor.opinions,
    Outdoor.sizes,
    Outdoor.qualities,
    Outdoor.properties,
    Outdoor.biomes,
    Outdoor.ages,
    Outdoor.appearances,
    Outdoor.origins,
    Outdoor.materials,
    Outdoor.types,
    Outdoor.locations
  ])

  return [
    h('h1', ['Outdoor Setting']),
    h('p', [[capital(draw(a)), draw(b), draw(c)].join(', ')])
  ]
}
