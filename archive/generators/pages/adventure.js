import foes from '~/data/foes'
import h from '~/lib/h'
import moods from '~/data/moods'
import motivations from '~/data/motivations'
import adventurePlots from '~/data/adventure-plots'
import themes from '~/data/themes'
import twists from '~/data/twists'
import { capital } from '~/lib/text'
import { draw } from '~/lib/random'

export function view () {
  return [
    h('h1', ['Adventure']),
    h('p', ['Plot: ', capital(draw(adventurePlots))]),
    h('p', ['Foe: ', capital(draw(foes))]),
    h('p', ['Theme: ', capital(draw(themes))]),
    h('p', ['Motivation: ', capital(draw(motivations))]),
    h('p', ['Mood: ', capital(draw(moods))]),
    h('p', ['Twist: ', capital(draw(twists))])
  ]
}
