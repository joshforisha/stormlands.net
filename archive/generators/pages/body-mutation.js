import weightedBodyFormMutations from '~/data/weighted-body-form-mutations'
import weightedBodyFunctionMutations from '~/data/weighted-body-function-mutations'
import h from '~/lib/h'
import { weighted } from '~/lib/random'

export function view () {
  const [formName, makeFormInfo] = weighted(weightedBodyFormMutations)
  const formInfo = makeFormInfo()

  const [functionName, makeFunctionInfo] = weighted(weightedBodyFunctionMutations)
  const functionInfo = makeFunctionInfo()

  return [
    h('h1', ['Body Mutation']),
    h('h2', ['Body Form: ', formName]),
    ...formInfo.map(i => h('p', [i])),
    h('h2', ['Body Function: ', functionName]),
    ...functionInfo.map(i => h('p', [i]))
  ]
}
