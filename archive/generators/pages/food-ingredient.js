import animals from '~/data/animals'
import colors from '~/data/colors'
import fruits from '~/data/fruits'
import h from '~/lib/h'
import nativeFruits from '~/data/native-fruits'
import nuts from '~/data/nuts'
import suffixes from '~/data/suffixes'
import vegetables from '~/data/vegetables'
import { capital } from '~/lib/text'
import { uniform } from '~/lib/random'
import { randomNonsense } from '~/data/nonsense'

function randomNonsenseWithSuffix () {
  return `${randomNonsense()}${uniform(suffixes)}`
}

export function view () {
  const adjective = uniform([
    () => uniform(colors),
    randomNonsenseWithSuffix]
  )()

  const noun = uniform([
    () => `${uniform(animals)} meat`,
    () => uniform(fruits),
    () => uniform(nativeFruits),
    () => uniform(nuts),
    () => uniform(vegetables)
  ])()

  return [
    h('h1', ['Food Ingredient']),
    h('p', [capital(`${adjective} ${noun}`)])
  ]
}
