import h from '~/lib/h'
import { generateBackstory } from '~/data/backstory'
import { capital } from '~/lib/text'

export function view () {
  return [
    h('h1', ['NPC']),
    h('p', ['Backstory: ', capital(generateBackstory())])
  ]
}
