import colors from '~/data/colors'
import h from '~/lib/h'
import { capital } from '~/lib/text'
import { draw, rand } from '~/lib/random'

function randomAdministered (isLiquid) {
  if (isLiquid) {
    return draw([
      'Injected',
      'Nasal',
      'Optical',
      'Oral',
      'Smoked',
      'Swallowed',
      'Vaporized'
    ])
  }
  return draw([
    'Chewed',
    'Eaten',
    'Nasal',
    'Optical',
    'Oral',
    'Smoked',
    'Vaporized'
  ])
}

function randomCategory () {
  return draw([
    'Anti-inflammatory',
    'Anticonvulsant',
    'Antiemetic (treats nausea and vomiting)',
    'Antihistamine',
    'Antipsychotic (major tranquilizer)',
    'Antipyretic (reduces fever)',
    'Barbiturate (major sedative)',
    'Benzodiazepine (minor sedative)',
    'Cough suppressant (reflex)',
    'Cytotoxin',
    'Decongestant',
    'Diuretic',
    'Expectorant',
    'Immunosuppressive',
    'Laxative',
    'Muscle relaxant',
    'Narcotic analgesic (severely reduces pain)',
    'Non-narcotic analgesic (mildly reduces pain)',
    'Vitamin'
  ])
}

function randomForm () {
  const [f, isLiquid] = draw([
    ['cream', false],
    ['leaves', false],
    ['liquid', true],
    ['powder', false],
    ['produce', false],
    ['root', false]
  ])

  const color = capital(draw(colors))
  const form = `${color} ${f}`

  return [form, isLiquid]
}

function randomSideEffect () {
  return draw([
    'Blurred vision',
    'Confusion',
    'Diarrhea',
    'Loss of sense',
    'Nausea',
    'Nose-bleed',
    'Pupil dilation',
    'Tremors',
    'Weakness'
  ])
}

export function view () {
  const [form, isLiquid] = randomForm()

  return [
    h('h1', ['Drug']),
    h('p', [`Category: ${randomCategory()}`]),
    h('p', [`Form: ${form}`]),
    h('p', [`Administered: ${randomAdministered(isLiquid)}`]),
    h('p', [`Side Effect: ${randomSideEffect()}`]),
    h('p', [`Addiction: ${rand(1, 100)}%`])
  ]
}
