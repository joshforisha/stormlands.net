import * as Indoor from '~/data/indoor-setting'
import h from '~/lib/h'
import { capital } from '~/lib/text'
import { draw, shuffle } from '~/lib/random'

export function view () {
  const [a, b, c] = shuffle([
    Indoor.ages,
    Indoor.appearances,
    Indoor.colors,
    Indoor.determiners,
    Indoor.locations,
    Indoor.materials,
    Indoor.opinions,
    Indoor.origins,
    Indoor.properties,
    Indoor.qualities,
    Indoor.sizes,
    Indoor.types
  ])

  return [
    h('h1', ['Indoor Setting']),
    h('p', [[capital(draw(a)), draw(b), draw(c)].join(', ')])
  ]
}
