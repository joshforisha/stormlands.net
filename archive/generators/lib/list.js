export function count (xs, pred = (x) => x) {
  return xs.reduce((results, x) => {
    const y = pred(x)
    if (y in results) results[y] += 1
    else results[y] = 1
    return results
  }, {})
}

export function initialize (num, value = (i) => i) {
  const xs = []
  for (let i = 0; i < num; i++) {
    xs.push(typeof value === 'function' ? value(i) : value)
  }
  return xs
}

export function without (index, xs) {
  return xs.filter((_, i) => i !== index)
}
