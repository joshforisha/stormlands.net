import { initialize, without } from '~/lib/list'

export function chance (x) {
  return Math.random() < x
}

export const d3 = (n = 1) => roll(n, 3)
export const d4 = (n = 1) => roll(n, 4)
export const d6 = (n = 1) => roll(n, 6)
export const d8 = (n = 1) => roll(n, 8)
export const d10 = (n = 1) => roll(n, 10)
export const d12 = (n = 1) => roll(n, 12)
export const d20 = (n = 1) => roll(n, 20)

export const draw = uniform

export function rand (min, max) {
  return Math.floor(min + Math.random() * (max - min + 1))
}

export function random (min, max) {
  return min + Math.random() * (max - min)
}

export function roll (num, sides) {
  return initialize(num, () => rand(1, sides))
    .reduce((sum, x) => sum + x, 0)
}

export function shuffle (xs) {
  const ys = [...xs]
  let i = xs.length - 1
  for (i; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1))
    const y = ys[i]
    ys[i] = ys[j]
    ys[j] = y
  }
  return ys
}

export function takeUniform (count, xs) {
  let ys = xs
  const results = []
  for (let i = 0; i < count; i++) {
    const index = rand(0, ys.length - 1)
    const y = ys[index]
    if (typeof y === 'function') results.push(y())
    else results.push(y)
    ys = without(index, ys)
  }
  return results
}

export function takeWeighted (count, xs) {
  let ys = xs
  const results = []
  for (let i = 0; i < count; i++) {
    const index = weightedIndex(ys)
    const y = ys[index][1]
    if (typeof y === 'function') results.push(y())
    else results.push(y)
    ys = without(index, ys)
  }
  return results
}

export function uniform (xs) {
  const x = xs[rand(0, xs.length - 1)]
  if (typeof x === 'function') return x()
  return x
}

export function weighted (xs, max) {
  const [, x] = xs[weightedIndex(xs, max)]
  if (typeof x === 'function') return x()
  return x
}

export function weightedIndex (xs, max) {
  const total = xs.reduce((t, [w]) => t + w, 0)
  const roll = rand(0, typeof max === 'number' ? max : total - 1)
  let y = 0
  for (let i = 0; i < xs.length; i++) {
    const [w] = xs[i]
    if (roll < y + w) return i
    y += w
  }
}
