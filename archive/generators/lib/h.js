export default function h (elementType, a, b) {
  const props = typeof a === 'object' && !Array.isArray(a) ? a : {}
  const children = Array.isArray(a) ? a : b || []
  const element = document.createElement(elementType)
  for (const key in props) {
    switch (key) {
      case 'onClick':
        element.addEventListener('click', props[key])
        break
      case 'text':
        element.textContent = props[key]
        break
      default: element.setAttribute(key, props[key])
    }
  }
  children.forEach(c => {
    if (typeof c === 'string') element.appendChild(document.createTextNode(c))
    else element.appendChild(c)
  })
  return element
}
