const vowels = ['a', 'e', 'i', 'o', 'u']

export function capital (str) {
  return str[0].toUpperCase() + str.substring(1)
}

export function indefinite (noun) {
  const first = noun[0].toLowerCase()
  const article = vowels.some(v => first === v) ? 'an' : 'a'
  return `${article} ${noun}`
}

export function listWords (words) {
  if (words.length === 1) return words[0]
  if (words.length === 2) return words.join(' and ')
  return [
    words.slice(0, -1).join(', '),
    words[words.length - 1]
  ].join(', and ')
}

export function plural (num, noun, pnoun) {
  return `${num} ${num === 1 ? noun : (pnoun || `${noun}s`)}`
}
