module Data.Waylay exposing (arcGenerator, generator)

import Data.Waylay.Modifier as Modifier
import Data.Waylay.Noun as Noun
import Random exposing (Generator, uniform, weighted)
import Random.List exposing (shuffle)


arcGenerator : Generator (List String)
arcGenerator =
    Random.map4
        (\theme exposition risingAction climax ->
            [ "Theme: " ++ theme
            , "Exposition: " ++ exposition
            , "Rising Action: " ++ risingAction
            , "Climax: " ++ climax
            ]
        )
        descriptorGenerator
        generator
        generator
        generator


descriptorGenerator : Generator String
descriptorGenerator =
    Random.map2
        (\modifier noun -> String.join " " [ modifier, noun ])
        Modifier.generator
        Noun.generator


generator : Generator String
generator =
    Random.map2
        (\descriptor solution ->
            descriptor
                ++ " overcome by "
                ++ solution
        )
        descriptorGenerator
        solutionGenerator


solutionGenerator : Generator String
solutionGenerator =
    weighted
        ( 1, "legendary help" )
        [ ( 2, "an act of nature" )
        , ( 3, "the people" )
        , ( 4, "enemy help" )
        , ( 5, "avoidance" )
        , ( 6, "a scarcely used ability" )
        , ( 7, "personal resources" )
        , ( 8, "a close friend" )
        , ( 9, "a strong attribute" )
        , ( 10, "a favored ability" )
        , ( 9, "a favored skill" )
        , ( 8, "accident" )
        , ( 7, "a weak attribute" )
        , ( 6, "counteraction" )
        , ( 5, "faction intervention" )
        , ( 4, "authority" )
        , ( 3, "fate" )
        , ( 2, "a change of heart" )
        , ( 1, "a deus ex machina" )
        ]
