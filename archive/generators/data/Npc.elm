module Data.Npc exposing (generator)

import Data.Npc.Modifier as Modifier
import Data.Npc.MotivationNoun as MotiveNoun
import Data.Npc.MotivationVerb as MotiveVerb
import Data.Npc.Noun as Noun
import Random exposing (Generator, constant, uniform, weighted)
import Text exposing (capital, withIndefinite)


type Demeanor
    = Friendly
    | Hostile
    | Inquisitive
    | Insane
    | Knowing
    | Mysterious
    | Prejudiced
    | Scheming


bearingGenerator : Demeanor -> Generator String
bearingGenerator demeanor =
    case demeanor of
        Friendly ->
            uniform
                "alliance"
                [ "comfort"
                , "gratitude"
                , "shelter"
                , "happiness"
                , "support"
                , "promise"
                , "delight"
                , "aid"
                , "celebration"
                ]

        Hostile ->
            uniform "death"
                [ "capture"
                , "judgement"
                , "combat"
                , "surrender"
                , "rage"
                , "resentment"
                , "submission"
                , "injury"
                , "destruction"
                ]

        Inquisitive ->
            uniform "questions"
                [ "investigation"
                , "interest"
                , "demand"
                , "suspicion"
                , "request"
                , "curiosity"
                , "skepticism"
                , "command"
                , "petition"
                ]

        Insane ->
            uniform "madness"
                [ "fear"
                , "accident"
                , "chaos"
                , "idiocy"
                , "illusion"
                , "turmoil"
                , "confusion"
                , "façade"
                , "bewilderment"
                ]

        Knowing ->
            uniform "report"
                [ "effects"
                , "examination"
                , "records"
                , "account"
                , "news"
                , "history"
                , "telling"
                , "discourse"
                , "speech"
                ]

        Mysterious ->
            uniform "rumor"
                [ "uncertainty"
                , "secrets"
                , "misdirection"
                , "whispers"
                , "lies"
                , "shadows"
                , "enigma"
                , "obscurity"
                , "conundrum"
                ]

        Prejudiced ->
            uniform "reputation"
                [ "doubt"
                , "bias"
                , "dislike"
                , "partiality"
                , "belief"
                , "view"
                , "discrimination"
                , "assessment"
                , "difference"
                ]

        Scheming ->
            uniform "intent"
                [ "bargain"
                , "means"
                , "proposition"
                , "plan"
                , "compromise"
                , "agenda"
                , "arrangement"
                , "negotiation"
                , "plot"
                ]


contentGenerator : Generator String
contentGenerator =
    demeanorGenerator
        |> Random.andThen
            (\demeanor ->
                Random.map3
                    (\bearing focus mood ->
                        "They're "
                            ++ mood
                            ++ " and "
                            ++ demeanorString demeanor
                            ++ ", and speak of "
                            ++ bearing
                            ++ " regarding "
                            ++ focus
                            ++ "."
                    )
                    (bearingGenerator demeanor)
                    focusGenerator
                    moodGenerator
            )


demeanorGenerator : Generator Demeanor
demeanorGenerator =
    uniform
        Friendly
        [ Hostile
        , Inquisitive
        , Insane
        , Knowing
        , Mysterious
        , Prejudiced
        , Scheming
        ]


demeanorString : Demeanor -> String
demeanorString demeanor =
    case demeanor of
        Friendly ->
            "friendly"

        Hostile ->
            "hostile"

        Inquisitive ->
            "inquisitive"

        Insane ->
            "insane"

        Knowing ->
            "knowing"

        Mysterious ->
            "mysterious"

        Prejudiced ->
            "prejudiced"

        Scheming ->
            "scheming"


descriptionGenerator : Generator String
descriptionGenerator =
    Random.map2
        (\m n -> String.join " " [ m, n ])
        Modifier.generator
        Noun.generator


focusGenerator : Generator String
focusGenerator =
    uniform "a future action"
        [ "a recent scene"
        , "allies"
        , "an antagonist"
        , "an enemy"
        , "contacts"
        , "equipment"
        , "experience"
        , "fame"
        , "family"
        , "flaws"
        , "friends"
        , "history"
        , "knowledge"
        , "parents"
        , "power"
        , "relics"
        , "retainers"
        , "rewards"
        , "skills"
        , "superiors"
        , "the campaign"
        , "the character"
        , "the community"
        , "the current scene"
        , "the current story"
        , "the last action"
        , "the last scene"
        , "the last story"
        , "the previous scene"
        , "treasure"
        , "wealth"
        , "weapons"
        ]


generator : Generator (List String)
generator =
    Random.map2
        (\summary content -> [ summary, content ])
        summaryGenerator
        contentGenerator


moodGenerator : Generator String
moodGenerator =
    weighted
        ( 5, "withdrawn" )
        [ ( 10, "guarded" )
        , ( 15, "cautious" )
        , ( 30, "neutral" )
        , ( 25, "sociable" )
        , ( 10, "helpful" )
        , ( 5, "forthcoming" )
        ]


motivationGenerator : Generator String
motivationGenerator =
    Random.map2
        (\v n -> String.join " " [ v, n ])
        MotiveVerb.generator
        MotiveNoun.generator


powerLevelGenerator : Generator String
powerLevelGenerator =
    weighted
        ( 5, "much weaker than" )
        [ ( 15, "slightly weaker than" )
        , ( 60, "comparable to" )
        , ( 15, "slightly stronger than" )
        , ( 5, "much stronger than" )
        ]


summaryGenerator : Generator String
summaryGenerator =
    Random.map5
        (\description powerLevel motivation1 motivation2 motivation3 ->
            capital (withIndefinite description)
                ++ ", "
                ++ powerLevel
                ++ " the party, "
                ++ motivation1
                ++ ", "
                ++ motivation2
                ++ ", and "
                ++ motivation3
                ++ "."
        )
        descriptionGenerator
        powerLevelGenerator
        motivationGenerator
        motivationGenerator
        motivationGenerator
