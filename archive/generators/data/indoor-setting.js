export const ages = [
  'recent',
  'ancient',
  'new',
  'empty',
  'rustic',
  'old',
  'early',
  'well-used',
  'modern',
  'late',
  'run down',
  'untouched'
]

export const appearances = [
  'bright',
  'dark',
  'murky',
  'vibrant',
  'dusty',
  'burnt',
  'minimalist',
  'picturesque',
  'ruined',
  'developed',
  'charming',
  'artistic'
]

export const colors = [
  'red',
  'yellow',
  'blue',
  'green',
  'cyan',
  'magenta',
  'black',
  'white',
  'orange',
  'brown',
  'gray',
  'pink'
]

export const determiners = [
  'specific',
  'familiar',
  'encompassing',
  'possessed',
  'previously mentioned',
  'specific distant',
  'unspecific',
  'unspecified',
  'one or some',
  'large amount',
  'small amount',
  'not any'
]

export const locations = [
  'kitchen, food area',
  'storage, stable, barn, garage',
  'bedroom, sleeping quarters',
  'bathroom',
  'work area, desks, merchant area',
  'entertainment room',
  'library, study, computer room',
  'security, guard, hidden',
  'stairs, ramp',
  'hallway',
  'meeting room',
  'entrance, exit'
]

export const materials = [
  'stone',
  'marble',
  'brick',
  'carpeted',
  'tiled',
  'hardwood',
  'silk curtains, sheets',
  'bamboo',
  'wooden',
  'ceramic',
  'glass',
  'metal'
]

export const opinions = [
  'colorful',
  'beautiful',
  'eery',
  'dull',
  'ugly',
  'peaceful',
  'awe-inspiring',
  'dislikable, unfavorable',
  'strange',
  'unique',
  'favorite',
  'least favorite'
]

export const origins = [
  'northern',
  'southern',
  'eastern',
  'western',
  'urban',
  'rural',
  'foreign',
  'historical',
  'traditional',
  'luxurious',
  'contemporary',
  'vintage'
]

export const properties = [
  'deliberate',
  'stolen goods',
  'smelly',
  'aromatic',
  'noisy',
  'silent',
  'hidden',
  'clear',
  'natural, plants',
  'pictures, paintings, images',
  'adaptable',
  'statues, figures, taxidermy'
]

export const qualities = [
  'elegant',
  'simple',
  'playful',
  'creative',
  'messy',
  'perfect',
  'organized',
  'clean',
  'comfortable',
  'decorative',
  'hot',
  'cold'
]

export const sizes = [
  'room for one',
  'tiny',
  'small',
  'medium',
  'large',
  'huge',
  'gargantuan',
  'colossal',
  'wide',
  'lengthy',
  'narrow',
  'towerous'
]

export const types = [
  'cubical',
  'rectangular',
  'U-shaped',
  'V-shaped',
  'O-shaped',
  'specialized',
  'single purpose',
  'repurposed',
  'T-shaped',
  'S-shaped',
  'multi-purpose',
  'L-shaped'
]
