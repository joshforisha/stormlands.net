export default [
  'black',
  'blue',
  'brown',
  'cyan',
  'green',
  'orange',
  'pink',
  'purple',
  'red',
  'turquoise',
  'violet',
  'white',
  'yellow'
]
