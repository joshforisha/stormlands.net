import weightedAmphibiansAndReptiles from './weighted-amphibians-and-reptiles'
import weightedAquaticAnimals from './weighted-aquatic-animals'
import weightedBirds from './weighted-birds'
import weightedFantasticAnimals from './weighted-fantastic-animals'
import weightedMammals from './weighted-mammals'
import { weighted } from '~/lib/random'

export default [
  [2, () => weighted(weightedAmphibiansAndReptiles)],
  [1, () => weighted(weightedAquaticAnimals)],
  [1, () => weighted(weightedBirds)],
  [4, () => weighted(weightedMammals)],
  // [1, () => weighted(weightedOtherAnimals)],
  // [1, () => weighted(weightedPlants)],
  [1, () => weighted(weightedFantasticAnimals)]
  // [1, () => weighted(weightedFantasticPeoples)],
]
