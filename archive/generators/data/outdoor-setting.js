
export const ages = [
  'young',
  'ancient',
  'new',
  'barren',
  'forgotten',
  'old',
  'early',
  'changing',
  'modern',
  'late',
  'run down',
  'prime'
]

export const appearances = [
  'bright',
  'dark',
  'murky',
  'vibrant',
  'dusty',
  'burnt',
  'lush',
  'war torn',
  'ruins',
  'developed',
  'volcanic',
  'artistic'
]

export const biomes = [
  'jungle',
  'desert',
  'grassland',
  'savanna',
  'deciduous forest',
  'coniferous forest',
  'high plateaus',
  'hills',
  'mountains',
  'ocean',
  'river, lake',
  'tundra'
]

export const determiners = [
  'specific',
  'familiar',
  'encompassing',
  'possessed',
  'previously mentioned',
  'specific distant',
  'unspecific',
  'unspecified',
  'one or some',
  'large amount',
  'small amount',
  'not any'
]

export const locations = [
  'village',
  'town',
  'city',
  'farmlands',
  'base/fortress',
  'outpost',
  'mining',
  'hiding',
  'industrial',
  'ghost town',
  'recreation',
  'trade center'
]

export const materials = [
  'woody',
  'sandy',
  'clay',
  'gravelly',
  'rocky',
  'swampy',
  'grassy',
  'metals & gems',
  'rich soil/loamy/compost',
  'silty',
  'clay',
  'limestone/chalky'
]

export const opinions = [
  'colorful',
  'beautiful',
  'eery',
  'dull',
  'ugly',
  'peaceful',
  'awe-inspiring',
  'dislikable, unfavorable',
  'strange',
  'unique',
  'favorite',
  'least favorite'
]

export const origins = [
  'borderland',
  'intersection',
  'valley',
  'highland',
  'urban',
  'rural',
  'nearby',
  'historical',
  'distant',
  'ancestral',
  'local',
  'adjacent'
]

export const properties = [
  'smelly',
  'aromatic',
  'inhabited',
  'uninhabited',
  'desolate',
  'rainy/snowy',
  'hidden',
  'clear',
  'natural',
  'artificial',
  'well-used',
  'untouched'
]

export const qualities = [
  'wet',
  'dry',
  'rough',
  'flat',
  'broken up',
  'perfect',
  'layers/tiers',
  'sunny',
  'cloudy',
  'stormy',
  'hot',
  'cold'
]

export const sizes = [
  'expansive',
  'narrow',
  'neck',
  'massive',
  'lengthy',
  'wide',
  'dense',
  'small',
  'venous',
  'boundless',
  'great',
  'scattered'
]

export const types = [
  'general purpose',
  'multi-purpose',
  'U-shaped',
  'V-shaped',
  'O-shaped',
  'specialized',
  'single purpose',
  'repurposed',
  'square-shaped',
  'S-shaped',
  'adaptable',
  'L-shaped'
]
