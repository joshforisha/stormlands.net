module Data.Spaceport.Encounter exposing (encounters)


encounters : List String
encounters =
    [ "Annoying official"
    , "Neglected labor bot"
    , "Weird looking mutant offworlder"
    , "Suspicious acting staff/occupant"
    , "Information broker with some good rumors"
    , "Over talkative drunk with checkered past"
    , "Seedy acting trader"
    , "Trigger happy security officer"
    , "Strange smelling stranger in thick garb"
    , "Offworld spy trying to blend in with others"
    , "Naive acting occupant/staff"
    , "Disorderly acting vagrant"
    , "Thuggish criminal enforcer"
    , "Aggressive young thugs"
    , "Group of intolerant locals"
    , "Malfunctioning security bot"
    , "Gruff acting communications android"
    , "Android with sleazy programming"
    , "Sarcastic acting linguistics bot"
    , "Grumbling technician"
    ]
