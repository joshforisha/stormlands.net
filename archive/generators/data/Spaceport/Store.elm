module Data.Spaceport.Store exposing (stores)


stores : List String
stores =
    [ "Black Hole Donuts"
    , "Blue Burger Restaurant"
    , "Capital Worlds Fashion"
    , "Cheers Beers"
    , "Coreward Cuisine"
    , "Deep Black Pub"
    , "Duke of Sandwich"
    , "Duty-Free Gift Shop"
    , "Eat At Joe's Grill"
    , "Forever Love Jewelry"
    , "Free Faith Chapel"
    , "Holovid Entertainment"
    , "Hyperspace Games and Hobbies"
    , "Launchpad Children's Play Area"
    , "Low Berth Last Will and Testament"
    , "MacBurgers Restaurant"
    , "Michael Taylor's"
    , "Moon Pie with Cheese"
    , "News and Gifts"
    , "On the Mark Weapons Range"
    , "Orbital HighBar"
    , "Pilot's Fuel Coffee Bar"
    , "Planetary Weapon Registry"
    , "QuickTailored Suits and Dresses"
    , "Rimward of the Border Restaurant"
    , "S-Mart Department Store"
    , "Space Fleet Hoagies"
    , "Spacer's Club and Lounge"
    , "SpeedySpa"
    , "StarBeans Coffee"
    , "Starfire Jewelry"
    , "Startel Currency Exchange"
    , "StayFresh Snacks"
    , "Travel in Comfort Clothing"
    , "Vital Vaccsuit Supplies"
    , "World of Shoes"
    ]
