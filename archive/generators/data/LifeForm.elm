module Data.LifeForm exposing (LifeForm, generator, summaryGenerator)

import Data.LifeForm.BaseForm as BaseForm
import Data.LifeForm.Size exposing (Size(..), sizeString)
import Lib.Dice exposing (d10, d100, d12, d2, d20, d3, d4, d6, d8, roll, rollf, rollfm)
import Lib.Length exposing (Length, feet, inches, lengthString)
import Lib.Random exposing (uniformS)
import Lib.Text exposing (count, countWord)
import Lib.Time exposing (Time, centuries, hours, months, timeString, weeks, years)
import Random exposing (Generator, andThen, constant, uniform, weighted)
import Random.Extra exposing (andMap)


type alias LifeForm =
    { activePeriod : String
    , baseForm : String
    , colouration : String
    , communication : String
    , diet : String
    , hearing : String
    , intelligence : String
    , length : Length
    , lifespan : Time
    , limbs : String
    , maximumSpeed : String
    , movementType : String
    , reproduction : String
    , shape : String
    , sight : String
    , size : Size
    , smell : String
    , taste : String
    , touch : String
    }


genActivePeriod : Generator String
genActivePeriod =
    weighted
        ( 10, "Day" )
        [ ( 5, "Dusk/dawn" )
        , ( 5, "Night" )
        ]


genColouration : Generator String
genColouration =
    uniform
        "Blue"
        [ "Brown"
        , "Green"
        , "Grey"
        , "Orange"
        , "Original"
        , "Other"
        , "Pink"
        , "White"
        , "Yellow"
        ]


genCommunication : Generator String
genCommunication =
    weighted
        ( 1, "Body language" )
        [ ( 1, "Colours" )
        , ( 1, "Electrocommunication" )
        , ( 1, "Movement/position" )
        , ( 5, "Pheromones/olfactory" )
        , ( 8, "Speech/vocalisation" )
        , ( 1, "Telepathy/mental" )
        , ( 1, "Touch/contact" )
        , ( 1, "Vibration" )
        ]


genDiet : Generator String
genDiet =
    weighted
        ( 5, "Carnivore" )
        [ ( 4, "Herbivore" )
        , ( 7, "Omnivore" )
        , ( 2, "Photosynthesis" )
        , ( 1, "Radiation" )
        , ( 1, "Mineral eater" )
        ]


genEar : Generator ( String, Bool )
genEar =
    Random.andThen
        (\( genNum, location ) ->
            Random.andThen
                (\num ->
                    if num > 0 then
                        Random.map
                            (\earType ->
                                ( String.fromInt num
                                    ++ " "
                                    ++ earType
                                    ++ " "
                                    ++ countWord num "ear"
                                    ++ ", "
                                    ++ location
                                , True
                                )
                            )
                            genEarType

                    else
                        constant ( location, False )
                )
                genNum
        )
        (weighted
            ( 1, ( constant 0, "None visible" ) )
            [ ( 1, ( constant 1, "top of head" ) )
            , ( 1, ( constant 1, "back of head" ) )
            , ( 7, ( constant 2, "top of head" ) )
            , ( 7, ( constant 2, "side of head" ) )
            , ( 1, ( constant 3, "equally around head" ) )
            , ( 1, ( constant 4, "equally around head" ) )
            , ( 1, ( roll 2 d4, "equally around head" ) )
            ]
        )


genEarType : Generator String
genEarType =
    uniform
        "batlike"
        [ "blunt"
        , "candle/flame"
        , "cropped"
        , "flappy"
        , "hooded"
        , "human-like"
        , "pits"
        , "pointed"
        , "triangular"
        ]


generator : Generator LifeForm
generator =
    andThen
        (\( size, sizeRating ) ->
            Random.map LifeForm genActivePeriod
                |> andMap BaseForm.generator
                |> andMap genColouration
                |> andMap genCommunication
                |> andMap genDiet
                |> andMap genHearing
                |> andMap genIntelligence
                |> andMap (genLength size sizeRating)
                |> andMap (genLifespan sizeRating)
                |> andMap genLimbs
                |> andMap (genMaximumSpeed sizeRating)
                |> andMap genMovementType
                |> andMap genReproduction
                |> andMap genShape
                |> andMap genSight
                |> andMap (constant size)
                |> andMap genSmell
                |> andMap genTaste
                |> andMap (genTouch sizeRating)
        )
        genSize


genEyeType : Generator ( String, Bool )
genEyeType =
    weighted
        ( 1, ( "compound", False ) )
        [ ( 10, ( "standard", True ) )
        , ( 3, ( "retractable stalk", True ) )
        , ( 3, ( "peripheral", True ) )
        , ( 2, ( "stalk", True ) )
        , ( 1, ( "pit", False ) )
        ]


genHearing : Generator String
genHearing =
    Random.andThen
        (\( ear, canHear ) ->
            if canHear then
                Random.map
                    (\quality -> "[" ++ quality ++ "%] " ++ ear)
                    (Random.map String.fromInt d100)

            else
                constant ear
        )
        genEar


genIntelligence : Generator String
genIntelligence =
    Random.andThen identity
        (weighted
            ( 1, Random.map (\i -> "Genius (" ++ String.fromInt i ++ " IQ)") (Random.int 140 160) )
            [ ( 2, Random.map (\i -> "Exceptional (" ++ String.fromInt i ++ " IQ)") (Random.int 120 140) )
            , ( 2, Random.map (\i -> "Above Average (" ++ String.fromInt i ++ " IQ)") (Random.int 100 120) )
            , ( 9, Random.map (\i -> "Average (" ++ String.fromInt i ++ " IQ)") (Random.int 90 100) )
            , ( 2, Random.map (\i -> "Below Average (" ++ String.fromInt i ++ " IQ)") (Random.int 80 90) )
            , ( 1, Random.map (\i -> "Dull (" ++ String.fromInt i ++ " IQ)") (Random.int 70 80) )
            , ( 1, Random.map (\i -> "Borderline Deficiency (" ++ String.fromInt i ++ " IQ)") (Random.int 60 70) )
            , ( 1, Random.map (\i -> "Moron (" ++ String.fromInt i ++ " IQ)") (Random.int 50 60) )
            , ( 1, Random.map (\i -> "Imbecile (" ++ String.fromInt i ++ " IQ)") (Random.int 30 50) )
            , ( 1, Random.map (\i -> "Idiot (" ++ String.fromInt i ++ " IQ)") (Random.int 1 20) )
            ]
        )


genLength : Size -> Float -> Generator Length
genLength size sizeRating =
    case size of
        Microscopic ->
            Random.map inches (Random.float 0.1 0.2)

        Minuscule ->
            Random.map inches (Random.float 0.2 1.0)

        Fine ->
            Random.map inches (rollf 1 d6)

        Diminutive ->
            Random.map inches (rollfm 1 d6 <| (+) 6)

        Tiny ->
            Random.map inches (rollfm 1 d12 <| (+) 12)

        Small ->
            Random.map feet (rollf 2 d2)

        Medium ->
            Random.map feet (rollfm 1 d3 <| (+) 5)

        Large ->
            Random.map feet (rollf 8 d2)

        Huge ->
            Random.map feet (rollf 16 d2)

        Gargantuan ->
            Random.map feet (rollf 32 d2)

        Colossal ->
            Random.map feet (rollf 64 d2)

        Epic ->
            Random.map feet (rollfm 1 d4 <| (+) 1 >> (*) sizeRating)


genLifespan : Float -> Generator Time
genLifespan sizeRating =
    if sizeRating < 1 then
        Random.map hours (Random.float 12 24)

    else
        Random.andThen
            (\r ->
                if r < 4 then
                    Random.constant (weeks 1)

                else if r < 7 then
                    Random.map weeks (rollf 2 d4)

                else if r < 12 then
                    Random.map months (rollf 1 d10)

                else if r < 16 then
                    Random.map months (rollf 2 d10)

                else if r < 21 then
                    Random.map years (rollf 1 d6)

                else if r < 25 then
                    Random.map years (rollf 1 d8)

                else if r < 29 then
                    Random.map years (rollfm 1 d10 <| (+) 1)

                else if r < 34 then
                    Random.map years (rollfm 1 d10 <| (+) 10)

                else if r < 39 then
                    Random.map years (rollfm 2 d10 <| (+) 20)

                else if r < 46 then
                    Random.map years (rollfm 2 d10 <| (+) 40)

                else if r < 65 then
                    Random.map years (rollfm 2 d10 <| (+) 60)

                else if r < 71 then
                    Random.map years (rollfm 2 d10 <| (+) 80)

                else if r < 76 then
                    Random.map years (rollfm 2 d20 <| (+) 100)

                else if r < 81 then
                    Random.map years (rollfm 2 d20 <| (+) 120)

                else if r < 86 then
                    Random.map years (rollfm 2 d20 <| (+) 140)

                else if r < 91 then
                    Random.map years (rollfm 2 d20 <| (+) 160)

                else if r < 95 then
                    Random.map years (rollfm 2 d20 <| (+) 180)

                else if r < 98 then
                    Random.map centuries (rollf 1 d6)

                else if r < 100 then
                    Random.map centuries (rollf 1 d8)

                else
                    Random.map centuries (rollf 1 d10)
            )
            (roll (round sizeRating) d10)


genLimbs : Generator String
genLimbs =
    Random.andThen identity
        (weighted
            ( 12, constant "No change" )
            [ ( 3
              , Random.map2
                    (\d n -> count (maybe (d == 20) n (2 * n)) "extra arm")
                    d20
                    d4
              )
            , ( 2
              , Random.map2
                    (\d n -> count (maybe (d == 20) n (2 * n)) "extra leg")
                    d20
                    d4
              )
            , ( 1
              , Random.map2
                    (\d n -> count (maybe (d == 20) n 1) "normal tail")
                    d20
                    d4
              )
            , ( 1
              , Random.map2
                    (\d n -> count (maybe (d == 20) n 1) "prehensile tail")
                    d20
                    d4
              )
            , ( 1, constant "Tentacles" )
            ]
        )


genMaximumSpeed : Float -> Generator String
genMaximumSpeed sizeRating =
    let
        mps mph =
            String.fromInt (round (1.609 * mph)) ++ " km/h"
    in
    Random.andThen
        (\d ->
            let
                r =
                    round (d + sizeRating)
            in
            if r < 2 then
                constant "Faster than most eyes can see"

            else if r < 3 then
                Random.map mps (Random.float 200 300)

            else if r < 4 then
                Random.map mps (Random.float 100 200)

            else if r < 5 then
                Random.map mps (Random.float 60 100)

            else if r < 6 then
                Random.map mps (Random.float 41 60)

            else if r < 9 then
                Random.map mps (Random.float 21 40)

            else if r < 13 then
                Random.map mps (Random.float 11 20)

            else if r < 16 then
                Random.map mps (Random.float 6 10)

            else if r < 17 then
                Random.map mps (Random.float 1 5)

            else if r < 18 then
                Random.map mps (Random.float 0.5 1)

            else if r < 19 then
                Random.map mps (Random.float 0.1 0.5)

            else if r < 20 then
                Random.map mps (Random.float 0.01 0.1)

            else
                constant "So slow as to be almost not moving"
        )
        (rollf 1 d20)


genMouth : Generator String
genMouth =
    weighted
        ( 6, "Human type" )
        [ ( 6, "Extended jaw" )
        , ( 1, "Mandibles" )
        , ( 3, "Short beak" )
        , ( 3, "Long beak" )
        , ( 1, "Proboscis" )
        ]


genMovementType : Generator String
genMovementType =
    weighted
        ( 2, "Crawl/slide etc." )
        [ ( 2, "Flight" )
        , ( 2, "Gliding" )
        , ( 3, "Jumping" )
        , ( 11, "Walking" )
        ]


genNose : Generator ( String, Bool )
genNose =
    weighted
        ( 2, ( "No visible nose", False ) )
        [ ( 4, ( "Smells with tongue/mouth", True ) )
        , ( 9, ( "Standard nose", True ) )
        , ( 3, ( "Prehensile", True ) )
        , ( 2, ( "Pits", True ) )
        ]


genNumEyes : Generator Int
genNumEyes =
    weighted
        ( 1, constant 0 )
        [ ( 2, constant 1 )
        , ( 13, constant 2 )
        , ( 1, constant 3 )
        , ( 2, constant 4 )
        , ( 1, Random.map ((+) 4) d6 )
        ]
        |> Random.andThen identity


genPupilType : Generator String
genPupilType =
    weighted
        ( 2, "\"W\" shaped" )
        [ ( 9, "circular" )
        , ( 2, "crescent" )
        , ( 1, "irregular" )
        , ( 2, "horizontal slit" )
        , ( 2, "vertical slit" )
        , ( 1, "star" )
        , ( 1, "string of pearls" )
        ]


genReproduction : Generator String
genReproduction =
    Random.andThen
        (\( method, litterRoll ) ->
            Random.map
                (\litter -> method ++ " (Litter: " ++ String.fromInt litter ++ ")")
                litterRoll
        )
        (weighted
            ( 1, ( "A form of mitosis/splitting", d4 ) )
            [ ( 2, ( "Budding", d8 ) )
            , ( 3, ( "Egg layer", d6 ) )
            , ( 8, ( "Mammalian/internal", d2 ) )
            , ( 3, ( "Marsupial/pouch", d3 ) )
            , ( 1, ( "Spore", Random.map ((*) 1000) d4 ) )
            , ( 1, ( "Seed", Random.map ((*) 10) d4 ) )
            , ( 1, ( "Implantation/host", constant 1 ) )
            ]
        )


genShape : Generator String
genShape =
    weighted
        ( 1, "Amoeba/blob-like" )
        [ ( 2, "Ball - oval" )
        , ( 2, "Ball - spherical" )
        , ( 2, "Circular" )
        , ( 4, "Humanoid" )
        , ( 1, "Pyramidal/cone" )
        , ( 3, "Resembles base creature" )
        , ( 2, "Tubular" )
        , ( 2, "Cubic/square" )
        , ( 1, "Changeable/other" )
        ]


genSight : Generator String
genSight =
    genNumEyes
        |> Random.andThen
            (\num ->
                if num < 1 then
                    constant "No eyes"

                else
                    Random.Extra.andThen2
                        (\quality ( style, hasPupils ) ->
                            if hasPupils then
                                Random.map
                                    (\pupilStyle ->
                                        "["
                                            ++ quality
                                            ++ "%] "
                                            ++ count num (style ++ " eye")
                                            ++ " with "
                                            ++ countWord num (pupilStyle ++ " pupil")
                                    )
                                    genPupilType

                            else
                                constant ("[" ++ quality ++ "%] " ++ count num (style ++ " eye"))
                        )
                        (Random.map String.fromInt d100)
                        genEyeType
            )


genSize : Generator ( Size, Float )
genSize =
    Random.weighted
        ( 1, ( Microscopic, 0 ) )
        [ ( 1, ( Minuscule, 0.5 ) )
        , ( 1, ( Fine, 1 ) )
        , ( 6, ( Diminutive, 2 ) )
        , ( 10, ( Tiny, 3 ) )
        , ( 10, ( Small, 4 ) )
        , ( 40, ( Medium, 5 ) )
        , ( 10, ( Large, 6 ) )
        , ( 10, ( Huge, 7 ) )
        , ( 7, ( Gargantuan, 8 ) )
        , ( 2, ( Colossal, 9 ) )
        , ( 1, ( Epic, 10 ) )
        ]


genSmell : Generator String
genSmell =
    Random.andThen
        (\( nose, canSmell ) ->
            if canSmell then
                Random.map
                    (\quality -> "[" ++ quality ++ "%] " ++ nose)
                    (Random.map String.fromInt d100)

            else
                constant nose
        )
        genNose


genTaste : Generator String
genTaste =
    Random.map2
        (\q t -> "[" ++ q ++ "%] " ++ t)
        (Random.map String.fromInt d100)
        genMouth


genTouch : Float -> Generator String
genTouch sizeRating =
    Random.map2
        (\q i ->
            "["
                ++ String.fromInt ((10 * max 1 (round (q - sizeRating))) + i)
                ++ "%]"
        )
        (rollf 1 d10)
        d10


maybe : Bool -> a -> a -> a
maybe okay yes no =
    if okay then
        yes

    else
        no


summaryGenerator : Random.Generator (List String)
summaryGenerator =
    Random.map
        (\lifeForm ->
            [ "Base Form: " ++ lifeForm.baseForm
            , "Size: " ++ sizeString lifeForm.size
            , "Lifespan: "
                ++ timeString lifeForm.lifespan
                ++ " (Gestation: "
                ++ timeString (0.1 * lifeForm.lifespan)
                ++ ", Maturation: "
                ++ timeString (0.2 * lifeForm.lifespan)
                ++ ")"
            , "Colouration: " ++ lifeForm.colouration
            , "Communication: " ++ lifeForm.communication
            , "Diet: " ++ lifeForm.diet
            , "Length: " ++ lengthString lifeForm.length
            , "Limbs: " ++ lifeForm.limbs
            , "Movement: " ++ lifeForm.movementType ++ "; " ++ lifeForm.maximumSpeed
            , "Reproduction: " ++ lifeForm.reproduction
            , "Sight: " ++ lifeForm.sight
            , "Smell: " ++ lifeForm.smell
            , "Hearing: " ++ lifeForm.hearing
            , "Taste: " ++ lifeForm.taste
            , "Touch: " ++ lifeForm.touch
            , "Shape: " ++ lifeForm.shape
            , "Active: " ++ lifeForm.activePeriod
            ]
        )
        generator
