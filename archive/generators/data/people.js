export default [
  'child',
  'criminal',
  'explorer',
  'fighter',
  'friend',
  'leader',
  'parent',
  'relative',
  'scoundrel',
  'seeker',
  'sibling',
  'warrior'
]
