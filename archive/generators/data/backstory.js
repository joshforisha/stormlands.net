import actions from '~/data/actions'
import adjectives from '~/data/adjectives'
import moods from '~/data/moods'
import people from '~/data/people'
import plots from '~/data/plots'
import themes from '~/data/themes'
import { draw } from '~/lib/random'

export function generateBackstory () {
  return `${draw(adjectives)} ${draw(people)}; ${draw(actions)} ${draw(plots)}; ${draw(moods)} ${draw(themes)}`
}
