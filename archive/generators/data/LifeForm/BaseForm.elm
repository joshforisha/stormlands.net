module Data.LifeForm.BaseForm exposing (generator)

import Lib.Random exposing (uniformS)
import Random exposing (Generator, weighted)


generator : Generator String
generator =
    Random.andThen identity
        (weighted
            ( 1, Random.map ((++) "Aquatic - ") (uniformS aquaticForms) )
            [ ( 2, Random.map ((++) "Avian - ") (uniformS avianForms) )
            , ( 2, Random.map ((++) "Insect - ") (uniformS insectForms) )
            , ( 2, Random.map ((++) "Invertebrate - ") (uniformS invertebrateForms) )
            , ( 7, Random.map ((++) "Mammal - ") (uniformS mammalForms) )
            , ( 1, Random.map ((++) "Plant - ") (uniformS plantForms) )
            , ( 4, Random.map ((++) "Reptilian - ") (uniformS reptilianForms) )
            , ( 1, Random.map ((++) "Rock - ") (uniformS rockForms) )
            ]
        )


aquaticForms : List String
aquaticForms =
    [ "Angelfish"
    , "Barracuda"
    , "Bass"
    , "Blue Whale"
    , "Catfish"
    , "Clownfish"
    , "Cod"
    , "Dolphin"
    , "Eel"
    , "Goldfish"
    , "Killer Whale"
    , "Narwhal"
    , "Salmon"
    , "Sea Horse"
    , "Shark"
    , "Squid/Octopus"
    , "Stingray"
    , "Sturgeon"
    , "Trout"
    , "Whale"
    ]


avianForms : List String
avianForms =
    [ "Budgerigar"
    , "Chicken"
    , "Crow"
    , "Duck"
    , "Eagle"
    , "Flamingo"
    , "Goose"
    , "Magpie"
    , "Ostrich"
    , "Owl"
    , "Parrot"
    , "Penguin"
    , "Pigeon"
    , "Raven"
    , "Robin"
    , "Swan"
    , "Turkey"
    , "Vulture"
    , "Woodpecker"
    ]


insectForms : List String
insectForms =
    [ "Ant"
    , "Aphid"
    , "Bee"
    , "Butterfly"
    , "Centipede"
    , "Cockroach"
    , "Cricket"
    , "Dragonfly"
    , "Earwig"
    , "Flea"
    , "Fly"
    , "Ladybug"
    , "Locust"
    , "Moth"
    , "Scarab"
    , "Slug"
    , "Snail"
    , "Spider"
    , "Stag Beetle"
    , "Wasp"
    ]


invertebrateForms : List String
invertebrateForms =
    [ "Clam"
    , "Crab"
    , "Jellyfish"
    , "Lobster"
    , "Shrimp"
    , "Sponge"
    , "Starfish"
    , "Worm"
    , "Scorpion"
    , "Wood Louse"
    , "Leech"
    , "Millipede"
    , "Spider Crab"
    , "Isopod"
    , "Hornet"
    , "Coral"
    , "Krill"
    , "Urchin"
    , "Termite"
    , "Tick"
    ]


mammalForms : List String
mammalForms =
    [ "Antelope"
    , "Ape"
    , "Bear"
    , "Boar"
    , "Cat"
    , "Cheetah"
    , "Cow"
    , "Dog"
    , "Giraffe"
    , "Hippo"
    , "Horse"
    , "Kangaroo"
    , "Lion"
    , "Lynx"
    , "Mouse"
    , "Rabbit"
    , "Rat"
    , "Seal"
    , "Sheep"
    , "Tiger"
    ]


plantForms : List String
plantForms =
    [ "Aloe Vera"
    , "Bamboo"
    , "Cactus"
    , "Corn"
    , "Fruit Tree"
    , "Grass"
    , "Lichen"
    , "Lilly"
    , "Moss"
    , "Mushroom"
    , "Nettle"
    , "Oak"
    , "Poison Ivy"
    , "Poppy"
    , "Pumpkin"
    , "Redwood Tree"
    , "Rose"
    , "Sunflower"
    , "Venus Fly-Trap"
    ]


reptilianForms : List String
reptilianForms =
    [ "Alligator"
    , "Anole Lizard"
    , "Boa"
    , "Box Turtle"
    , "Cobra"
    , "Copperhead Snake"
    , "Crocodile"
    , "Garter Snake"
    , "Gecko"
    , "Iguana"
    , "Komodo Dragon"
    , "Lizard"
    , "Painted Turtle"
    , "Python"
    , "Rattlesnake"
    , "Salamander"
    , "Skink"
    , "Terrapin"
    , "Turtle"
    , "Viper"
    ]


rockForms : List String
rockForms =
    [ "Anthracite"
    , "Basalt"
    , "Bauxite"
    , "Breccia"
    , "Chalk"
    , "Clay"
    , "Coal"
    , "Dolomite"
    , "Flint"
    , "Gneiss"
    , "Granite"
    , "Limestone"
    , "Marble"
    , "Mudrock"
    , "Pumice"
    , "Quartz"
    , "Sandstone"
    , "Shale"
    , "Silicon"
    , "Slate"
    ]
