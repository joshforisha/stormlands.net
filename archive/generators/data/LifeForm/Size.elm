module Data.LifeForm.Size exposing (Size(..), sizeString)


type Size
    = Microscopic
    | Minuscule
    | Fine
    | Diminutive
    | Tiny
    | Small
    | Medium
    | Large
    | Huge
    | Gargantuan
    | Colossal
    | Epic


sizeString : Size -> String
sizeString size =
    case size of
        Colossal ->
            "Colossal"

        Diminutive ->
            "Diminutive"

        Epic ->
            "Epic"

        Fine ->
            "Fine"

        Gargantuan ->
            "Gargantuan"

        Huge ->
            "Huge"

        Large ->
            "Large"

        Medium ->
            "Medium"

        Microscopic ->
            "Microscopic"

        Minuscule ->
            "Minuscule"

        Small ->
            "Small"

        Tiny ->
            "Tiny"
