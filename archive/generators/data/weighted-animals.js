import weightedAmphibiansAndReptiles from '~/data/weighted-amphibians-and-reptiles'
import weightedAquaticAnimals from '~/data/weighted-aquatic-animals'
import weightedBirds from '~/data/weighted-birds'
import weightedMammals from '~/data/weighted-mammals'

export default [
  ...weightedAmphibiansAndReptiles,
  ...weightedAquaticAnimals,
  ...weightedBirds,
  ...weightedMammals
]
