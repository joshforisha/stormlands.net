export default [
  'black',
  'blue',
  'brown',
  'cerulean',
  'cyan',
  'gray',
  'green',
  'indigo',
  'magenta',
  'maroon',
  'mauve',
  'orange',
  'pink',
  'purple',
  'red',
  'tan',
  'taupe',
  'teal',
  'turquoise',
  'violet',
  'white',
  'yellow'
]
