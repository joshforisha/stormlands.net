import { chance, draw } from '~/lib/random'

export const digraphs = [
  'br',
  'ch',
  'cl',
  'cr',
  'ct',
  'dr',
  'fl',
  'fr',
  'gh',
  'gl',
  'gr',
  'gs',
  'kh',
  'kl',
  'kr',
  'ks',
  'mr',
  'nr',
  'ph',
  'pl',
  'pr',
  'ps',
  'sh',
  'sr',
  'st',
  'th',
  'vr',
  'wh',
  'wl',
  'wr',
  'zh',
  'zl',
  'zr'
]

export const diphthongs = [
  'aa',
  'ae',
  'ai',
  'ao',
  'au',
  'ea',
  'ee',
  'ei',
  'eo',
  'eu',
  'ia',
  'ie',
  'ii',
  'io',
  'iu',
  'oa',
  'oe',
  'oi',
  'oo',
  'ou',
  'ua',
  'ue',
  'ui',
  'uo',
  'uu'
]

export const monographs = [
  'b',
  'c',
  'd',
  'f',
  'g',
  'h',
  'j',
  'k',
  'l',
  'm',
  'n',
  'p',
  'q',
  'r',
  's',
  't',
  'v',
  'w',
  'x',
  'y',
  'z'
]

export const monophthongs = [
  'a', 'e', 'i', 'o', 'u', 'y'
]

export function randomConsonant () {
  if (chance(0.8)) return draw(monographs)
  return draw(digraphs)
}

export function randomVowel () {
  if (chance(0.8)) return draw(monophthongs)
  return draw(diphthongs)
}

export function randomNonsense () {
  let str = ''
  if (chance(0.5)) str += randomConsonant()
  let c = 1
  do {
    str += randomVowel()
    str += randomConsonant()
    c *= 0.5
  } while (chance(c))
  if (chance(0.5)) str += randomVowel()
  return str
}
