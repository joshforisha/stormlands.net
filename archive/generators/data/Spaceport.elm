module Data.Spaceport exposing (generator)

import Data.Spaceport.Encounter exposing (encounters)
import Data.Spaceport.Event exposing (events)
import Data.Spaceport.Location exposing (details, locations)
import Data.Spaceport.Store exposing (stores)
import Lib.Random exposing (uniformS)
import Random


generator : Random.Generator (List String)
generator =
    Random.map5
        (\encounter event location detail store ->
            [ "Encounter: " ++ encounter
            , "Event: " ++ event
            , "Location: " ++ location ++ "; " ++ detail
            , "Store: " ++ store
            ]
        )
        (uniformS encounters)
        (uniformS events)
        (uniformS locations)
        (uniformS details)
        (uniformS stores)
