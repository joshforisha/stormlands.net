import h from '~/lib/h'
import pages from '~/pages'

const main = document.querySelector('main')
const nav = document.querySelector('nav')

function render (viewFn) {
  return () => {
    main.innerHTML = ''

    const view = viewFn()
    if (Array.isArray(view)) view.forEach(e => main.appendChild(e))
    else main.appendChild(viewFn())
  }
}

Object.entries(pages)
  .forEach(([pageName, pageView]) => {
    const link = h('a', { onClick: render(pageView), text: pageName })
    nav.appendChild(link)
  })
