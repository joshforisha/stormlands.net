# NPCs

### Abbot Ruskin

The @Cretawk@ leader of the @Solarian@ temple at the @Voiceless Chasm@.

### Bryony

A former lobby manager at the *Medeon Fun Park*, where the crew met her while loudly disagreeing with many displays at the resort. She was fired briefly after, and @Rishnii@ sent her to the Cachak to take her offworld. Bryony has since gone missing.

### Geoffrie

An anxious @Dagan@ that approached @Azem@ in the Omni Square capsule hotel in @Merino@ to help smuggle him around the @Tamertoni@ officers searching for him. He paid @Azem@ well for his trouble, and has an agreement with the Cachak crew to cover the ship upgrade costs in exchange for getting him out of the @Sogantu@ system.

### Kavitha

A famously skilled ship mechanic operating out of a mechanical depot on the outskirts of @Merino@ on @Sogantu Prime@. The Cachak crew brought the ship into her depot to prepare it for safe recovery of @Rishnii@'s podmates.

### Slade

*(Deceased)* A supposedly well-connected producer from the Core Worlds. He had a number of expensive enhancements added to his private yacht at @Kavitha@'s depot, which @Garu@ took for a joyride. Once convinced that @Kavitha@ was not to be trusted for further business, Slade drunkenly crashed his yacht into the hills north of the depot, perishing in the process.

## Past Characters

### Dalibor

A wealthy @Human@ biomerchant stationed on @Omnia@. Dalibor tasked the Cachak crew with a mission to retrieve a holodisk from the @Monarch@ labs within the @Nimbus District@. The crew discovered that he is suspiciously on the @Monarch@ board of directors.

### Tavi

The @Ersatz@ owner and operator of @Tavi's Tavern@. The Cachak crew got into a brawl with @Dalibor@ in his tavern, and were hastily asked to leave once things turned lethal.

### Valerys Kaine

*(Deceased)* A @Human@ street doctor who joined the Cachak crew for a short time. He was killed by a couple of private guards, as part of a violent debriefing with @Dalibor@. He had kept many secrets from the crew, breeding distrust aboard the ship, and his last secret proved to be his undoing: a requested assassination requirement that he had kept quiet.
