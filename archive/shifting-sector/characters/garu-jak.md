# Garu Jak

> And Malikan placed its hand against the great creature’s brow, watching as the light swam from its eyes at last. "Safe journeys, Cachak. May that one’s eyeshine guide it to the Blue beyond the Black.
>
> Garu leaned against the cockpit’s large window, the fingers of his right hand spread against the hybridized metal-glass. He reflexively adjusted his feet’s grip on the railing. Space had never felt so empty to him as it did now. "When your roots are severed, even the gentlest wind portends chaos." This was not the time for dusty Keb’id adages, though. Klovin was gone, unable to protect him. The Bazaar was no longer a place of refuge. Once Ton Kula discovered the items Garu had liberated from his pleasure barge in order to get this derelict heap up and running…well, things were going to get uncomfortable fast.
>
> And then there’s this. Garu rolled the shimmering platinum sphere along the furry knuckles of his left hand. I somehow need to find your home. Although, why Klovin would leave you to a stranger….
>
> Garu bared his teeth and clucked his tongue. No matter. There were more important fish to fry, though Klovin would rap his knuckles with a switch for using the phrase. He effortlessly vaulted over the ancient instrument panel and into the pilot’s chair. He glanced around the cockpit, with its chaotic mix of dangling cables, inscrutable alien technology, and modern tech hacked in to interface. He sucked air through his teeth, then sighed. Moment of truth. Once I hit this and pull this thing away from the dock, things will get…interesting.
>
> Garu Jak grinned. He **loved** interesting. He threw the switch. The ship powered up, thrumming with power that set Garu’s teeth on edge. Then he blew the charges on the docking clamps, let out a whoop, and threw the ship into overdrive.
