# Azem & Elpis

> On a rainy night Azem and Elpis are jogging down an alley in a shady sector of Omnia. Both of them soak from top to bottom but on their face you can see it is not that that will stop them. Elpis' nose sniffing around following a strong scent. That led them to the entrance of a gambling house where their target is hidden. Azem grabs his rifle from his back and looks at Elpis then says “Let the hunt begin”.
