# Radovan Balmont

> Quietly meditating near a brazier of incense in his room, Radovan's face twitches and he grunts in discomfort. You call out his name, and his eyes shoot open in terror, his mouth suddenly agape in a silent scream for a second. He then closes his eyes, gathers his composure, stands up and faces you.
>
> "Yes, comrade? I presume der something that requires Radovan's assistance in dealing wit', yes?"
