# Slipstreams

It was discovered, many gigaseconds ago, that faster-than-light travel is possible by embracing the natural physical phenomena of the so-called *slipstreams*. These streams allow slip-capable vessels to travel along specific routes in order to arrive parsecs away in the matter of seconds from departure.

The many connected worlds throughout the galactic civilization have slip stations deployed to help stabilize the slipstream connections, typically centered perpendicular to the system's center and/or star(s).
