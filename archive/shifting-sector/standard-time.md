# Standard Time

The most commonly used system of Standard Time is based on the number of seconds elapsed.

While minutes and hours are still used colloquially, they are generally considered antiquated in the era of space travel. The more "metric" basis of seconds to powers of ten is often easier for conversions.

A *hectosecond (hs)* equals 100 seconds, and is approximately 1.67 minutes.

A *kilosecond (ks)* equals 1,000 seconds, and is approximately 16.67 minutes. An hour equals 36 ks, or 360 hs.

A *myriasecond (mys)* equals 10,000 seconds, is analogous to an hour, and is approximately 2.78 hours.

A *cycle (cy)* equals 100,000 seconds, and is approximately 27.78 hours. This is the most common basis for schedules, and is analogous to a "day".

Standard Time clocks usually divide a cycle into 1000 kilosecond blocks, and display the current seconds within the current block, in the format `XXX:XX`.

---

For larger time scales, the following are used:

- A *megasecond (Ms)* equals 1,000,000 (one million) seconds, or 10 cycles, is analogous to a week, and is approximately 11.57 Earth days, or 1.65 weeks
- A *gigasecond (Gs)* equals 1,000,000,000 (one billion) seconds, and is approximately 31.69 Earth years
