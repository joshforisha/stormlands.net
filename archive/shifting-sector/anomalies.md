# Anomalies

Within the past @gigasecond@, the sector has seen the emergence of strange rifts in space-time, colloquially called *anomalies*. These anomalies have appeared to some extent on every planetary body within the sector, with some planets being particularly plagued with them. Strangely, the anomalies have only occasionally (and temporarily) appeared away from a planet or massive body—they seem to appear near otherwise massive objects only.

## Radiation

Unique forms of radiation have been recorded emanating from the anomalies, and despite focused study being publically available, it has generally been deemed dangerous enough that proximity is discouraged.

## Incidents

Hundreds of reports of people affected by the anomalies have been reported, with effects ranging from acute sickness to outright disappearance.
