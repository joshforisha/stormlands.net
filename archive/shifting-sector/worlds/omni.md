# The Omni System

![Omni system](worlds/omni-system.jpg)

Slipstream Connections: @Sogantu@, @Xyankor@, @Zima@

**Technology**: (+3) Slipstream mastery
**Environment**: (-1) Survivable world
**Resources**: (+1) Rich

### *Omnistream Conglomerate Owns Almost Everything*

The @Omnistream Conglomerate@ is the overbearing corporate entity that manufactures nearly every product and consumer good in the system, if not the sector. Their presence is felt within every corner of the Omni system.

### *Rich in Slipstream Materials*

@Omnistream@ began its far-reaching business dealings within the system due to its abundance of materials needed for @slipstream@ technologies. They quickly took control of these resources, and capitalized on their usage with research and development of related technological marvels to gain early power within the sector.

---

## Tavi's Tavern

A small independently-operated station orbiting Omnia. @Tavi@ runs a bar and inn, sells fuel, and allows a small makeshift market to run out of the ship docks.

---

## Omnia

The highest-tech planet in the sector, with a single megatropolis city spanning its entire surface. @Omnistream@ is headquartered here.

### Monarch

A privately-owned scientific research group operating on Omnia. They receive funding from @Omnistream@ for their project work. When retrieving a holodisk from their lab in the Nimbus District, the Cachak crew discovered that @Dalibor@ was a member of the board of directors there, leading them to not trust his involvement or the mission they were actively on.

### Omnicore

Reactors around the planet core, and machinery serving power to the whole of Omnia. Countless engineers, technical personnel, and manual workers—including many constructs—work among the mechanical equipment here.

### Ominous Anomalous Cathedral

An ancient, strangely constructed building near the southern pole of Omnia. Recently, it has been turned into more of a tourist destination, as wealthy travelers visit the sector to witness the odd @anomalies@ that this cathedral seems to relate to in some way.
