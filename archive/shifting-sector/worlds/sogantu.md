# Sogantu

![Sogantu system](worlds/sogantu-system.jpg)

Slipstream Connections: @Paredes@, @Xyankor@, @Omni@, @Zima@, @Vakella@

**Technology**: (+0) Exploring the system
**Environment**: (+1) One garden world and several hostile worlds
**Resources**: (-2) Needs imports

---

## Twin Suns

Two dwarf stars orbit one another at the center of the system, one slightly larger and lighter in color.

---

## Alba

The innermost planet of the system, and surprisingly cold in temperature due to its overwhelmingly thick atmosphere. Not much is known of its dark and shrouded surface.

---

## Sogantu Prime

The idyllic garden world and prime popular destination of the system.

### *The Tourists Get the Good Stuff*

The very existence of Sogantu as a destination is owed to its abundant tourism industry. Non-tourists often feel the disparity in public treatment and limited availability of amenities.

### *Salvaged by Shareholders*

In spite of its lush environs, Sogantu-Prime does not have enough of its own resources to sustain a healthy population. A collection of shareholders—corporations and private wealthy investors from the core worlds—have injected money and goods to the planet (and system) that keep the gears turning.

### *Workers Unionizing Despite Mind Control*

The council(s) that run Merino on Sogantu-Prime utilize systems of mind control to keep their work force operating efficiently. There are rumors of methods some workers are beginning to use to subvert this, however, and whispers of unionizing have spread throughout the system.

### Merino

The central city of commercialized tourism on Sogantu Prime. In its heart is a large plaza with numerous high-end resorts.

### The Voiceless Chasm

*Podia of Manifestation*
A great lush canyon, with rocks that float mid-air between the two edges and a river that runs through it and a waterfall at the beginning of it, pouring down from the top into the canyon. People have a tradition of shouting out wishes into the chasm but the canyon never echoes back. It is an anomaly on the planet and something that is very interesting to @Radovan@ from a spiritual perspective.

---

## Tamertoni Shipyard

The @Tamertoni Electorate@ has its base of operations in orbit around Sogantu Prime.

---

### Rubina

A Mars-like companion of Sogantu Prime, Rubina once had similarly vibrant ecosystems
