# Paredes

**Technology**: (-2) Industrialization
**Environment**: (-1) Survivable world
**Resources**: (+1) Rich

Slipstream Connections: @Sogantu@

### *Inundated with Countless Anomalies*

This system is especially impacted by anomalies, most averaging the size of a car, that phase in and out of existence and tend to cause chaos in their vicinity.

---

## Paredes-a

The central white supergiant star, said to drive admirers insane with its pure white light.

---

## Paredes-b, "Char"

A tiny, derelict, scorched and unbearingly hot planet, orbits close to the star Paredes-a and discourages anything living to venture near.

---

## Reed Station

The main working-class hub; most comfortable (though barely) settlement in the system, Reed Station orbits the swamp planet Paredes-c, and acts as a hub for transporting the rich materials off-system.

---

## Paredes-c "Muskeg"

A mostly untamed swamp world; rich in useful minerals and building materials, Paredes-c is infamously difficult to navigate, but there are a handful of settlements that house workers and refineries. The popular saying "It ain't paradise, it's Paredes" originated here.

### Eternal Gate

A notorious anomaly; one of the most persistent anomalies within the sector, it has caused innumerable deaths and complications as workers on Paredes-c have attempted to work around the object over the years, far more often unsuccessfully.

---

## Paredes-d

A desolate ice giant, of partially-frozen liquids, containing numerous chemicals useful in industrial applications. Most in-system travel is made up of haulers bringing these chemicals to Paredes-c or Reed Station for processing.
