# Vakella

**Technology**: (-1) Atomic power
**Environment**: (-1) Survivable world
**Resources**: (+0) Sustainable

Slipstream Connections: @Sogantu@, @Zima@

### *All-out Civil War*

A widespread, violent civil war has been ongoing for years within the system, wherein the local government *Venusian Proletariat* has been holding off attacks from a revolution force recently growing in strength.

### *Sivni Crime Syndicate*

A very well-connected group of smugglers operates primarily from the system, taking advantage of the chaos of the civil war as a cover for their activities.

---

## Medeon-1

The first station of the system, Medeon-1 orbits its namesake planet Medeon, and serves as the data center and research platform for many surface and spacial experiments that can't be conducted under the crushing depths of planet below.

---

## Medeon

From a surface dweller's perspective, Medeon is a temperate–humid planet rife with storms and waves that can reach hundreds of feet in height, but from the perspective of those below, the angry seas are an underwater haven.

### Verus

The capitol city and sprawling subaquatic stronghold, headquarters to the *Venusian Proletariat* and home to millions of Angalan citizens. The capitol has remained largely untouched from the violence of the civil war, primarily due to the heavy-handed security protocols in place by the Proletariat.

---

## Stormvault Penitentiary

A maximum security prison station, currently close to overflowing with inmates. Many of the resident criminals here are relatively recent convicts associated with rebel violence related to the ongoing civil war.

---

## Firia

A gas giant and the biggest planet in the system, known for its faded purple hue when seen from the surface of Medeon.

### Murii

A small barren planetoid that orbits Firia. Scientists believe Murii was a rogue planet that was caught in Firia's gravitational pull. Researchers of Medeon have noticed that every three years, the planetoid moves slightly closer to Medeon; it's projected that within decades Murii will fall into and be engulfed by Firia's swirling surface.
