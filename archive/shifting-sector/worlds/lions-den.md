# Lion's Den

A mobile, slip-capable station of entertainment and trade. Its conglomerate construction houses the main *Arena*, is surrounded by the *Ustaari Bazaar*, and countless docking ports for ships. The Lion's Den jumps to a different system within the sector every few megaseconds, adapting to its host system and local population accordingly.

## The Arena

### *Slip-Capable Mobile Gladiator Arena*

Initially, the main attraction and original namesake of the Lion's Den. It's a giant spherical space used for any number of events: combat competitions, sports, expositions, public demonstrations, etc. The arena began as a gladiatoral zone, and gained enough popular interest early on to fund slip-capable engineering onto the structure.

## Ustaari Bazaar

### *The Myriad Market*

An amalgamation of marketplaces, shopping stalls, and public plazas that forms a huge shell of atmosphere-controlled structures surrounding the Arena and mechanical framework of the Den. Countless merchants and businesses rent space throughout the Bazaar, and operate as the Den travels around the sector.
