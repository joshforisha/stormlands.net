# Xyankor

**Technology**: (-2) Industrialization
**Environment**: (+1) One garden world and several hostile environments
**Resources**: (+1) Rich

Slipstream Connections: @Sogantu@, @Omni@

### *Rustic, Xenophobic Populace*

Hardworking, rustic people who take pride in their life and do not like outsiders poking into their business.

### *Frontier Justice Trumps Law*

The populace thoroughly believes in taking matters into their own hands. Militias, minutemen, and improvised judges are commonplace in dealing with legal troubles in the system.

### Creoberus

A famed space cryptid; it is said that on a dark night on any body in the system, as the wind dies down, you can feel the Creoberus watching you intently.

---

## Beoid

Second planet of the system. A farming world, providing much of the food to the sector.

---

## Gurala and Cattet

Valuable and rare gases are mined from the dual gas giants.
