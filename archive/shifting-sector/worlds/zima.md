# Zima

**Technology**: (-1) Atomic power
**Environment**: (-1) Survivable world
**Resources**: (+0) Sustainable

Slipstream Connections: @Sogantu@, @Omni@, @Vakella@

### *Held Back by Void Cult*

The @Emissaries of the Void@ have successfully halted many attempts to incorporate newer technologies into the system, citing their problematic links and reliance on @slipstreams@. Most citizens are kept in the dark about the cult's involvement and effectiveness in system-wide governance, though it is a growing concern.

### *With the Nukes, Came Winter*

The largest terrestrial planet, Niven, was home to a series of incidents involving nuclear weapons. The details of the events have been lost with time, but the ensuing nuclear winter has lasted for @gigaseconds@.

---

## Eros Station

An overcrowded hub, but considered the safest location in the system due to active security measures.

---

## Niven

A frozen terrestrial planet, desolate from nuclear conflict, but home to protected enclaves of citizens.
