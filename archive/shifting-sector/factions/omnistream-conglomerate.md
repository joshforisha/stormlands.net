# Omnistream Conglomerate

### *Omnipresent High-Tech Corporate Entity*

A huge corporate entity that owns myriad smaller companies and seemingly has business everywhere throughout the sector. Early in their history, Omnistream capitalized on the availability of rare slipstream materials, built a metropolis in the Omni system, and have made record profits consistently since.

A division within Omnistream also pioneered a unique approach to artificial intelligence, and created an automated workforce to aid the company efforts. A splinter group of the constructs, calling themselves the @Ersatz@, freed themselves from the corporate overlords.
