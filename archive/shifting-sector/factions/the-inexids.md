# The Inexids

### *Industrious Hive-mind Insectoids*

Communities of the mantid-like Inexids typically have a central figure, acting as a queen to focus the "hive". Inexids sacrifice their individual freedoms to better the social order they're a part of.
