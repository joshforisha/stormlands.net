# The Angala

### *Technologically Curious Aquatic Bipeds*

Angalans are widely known for their intensive technological prowess, embracing a natural curiosity in all things mechanical in order to invent and creatively solve scientifically advanced problems.

### *Ships Don't Sink in Vakella*

Due to their resourceful and thorough approach to engineering, Angalan crafts are renowned for their durability.

### **At Home in the Dark**

Because of their excellent low-light vision, Angala gain +2 when creating a visual advantage in darkness.
