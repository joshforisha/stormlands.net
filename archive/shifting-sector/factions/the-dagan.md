# The Dagan

### *Living Teddy Bears*

As diminutive thick-furred mammals with outsized facial features, it's inevitable that the Dagan draw comparisons to children's stuffed animal toys.

### *Easily Overlooked*

Dagans are too closely associated with innocent, easy-going, fun-loving creatures to generally draw much concern from even paranoid individuals. They are, however, seen as incapable of many otherwise demanding tasks.
