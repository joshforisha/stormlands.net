# Solarians

### *Cloistered Order of Sun Worshippers*

## Ranks

- **Novice**: Common follower of The Way; sometimes referred to as brother or sister by follow Solarians
- **Guru** or **Sage**: Priest and teacher of The Way; the particular term used varies by chapter, but functionally the same
- **Abbot**: The leaders of the Solarians in the sector who each lead a planet's or system's group of Solarians
- **Sol Saint**: Position of leader of the Solarians; Unclaimed in the last 20 years since the death of the prior Sol Saint while believers await the reincarnation of the Sol Saint to reveal him or herself

## The Solarian Code

> We were born from the Stars and to the Stars we shall return.
> Warm the Souls of Others as the Stars Warm Our Worlds.
> Illuminate the Shadows of the Unknown and Make them Known to All.
> Do as the Stars and Drift Stoically Through Existence.
> Be a Beacon; Shine Brightly so others may follow the Path.
