# Tamertoni Electorate

### *Imperial Force Looming Over the Sector*

The heavy-handed galactic naval power, headquartered within the Core Worlds. They are wildly unpopular in the outer rim, including Alkonost-387, and mostly keep out of local matters—though that seems to be due to lack of interest and not out of respect.

Nevertheless, the Electorate has a presence in the sector, and regularly transports interested tourists to and from the systems here.

### *Outsourced Security Concerns*
