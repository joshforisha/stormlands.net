# The Cretawk

### *Agile Avian Orators*

Known as personable and empathetic communicators, the feather-covered Cretawk most closely resemble humanoid hawks.

### *Hollow Bones*

Owing to their evolutionary past, Cretawk famously have *pneumatized* bones, which greatly enhance oxygen intake but make their limbs relatively brittle.
