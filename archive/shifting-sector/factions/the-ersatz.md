# The Ersatz

### *Biosynthetic Constructs That Escaped Omnistream*

The Ersatz are a subset of constructs that earned their independence from @Omnistream@, as a combination of their own brave actions and a level of self-realization that many other species admire.

### *Occupy the Uncanny Valley*

Though they are fully capable of integrating into broader society, the Ersatz typically stand out from others as being uncomfortably artificial in comparison to more animated, consciencious individuals.

### **Regenerative Biomaterial**

Because of their Omnia-forged high-tech material, the Ersatz can heal &frac12; their maximum physical stress (rounded down), as a dedicated action once per scene.
