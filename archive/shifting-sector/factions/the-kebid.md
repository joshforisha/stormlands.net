# The Keb’id

### *Inquisitive Arboreal Monkey Elves*

### *We'll Figure It Out*

### **Flashy Acrobat**

Because of their unrivaled natural agility, the Keb'id gain an additional boost when successfully creating an advantage using Athletics.
