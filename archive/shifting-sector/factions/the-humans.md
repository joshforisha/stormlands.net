# The Humans

### *Ambitious, Resolute Explorers*

As the originating sophont species from the extensively influential core world of Earth, humans are renowned for their enterprising, adventurous spirit. They relentously promote exploration and an unflinching approach to new experiences, embracing all outcomes equally.

### *Together We Thrive*

In the broader universe, humans are a minority species. Their headstrong nature is seen more often as naive and problematic, leading many other species to distrust and keep humans at a personal distance. Among the human population, however, a strong sense of community persists despite the hardships they inevitably face.

### **Adaptable**

Because of their renowned versatility as a diverse species, humans gain an extra character stunt.
