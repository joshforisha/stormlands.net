import { makeNoise2D, makeNoise3D } from 'fast-simplex-noise'
import { makeRectangle, makeSphereSurface } from 'fractal-noise'

function rand(min, max) {
  return min + Math.random() * (max - min)
}

for (const canvas of document.querySelectorAll('canvas')) {
  const frequency = parseFloat(canvas.getAttribute('data-frequency'))
  const height = parseInt(canvas.getAttribute('height'))
  const width = parseInt(canvas.getAttribute('width'))

  const ctx = canvas.getContext('2d')
  const imageData = ctx.createImageData(width, height)

  const p = makeRectangle(width, height, makeNoise2D(), {
    frequency: frequency * rand(0.9, 1.1),
    octaves: 8,
    scale: (v) => 128 * (v + 1)
  })

  const q = makeRectangle(width, height, makeNoise2D(), {
    frequency: frequency * rand(0.9, 1.1),
    octaves: 2,
    scale: (v) => 128 * (v + 1)
  })

  for (let x = 0; x < width; x++) {
    for (let y = 0; y < height; y++) {
      const i = (x + y * width) * 4
      const value = (p[x][y] + q[x][y]) * 0.5
      imageData.data[i] = value
      imageData.data[i + 1] = value
      imageData.data[i + 2] = value
      imageData.data[i + 3] = 255
    }
  }
  ctx.putImageData(imageData, 0, 0)
}
