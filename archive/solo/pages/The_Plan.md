# The Plan

## Decide Difficulty

| Difficulty Rating | Descriptions | Required Roll |
| ----------------- | ------------ | ------------- |
| Shaky | Quite a few things could go wrong | 10+ |
| Solid | Most eventualities catered for | 8+ |
| Fool proof | Almost nothing can go wrong | 6+ |

## Decide Danger Level

| Danger Level | Description |
| ------------ | ----------- |
| Safe | Little if any physical danger exists |
| Dangerous | There is chance of physical injury—even death—if things go wrong |

## Roll 2D to Resolve

- DM+1 for a PC with a significant skill
- DM+1 for use of a crucial piece of kit or a great asset
- DM-1 for a PC unsuited to the mission

## Roll 2D for Consequences

### Under: Bad Consequence

- 1D+6 if Plan was *Safe*
- 2D otherwise
- DM+2 if the Plan was a success

| Roll | Bad Consequence |
| ---- | --------------- |
| 2–4 | Death |
| 5 | Serious injury |
| 6 | Minor injury |
| 7 | Trapped, lost or delayed |
| 8 | Part of the mission was failed or incriminating evidence left behind |
| 9 | Damage to a useful or valuable piece of kit |
| 10 | Seriously upset or antagonize an NPC |
| 11–12 | The task takes four times longer than planned |

### Equal or Over: Good Consequence

- DM+2 if the Plan was a success

| 2D | Good Consequence |
| -- | ---------------- |
| 2–5 | The task took half the expected time
| 6 | Tracks covered, no evidence left behind |
| 7–8 | Hear a rumour or discover a valuable piece of information |
| 9–10 | Find a useful or valuable piece of kit |
| 11–12 | Make a Contact of friend |
