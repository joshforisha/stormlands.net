# Random Rolls

## Tell Me, D6

| 1D | Person |
| -- | ------ |
| 1 | Bad, bad, bad |
| 2 | Untrustworthy; if he can double-cross he might |
| 3 | OK but quirky |
| 4 | OK, or so he seems |
| 5 | Decent, don't worry |
| 6 | Honest, good, dependable |

| 1D | Situation |
| -- | --------- |
| 1 | The worst possible thing happens |
| 2 | Bad stuff happens, but it's not yet catastrophic |
| 3 | OK for now |
| 4 | Ok for now |
| 5 | We're good |
| 6 | The best possible result |

## Colourful Locals

| D66 | Individuals | | D66 | Individuals |
| --- | ----------- | | --- | ----------- |
| 11 | Adventurers | | 41 | Political Dissident |
| 12 | Alien Starship Crew | | 42 | Potential Patron |
| 13 | Ambushing Brigands | | 43 | Public Demonstration |
| 14 | Bandits | | 44 | Religious Pilgrims |
| 15 | Beggars | | 45 | Reporters |
| 16 | Belters | | 46 | Researchers |
| 21 | Drunken Crew | | 51 | Riotous Mob |
| 22 | Fugitives | | 52 | Security Troops |
| 23 | Government Officials | | 53 | Servant Robots |
| 24 | Guards | | 54 | Soldiers on Patrol |
| 25 | Hunters and Guides | | 55 | Street Vendors |
| 26 | Law Enforcers on Patrol | | 56 | Technicians |
| 31 | Local Performers | | 61 | Thugs |
| 32 | Maintenance Crew | | 62 | Tourists |
| 33 | Merchants | | 63 | Traders |
| 34 | Military Personnel on Leave | | 64 | Vigilantes |
| 35 | Noble with Retinue | | 65 | Workers |
| 36 | Peasants | | 66 | Player’s Choice |

## NPC Reactions

| 2D | Reaction | |
| -- | -------- | |
| 2–3 | Hostile | NPC will actively work against the PCs |
| 4–5 | Guarded | NPC does not trust the PCs; will show no favours |
| 6–8 | Neutral | Treats PCs like everyone else; unconcerned |
| 9–10 | Friendly | There is some point of connection or common interest; may show some favour to the PCs |
| 11–12 | Allied | NPC finds a common cause with PCs and shows favour; assists or helps further the cause of the group |
