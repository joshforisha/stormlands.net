import * as Airlock from './airlock.js'
import * as Arrow from './arrow.js'
import * as Door from './door.js'
import * as Ellipse from './ellipse.js'
import * as Grid from './grid.js'
import * as Line from './line.js'
import * as Mesh from './mesh.js'
import * as Room from './room.js'
import { ObjectType } from './types.js'
import { align, clamp } from './utils.js'

const clearAllButton = document.getElementById('ClearAll')
const exportButton = document.getElementById('Export')
const importButton = document.getElementById('Import')
const saveButton = document.getElementById('Save')
const toggleGridButton = document.getElementById('ToggleGrid')
const zoomInButton = document.getElementById('ZoomIn')
const zoomOutButton = document.getElementById('ZoomOut')
const zoomResetButton = document.getElementById('ZoomReset')

const map = document.getElementById('Map')
const [mapRooms, mapObjects, mapLines] = map.children

const placementButtons = [
  ...document.querySelectorAll('button.placement[data-object]')
]

let mapScale = 1

const dragSource = {
  action: null,
  started: false
}

const defaultState = {
  objects: []
}

let state = JSON.parse(window.localStorage.getItem('state')) || defaultState

// Functions -------------------------------------------------------------------

function addObject (createFn, x, y) {
  updateState({
    objects: state.objects.concat(
      createFn({ id: Date.now().toString(), x, y })
    )
  })
}

function affect (data, changes) {
  return typeof changes === 'function' ? changes(data) : changes
}

function createGroup (object) {
  return objectModule(object.type).createGroup(object)
}

function insert (objectType, shape) {
  if (objectType === ObjectType.Room) mapRooms.appendChild(shape)
  else if (objectType === ObjectType.Line) mapLines.appendChild(shape)
  else mapObjects.appendChild(shape)
  return shape
}

const maxObjectX = () => Math.max(...state.objects.map(o => o.x + o.width))
const maxObjectY = () => Math.max(...state.objects.map(o => o.y + o.height))
const minObjectX = () => Math.min(...state.objects.map(o => o.x))
const minObjectY = () => Math.min(...state.objects.map(o => o.y))

function objectModule (objectType) {
  switch (objectType) {
    case ObjectType.Airlock: return Airlock
    case ObjectType.Arrow: return Arrow
    case ObjectType.Door: return Door
    case ObjectType.Ellipse: return Ellipse
    case ObjectType.Grid: return Grid
    case ObjectType.Line: return Line
    case ObjectType.Mesh: return Mesh
    case ObjectType.Room: return Room
  }
  console.warn(`No object module found for "${objectType}"`)
}

function placeObject (pressEvent) {
  pressEvent.stopPropagation()
  window.onclick = clickEvent => {
    window.onclick = null
    if (clickEvent.target === map) {
      addObject(
        objectModule(pressEvent.target.getAttribute('data-object')).create,
        align(clickEvent.pageX / mapScale),
        align(clickEvent.pageY / mapScale)
      )
    }
    map.classList.remove('-placing')
    placementButtons.forEach(button => {
      button.removeAttribute('disabled')
    })
  }
  map.classList.add('-placing')
  placementButtons.forEach(button => {
    if (button !== pressEvent.target) button.setAttribute('disabled', '')
  })
}

function removeObject (objectId) {
  document.getElementById(objectId).remove()
  updateState({ objects: state.objects.filter(o => o.id !== objectId) })
}

function render () {
  state.objects.forEach(object => {
    if (!('id' in object)) return console.error('Object has no ID', object)
    const group = document.getElementById(object.id) ||
      insert(object.type, createGroup(object))
    updateGroup(group, object)
  })
}

function resizeMap () {
  const height = Math.max(window.innerHeight - 4, mapScale * (maxObjectY() + 350))
  const width = Math.max(window.innerWidth - 15, mapScale * (maxObjectX() + 350))
  map.setAttribute('height', height)
  map.setAttribute('width', width)
  map.setAttribute('viewbox', `0 0 ${width} ${height}`)
}

function saveSVG () {
  zoomMap(1.0)

  const canvas = document.createElement('canvas')
  const image = document.createElement('img')
  const link = document.createElement('a')
  const xml = new window.XMLSerializer().serializeToString(map)

  const minX = minObjectX()
  const offsetX = 70 - minX
  const width = 70 * Math.ceil((maxObjectX() + offsetX + 70) / 70)

  const minY = minObjectY()
  const offsetY = 70 - minY
  const height = 70 * Math.ceil((maxObjectY() + offsetY + 70) / 70)

  canvas.setAttribute('height', height)
  canvas.setAttribute('width', width)
  image.onload = () => {
    canvas.getContext('2d').drawImage(image, offsetX, offsetY)
    link.href = canvas.toDataURL()
    link.download = `map-${(new Date()).toJSON().substring(0, 10)}`
    document.body.appendChild(link)
    link.click()
    document.body.removeChild(link)
  }
  image.src = 'data:image/svg+xml;base64,' + window.btoa(xml)
}

function setState (newState) {
  state = newState
  resizeMap()
  window.localStorage.setItem('state', JSON.stringify(state))
  if (state.objects.length > 0) {
    clearAllButton.removeAttribute('disabled')
    exportButton.removeAttribute('disabled')
    saveButton.removeAttribute('disabled')
  } else {
    clearAllButton.setAttribute('disabled', '')
    exportButton.setAttribute('disabled', '')
    saveButton.setAttribute('disabled', '')
  }
  render()
}

function startDrag (action) {
  dragSource.action = action
  dragSource.started = false
}

function startMove (startObject) {
  return startEvent => {
    const startX = (startEvent.pageX / mapScale) - startObject.x
    const startY = (startEvent.pageY / mapScale) - startObject.y
    startDrag(event => {
      updateObject(startObject.id, object => ({
        x: align(clamp(0, (event.pageX / mapScale) - startX, (map.clientWidth / mapScale) - object.width)),
        y: align(clamp(0, (event.pageY / mapScale) - startY, (map.clientHeight / mapScale) - object.height))
      }))
    })
  }
}

function stopDrag () {
  dragSource.action = null
  dragSource.started = false
}

function updateGroup (group, object) {
  const handlers = {
    confirmDelete: name => {
      return event => {
        event.stopPropagation()
        if (window.confirm(`Are you sure you want to delete ${name}?`)) {
          removeObject(object.id)
        }
      }
    },
    mapScale,
    onUpdate: changes => updateObject(object.id, changes),
    startDrag,
    startMove,
    whenClicked: fn => () => !dragSource.started ? fn() : null
  }
  return objectModule(object.type).updateGroup(group, object, handlers)
}

function updateObject (objectId, changes) {
  updateState({
    objects: state.objects.map(object => object.id === objectId
      ? { ...object, ...affect(object, changes) }
      : object)
  })
}

function updateState (changes) {
  setState({ ...state, ...changes })
}

function zoomMap (newFactor) {
  mapScale = newFactor;
  [...map.children].forEach(g => g.setAttribute('transform', `scale(${mapScale})`))
  map.style['background-size'] = `${newFactor * 7}px`
  resizeMap()
  if (mapScale < 1.1 && mapScale > 0.9) {
    zoomResetButton.setAttribute('disabled', '')
  } else zoomResetButton.removeAttribute('disabled')
  if (mapScale < 0.5) zoomOutButton.setAttribute('disabled', '')
  else zoomOutButton.removeAttribute('disabled')
  if (mapScale > 1.5) zoomInButton.setAttribute('disabled', '')
  else zoomInButton.removeAttribute('disabled')
  render()
}

// Event handling --------------------------------------------------------------

window.onkeydown = event => {
  const button = document.querySelector(`button[data-key="${event.key}"]`)
  if (button) button.click()
}

window.onpointerup = stopDrag
window.onresize = resizeMap

map.ondblclick = event => {
  addObject(Room.create, align(event.pageX / mapScale), align(event.pageY / mapScale))
}
map.onpointerleave = stopDrag
map.onpointermove = event => {
  if (dragSource.action !== null) {
    dragSource.started = true
    dragSource.action(event, dragSource.x, dragSource.y)
  }
}

placementButtons.forEach(button => {
  button.onclick = placeObject
})

let showingGrid = false
toggleGridButton.onclick = () => {
  map.classList.toggle('-grid')
  toggleGridButton.textContent = showingGrid ? 'G +' : 'G -'
  showingGrid = !showingGrid
}

zoomOutButton.onclick = () => {
  zoomMap(parseFloat((mapScale - 0.2).toFixed(1)))
}
zoomResetButton.onclick = () => {
  zoomMap(1.0)
}
zoomInButton.onclick = () => {
  zoomMap(parseFloat((mapScale + 0.2).toFixed(1)))
}

importButton.onclick = () => {
  const hash = window.prompt('Paste hash:')
  if (hash) {
    let newState
    try {
      newState = JSON.parse(window.atob(hash))
    } catch (error) {
      return window.alert(error)
    }
    setState(newState)
  }
}
exportButton.onclick = () => {
  navigator.clipboard.writeText(window.btoa(JSON.stringify(state)))
  window.alert('Copied to clipboard!')
}

clearAllButton.onclick = () => {
  if (window.confirm('Are you sure you want to delete all objects?')) {
    [...map.children].forEach(child => {
      child.innerHTML = ''
    })
    setState(defaultState)
  }
}

saveButton.onclick = saveSVG

// Initialization --------------------------------------------------------------

if (state.objects.length > 0) {
  clearAllButton.removeAttribute('disabled')
  exportButton.removeAttribute('disabled')
}

resizeMap()
render()
