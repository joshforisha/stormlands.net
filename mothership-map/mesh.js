import { ObjectType } from './types.js'
import { createShape, initialize } from './utils.js'

export function create ({ x, y, ...props }) {
  return {
    ...props,
    height: 28,
    type: ObjectType.Mesh,
    width: 28,
    x: x - 14,
    y: y - 14
  }
}

function createBlock (x, y) {
  return createShape('rect', { class: 'block', fill: '#eee8', height: 3.5, width: 3.5, x, y })
}

export function createGroup (mesh) {
  return createShape('g', { class: 'mesh', id: mesh.id }, [
    createShape('rect', { class: 'edge', fill: '#1118', height: mesh.height, width: mesh.width }),
    ...initialize(4, pair => [
      initialize(4, column => createBlock(7 * column, 7 * pair)),
      initialize(4, column => createBlock(3.5 + 7 * column, 3.5 + 7 * pair))
    ].flat()).flat()
  ])
}

export function updateGroup (group, mesh, { confirmDelete, startMove }) {
  group.setAttribute('transform', `translate(${mesh.x} ${mesh.y})`)
  group.onpointerdown = startMove(mesh)
  group.ondblclick = confirmDelete('this mesh')
}
