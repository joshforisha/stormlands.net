export const Direction = {
  Up: 'up',
  Right: 'right',
  Down: 'down',
  Left: 'left'
}

export const ObjectType = {
  Airlock: 'airlock',
  Arrow: 'arrow',
  Door: 'door',
  Ellipse: 'ellipse',
  Grid: 'grid',
  Line: 'line',
  Mesh: 'mesh',
  Room: 'room'
}

export const Orientation = {
  Horizontal: 'horizontal',
  Vertical: 'vertical'
}

export function nextDirection (direction) {
  switch (direction) {
    case Direction.Up: return Direction.Right
    case Direction.Right: return Direction.Down
    case Direction.Down: return Direction.Left
    case Direction.Left: return Direction.Up
  }
}

export function nextOrientation (orientation) {
  if (orientation === Orientation.Horizontal) return Orientation.Vertical
  return Orientation.Horizontal
}
