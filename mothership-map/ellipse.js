import { ObjectType } from './types.js'
import { align, clamp, createShape } from './utils.js'

const minSize = 21
const maxSize = 700

export function create ({ x, y, ...props }) {
  return {
    ...props,
    height: 28,
    type: ObjectType.Ellipse,
    width: 28,
    x: x - 14,
    y: y - 14
  }
}

export function createGroup (ellipse) {
  return createShape('g', { class: 'ellipse', id: ellipse.id }, [
    createShape('rect', { class: 'edge', style: 'display: none' }),
    createShape('ellipse', { class: 'shape', fill: '#222c', stroke: '#eee' }),
    createShape('path', {
      class: 'sizer',
      d: 'm 16,0 L 16,16 L 0,16 z',
      fill: 'transparent',
      style: 'display: none'
    })
  ])
}

export function updateGroup (group, ellipse, { confirmDelete, mapScale, onUpdate, startDrag, startMove }) {
  const [edge, shape, sizer] = group.children

  group.setAttribute('transform', `translate(${ellipse.x} ${ellipse.y})`)

  edge.setAttribute('width', ellipse.width)
  edge.setAttribute('height', ellipse.height)

  shape.setAttribute('cx', ellipse.width / 2)
  shape.setAttribute('cy', ellipse.height / 2)
  shape.setAttribute('rx', ellipse.width / 2)
  shape.setAttribute('ry', ellipse.height / 2)
  shape.ondblclick = confirmDelete('this ellipse')
  shape.onpointerdown = startMove(ellipse)

  sizer.setAttribute('transform',
    `translate(${ellipse.width - 15.5} ${ellipse.height - 15.5})`)
  sizer.onpointerdown = startEvent => {
    const startX = (startEvent.pageX / mapScale) - ellipse.width
    const startY = (startEvent.pageY / mapScale) - ellipse.height
    startDrag(event => {
      onUpdate({
        height: align(clamp(minSize, (event.pageY / mapScale) - startY, maxSize)),
        width: align(clamp(minSize, (event.pageX / mapScale) - startX, maxSize))
      })
    })
  }
}
