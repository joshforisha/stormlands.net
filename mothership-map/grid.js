import { ObjectType } from './types.js'
import { createShape } from './utils.js'

export function create ({ x, y, ...props }) {
  return {
    ...props,
    height: 28,
    type: ObjectType.Grid,
    width: 28,
    x: x - 14,
    y: y - 14
  }
}

export function createGroup (grid) {
  return createShape('g', { class: 'grid', id: grid.id }, [
    createShape('rect', {
      fill: 'transparent',
      height: grid.height,
      width: grid.width
    }),
    createShape('rect', { class: 'block', fill: '#eeec', x: 2, y: 2, height: 11, width: 11 }),
    createShape('rect', { class: 'block', fill: '#eeec', x: 15, y: 2, height: 11, width: 11 }),
    createShape('rect', { class: 'block', fill: '#eeec', x: 15, y: 15, height: 11, width: 11 }),
    createShape('rect', { class: 'block', fill: '#eeec', x: 2, y: 15, height: 11, width: 11 })
  ])
}

export function updateGroup (group, grid, { confirmDelete, onUpdate, startMove }) {
  group.setAttribute('transform', `translate(${grid.x} ${grid.y})`)
  group.ondblclick = confirmDelete('this grid')
  group.onpointerdown = startMove(grid)
}
