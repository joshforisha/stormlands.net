import { ObjectType } from './types.js'
import { align, createShape, diff } from './utils.js'

const Style = {
  Solid: '',
  ShortDashed: '7',
  MediumDashed: '14',
  LongDashed: '28'
}

export function create ({ x, y, ...props }) {
  return {
    ...props,
    destination: { x: 35, y: 35 },
    height: 35,
    origin: { x: 0, y: 0 },
    style: Style.Solid,
    type: ObjectType.Line,
    width: 35,
    x,
    y
  }
}

export function createGroup (line) {
  return createShape('g', { class: 'line', id: line.id }, [
    createShape('path', { class: 'bg', stroke: 'transparent', 'stroke-width': 24, style: 'display: none' }),
    createShape('path', { class: 'path', stroke: '#eee', 'stroke-width': 1.5 }),
    createShape('circle', { class: 'anchor', r: 9, style: 'display: none' }),
    createShape('circle', { class: 'anchor', r: 9, style: 'display: none' })
  ])
}

function nextStyle (currentStyle) {
  switch (currentStyle) {
    case Style.Solid:
      return Style.ShortDashed
    case Style.ShortDashed:
      return Style.MediumDashed
    case Style.MediumDashed:
      return Style.LongDashed
    case Style.LongDashed:
    default:
      return Style.Solid
  }
}

export function updateGroup (group, line, { confirmDelete, mapScale, onUpdate, startDrag, startMove, whenClicked }) {
  const [bg, path, originAnchor, destinationAnchor] = group.children
  group.setAttribute('transform', `translate(${line.x} ${line.y})`)
  const d = `m ${line.origin.x},${line.origin.y} L ${line.destination.x},${line.destination.y}`
  bg.setAttribute('d', d)
  path.setAttribute('d', d)
  if (line.style === Style.Solid) path.removeAttribute('stroke-dasharray')
  else path.setAttribute('stroke-dasharray', line.style)
  originAnchor.setAttribute('cx', line.origin.x)
  originAnchor.setAttribute('cy', line.origin.y)
  originAnchor.onpointerdown = startEvent => {
    const startX = (startEvent.pageX / mapScale) - line.origin.x
    const startY = (startEvent.pageY / mapScale) - line.origin.y
    startDrag(event => {
      const x = align((event.pageX / mapScale) - startX)
      const y = align((event.pageY / mapScale) - startY)
      onUpdate({
        origin: { x, y },
        height: diff(y, line.destination.y),
        width: diff(x, line.destination.x)
      })
    })
  }
  destinationAnchor.setAttribute('cx', line.destination.x)
  destinationAnchor.setAttribute('cy', line.destination.y)
  destinationAnchor.onpointerdown = startEvent => {
    const startX = (startEvent.pageX / mapScale) - line.destination.x
    const startY = (startEvent.pageY / mapScale) - line.destination.y
    startDrag(event => {
      const x = align((event.pageX / mapScale) - startX)
      const y = align((event.pageY / mapScale) - startY)
      onUpdate({
        destination: { x, y },
        height: diff(y, line.origin.y),
        width: diff(x, line.origin.x)
      })
    })
  }
  bg.ondblclick = confirmDelete('this line')
  bg.onpointerdown = startMove(line)
  bg.onpointerup = whenClicked(() => {
    onUpdate({ style: nextStyle(line.style) })
  })
}
