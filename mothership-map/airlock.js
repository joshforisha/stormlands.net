import { ObjectType } from './types.js'
import { createShape } from './utils.js'

export function create ({ x, y, ...props }) {
  return {
    ...props,
    height: 28,
    type: ObjectType.Airlock,
    width: 28,
    x: x - 14,
    y: y - 14
  }
}

export function createGroup (airlock) {
  return createShape('g', { class: 'airlock', id: airlock.id }, [
    createShape('rect', {
      class: 'edge',
      fill: '#eee',
      height: airlock.height,
      width: airlock.width
    }),
    createShape('circle', { class: 'aperture', cx: 14, cy: 14, fill: '#222', r: 13 })
  ])
}

export function updateGroup (group, airlock, { confirmDelete, startMove }) {
  group.setAttribute('transform', `translate(${airlock.x} ${airlock.y})`)
  group.ondblclick = confirmDelete('this airlock')
  group.onpointerdown = startMove(airlock)
}
