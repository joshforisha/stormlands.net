import { ObjectType } from './types.js'
import { align, clamp, createShape } from './utils.js'

const maxSize = 3500
const minSize = 84

export function create ({ x, y, ...props }) {
  return {
    color: '#222',
    name: '',
    ...props,
    height: 140,
    type: ObjectType.Room,
    width: 280,
    x: x - 140,
    y: y - 70
  }
}

export function createGroup (room) {
  return createShape('g', { class: 'room', id: room.id }, [
    createShape('rect', { class: 'edge', fill: room.color, stroke: '#eee' }),
    createShape('rect', { class: 'header', fill: '#fff2', height: 28 }),
    createShape('text', { fill: '#eee', style: 'font-family: "Helvetica Neue", sans-serif; font-size: 16px; text-transform: uppercase', x: 6, y: 20 }),
    createShape('path', { class: 'sizer', d: 'm 16,0 L 16,16 L 0,16 z', fill: 'transparent', style: 'display: none' }),
    createShape('g', { class: 'color-select', style: 'display: none' }, [
      createShape('circle', { class: 'current', fill: room.color, r: 12 }),
      createShape('g', { class: 'colors', transform: 'translate(-44 10)' }, [
        createShape('rect', { class: 'box', fill: '#666', height: 60, rx: 4, width: 88 }),
        createShape('circle', { class: 'color', cx: 16, cy: 16, fill: '#322', r: 13 }),
        createShape('circle', { class: 'color', cx: 44, cy: 16, fill: '#332', r: 13 }),
        createShape('circle', { class: 'color', cx: 72, cy: 16, fill: '#232', r: 13 }),
        createShape('circle', { class: 'color', cx: 16, cy: 44, fill: '#233', r: 13 }),
        createShape('circle', { class: 'color', cx: 44, cy: 44, fill: '#223', r: 13 }),
        createShape('circle', { class: 'color', cx: 72, cy: 44, fill: '#323', r: 13 })
      ])
    ])
  ])
}

export function updateGroup (group, room, { confirmDelete, mapScale, onUpdate, startDrag, startMove }) {
  const [edge, header, text, sizer, colorSelect] = group.children

  group.setAttribute('transform', `translate(${room.x} ${room.y})`)
  edge.setAttribute('fill', room.color)
  edge.setAttribute('height', room.height)
  edge.setAttribute('width', room.width)
  header.setAttribute('width', room.width)
  colorSelect.setAttribute('transform', `translate(${room.width - 14} 14)`)
  colorSelect.children[0].setAttribute('fill', room.color)
  text.textContent = room.name

  if (room.name.trim().length < 1) header.style.opacity = 0
  else header.style.opacity = 1
  colorSelect.querySelectorAll('.color').forEach(color => {
    color.onclick = event => {
      event.stopPropagation()
      if (room.color === color.getAttribute('fill')) {
        onUpdate({ color: '#222' })
      } else {
        onUpdate({ color: color.getAttribute('fill') })
      }
    }
  })

  edge.onpointerdown = startMove(room)
  header.onclick = () => {
    const newName = window.prompt('New room name:', room.name)
    if (newName !== null) onUpdate({ name: newName })
  }
  sizer.setAttribute('transform',
    `translate(${room.width - 15.5} ${room.height - 15.5})`)
  sizer.onpointerdown = startEvent => {
    const startX = (startEvent.pageX / mapScale) - room.width
    const startY = (startEvent.pageY / mapScale) - room.height
    startDrag(event => {
      onUpdate({
        height: align(clamp(minSize, (event.pageY / mapScale) - startY, maxSize)),
        width: align(clamp(minSize, (event.pageX / mapScale) - startX, maxSize))
      })
    })
  }
  group.ondblclick = confirmDelete(room.name.length > 0 ? `room "${room.name}"` : 'this room')
}
