export function align (x, z = 7) {
  return z * Math.round(x / z)
}

export function clamp (min, x, max) {
  return Math.max(min, Math.min(max, x))
}

export function createShape (elementType, attrs = {}, children = []) {
  const shape = document.createElementNS('http://www.w3.org/2000/svg', elementType)
  Object.entries(attrs).forEach(([key, value]) => {
    shape.setAttribute(key, value)
  })
  children.forEach(child => {
    shape.appendChild(child)
  })
  return shape
}

export function diff (x, y) {
  return Math.abs(x - y)
}

export function initialize (count, construct) {
  const array = []
  for (let i = 0; i < count; i++) array.push(construct(i))
  return array
}
