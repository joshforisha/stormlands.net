import { ObjectType, Orientation, nextOrientation } from './types.js'
import { createShape } from './utils.js'

export function create ({ x, y, ...props }) {
  return {
    orientation: Orientation.Vertical,
    ...props,
    height: 28,
    type: ObjectType.Door,
    width: 14,
    x: x - 7,
    y: y - 14
  }
}

export function createGroup (door) {
  return createShape('g', { class: 'door', id: door.id }, [
    createShape('rect', {
      class: 'edge',
      fill: '#eee',
      height: door.height,
      width: door.width
    })
  ])
}

export function updateGroup (group, door, { confirmDelete, onUpdate, startMove, whenClicked }) {
  const rotation = door.orientation === Orientation.Vertical ? 0 : 90
  group.setAttribute('transform', `translate(${door.x} ${door.y}) rotate(${rotation})`)

  group.ondblclick = confirmDelete('this door')
  group.onpointerdown = startMove(door)
  group.onpointerup = whenClicked(() => {
    onUpdate({
      orientation: nextOrientation(door.orientation),
      x: door.orientation === Orientation.Vertical ? door.x + 21 : door.x - 21,
      y: door.orientation === Orientation.Vertical ? door.y + 7 : door.y - 7
    })
  })
}
