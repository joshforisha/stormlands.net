import { Direction, ObjectType, nextDirection } from './types.js'
import { createShape } from './utils.js'

export function create ({ x, y, ...props }) {
  return {
    direction: Direction.Up,
    ...props,
    height: 28,
    type: ObjectType.Arrow,
    width: 28,
    x: x - 14,
    y: y - 14
  }
}

export function createGroup (arrow) {
  return createShape('g', { class: 'arrow', id: arrow.id }, [
    createShape('rect', {
      class: 'edge',
      fill: '#eee',
      height: arrow.height,
      width: arrow.width
    }),
    createShape('path', { fill: '#222', d: 'M 0,-14 L 14,0 L 0,14 L 0,6 L -14,6 L -14,-6 L 0,-6 z' })
  ])
}

export function updateGroup (group, arrow, { confirmDelete, onUpdate, startMove, whenClicked }) {
  const [, path] = group.children

  group.setAttribute('transform', `translate(${arrow.x} ${arrow.y})`)

  let rotation = 0
  if (arrow.direction === Direction.Down) rotation = 90
  else if (arrow.direction === Direction.Left) rotation = 180
  else if (arrow.direction === Direction.Up) rotation = 270
  path.setAttribute('transform', `translate(14 14) rotate(${rotation})`)

  group.ondblclick = confirmDelete('this arrow')
  group.onpointerdown = startMove(arrow)
  group.onpointerup = whenClicked(() => {
    onUpdate({ direction: nextDirection(arrow.direction) })
  })
}
