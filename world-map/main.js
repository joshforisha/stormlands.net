import * as Sketch from 'webgl-sketch'
import fragmentShaderSource from './shaders/terrain.glsl'
import vertexShaderSource from './shaders/basic-vertex.glsl'

const generateButton = document.getElementById('Generate')
const hydrographicsInput = document.getElementById('Hydrographics')
const populationInput = document.getElementById('Population')
const sizeInput = document.getElementById('Size')
const temperatureInput = document.getElementById('Temperature')

const roff = () => Math.random() * 20 - 10

let sketch

function render() {
  const existingCanvas = document.querySelector('canvas')
  if (existingCanvas) {
    document.body.removeChild(existingCanvas)
    sketch = null
  }

  const hydrographics = parseInt(hydrographicsInput.value, 10) / 16
  const population = parseInt(populationInput.value, 10) / 16
  const size = parseInt(sizeInput.value, 10)
  const temperature = parseInt(temperatureInput.value, 10) / 16

  const height = 350 + size * 70
  const width = 2 * height

  sketch = Sketch.create({
    fragmentShaderSource,
    size: [width, height],
    uniforms: {
      hydrographics,
      population,
      qxOffset: [roff(), roff()],
      qyOffset: [roff(), roff()],
      rxOffset: [roff(), roff()],
      ryOffset: [roff(), roff()],
      seed: [
        (Math.random() < 0.5 ? -1 : 1) * Math.random() * 999.999,
        (Math.random() < 0.5 ? -1 : 1) * Math.random() * 999.999
      ],
      temperature
    },
    vertexShaderSource
  })

  document.body.appendChild(sketch.canvas)
}

generateButton.addEventListener('click', render)
render()
