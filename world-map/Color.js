const Color = {
  aqua: [0.498, 0.859, 1.0],
  black: [0.067, 0.067, 0.067],
  blue: [0.0, 0.455, 0.851],
  charcoal: [0.267, 0.267, 0.267],
  fuchsia: [0.941, 0.071, 0.745],
  gray: [0.667, 0.667, 0.667],
  green: [0.18, 0.8, 0.251],
  lime: [0.004, 1.0, 0.439],
  maroon: [0.522, 0.078, 0.294],
  navy: [0.0, 0.122, 0.247],
  olive: [0.239, 0.6, 0.439],
  orange: [1.0, 0.522, 0.114],
  purple: [0.694, 0.051, 0.788],
  red: [1.0, 0.255, 0.212],
  silver: [0.867, 0.867, 0.867],
  teal: [0.224, 0.8, 0.8],
  white: [1.0, 1.0, 1.0],
  yellow: [1.0, 0.863, 0.0]
};

export default Color;

export function blend(a, b, amt) {
  return [
    Math.min(a[0], b[0]) + Math.abs(a[0] - b[0]) * amt,
    Math.min(a[1], b[1]) + Math.abs(a[1] - b[1]) * amt,
    Math.min(a[2], b[2]) + Math.abs(a[2] - b[2]) * amt
  ];
}
