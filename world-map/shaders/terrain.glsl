#version 100

precision mediump float;

#define HASHSCALE4 vec4(0.1031, 0.103, 0.0973, 0.1099)
#define M_2PI 6.2831853071795864769252867665590
#define M_PI 3.1415926535897932384626433832795

#define Black vec3(0.0, 0.0, 0.0)
#define Dirt vec3(0.42, 0.30, 0.20)
#define Grass vec3(0.1, 0.35, 0.25)
#define Rock vec3(0.12, 0.12, 0.12)
#define Sand vec3(0.76, 0.70, 0.50)
#define Shore vec3(0.02, 0.60, 0.62)
#define Water vec3(0.024, 0.227, 0.369)
#define White vec3(1.0, 1.0, 1.0)

uniform vec2 u_qxOffset;
uniform vec2 u_qyOffset;
uniform vec2 u_resolution;
uniform vec2 u_rxOffset;
uniform vec2 u_ryOffset;
uniform vec2 u_seed;

uniform float u_hydrographics;
uniform float u_population;
uniform float u_temperature;

vec2 hash2(in vec2 p) {
  p = vec2(
    dot(p, vec2(127.1, 311.7)),
    dot(p, vec2(269.5, 183.3))
  );
  return fract(sin(p) + 43758.5453123);
}

vec3 hash3(in vec3 p) {
  p = vec3(
    dot(p, vec3(127.1, 311.7, 74.7)),
    dot(p, vec3(269.5, 183.3, 246.1)),
    dot(p, vec3(113.5, 271.9, 124.6))
  );
  return fract(sin(p) + 43758.5453123);
}

vec3 hash3(in vec2 p) {
  vec3 q = vec3(
    dot(p, vec2(127.1, 311.7)),
    dot(p, vec2(269.5, 183.3)),
    dot(p, vec2(419.2, 371.9))
  );
  return fract(sin(q) * 43758.5453123);
}

vec4 hash4(in vec3 p) {
  vec4 p4 = fract(vec4(p.xyzx) * HASHSCALE4);
  p4 += dot(p4, p4.wzxy + 19.19);
  return fract((p4.xxyz + p4.yzzw) * p4.zywx);
}

float noise(in vec3 p) {
  vec3 e = floor(p);
  vec3 f = fract(p);
  float t = 1.0 + 63.0 * pow(0.0, 4.0);
  float va = 0.0;
  float wt = 0.0;
  for (int k = -2; k <= 2; k++) {
    for (int j = -2; j <= 2; j++) {
      for (int i = -2; i <= 2; i++) {
        vec3 g = vec3(float(i), float(j), float(k));
        vec4 o = hash4(e + g) * vec4(0.0, 0.0, 0.0, 1.0);
        vec3 r = g - f + o.xyz;
        float d = dot(r, r);
        float ww = pow(1.0 - smoothstep(0.0, 1.414, sqrt(d)), t);
        va += o.w * ww;
        wt += ww;
      }
    }
  }
  return va / wt;
}

float cyl(in int octaves, in float scale, in vec2 p) {
  float circ = 4.0 * scale;
  float r = circ / M_2PI;
  float nx = scale * p.x / circ;
  float rads = nx * M_2PI;
  float a = r * sin(rads);
  float b = r * cos(rads);
  float value = 0.0;
  for (int o = 0; o < 100; o++) {
    if (o >= octaves) break;
    float m = pow(0.5, float(o + 1));
    value += m * noise(pow(2.0, float(o)) * vec3(a, scale * p.y, b));
  }
  return value / (1.0 - pow(0.5, float(octaves)));
}

vec3 contrast(in vec3 c, in float f) {
  return vec3(
    pow(c.r, 1.0 - f * (c.r - 0.5)),
    pow(c.g, 1.0 - f * (c.g - 0.5)),
    pow(c.b, 1.0 - f * (c.b - 0.5))
  );
}

float morph(in int octaves, in float scale, in vec2 p, out vec2 q, out vec2 r) {
  q.x = cyl(octaves, scale, p + u_qxOffset);
  q.y = cyl(octaves, scale, p + u_qyOffset);

  r.x = cyl(octaves, scale, p + scale * q + u_rxOffset);
  r.y = cyl(octaves, scale, p + scale * q + u_ryOffset);

  return cyl(octaves, scale, p + scale * r);
}

float span(in float level, in float floor, in float ceiling) {
  return (level - floor) / (ceiling - floor);
}

void main() {
  vec2 uv = (2.0 * gl_FragCoord.xy - u_resolution.xy) / u_resolution.y;
  vec2 p = u_seed.xy + uv;

  float nmax = 0.72;
  float nmin = 0.24;

  float latTemperature = min(1.0, 1.0 - ((abs(uv.y) - 0.5) / 0.5));
  float latElevation = min(1.0, 1.0 - ((abs(uv.y) - 0.9) / 0.8));

  float cityLevel = 128.0; // FIXME
  float city = cyl(2, 3.1, p);

  vec2 eq, er;
  float elevation = morph(8, 3.0, p, eq, er) * latElevation;

  vec2 tq, tr;
  float temperature = morph(8, 3.9, p, tq, tr) * latTemperature;
  float polarIceTemp = 0.08;
  float polarIceThreshold = 0.16;
  float snowTemp = 0.1;

  float seaMin = 0.26;
  float seaMax = 0.71;
  float seaLevel = u_hydrographics * (seaMax - seaMin) + seaMin;

  float riverLevel = 0.25; // FIXME
  float river = cyl(8, 5.0, p);

  float shoreLevel = seaLevel - 0.025;
  float snowLevel = seaLevel + 0.14; // FIXME
  float vegetationLevel = seaLevel + 0.02; // FIXME

  vec3 color;
  if (elevation < seaLevel) {
    if (elevation < shoreLevel) {
      color = mix(Black, Water, elevation / shoreLevel);
    } else {
      float v = pow((elevation - shoreLevel) / (seaLevel - shoreLevel), 3.0);
      color = mix(Water, Shore, v);
    }

    if (temperature < polarIceTemp) {
      color = mix(color, White, polarIceThreshold - temperature);
    }
  } else if (river > riverLevel && river < riverLevel + 0.005) { // 0.495–0.500
    color = Water;

    if (temperature < polarIceTemp) {
      color = mix(color, White, polarIceThreshold - temperature);
    }
  } else {
    float beachCeil = seaLevel + 0.005 * (1.0 - seaLevel);
    float mountainFloor = seaLevel + 0.80 * (1.0 - seaLevel);

    if (elevation < beachCeil) {
      color = mix(Water, Sand, span(elevation, seaLevel, beachCeil));
    } else if (elevation < mountainFloor) {
      color = mix(Sand, Dirt, span(elevation, beachCeil, mountainFloor));
    } else {
      color = mix(Dirt, Rock, span(elevation, mountainFloor, nmax));
    }

    if (elevation >= snowLevel) {
      color = mix(color, White, pow(span(elevation, snowLevel, nmax), 0.2));
    } else if (temperature < snowTemp) {
      color = mix(color, White, pow(span(snowTemp, temperature, nmax), 0.2));
    } else {
      vec2 vq, vr;
      float vegetation = morph(8, 3.7, p, vq, vr) * pow(latTemperature, 0.12);
      if (vegetation > vegetationLevel) {
        color = mix(color, Grass, span(vegetation, 0.35, nmax));
        color = mix(color, Black, 0.1 * length(vq));
      }
    }

    if (city > cityLevel) {
      color = mix(color, vec3(0.55, 0.55, 0.55),
        0.75 + 0.25 * span(city, cityLevel, nmax));
    }
  }

  gl_FragColor = vec4(color, 1.0);
}
