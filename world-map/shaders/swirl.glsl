#version 100

precision mediump float;

uniform float u_octaves;
uniform float u_time;
uniform vec2 u_offset;
uniform vec2 u_qxOffset;
uniform vec2 u_qyOffset;
uniform vec2 u_resolution;
uniform vec2 u_rxOffset;
uniform vec2 u_ryOffset;
uniform vec3 u_baseColorA;
uniform vec3 u_baseColorB;
uniform vec3 u_highColor;
uniform vec3 u_lowColor;
uniform vec3 u_midColor;

vec2 hash(in vec2 x) {
  const vec2 k = vec2(0.3183099, 0.3678794);
  x = x * k + k.yx;
  return -1.0 + 2.0 * fract(16.0 * k * fract(x.x * x.y * (x.x + x.y)));
}

float noise(in vec2 p) {
  vec2 i = floor(p);
  vec2 f = fract(p);
  vec2 u = f * f * (3.0 - 2.0 * f);
  return mix(
    mix(dot(hash(i + vec2(0.0, 0.0)), f - vec2(0.0, 0.0)),
        dot(hash(i + vec2(1.0, 0.0)), f - vec2(1.0, 0.0)), u.x),
    mix(dot(hash(i + vec2(0.0, 1.0)), f - vec2(0.0, 1.0)),
        dot(hash(i + vec2(1.0, 1.0)), f - vec2(1.0, 1.0)), u.x),
    u.y
  ) + 0.5;
}

float fbm(in vec2 p) {
  float value = 0.0;
  for (int o = 0; o < 512; o++) {
    if (o >= int(u_octaves)) break;
    value += noise(pow(2.0, float(o)) * p) * pow(0.5, float(o));
  }
  return value / (2.0 - (1.0 / pow(2.0, u_octaves - 1.0)));
}

float func(vec2 p, out vec2 q, out vec2 r) {
  q.x = fbm(p + u_qxOffset);
  q.y = fbm(p + u_qyOffset);

  float ql = length(q);
  q.x += (0.04 * sin(0.35 * u_time * ql) / ql) - 0.02;
  q.y += (0.04 * sin(0.45 * u_time * ql) / ql) - 0.02;

  r.x = fbm(p + 4.0 * q + u_rxOffset);
  r.y = fbm(p + 4.0 * q + u_ryOffset);

  float rl = length(r);
  r.x += (0.1 * sin(0.15 * u_time * rl) / rl) - 0.05;
  r.y += (0.1 * sin(0.25 * u_time * rl) / rl) - 0.05;

  return fbm(p + 4.0 * r);
}

void main() {
  vec2 p = (2.0 * gl_FragCoord.xy - u_resolution.xy) / u_resolution.y;
  p += u_offset;

  vec2 q, r;
  float v = func(3.1 * p, q, r);

  vec3 col = vec3(0.0);
  col = mix(u_baseColorA, u_baseColorB, v);
  col = mix(col, u_highColor, dot(r, r));
  col = mix(col, u_midColor, 0.5 * q.y * q.y);
  col = mix(
    col,
    u_lowColor,
    0.5 * smoothstep(0.6, 1.4, abs(r.y) + abs(r.x))
  );
  col *= v * 2.0;

  gl_FragColor = vec4(col, 1.0);
}
