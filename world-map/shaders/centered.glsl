#version 100

precision mediump float;

uniform float u_seed;
uniform vec2 u_resolution;

vec3 landColor = vec3(0.9, 0.88, 0.8);
vec3 landColor2 = vec3(0.8, 0.78, 0.7);
vec3 mountainColor = vec3(0.6, 0.57, 0.52);
vec3 oceanColor = vec3(0.2, 0.25, 0.3);
vec3 riverColor = vec3(0.35, 0.4, 0.45);
vec3 waterColor = vec3(0.3, 0.35, 0.4);

// From https://www.shadertoy.com/view/4djSRW
vec3 hash(in vec2 p) {
  vec3 p3 = fract(vec3(p.xyx) * vec3(0.1031, 0.103, 0.0973));
  p3 += dot(p3, p3.yxz + 19.19);
  return fract((p3.xxy + p3.yzz) * p3.zyx);
}

// Adapted from https://www.shadertoy.com/view/Xd23Dh
float voronoise(in vec2 p) {
  vec2 i = floor(p);
  vec2 f = fract(p);
  float k = 1.0;
  float va = 0.0;
  float wt = 0.0;
  for (int b = -2; b <= 2; b++) {
    for (int a = -2; a <= 2; a++) {
      vec2 g = vec2(float(a), float(b));
      vec3 o = hash(i + g);
      vec2 r = g - f + o.xy;
      float d = dot(r, r);
      float ww = 1.0 - smoothstep(0.0, 1.414, sqrt(d));
      va += o.z * ww;
      wt += ww;
    }
  }
  return va / wt;
}

float fractal(in int octaves, in vec2 p) {
  float value = 0.0;
  for (int i = 0; i < 64; i++) {
    if (i >= octaves) break;
    float fi = float(i);
    value += pow(0.5, fi) * voronoise(p * pow(2.0, fi));
  }
  return value / (2.0 - pow(0.5, float(octaves - 1)));
}

void main() {
  vec2 st = gl_FragCoord.xy / u_resolution.xy;

  float xi = st.x < 0.5 ? 2.0 * st.x : 2.0 - 2.0 * st.x;
  float yi = st.y < 0.5 ? 0.5 + st.y : 1.5 - st.y;
  float centered = dot(xi, yi);

  vec2 pos = u_seed + vec2(st.x * (u_resolution.x / u_resolution.y), st.y);
  float elevation = (0.25 * centered) + (0.75 * fractal(16, 11.3 * pos));
  vec3 color = vec3(0.0);
  if (elevation > 0.55) {
    float river = fractal(6, 15.9 * pos);
    if (river > 0.51 && river < 0.52) {
      color = riverColor;
    } else if (elevation > 0.7) {
      float mountain = fractal(6, 32.1 * pos);
      if (mountain > 0.45) {
        color = mix(landColor, mountainColor, 0.55 + mountain);
      } else {
        color = mix(landColor, landColor2, (elevation - 0.6) / 0.2);
      }
    } else {
      color = mix(landColor, landColor2, (elevation - 0.6) / 0.2);
    }
  } else {
    color = mix(oceanColor, waterColor, elevation / 0.6);
  }

  gl_FragColor = vec4(color, 1.0);
}
