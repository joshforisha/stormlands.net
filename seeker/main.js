import {
  $,
  h,
  load,
  main,
  reset,
  scoreRating,
  show,
  state
} from './utilities.js'
import { setupBackgroundSkills } from './select-background-skills.js'
import { setupCharacteristics } from './generate-characteristics.js'
import { setupTermEntry } from './term-entry.js'

window.reset = reset

function initialize(...elements) {
  main.innerHTML = ''
  main.append(...elements)
}

function initializeNextView() {
  let viewElements = []

  if (!state.generatedCharacteristics)
    viewElements = setupCharacteristics(initializeNextView)
  else if (!state.selectedBackgroundSkills)
    viewElements = setupBackgroundSkills(initializeNextView)
  else viewElements = setupTermEntry(initializeNextView)

  initialize(...viewElements)
}

load()
initializeNextView()
