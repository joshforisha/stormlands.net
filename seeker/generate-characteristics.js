import {
  h,
  main,
  roll,
  save,
  state,
  updateCharacteristics
} from './utilities.js'

export function setupCharacteristics(onContinue) {
  const title = h('h1', { textContent: 'Generate Characteristics' })
  main.appendChild(title)

  const genStrength = h('rating-block', { label: 'Strength' })
  const genDexterity = h('rating-block', { label: 'Dexterity' })
  const genEndurance = h('rating-block', { label: 'Endurance' })
  const genIntellect = h('rating-block', { label: 'Intellect' })
  const genEducation = h('rating-block', { label: 'Education' })
  const genSocialStanding = h('rating-block', { label: 'Social Standing' })

  let strScore, dexScore, endScore, intScore, eduScore, socScore

  const generate = () => {
    strScore = roll()
    dexScore = roll()
    endScore = roll()
    intScore = roll()
    eduScore = roll()
    socScore = roll()

    genStrength.setValue(strScore)
    genDexterity.setValue(dexScore)
    genEndurance.setValue(endScore)
    genIntellect.setValue(intScore)
    genEducation.setValue(eduScore)
    genSocialStanding.setValue(socScore)
  }

  generate()

  return [
    title,
    h('div.frame', [
      h('div.row', [
        h('div.column.fifth', [genStrength]),
        h('div.column.fifth', [genDexterity]),
        h('div.column.fifth', [genEndurance])
      ]),
      h('div.row', [
        h('div.column.fifth', [genIntellect]),
        h('div.column.fifth', [genEducation]),
        h('div.column.fifth', [genSocialStanding])
      ])
    ]),
    h('div.actions', [
      h('button.blue', {
        on: {
          click: generate
        },
        textContent: 'Regenerate'
      }),
      h('button.green', {
        on: {
          click: () => {
            state.strength = strScore
            state.dexterity = dexScore
            state.endurance = endScore
            state.intellect = intScore
            state.education = eduScore
            state.socialStanding = socScore
            state.generatedCharacteristics = true
            save()
            updateCharacteristics()
            onContinue()
          }
        },
        textContent: 'Continue'
      })
    ])
  ]
}
