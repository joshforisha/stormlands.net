class RatingBlock extends HTMLElement {
  static get observedAttributes() {
    return ['label', 'value']
  }

  constructor() {
    super()

    const shadow = this.attachShadow({ mode: 'open' })

    const style = document.createElement('style')
    style.textContent = `
      :host {
        align-items: center;
        display: flex;
        flex-direction: column;
        padding: var(--small);
        width: 100%;
      }

      label {
        line-height: 1;
        margin: 0;
        white-space: nowrap;
      }

      .value {
        align-items: center;
        display: flex;
        font-size: 1.5em;
        height: 1em;
      }
    `
    shadow.appendChild(style)

    this.valueSpan = document.createElement('span')
    this.valueSpan.setAttribute('class', 'value')
    shadow.appendChild(this.valueSpan)

    this.label = document.createElement('label')
    shadow.appendChild(this.label)
  }

  attributeChangedCallback(name, oldValue, newValue) {
    switch (name) {
      case 'label':
        this.setLabel(newValue)
        break
      case 'value':
        this.setValue(newValue)
        break
    }
  }

  connectedCallback() {
    if (this.hasAttribute('label')) this.setLabel(this.getAttribute('label'))
    if (this.hasAttribute('value')) this.setValue(this.getAttribute('value'))
  }

  setLabel(labelText) {
    this.label.textContent = labelText
  }

  setValue(value) {
    this.valueSpan.textContent = value
  }
}

customElements.define('rating-block', RatingBlock)
