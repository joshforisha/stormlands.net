import { check, h, infoItem, list, state, ordinal } from '../utilities.js'

export function universityEntry() {
  return check(
    'intellect',
    7,
    -(state.socialStanding >= 9 ? 1 : 0),
    -state.termsPassed
  )
}

export function universityGraduation() {
  return check('intellect', 7)
}

export function universityGraduationWithHonours() {
  return check('intellect', 11)
}

export function universityDetails(onContinue) {
  const entry = universityEntry()
  const graduation = universityGraduation()
  const graduationWithHonours = universityGraduationWithHonours()

  return [
    h('h2', 'Pre-Career: University'),
    h(
      'p',
      'Most worlds have one or more universities within their major settlements but even citizens of the most remote system can engage in university education through the use of computer networks and interstellar communications. A university education will channel a character’s efforts into a narrow range of skills but there are few other ways to become more skillful within them so quickly.'
    ),
    h('p', [
      h('b', 'Entry: '),
      `${entry.asString}. DM-1 if in Term Two, DM-2 if in Term Three, DM+1 if SOC 9+.`
    ]),
    h('p', 'Attendance benefits include:'),
    list('Gain skills.', 'Increase EDU by +1.'),
    h('p', [
      h('b', 'Graduation: '),
      `${graduation.asString}. ${graduationWithHonours.asString} with honours. Benefits include:`
    ]),
    list(
      'Increase gained skill levels.',
      'Increase EDU by an additional +2.',
      'Gain DM+1 (DM+2 with honours) to qualify for careers: Agent, Army, Citizen (corporate), Entertainer (journalist), Marines, Navy, Scholar, Scouts.',
      'Allows a Commission roll to be taken before the following first term of a military career.'
    ),
    h('button.green', {
      textContent: `Apply to University ${entry.chanceString}`,
      on: {
        click: onContinue
      }
    })
  ]
}
