import { check, h, h2, hr, infoText, list, p, state } from '../utilities.js'

export const fieldResearcherSurvival = () => check('endurance', 6)
export const fieldResearcherAdvancement = () => check('intellect', 6)
export const scientistSurvival = () => check('education', 4)
export const scientistAdvancement = () => check('intellect', 8)
export const physicianSurvival = () => check('education', 4)
export const physicianAdvancement = () => check('education', 8)

export function scholarQualification() {
  return check('intellect', 6, -state.careersServed)
}

export function scholarDetails(onContinue) {
  const qualification = scholarQualification()

  return [
    h2('Career: Scholar'),
    p(
      'Individuals trained in technological or research sciences who conduct scientific investigations into materials, situations and phenomena, or who practise medicine.'
    ),
    infoText(
      'Qualification',
      `${qualification.asString}. DM-1 for every previous career.`
    ),
    hr(),
    infoText(
      'Field Researcher',
      'You are an explorer or field researcher, equally at home in the laboratory or wilderness.'
    ),
    list(
      `Survival: ${fieldResearcherSurvival().asString}.`,
      `Advancement: ${fieldResearcherAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Field Researcher ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    }),
    hr(),
    infoText(
      'Scientist',
      'You are a researcher in some corporation or research institution, or a mad scientist in an orbiting laboratory.'
    ),
    list(
      `Survival: ${scientistSurvival().asString}.`,
      `Advancement: ${scientistAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Scientist ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    }),
    hr(),
    infoText('Physician', 'You are a doctor, healer or medical researcher.'),
    list(
      `Survival: ${physicianSurvival().asString}.`,
      `Advancement: ${physicianAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Physician ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    })
  ]
}
