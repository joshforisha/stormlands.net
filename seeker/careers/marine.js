import { check, h, h2, hr, infoText, list, p, state } from '../utilities.js'

export const groundAssaultAdvancement = () => check('education', 5)
export const groundAssaultSurvival = () => check('endurance', 7)
export const starMarineAdvancement = () => check('education', 6)
export const starMarineSurvival = () => check('endurance', 6)
export const supportAdvancement = () => check('education', 7)
export const supportSurvival = () => check('endurance', 5)

export function marineCommission() {
  return check('socialStanding', 8)
}

export function marineQualification() {
  return check('endurance', 6, -state.careersServed, state.age >= 30 ? -2 : 0)
}

export function marineDetails(onContinue) {
  const qualification = marineQualification()

  return [
    h2('Career: Marine'),
    p(
      'Members of the armed fighting forces carried aboard starships, marines deal with piracy and boarding actions in space, defend the starports and bases belonging to the navy and supplement ground forces such as the army.'
    ),
    infoText(
      'Qualification',
      `${qualification.asString}. DM-1 for every previous career. DM-2 if you are aged 30 or more.`
    ),
    infoText('Commission', `${marineCommission().asString}.`),
    hr(),
    infoText(
      'Support',
      'You are a quartermaster, engineer or battlefield medic in the marines.'
    ),
    list(
      `Survival: ${supportSurvival().asString}.`,
      `Advancement: ${supportAdvancement().astString}.`
    ),
    h('button.green', {
      textContent: `Join as Support ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    }),
    hr(),
    infoText(
      'Star Marine',
      'You are trained to fight boarding actions and capture enemy vessels.'
    ),
    list(
      `Survival: ${starMarineSurvival().asString}.`,
      `Advancement: ${starMarineAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Star Marine ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    }),
    hr(),
    infoText(
      'Ground Assault',
      'You are kicked out of a spacecraft in high orbit and told to "capture the planet".'
    ),
    list(
      `Survival: ${groundAssaultSurvival().asString}.`,
      `Advancement: ${groundAssaultAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Ground Assault ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    })
  ]
}
