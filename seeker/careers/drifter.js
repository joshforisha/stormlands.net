import { check, h, h2, hr, infoText, list, p } from '../utilities.js'

const barbarianAdvancement = () => check('strength', 7)
const barbarianSurvival = () => check('endurance', 7)
const scavengerAdvancement = () => check('endurance', 7)
const scavengerSurvival = () => check('dexterity', 7)
const wandererAdvancement = () => check('intellect', 7)
const wandererSurvival = () => check('endurance', 7)

export function drifterQualification() {
  return check(null, 0)
}

export function drifterDetails(onContinue) {
  const qualification = drifterQualification()

  return [
    h2('Career: Drifter'),
    p(
      'Wanderers, hitchhikers and travellers, drifters are those who roam the stars without obvious purpose or direction.'
    ),
    infoText('Qualification', 'Automatic.'),
    hr(),
    infoText(
      'Barbarian',
      'You live on a primitive world without the benefits of technology.'
    ),
    list(
      `Survival: ${barbarianSurvival().asString}.`,
      `Advancement: ${barbarianAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Barbarian ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    }),
    hr(),
    infoText(
      'Wanderer',
      'You are a space bum, living hand-to-mouth in slums and spaceports across the galaxy'
    ),
    list(
      `Survival: ${wandererSurvival().asString}.`,
      `Advancement: ${wandererAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Wanderer ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    }),
    hr(),
    infoText(
      'Scavenger',
      'You work as a belter (asteroid miner) or on a salvage crew.'
    ),
    list(
      `Survival: ${scavengerSurvival().asString}.`,
      `Advancement: ${scavengerAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Scavenger ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    })
  ]
}
