import { check, h, list, state } from '../utilities.js'

export const corporateAdvancement = () => check('intellect', 7)
export const corporateSurvival = () => check('intellect', 5)
export const intelligenceAdvancement = () => check('intellect', 5)
export const intelligenceSurvival = () => check('intellect', 7)
export const lawEnforcementAdvancement = () => check('intellect', 6)
export const lawEnforcementSurvival = () => check('endurance', 6)

export function agentQualification() {
  return check('intellect', 6, -state.careersServed)
}

export function agentDetails(onContinue) {
  const qualification = agentQualification()

  return [
    h('h2', 'Career: Agent'),
    h(
      'p',
      'Law enforcement agencies, corporate operatives, spies, and others who work in the shadows.'
    ),
    h('p', [
      h('b', 'Qualification: '),
      `${qualification.asString}. DM-1 for every previous career.`
    ]),
    h('hr'),
    h('p', [
      h('b', 'Law Enforcement: '),
      'You are a police officer or detective.'
    ]),
    list(
      `Survival: ${lawEnforcementSurvival().asString}.`,
      `Advancement: ${lawEnforcementAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Law Enforcement ${qualification.chanceString}`,
      on: {
        click: () => {
          onContinue()
        }
      }
    }),
    h('hr'),
    h('p', [h('b', 'Intelligence: '), 'You work as a spy or saboteur.']),
    list(
      `Survival: ${intelligenceSurvival().asString}.`,
      `Advancement: ${intelligenceAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Intelligence ${qualification.chanceString}`,
      on: {
        click: () => {
          onContinue()
        }
      }
    }),
    h('hr'),
    h('p', [
      h('b', 'Corporate: '),
      'You work for a corporation, spying on rival organisations.'
    ]),
    list(
      `Survival: ${corporateSurvival().asString}.`,
      `Advancement: ${corporateAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Corporate ${qualification.chanceString}`,
      on: {
        click: () => {
          onContinue()
        }
      }
    })
  ]
}

// Mustering Out Benefits
// 1. Cr1000 / Scientific Equipment
// 2. Cr2000 / INT +1
// 3. Cr5000 / Ship Share
// 4. Cr7500 / Weapon
// 5. Cr10,000 / Combat Implant
// 6. Cr25,000 / SOC +1 or Combat Implant
// 7. Cr50,000 / TAS Membership
