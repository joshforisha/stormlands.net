import { check, h, h2, hr, infoText, list, p, state } from '../utilities.js'

export const courierAdvancement = () => check('education', 9)
export const courierSurvival = () => check('endurance', 5)
export const explorerAdvancement = () => check('education', 7)
export const explorerSurvival = () => check('endurance', 7)
export const surveyorAdvancement = () => check('intellect', 8)
export const surveyorSurvival = () => check('endurance', 6)

export function scoutQualification() {
  return check('intellect', 5, -state.careersServed)
}

export function scoutDetails(onContinue) {
  const qualification = scoutQualification()

  return [
    h2('Career: Scout'),
    p(
      'Members of the exploratory service. Scouts explore new areas, map and survey known or newly discovered areas, and maintain communication ships which carry information and messages between the worlds of the galaxy.'
    ),
    infoText(
      'Qualification',
      `${qualification.asString}. DM-1 for every previous career. DM-2 if you are aged 34 or more.`
    ),
    hr(),
    infoText(
      'Courier',
      'You are responsible for shuttling messages and high value packages around the galaxy.'
    ),
    list(
      `Survival: ${courierSurvival().asString}.`,
      `Advancement: ${courierAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Courier ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    }),
    hr(),
    infoText('Surveyor', 'You visit border worlds and assess their worth.'),
    list(
      `Survival: ${surveyorSurvival().asString}.`,
      `Advancement: ${surveyorAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Surveyor ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    }),
    hr(),
    infoText(
      'Explorer',
      'You go wherever the map is blank, exploring unknown worlds and uncharted space.'
    ),
    list(
      `Survival: ${explorerSurvival().asString}.`,
      `Advancement: ${explorerAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Explorer ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    })
  ]
}
