import { check, h, h2, hr, infoText, list, p, state } from '../utilities.js'

export const thiefSurvival = () => check('intellect', 6)
export const thiefAdvancement = () => check('dexterity', 6)
export const enforcerSurvival = () => check('endurance', 6)
export const enforcerAdvancement = () => check('strength', 6)
export const pirateSurvival = () => check('dexterity', 6)
export const pirateAdvancement = () => check('intellect', 6)

export function rogueQualification() {
  return check('dexterity', 6, -state.careersServed)
}

export function rogueDetails(onContinue) {
  const qualification = rogueQualification()

  return [
    h2('Career: Rogue'),
    p(
      'Criminal elements familiar with the rougher or more illegal methods of attaining goals.'
    ),
    infoText(
      'Qualification',
      `${qualification.asString}. DM-1 for every previous career.`
    ),
    hr(),
    infoText(
      'Thief',
      'You steal from the rick and give to the... well, yourself, actually.'
    ),
    list(
      `Survival: ${thiefSurvival().asString}.`,
      `Advancement: ${thiefAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Thief ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    }),
    hr(),
    infoText(
      'Enforcer',
      'You are a leg breaker, thug or assassin for a criminal group.'
    ),
    list(
      `Survival: ${enforcerSurvival().asString}.`,
      `Advancement: ${enforcerAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Enforcer ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    }),
    hr(),
    infoText('Pirate', 'You are a space-going corsair.'),
    list(
      `Survival: ${pirateSurvival().asString}.`,
      `Advancement: ${pirateAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Pirate ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    })
  ]
}
