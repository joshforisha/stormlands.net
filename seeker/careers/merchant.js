import { check, h, h2, hr, infoText, list, p, state } from '../utilities.js'

export const brokerAdvancement = () => check('intellect', 7)
export const brokerSurvival = () => check('education', 5)
export const freeTraderAdvancement = () => check('intellect', 6)
export const freeTraderSurvival = () => check('dexterity', 6)
export const merchantMarineAdvancement = () => check('intellect', 7)
export const merchantMarineSurvival = () => check('education', 5)

export function merchantQualification() {
  return check('intellect', 4, -state.careersServed)
}

export function merchantDetails(onContinue) {
  const qualification = merchantQualification()

  return [
    h2('Career: Merchant'),
    p(
      'Members of commercial enterprise. Merchants may crew the ships of the huge trading corporations, or they may work for independent free traders who carry chance cargoes and passengers between worlds.'
    ),
    infoText(
      'Qualification',
      `${qualification.asString}. DM-1 for every previous career.`
    ),
    hr(),
    infoText(
      'Merchant Marine',
      'You work on one of the massive cargo haulers run by the Imperium or the megacorporations.'
    ),
    list(
      `Survival: ${merchantMarineSurvival().asString}.`,
      `Advancement: ${merchantMarineAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Merchant Marine ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    }),
    hr(),
    infoText('Free Trader', 'You are part of the crew of a tramp trader.'),
    list(
      `Survival: ${freeTraderSurvival().asString}.`,
      `Advancement: ${freeTraderAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Free Trader ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    }),
    hr(),
    infoText('Broker', 'You work in a planetside brokerage or starport.'),
    list(
      `Survival: ${brokerSurvival().asString}.`,
      `Advancement: ${brokerAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Broker ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    })
  ]
}
