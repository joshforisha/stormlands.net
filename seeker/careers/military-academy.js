import { check, h, list, state } from '../utilities.js'

export function armyAcademyEntry() {
  return check('endurance', 8, -2 * state.termsPassed)
}

export function marineAcademyEntry() {
  return check('endurance', 9, -2 * state.termsPassed)
}

export function navyAcademyEntry() {
  return check('intellect', 9, -2 * state.termsPassed)
}

export function militaryAcademyEntryChanceString() {
  const armyChance = armyAcademyEntry().chance
  const marineChance = marineAcademyEntry().chance
  const navyChance = navyAcademyEntry().chance
  const min = Math.min(armyChance, marineChance, navyChance)
  const max = Math.max(armyChance, marineChance, navyChance)
  if (max > min) return `(${min}–${max}%)`
  return `(${max}%)`
}

export function militaryAcademyGraduation() {
  const endDM = state.endurance >= 8 ? 1 : 0
  const socDM = state.socialStanding >= 8 ? 1 : 0
  return check('intellect', 8, -endDM, -socDM)
}

export function militaryAcademyGraduationWithHonours() {
  const endDM = state.endurance >= 8 ? 1 : 0
  const socDM = state.socialStanding >= 8 ? 1 : 0
  return check('intellect', 11, -endDM, -socDM)
}

export function militaryAcademyDetails(onContinue) {
  const armyEntry = armyAcademyEntry()
  const marineEntry = marineAcademyEntry()
  const navyEntry = navyAcademyEntry()
  const graduation = militaryAcademyGraduation()
  const graduationWithHonours = militaryAcademyGraduationWithHonours()

  return [
    h('h2', 'Pre-Career: Military Academy'),
    h(
      'p',
      'For those who are looking to dedicate their lives to military service, there is no better option than joining an academy to round out an education. This is a popular choice for those coming from "military" families or those having grown up never considering anything other than a life in uniform. A term within a military academy can set a recruit’s career for great things, so competition to gain one of the limited number of open student slots is fierce.'
    ),
    h('p', [
      h('b', 'Entry: '),
      `Army: ${armyEntry.asString}; Marines: ${marineEntry.asString}; Navy: ${navyEntry.asString}. DM-2 if Term Two, DM-4 if in Term Three. Benefits include:`
    ]),
    list('Gain all related military Service Skills at level 0.'),
    h('p', [
      h('b', `Graduation: `),
      `${graduation.asString}. DM+1 if END 8+, DM+1 if SOC 8+. ${graduationWithHonours.asString} with honours. Benefits include:`
    ]),
    list(
      'Gain and increase military Service Skills if joining next term.',
      'Increase EDU by +1.',
      'With honours, increase SOC by +1.',
      'Automatic following entry into the related military career.',
      'Allows initial a Commission roll to be taken before first term, so long as it is the first career chosen after academy, with DM+2. With honours, this is an automatic pass.'
    ),
    h('div.actions.-left', [
      h('button.green', {
        textContent: `Apply to Army Academy ${armyEntry.chanceString}`,
        on: {
          click: onContinue
        }
      }),
      h('button.green', {
        textContent: `Apply to Marine Academy ${marineEntry.chanceString}`,
        on: {
          click: onContinue
        }
      }),
      h('button.green', {
        textContent: `Apply to Navy Academy ${navyEntry.chanceString}`,
        on: {
          click: onContinue
        }
      })
    ])
  ]
}
