import { check, h, h2, hr, infoText, list, p, state } from '../utilities.js'

export const colonistSurvival = () => check('intellect', 7)
export const colonistAdvancement = () => check('endurance', 5)
export const corporateSurvival = () => check('socialStanding', 6)
export const corporateAdvancement = () => check('intellect', 6)
export const workerSurvival = () => check('endurance', 4)
export const workerAdvancement = () => check('education', 8)

export function citizenQualification() {
  return check('education', 5, -state.careersServed)
}

export function citizenDetails(onContinue) {
  const qualification = citizenQualification()

  return [
    h2('Career: Citizen'),
    p(
      'Individuals serving in a corporation, bureaucracy or industry, or who are making a new life on an untamed planet.'
    ),
    infoText(
      'Qualification',
      `${qualification.asString}. DM-1 for every previous career.`
    ),
    hr(),
    infoText(
      'Corporate',
      'You are an executive or manager in a large corporation.'
    ),
    list(
      `Survival: ${corporateSurvival().asString}.`,
      `Advancement: ${corporateAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Corporate ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    }),
    hr(),
    infoText('Worker', 'You are a blue collar worker on an industrial world.'),
    list(
      `Survival: ${workerSurvival().asString}.`,
      `Advancement: ${workerAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Worker ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    }),
    hr(),
    infoText(
      'Colonist',
      'You are building a new life on a recently settled world that still needs taming.'
    ),
    list(
      `Survival: ${colonistSurvival().asString}.`,
      `Advancement: ${colonistAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Colonist ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    })
  ]
}
