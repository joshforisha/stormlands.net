import { check, h, h2, hr, infoText, list, p, state } from '../utilities.js'

export const crewSurvival = () => check('intellect', 5)
export const crewAdvancement = () => check('education', 7)
export const engineerSurvival = () => check('intellect', 6)
export const engineerAdvancement = () => check('education', 6)
export const flightSurvival = () => check('dexterity', 7)
export const flightAdvancement = () => check('education', 5)

export function navyCommission() {
  return check('socialStanding', 8)
}

export function navyQualification() {
  return check('intellect', 6, -state.careersServed, state.age >= 34 ? -2 : 0)
}

export function navyDetails(onContinue) {
  const qualification = navyQualification()

  return [
    h2('Career: Navy'),
    p(
      'Members of the interstellar navy which patrols space between the stars. The navy has the responsibility for the protection of society from foreign powers and lawless elements in the interstellar trade channels.'
    ),
    infoText(
      'Qualification',
      `${qualification.asString}. DM-1 for every previous career. DM-2 if you are aged 34 or more.`
    ),
    infoText('Commission', `${navyCommission().asString}.`),
    hr(),
    infoText(
      'Line/Crew',
      'You serve as a general crewman or officer on a ship of the line.'
    ),
    list(
      `Survival: ${crewSurvival().asString}.`,
      `Advancement: ${crewAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Line/Crew ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    }),
    hr(),
    infoText(
      'Engineer/Gunner',
      'You serve as a specialist technician on a starship.'
    ),
    list(
      `Survival: ${engineerSurvival().asString}.`,
      `Advancement: ${engineerAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Engineer/Gunner ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    }),
    hr(),
    infoText(
      'Flight',
      'You are a pilot of a shuttle, fighter or other light craft.'
    ),
    list(
      `Survival: ${flightSurvival().asString}.`,
      `Advancement: ${flightAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Flight ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    })
  ]
}
