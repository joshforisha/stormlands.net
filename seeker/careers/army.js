import { check, h, list, state } from '../utilities.js'

export const armyCommission = () => check('socialStanding', 8)
export const cavalryAdvancement = () => check('intellect', 5)
export const cavalrySurvival = () => check('intellect', 7)
export const infantryAdvancement = () => check('education', 6)
export const infantrySurvival = () => check('strength', 6)
export const supportAdvancement = () => check('education', 7)
export const supportSurvival = () => check('endurance', 5)

export function armyQualification() {
  const ageDM = state.age >= 30 ? 2 : 0
  return check('endurance', 5, -state.careersServed, -ageDM)
}

export function armyDetails(onContinue) {
  const qualification = armyQualification()

  return [
    h('h2', 'Career: Army'),
    h(
      'p',
      'Members of the planetary armed fighting forces. Soldiers deal with planetary surface actions, battles and campaigns. Such individuals may also be mercenaries for hire.'
    ),
    h('p', [
      h('b', 'Qualification: '),
      `${qualification.asString}. DM-1 for every previous career, DM-2 if you are aged 30 or more.`
    ]),
    h('p', [h('b', 'Commission: '), armyCommission().asString]),
    h('hr'),
    h('p', [
      h('b', 'Support: '),
      'You are an engineer, cook or in some other role behind the front lines.'
    ]),
    list(
      `Survival: ${supportSurvival().asString}.`,
      `Advancement: ${supportAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Support ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    }),
    h('hr'),
    h('p', [
      h('b', 'Infantry: '),
      'You are one of the Poor Bloody Infantry on the ground.'
    ]),
    list(
      `Survival: ${infantrySurvival().asString}.`,
      `Advancement: ${infantryAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Infantry ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    }),
    h('hr'),
    h('p', [
      h('b', 'Cavalry: '),
      'You are one of the crew of a gunship or tank.'
    ]),
    list(
      `Survival: ${cavalrySurvival().asString}.`,
      `Advancement: ${cavalryAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Cavalry ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    })
  ]
}
