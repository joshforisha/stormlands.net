import { check, h, h2, hr, infoText, list, p, state } from '../utilities.js'

export const administratorAdvancement = () => check('education', 6)
export const administratorSurvival = () => check('intellect', 4)
export const dilettanteAdvancement = () => check('intellect', 8)
export const dilettanteSurvival = () => check('socialStanding', 3)
export const diplomatAdvancement = () => check('socialStanding', 7)
export const diplomatSurvival = () => check('intellect', 5)

export function nobleQualification() {
  if (state.socialStanding >= 10) return check(null, 0)
  return check('socialStanding', 10, -state.careersServed)
}

export function nobleDetails(onContinue) {
  const qualification = nobleQualification()

  return [
    h2('Career: Noble'),
    p(
      'Individuals of the upper class who perform little consistent function, but often have large amounts of ready money.'
    ),
    infoText(
      'Qualification',
      `${qualification.asString}. DM-1 for every previous career. Automatic qualification if your SOC is 10 or higher.`
    ),
    hr(),
    infoText(
      'Administrator',
      'You serve in the planetary government or even ruled over a fiefdom or other domain.'
    ),
    list(
      `Survival: ${administratorSurvival().asString}.`,
      `Advancement: ${administratorAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Administrator ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    }),
    hr(),
    infoText('Diplomat', 'You are a diplomat or other state official.'),
    list(
      `Survival: ${diplomatSurvival().asString}.`,
      `Advancement: ${diplomatAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Diplomat ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    }),
    hr(),
    infoText(
      'Dilettante',
      'You are known for being known and have absolutely no useful function in society.'
    ),
    list(
      `Survival: ${dilettanteSurvival().asString}.`,
      `Advancement: ${dilettanteAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Dilettante ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    })
  ]
}
