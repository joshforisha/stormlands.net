import { check, h, h2, hr, infoText, list, p, state } from '../utilities.js'

const artistAdvancement = () => check('intellect', 6)
const artistSurvival = () => check('socialStanding', 6)
const journalistAdvancement = () => check('intellect', 5)
const journalistSurvival = () => check('education', 7)
const performerSurvival = () => check('intellect', 5)
const performerAdvancement = () => check('dexterity', 7)

export function entertainerQualification() {
  return check(['dexterity', 'intellect'], 5, -state.careersServed)
}

export function entertainerDetails(onContinue) {
  const qualification = entertainerQualification()

  return [
    h2('Career: Entertainer'),
    p(
      'Individuals who are involved with the media, whether as reporters, artists or celebrities.'
    ),
    infoText(
      'Qualification',
      `${qualification.asString}. DM-1 for every previous career.`
    ),
    hr(),
    infoText('Artist', 'You are a writer, holographer or other creative.'),
    list(
      `Survival: ${artistSurvival().asString}.`,
      `Advancement: ${artistAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Artist ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    }),
    hr(),
    infoText(
      'Journalist',
      'You report on local or galactic events for a news feed, the TAS or other organisation.'
    ),
    list(
      `Survival: ${journalistSurvival().asString}.`,
      `Advancement: ${journalistAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Journalist ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    }),
    hr(),
    infoText(
      'Performer',
      'You are an actor, dancer, acrobat, professional athlete or other public performer.'
    ),
    list(
      `Survival: ${performerSurvival().asString}.`,
      `Advancement: ${performerAdvancement().asString}.`
    ),
    h('button.green', {
      textContent: `Join as Performer ${qualification.chanceString}`,
      on: {
        click: onContinue
      }
    })
  ]
}
