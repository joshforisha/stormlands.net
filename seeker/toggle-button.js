class ToggleButton extends HTMLButtonElement {
  constructor() {
    super()

    this.addEventListener('click', () => {
      if (this.hasAttribute('data-selected')) {
        this.removeAttribute('data-selected')
        this.dispatchEvent(new Event('unselect'))
      } else {
        this.setAttribute('data-selected', '')
        this.dispatchEvent(new Event('select'))
      }
    })
  }
}

customElements.define('toggle-button', ToggleButton, { extends: 'button' })
