const initialState = {
  age: 18,
  skills: [],

  // Events and counters
  events: [],
  careersServed: 0,
  termsPassed: 0,

  // Flags
  generatedCharacteristics: false,
  selectedBackgroundSkills: false,

  // Characteristics
  strength: undefined,
  dexterity: undefined,
  endurance: undefined,
  intellect: undefined,
  education: undefined,
  socialStanding: undefined
}

export const age = document.getElementById('Age')
export const characterInfo = document.getElementById('CharacterInfo')
export const characteristics = document.getElementById('Characteristics')
export const main = document.querySelector('main')
export const skills = document.getElementById('Skills')
export const skillsList = document.getElementById('SkillsList')

export let state

// DOM Functions -------------------------------------------------------------------------------------------------------

export const $ = (sel) => document.querySelector(sel)
export const $$ = (sel) => Array.from(document.querySelectorAll(sel))

export function h(def, options = {}) {
  const [elementType, ...classNames] = def.split('.')
  const createOptions = options.is ? { is: options.is } : undefined
  const element = document.createElement(elementType, createOptions)
  if (classNames.length > 0) element.setAttribute('class', classNames.join(' '))
  if (typeof options === 'string') {
    element.textContent = options
  } else if (Array.isArray(options)) {
    for (const child of options) {
      element.appendChild(
        typeof child === 'string' ? document.createTextNode(child) : child
      )
    }
  } else {
    for (const [key, value] of Object.entries(options)) {
      switch (key) {
        case 'children':
          for (const child of value) {
            element.appendChild(
              typeof child === 'string' ? document.createTextNode(child) : child
            )
          }
          break
        case 'disabled':
          if (value) element.setAttribute('disabled', '')
        case 'is':
          break
        case 'on':
          for (const [eventName, handler] of Object.entries(value)) {
            element.addEventListener(eventName, handler)
          }
          break
        case 'textContent':
          element.textContent = value
          break
        default:
          element.setAttribute(key, value)
      }
    }
  }
  return element
}

export const b = (...cs) => h('b', ...cs)
export const h2 = (...cs) => h('h2', ...cs)
export const hr = () => h('hr')
export const p = (...cs) => h('p', ...cs)

// General Functions ---------------------------------------------------------------------------------------------------

export function actionItem(name, onSelect) {
  return h('button.choice', {
    disabled: !onSelect,
    textContent: name,
    on: {
      click: (event) => {
        if (onSelect) onSelect()
        event.target.classList.add('-selected')
      }
    }
  })
}

export function chance(score, difficulty, ...dms) {
  if (score === undefined) return 100

  const target =
    difficulty - scoreMod(score) - dms.reduce((total, dm) => total + dm, 0)

  if (target < 3) return 100
  if (target < 4) return 97
  if (target < 5) return 92
  if (target < 6) return 83
  if (target < 7) return 72
  if (target < 8) return 58
  if (target < 9) return 42
  if (target < 10) return 28
  if (target < 11) return 17
  if (target < 12) return 8
  if (target < 13) return 3
  return 0
}

export function characteristicAbbr(charKey) {
  if (charKey === null) return
  return charKey.substring(0, 3).toUpperCase()
}

export function check(charKey, difficulty, ...dms) {
  let abbr
  if (Array.isArray(charKey)) {
    abbr = charKey.map(characteristicAbbr).join(' or ')
    ;[charKey] = charKey
      .map((c) => [c, state[c]])
      .sort(([, a], [, b]) => (a < b ? -1 : 1))
      .at(0)
  } else {
    abbr = characteristicAbbr(charKey)
  }

  const score = state[charKey]
  const _chance = chance(score, difficulty, ...dms)

  return {
    asString: `${abbr} ${difficulty}+ (${_chance}%)`,
    chance: _chance,
    chanceString: `(${_chance}%)`
  }
}

export function classes(...classNames) {
  return classNames.filter((c) => typeof c === 'string').join(' ')
}

export function disable(element) {
  element.setAttribute('disabled', '')
}

export function enable(element) {
  element.removeAttribute('disabled')
}

export function hide(element) {
  element.classList.add('-hidden')
}

export function infoItem(label, text, children = []) {
  const item = h('li', [h('b', `${label}:`), h('span', ` ${text}`)])
  if (children.length > 0) {
    const sublist = h('ul')
    for (const child of children) sublist.appendChild(h('li', child))
    item.appendChild(sublist)
  }
  return item
}

export function infoText(label, text) {
  return h('p', [h('b', `${label}: `), text])
}

export function initialize(...elements) {
  main.innerHTML = ''
  main.append(...elements)
}

export function insert(array, item) {
  return array.concat(item).sort()
}

export function list(...items) {
  return h('ul', [...items.map((i) => h('li', i))])
}

export function load() {
  const savedStateString = localStorage.getItem('state')
  if (savedStateString) {
    state = JSON.parse(savedStateString)
    if (state.generatedCharacteristics) updateCharacteristics()
    if (state.selectedBackgroundSkills) {
      updateAge()
      updateSkills()
    }
  } else {
    state = initialState
  }
}

export function logEvent(text) {
  state.events.push(text)
}

export function ordinal(number) {
  switch (number) {
    case 1:
      return 'first'
    case 2:
      return 'second'
    case 3:
      return 'third'
  }
}

export function remove(array, item) {
  return array.filter((x) => x !== item)
}

export function reset() {
  localStorage.clear()
  state = initialState
  hide(ageContainer)
  hide(characteristics)
  hide(skills)
  initializeNextView()
}

export function roll(rating = 0) {
  return Math.floor(1 + Math.random() * 6 + (1 + Math.random() * 6) + rating)
}

export function save() {
  window.localStorage.setItem('state', JSON.stringify(state))
}

export function scoreMod(score) {
  if (score < 1) return -3
  if (score < 3) return -2
  if (score < 6) return -1
  if (score < 9) return 0
  if (score < 12) return 1
  if (score < 15) return 2
  return 3
}

export function scoreRating(score) {
  return score.toString(16).toUpperCase()
}

export function show(element) {
  element.classList.remove('-hidden')
}

export function sum(...xs) {
  return xs.reduce((y, x) => y + x, 0)
}

export function toggleButton(textContent, onSelect, onUnselect) {
  return h('button', {
    is: 'toggle-button',
    on: {
      select: onSelect,
      unselect: onUnselect
    },
    textContent,
    value: textContent
  })
}

export function updateAge() {
  age.textContent = state.age

  show(characterInfo)
}

export function updateCharacteristics() {
  $('#Strength').setValue(state.strength)
  $('#Dexterity').setValue(state.dexterity)
  $('#Endurance').setValue(state.endurance)
  $('#Intellect').setValue(state.intellect)
  $('#Education').setValue(state.education)
  $('#SocialStanding').setValue(state.socialStanding)

  $('#StrengthRating').textContent = scoreRating(state.strength)
  $('#DexterityRating').textContent = scoreRating(state.dexterity)
  $('#EnduranceRating').textContent = scoreRating(state.endurance)
  $('#IntellectRating').textContent = scoreRating(state.intellect)
  $('#EducationRating').textContent = scoreRating(state.education)
  $('#SocialStandingRating').textContent = scoreRating(state.socialStanding)

  show(characteristics)
}

export function updateSkills() {
  skillsList.innerHTML = ''

  for (const skill of state.skills) {
    skillsList.appendChild(h('li', { textContent: skill }))
  }

  show(skills)
}
