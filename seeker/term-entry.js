import { $$, h, actionItem, infoItem, state } from './utilities.js'
import { agentDetails, agentQualification } from './careers/agent.js'
import { armyDetails, armyQualification } from './careers/army.js'
import { citizenDetails, citizenQualification } from './careers/citizen.js'
import { drifterDetails, drifterQualification } from './careers/drifter.js'
import { marineDetails, marineQualification } from './careers/marine.js'
import { merchantDetails, merchantQualification } from './careers/merchant.js'
import { navyDetails, navyQualification } from './careers/navy.js'
import { nobleDetails, nobleQualification } from './careers/noble.js'
import { rogueDetails, rogueQualification } from './careers/rogue.js'
import { scholarDetails, scholarQualification } from './careers/scholar.js'
import { scoutDetails, scoutQualification } from './careers/scout.js'
import { universityDetails, universityEntry } from './careers/university.js'

import {
  entertainerDetails,
  entertainerQualification
} from './careers/entertainer.js'

import {
  militaryAcademyDetails,
  militaryAcademyEntryChanceString
} from './careers/military-academy.js'

export function setupTermEntry(onContinue) {
  const infoColumn = h('div.column')

  function show(view) {
    return () => {
      $$('button.choice.-selected').forEach((b) =>
        b.classList.remove('-selected')
      )
      infoColumn.innerHTML = ''
      infoColumn.append(...view())
    }
  }

  const view = [
    h('h1', 'Choose Term Action'),
    h('div.termactions', [
      h('div.column', [
        state.termsPassed < 3 &&
          actionItem(
            `Attend University ${universityEntry().chanceString}`,
            show(universityDetails)
          ),
        state.termsPassed < 3 &&
          actionItem(
            `Attend Military Academy ${militaryAcademyEntryChanceString()}`,
            show(militaryAcademyDetails)
          ),
        actionItem(
          `Join Agent Career ${agentQualification().chanceString}`,
          show(agentDetails)
        ),
        actionItem(
          `Join Army Career ${armyQualification().chanceString}`,
          show(armyDetails)
        ),
        actionItem(
          `Join Citizen Career ${citizenQualification().chanceString}`,
          show(citizenDetails)
        ),
        actionItem(
          `Join Drifter Career ${drifterQualification().chanceString}`,
          show(drifterDetails)
        ),
        actionItem(
          `Join Entertainer Career ${entertainerQualification().chanceString}`,
          show(entertainerDetails)
        ),
        actionItem(
          `Join Marine Career ${marineQualification().chanceString}`,
          show(marineDetails)
        ),
        actionItem(
          `Join Merchant Career ${merchantQualification().chanceString}`,
          show(merchantDetails)
        ),
        actionItem(
          `Join Navy Career ${navyQualification().chanceString}`,
          show(navyDetails)
        ),
        actionItem(
          `Join Noble Career ${nobleQualification().chanceString}`,
          show(nobleDetails)
        ),
        actionItem(
          `Join Rogue Career ${rogueQualification().chanceString}`,
          show(rogueDetails)
        ),
        actionItem(
          `Join Scholar Career ${scholarQualification().chanceString}`,
          show(scholarDetails)
        ),
        actionItem(
          `Join Scout Career ${scoutQualification().chanceString}`,
          show(scoutDetails)
        )
      ]),
      infoColumn
    ])
  ]

  // view[1].children[0].children[12].dispatchEvent(new Event('click'))

  return view
}
