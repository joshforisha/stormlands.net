import {
  disable,
  enable,
  h,
  insert,
  remove,
  save,
  scoreMod,
  state,
  toggleButton,
  updateAge,
  updateSkills
} from './utilities.js'

export function setupBackgroundSkills(onContinue) {
  const numBackgroundSkills = scoreMod(state.education) + 3
  let selections = []

  const title = h('h1', 'Background Skills')
  const continueButton = h('button.green', {
    textContent: 'Continue',
    on: {
      click: () => {
        state.skills = selections
        state.selectedBackgroundSkills = true
        save()
        updateSkills()
        updateAge()
        onContinue()
      }
    }
  })

  const actions = h('div.actions', [continueButton])

  if (numBackgroundSkills < 1) {
    return [
      title,
      h('p', 'You don’t have enough INT to select any background skills.'),
      actions
    ]
  }

  const selectionsDiv = h('div.blocks')
  disable(continueButton)

  function handleSelect(event) {
    selections = insert(selections, event.target.value)
    if (selections.length >= numBackgroundSkills) {
      for (const button of selectionsDiv.querySelectorAll(
        'button:not([data-selected])'
      )) {
        disable(button)
      }
      enable(continueButton)
    }
  }

  function handleUnselect(event) {
    selections = remove(selections, event.target.value)
    if (selections.length < numBackgroundSkills) {
      for (const button of selectionsDiv.querySelectorAll('button:disabled')) {
        enable(button)
      }
      disable(continueButton)
    }
  }

  selectionsDiv.append(
    toggleButton('Admin 0', handleSelect, handleUnselect),
    toggleButton('Animals 0', handleSelect, handleUnselect),
    toggleButton('Art 0', handleSelect, handleUnselect),
    toggleButton('Athletics 0', handleSelect, handleUnselect),
    toggleButton('Carouse 0', handleSelect, handleUnselect),
    toggleButton('Drive 0', handleSelect, handleUnselect),
    toggleButton('Electronics 0', handleSelect, handleUnselect),
    toggleButton('Flyer 0', handleSelect, handleUnselect),
    toggleButton('Language 0', handleSelect, handleUnselect),
    toggleButton('Mechanic 0', handleSelect, handleUnselect),
    toggleButton('Medic 0', handleSelect, handleUnselect),
    toggleButton('Profession 0', handleSelect, handleUnselect),
    toggleButton('Science 0', handleSelect, handleUnselect),
    toggleButton('Seafarer 0', handleSelect, handleUnselect),
    toggleButton('Streetwise 0', handleSelect, handleUnselect),
    toggleButton('Survival 0', handleSelect, handleUnselect),
    toggleButton('Vacc Suit 0', handleSelect, handleUnselect)
  )

  return [
    title,
    h('p', `Select ${numBackgroundSkills} (EDU DM + 3) background skills.`),
    selectionsDiv,
    actions
  ]
}
