const mdAnchor = require('markdown-it-anchor')

module.exports = (config) => {
  config.addPassthroughCopy('favicon.ico')
  config.addPassthroughCopy('icon.svg')
  config.addPassthroughCopy('manifest.json')

  config.setTemplateFormats(['css', 'html', 'js', 'md', 'njk', 'png'])

  config.setServerOptions({
    domDiff: false
  })

  const markdown = require('markdown-it')({
    breaks: true,
    html: true,
    linkify: true
  }).use(mdAnchor)

  config.setLibrary('md', markdown)

  return {
    dir: {
      output: 'public'
    }
  }
}
