# Sitcom World

Every session of gameplay is one **episode** of the sitcom.
Each episode is divided into **scenes**.

Characters can either be _present_ in the scene (can interact with others and earn favor) or _absent_ from the scene (can tend to personal matters and setup background details). Some Moves are only possible in one mode or the other, and characters can enter or exit the scene in a variety of ways.

## Characters
Players control *main characters,* and the GM controls *supporting characters.*

**Stats:**
- _Brains_ are how generally intelligent you are, and how well-versed you are in studied topics.
- _Heart_ is how outwardly empathetic you are, and your general level of kindness towards others.
- _Looks_ are how physically attractive you are to others.
- *Words* are how well you speak and your ability to communicate clearly.
- _Quirk_ is how odd, off-putting, or otherwise strange-seeming to others you typically are.

**Drive:** what you want to accomplish throughout an episode. You mark XP once per scene, whenever the Drive is realized.

**Stress:** how stressed out you currently are, rated up to 3. When a character is at max Stress and suffers more, a ***FREAK OUT*** is triggered automatically, with the GM in control of the resulting situation. Stress is cleared at the end of the episode.

**Relief:** how the character relieves Stress. This typically happens when a character is absent from the scene, but can also be a featured scene itself and involve other characters.

**Bonds:** how the character feels about each of the other main characters, rated between -3 (loathe them) and +3 (best friend).

The following Common Moves apply to all characters.

> #### **DEAL WITH IT** _(Common Move)_
> When **you are faced with an obstacle to overcome and:**
> - attempt to out-think it, roll +Brains.
> - appeal to someone's feelings to solve it, roll +Heart.
> - use your appearance to face it, roll +Looks.
> - try to talk your way through it, roll +Words.
> - act in an unexpected way, roll +Quirk.
> 
> **On a 10+,** describe how you successfully deal with the obstacle.
> **On a 7–9,** you succeed, but the GM will narrate and offer you a hard choice.

> #### **FREAK OUT** _(Common Move)_
> When **you are stressed out and willing to raise hell,** roll -Stress.
> 
> **On a 10+,** describe the loud commotion you cause and remove _2 Stress_.
> **On a 7–9,** describe your dramatic rant, and remove _1 Stress_. The GM describes the ensuing tumult.
> **On a 6-,** the GM describes the outrageous brouhaha you provoke.
> ---
> When **you are _fully_ stressed out and take on more stress,** the GM describes the pandemonium you unleash. Roll -Stress.
> 
> **On a 7+,** you feel somewhat better. Remove _1 Stress_.
> **On a 6-,** you don't feel any relief at all. The GM narrates the ensuing chaos.

> #### **MAKE AN ENTRANCE** _(Common Move)_
> When **you are absent and enter the scene:**
> - with an intelligent tidbit or enlightening response to something just said, roll +Brains.
> - with an emotional statement or personal announcement, roll +Heart.
> - with an attention-grabbing appearance, roll +Looks.
> - with a sarcastic remark or witty revelation, roll +Words.
> - in an unconventional or perplexing way, roll +Quirk.
> 
> **On a 10+,** describe your entertaining entrance and the effect it has on those present. Take _+2 forward._
> **On a 7–9,** describe your underwhelming entrance and take _+1 forward._ Any main characters that are present describe their lukewarm responses.
> **On a 6-,** your entrance doesn't go well. The GM narrates the minor distraction.

> #### **TAKE IT EASY** _(Common Move)_
> When **you are:**
> - **absent from the scene and partake in your relief activity for the duration,** _or_
> - **present in the scene while your relief activity is featured,**
> roll -Stress.
> 
> **On a 10+,** describe the immense relief and remove _2 Stress_.
> **On a 7–9,** remove _1 Stress_ and choose a complication:
> - The activity takes far longer than expected. You're absent for part of the next scene.
> - Something about the activity is different than normal, impacting your relaxation.
> - A character is unexpectedly involved, and disrupts your experience.
> 
> **On a 6-,** you're distracted or prevented from relaxing. The GM chooses a complication.

### _The Anchor_ Archetype
_Intelligent and grounded, they're often the pillar of the group. Other characters likely don't take them as seriously as they'd like, leading to the Anchor having to clean up problems that otherwise could have been prevented._

_They think they're teachers to those around them who don't know better, but they're actually co-dependent busy-bodies who make themselves feel better by surrounding themselves with those who are less intelligent._

**Stats:** Start with _Quirk -1_. Assign ratings to other Stats: +2, +1, +1, +0.
**Relief:** Describe a somewhat normal activity you use to relieve stress.

**Drive:** Choose one and fill in details:
- Make _{someone}_ see your side on a topic.
- Restore order at _{location}._
- Lead anyone to a better fate.

**Moves:** Choose two starting Moves from below.

> #### **EXPLAIN THE SITUATION** _(Anchor Move)_
> When **you carefully explain the current situation to a main character,** roll +Brains.
> **On a 10+,** they're on the same page as you. You each gain _+1 forward._
> **On a 7–9,** they mostly seem to follow. They gain _+1 forward._ Choose a complication:
> - You get frustrated trying to explain. Gain _1 Stress._
> - They get frustrated or annoyed by you. Their Bond with you is reduced by one.
> - They misunderstand a critical detail. Their advantage applies to their version.
> 
> **On a 6-,** they only end up confused. The GM chooses a complication.

> #### **NOT MY PROBLEM** _(Anchor Move)_
> When **you are in harm's way and excuse yourself from the situation,** you can exit the scene safely.

> #### **SHARP TONGUE** _(Anchor Move)_
> When **you sarcastically respond to another character,** roll +Words.
> **On a 10+,** you recover _1 Stress,_ **and:**
> **On a 7+:**
> - if they're a main character, they suffer _1 Stress._
> - If they're a supporting character, you narrate their public embarrassment or exit from the scene.

> #### ==MOVE== _(Anchor Move)_

> #### ==MOVE== _(Anchor Move)_

### _The Dreamer_ Archetype
_An eternal optimist with a healthy dose of self-deprecating humor. They tend to act and be shaped more directly by their desires._

_They think they're capable and ambitious individuals who are victimized by unfortunate life circumstances, but are actually charming and hopeful jokesters with a Peter Pan complex._

**Stats:** Start with *Words -1.* Assign ratings to other Stats: +2, +1, +1, +0.
**Relief:** Describe a surprisingly calming activity that others would typically only be bored by.

**Drive:** Choose one and fill in details:
- ==Drive==
- ==Drive==
- ==Drive==

**Moves:** Choose two starting Moves from below.

> #### ==MOVE== _(Dreamer Move)_

> #### ==MOVE== _(Dreamer Move)_

> #### ==MOVE== _(Dreamer Move)_

> #### ==MOVE== _(Dreamer Move)_

> #### ==MOVE== _(Dreamer Move)_

### _The Neurotic_ Archetype
_Defined by insecurity filtered through their intelligence. They have a big brain that can process all possible outcomes, and often find themselves quite overwhelmed._

_They think they're the rightful boss of people because they've thought about it and know better, but are actually prickly know-it-alls whom everyone loves but knows to avoid when they're stressed._

**Stats:** Start with _Brains +2._ Assign ratings to other Stats: +1, +1, +0, -1.
**Relief:** Describe a tedious, potentially exhausting activity that you find ironically relaxing.

**Drive:** Choose one and fill in details:
- ==Drive==
- ==Drive==
- ==Drive==

**Moves:** Choose two starting Moves from below.

> #### **THINK ABOUT IT** _(Neurotic Move)_
> When **you think hard about a subject,** roll +Brains.
> 
> **On a 10+,** take _+1 forward_ on the information provided when you:
> **On a 7+,** ask one question about the subject. The GM answers truthfully.

> #### ==MOVE== _(Neurotic Move)_

> #### ==MOVE== _(Neurotic Move)_

> #### ==MOVE== _(Neurotic Move)_

> #### ==MOVE== _(Neurotic Move)_

### _The Rebel_ Archetype
_Has a god complex; their disdain for life's rules drives them to danger and deceit. They believe they can do anything they want and get away with it._

_They think they're untouchable masterminds who give away nothing and are aware of everything, but they're actually social terrorists who incite fear in those around them._

**Stats:** Start with *Words +2.* Assign ratings to other Stats: +1, +1, +0, -1.
**Relief:** Describe an aggressive, intensive activity that lets you blow off steam.

**Drive:** Choose one and fill in details:
- ==Drive==
- ==Drive==
- ==Drive==

**Moves:** Choose two starting Moves from below.

> #### **INTIMIDATE** _(Rebel Move)_
> When you **imply harm in order to force someone into doing something they wouldn't normally do,** roll +Words.
> 
> **On a 10+,** they're convinced. Describe their desperate willingness and successful action.
> **On a 7–9,** they go through with it, but half-heartedly. Choose a complication:
> - Someone nearby witnesses your bullying, and steps in.
> - They misunderstand the request, and perform their interpretation.
> - The requested act inadvertedly impacts another main character in the opposite intended way.
> 
> **On a 6-,** your attempt is laughed off. The GM describes your failure.

> #### ==MOVE== _(Rebel Move)_

> #### ==MOVE== _(Rebel Move)_

> #### ==MOVE== _(Rebel Move)_

> #### ==MOVE== _(Rebel Move)_

### _The Innocent_ Archetype
_Sweet and lovable; pure as the driven snow. They are often naive, but are very trustworthy. They think they're nice, but they're actually special._

**Stats:** Start with _Heart +2._ Assign ratings to other Stats: +1, +1, +0, -1.
**Relief:** Describe a mundane, borderline virtuous activity that allows you to let go of your worries.

**Drive:** Choose one and fill in details:
- ==Drive==
- ==Drive==
- ==Drive==

**Moves:** Choose two starting Moves from below.

> #### **COMMISERATE** _(Innocent Move)_
> When **you patiently listen to a main character's woes,** roll +Bond.
> 
> **On a 10+,** you offer them support. They remove _1 Stress._
> **On a 7–9,** as above, but choose a complication:
> - Their complaints wear on you. Your Bond with them is reduced by one.
> - They transfer their woes to you. You gain _1 Stress._
> - You get involved in their concerns. You spend a scene with them in their relief activity.
> 
> **On a 6-,** your listening does no good. The GM chooses a complication.

> #### **EMPATHIZE** _(Innocent Move)_
> When **you relate to someone to understand them more deeply,** roll +Heart.
> 
> **On a 7+,** you deduce the person's true intentions in the current situation. Take _+1 forward_ when acting to support them.

> #### ==MOVE== _(Innocent Move)_

> #### ==MOVE== _(Innocent Move)_

> #### ==MOVE== _(Innocent Move)_

### _The Eccentric_ Archetype
_Hyper-connected to the world—for better or worse—invested and curious._

_They think they're adventurers in a dull world, but are actually quirky friends that others love but don't trust with much responsibility._

**Stats:** Start with _Quirk +2._ Assign ratings to Stats: +1, +1, +0, -1.
**Relief:** Describe an utterly strange, unexpected activity that you enjoy.

**Drive:** Choose one and fill in details:
- ==Drive==
- ==Drive==
- ==Drive==

**Moves:** Choose two starting Moves from below.

> #### **PORTRAY** _(Eccentric Move)_
> _Take on a persona to confuse or distract (+Quirk)._

> #### ==MOVE== _(Eccentric Move)_

> #### ==MOVE== _(Eccentric Move)_

> #### ==MOVE== _(Eccentric Move)_

> #### ==MOVE== _(Eccentric Move)_

### _The Buffoon_ Archetype
_Socially inept with often iffy intentions._

_They think they're smart enough to take the lazy way out and still win, but they're actually constant losers who are just shy of being self-aware._

**Stats:** Start with _Brains -1._ Assign ratings to other Stats: +2, +1, +1, +0.
**Relief:** Describe a nuanced activity that you've utterly misunderstood the usual point of, but find relaxing or enjoyable anyway.

**Drive:** Choose one and fill in details:
- ==Drive==
- ==Drive==
- ==Drive==

**Moves:** Choose two starting Moves from below.

> #### ==MOVE== _(Buffoon Move)_

> #### ==MOVE== _(Buffoon Move)_

> #### ==MOVE== _(Buffoon Move)_

> #### ==MOVE== _(Buffoon Move)_

> #### ==MOVE== _(Buffoon Move)_

### _The Cynic_ Archetype
_A world-weary defeatist; while often negative, they are simultaneously wonderful friends, strong allies, and invested in life._

_They think they're cautious and smart enough to know the other shoe is always about to drop, but they're actually downers who consistently spoil the mood._

**Stats:** Start with _Looks -1._ Assign ratings to other Stats: +2, +1, +1, +0.
**Relief:** Describe a wholly depressing activity that you enjoy wallowing in.

**Drive:** Choose one and fill in details:
- Deflate _{someone}'s_ good mood.
- Change anyone's opinion of _{location}._
- One-up anyone's depressing story or situation with your own.

**Moves:** Choose two starting Moves from below.

> #### ==MOVE== _(Cynic Move)_

> #### ==MOVE== _(Cynic Move)_

> #### ==MOVE== _(Cynic Move)_

> #### ==MOVE== _(Cynic Move)_

> #### ==MOVE== _(Cynic Move)_

### _The Narcissist_ Archetype
_Love themselves and things, in exactly that order. "Entitled" is a very particular quality, which this character exhibits to an inordinate degree._

_They think they're super beings who deserve what they desire with little effort, but they're actually spoiled brats whom everyone wants to see fail._

**Stats:** Start with _Heart -1._ Assign ratings to other Stats: +2, +1, +1, +0.
**Relief:** Describe a solely selfish activity you enjoy doing, while taking advantage of or massively inconveniencing others.

**Drive:** Choose one and fill in details:
- Receive an authentic compliment from _{someone}._
- Become the center of attention at _{location}._
- Convincingly show anyone how much better you are at _{activity}_ than they are, despite actually being less skilled.

**Moves:** Choose two starting Moves from below.

> #### ==MOVE== _(Narcissist Move)_

> #### ==MOVE== _(Narcissist Move)_

> #### ==MOVE== _(Narcissist Move)_

> #### ==MOVE== _(Narcissist Move)_

> #### ==MOVE== _(Narcissist Move)_

### _The Player_ Archetype
_Lives in pursuit of just one thing: sex with no strings attached. They're fun, bold, and sexually charged, but generally lack substance._

_They think they're the life of the party 24/7, but they're actually lonely because they don't know how to form lasting relationships._

**Stats:** Start with _Looks +2._ Assign ratings to other Stats: +1, +1, +0, -1.
**Relief:** Describe an overly dramatic, morally reprehensible activity you enjoy.

**Drive:** Choose one and fill in details:
- Be recognized as more attractive than _{person}._
- Draw the most positive attention at _{location}._
- ==Drive==

**Moves:** Choose two starting Moves from below.

> #### **CHARM** _(Player Move)_
> When you **attempt to entice or seduce someone into doing something they wouldn't normally do,** roll +Looks.
> 
> **On a 10+,** describe their reaction and how they follow through successfully.
> **On a 7–9,** they begrudgingly cooperate. Choose a complication:
> - Your involvement is publicly known. Any main character acting against you with this information gains _+1 forward._
> - You promise the person something substantial in exchange. A main character can choose; the GM chooses for a supporting character.
> - The requested act doesn't go smoothly, and backfires for both of you.
> 
> **On a 6-,** your attempt is an embarrassing failure. The GM describes the resulting events.

> #### ==MOVE== _(Player Move)_

> #### ==MOVE== _(Player Move)_

> #### ==MOVE== _(Player Move)_

> #### ==MOVE== _(Player Move)_

## Playing the Game
Each episode has a central _problem_ that the main characters should be compelled to deal with, where the outcome would be disastrous for them if not handled correctly.

### Cold Open
The GM outlines an introductory scene involving some or all of the main characters. This scene can be:
- a follow-up to events from the previous episode, showing the characters' changing circumstances
- an isolated interaction at a typical location the group finds themselves in
- a foreshadowing of the episode's problem that the group needs to deal with

The goal should be to establish the characters' current situation(s) and set the focus for the episode. The episode's problem might not necessarily be evident from this scene, but should at least be hinted at.

Any XP earned during the Cold Open is doubled.
The Cold Open shouldn't last very long, and can end abruptly by the GM.

### Normal Scenes
After the Cold Open, the GM will hand one of the players control of the first main scene of the episode. The player will outline the location, time of day, and any important scenery. The GM selects which characters are initially present, main and supporting.

Once the scene comes to a conclusion (either the main characters move on naturally, or the GM calls an end) the player who crafted the scene chooses another player to outline the next one. Play continues in this way until the plots of the episode are concluded satisfyingly, or when the GM calls an end.

### End of Episode
Each character gains 1 XP for each question they can justifiably answer "yes" to.

- Did you experience character growth?
- Did you complicate a situation to your own advantage?
- Did you further a plot that substantially impacted other main characters?
- Was the episode's main problem dealt with successfully?

### Closing Scene
The GM outlines a final shorter scene focusing on the main character who gained the most XP this episode. This is an opportunity for the character to revel in their success.

### Spending XP
Once the episode scenes are concluded, you can spend any XP on the following, as many times as you'd like with your available points.

- **3 XP:** Change your Drive based on events in this episode.
- **5 XP:** Change your Relief based on events in this episode.
- **8 XP:** Swap two Skill ratings.
- **10 XP:** Purchase* a new Move from your Archetype's playbook.
- **15 XP:** Purchase* a new Move from another Archetype's playbook.
- **20 XP:** Adjust your Skills, keeping the overall _+2, +1, +1, +0, -1_ ratings.
- **30 XP:** Choose a new Archetype, then Relief and Drive from their playbook. You keep your current Skill ratings and Moves. Spend a scene in the next episode playing out your personal changes.

_* You can only have 5 Archetype Moves, and must replace existing Moves once you've reached that limit._