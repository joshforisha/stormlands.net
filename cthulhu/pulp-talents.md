---
layout: cthulhu.njk
---

<nav onclick="this.classList.toggle('-open')">
  <ul>
    <li><a href="#alert">Alert</a></li>
    <li><a href="#animal-companion">Animal Companion</a></li>
    <li><a href="#arcane-insight">Arcane Insight</a></li>
    <li><a href="#beady-eye">Beady Eye</a></li>
    <li><a href="#endurance">Endurance</a></li>
    <li><a href="#fast-load">Fast Load</a></li>
    <li><a href="#fleet-footed">Fleet Footed</a></li>
    <li><a href="#gadget">Gadget</a></li>
    <li><a href="#handy">Handy</a></li>
    <li><a href="#hardened">Hardened</a></li>
    <li><a href="#heavy-hitter">Heavy Hitter</a></li>
    <li><a href="#iron-liver">Iron Liver</a></li>
    <li><a href="#keen-hearing">Keen Hearing</a></li>
    <li><a href="#keen-vision">Keen Vision</a></li>
    <li><a href="#linguist">Linguist</a></li>
    <li><a href="#lore">Lore</a></li>
    <li><a href="#lucky">Lucky</a></li>
    <li><a href="#master-of-disguise">Master of Disguise</a></li>
    <li><a href="#mythos-knowledge">Mythos Knowledge</a></li>
    <li><a href="#night-vision">Night Vision</a></li>
    <li><a href="#nimble">Nimble</a></li>
    <li><a href="#outmaneuver">Outmaneuver</a></li>
    <li><a href="#photographic Memory">Photographic Memory</a></li>
    <li><a href="#power-lifter">Power Lifter</a></li>
    <li><a href="#psychic-power">Psychic Power</a></li>
    <li><a href="#quick-draw">Quick Draw</a></li>
    <li><a href="#quick-healer">Quick Healer</a></li>
    <li><a href="#quick-study">Quick Study</a></li>
    <li><a href="#rapid-attack">Rapid Attack</a></li>
    <li><a href="#rapid-fire">Rapid Fire</a></li>
    <li><a href="#resilient">Resilient</a></li>
    <li><a href="#resourceful">Resourceful</a></li>
    <li><a href="#scary">Scary</a></li>
    <li><a href="#shadow">Shadow</a></li>
    <li><a href="#sharp-witted">Sharp Witted</a></li>
    <li><a href="#smooth-talker">Smooth Talker</a></li>
    <li><a href="#stout-constitution">Stout Constitution</a></li>
    <li><a href="#strong-willed">Strong Willed</a></li>
    <li><a href="#tough-guy">Tough Guy</a></li>
    <li><a href="#weird-science">Weird Science</a></li>
  </ul>
  <button>&equiv;</button>
</nav>

<main>

# Pulp Talents

## Physical Talents

1. <strong id="keen-vision">Keen Vision</strong>: gain a bonus die to Spot Hidden rolls.
2. <strong id="quick-healer">Quick Healer</strong>: natural healing is increased to +3 hit points per day.
3. <strong id="night-vision">Night Vision</strong>: in darkness, reduce the difficulty level of Spot Hidden rolls and ignore penalty die for shooting in the dark.
4. <strong id="endurance">Endurance</strong>: gain a bonus die when making CON rolls (including to determine MOV rate for chases).
5. <strong id="power-lifter">Power Lifter</strong>: gain a bonus die when making STR rolls to lift objects or people.
6. <strong id="iron-liver">Iron Liver</strong>: may spend 5 Luck to avoid the effects of drinking excessive amounts of alcohol (negating penalty applied to skill rolls).
7. <strong id="stout-constitution">Stout Constitution</strong>: may spend 10 Luck to reduce poison or disease damage and effects by half.
8. <strong id="tough-guy">Tough Guy</strong>: soaks up damage, may spend 10 Luck points to shrug off up to 5 hit points worth of damage taken in one combat round.
9. <strong id="keen-hearing">Keen Hearing</strong>: gain a bonus die to Listen rolls.
10. <strong id="smooth-talker">Smooth Talker</strong>: gain a bonus die to Charm rolls.

## Mental Talents

1. <strong id="hardened">Hardened</strong>: ignores Sanity point loss from attacking other humans, viewing horrific injuries, or the deceased.
2. <strong id="resilient">Resilient</strong>: may spend Luck points to shrug-off points of Sanity loss, on a one-for-one basis.
3. <strong id="strong-willed">Strong Willed</strong>: gains a bonus die when making POW rolls.
4. <strong id="quick-study">Quick Study</strong>: halve the time required for Initial and Full Reading of Mythos tomes, as well as other books.
5. <strong id="linguist">Linguist</strong>: able to determine what language is being spoken (or what is written); gains a bonus die to Language rolls.
6. <strong id="arcane-insight">Arcane Insight</strong>: halve the time required to learn spells and gains bonus die to spell casting rolls.
7. <strong id="photographic-memory">Photographic Memory</strong>: can remember many details; gains a bonus die when making Know rolls.
8. <strong id="lore">Lore</strong>: has knowledge of a lore specialization skill (e.g., Dream Lore, Vampire Lore, Werewolf Lore, etc.). Note that occupational and/or personal interest skill points should be invested in this skill.
9. <strong id="psychic-power">Psychic Power</strong>: may choose one psychic power (Clairvoyance, Divination, Medium, Psychometry, or Telekinesis). Note that occupational and/or personal interest skill points should be invested in this skill (See Psychic Powers, page 83).
10. <strong id="sharp-witted">Sharp Witted</strong>: able to collate facts quickly; gain a bonus die when making Intelligence (but not Idea) rolls.

## Combat Talents

1. <strong id="alert">Alert</strong>: never surprised in combat.
2. <strong id="heavy-hitter">Heavy Hitter</strong>: may spend 10 Luck points to add an additional damage die when dealing out melee combat (die type depends on the weapon being used, e.d. 1d3 for unarmed combat, 1d6 for a sword, etc.).
3. <strong id="fast-load">Fast Load</strong>: choose a Firearm specialism; ignore penalty die for loading and firing in the same round.
4. <strong id="nimble">Nimble</strong>: does not lose next action when "diving for cover" versus firearms.
5. <strong id="beady-eye">Beady Eye</strong>: does not suffer penalty die when "aiming" at a small target (Build -2), and may also fire into melee without a penalty die.
6. <strong id="outmaneuver">Outmaneuver</strong>: character is considered to have one point higher Build when initiating a combat maneuver (e.g. Build 1 becomes Build 2 when comparing their hero to the target in a maneuver, reducing the likelihood of suffering a penalty on their Fighting roll).
7. <strong id="rapid-attack">Rapid Attack</strong>: may spend 10 Luck points to gain one further melee attack in a single combat round.
8. <strong id="fleet-footed">Fleet Footed</strong>: may spend 10 Luck to avoid being "outnumbered" in melee combat for one combat encounter.
9. <strong id="quick-draw">Quick Draw</strong>: does not need to have their firearm "readied" to gain +50 DEX when determining position in the DEX order for combat.
10. <strong id="rapid-fire">Rapid Fire</strong>: ignores penalty die for multiple handgun shots.

## Miscellaneous Talents

1. <strong id="scary">Scary</strong>: reduces difficulty by one level or gains bonus die (at the Keeper's discretion) to Intimidate rolls.
2. <strong id="gadget">Gadget</strong>: starts game with one weird science gadget (see Weird Science, page 86).
3. <strong id="lucky">Lucky</strong>: regains an additional +1d10 Luck points when Luck Recovery rolls are made.
4. <strong id="mythos-knowledge">Mythos Knowledge</strong>: begins the game with a Cthulhu Mythos Skill of 10 points.
5. <strong id="weird-science">Weird Science</strong>: may build and repair weird science devices (see Weird Science, page 86).
6. <strong id="shadow">Shadow</strong>: reduces difficulty by one level or gains bonus die (at the Keeper's discretion) to Stealth rolls, and if currently unseen is able to make two surprise attacks before their location is discovered.
7. <strong id="handy">Handy</strong>: reduces difficulty by one level or gains bonus die (at the Keeper's discretion) when making Electrical Repair, Mechanical Repair, and Operate Heavy Machinery rolls.
8. <strong id="animal-companion">Animal Companion</strong>: starts game with a faithful animal companion (e.g. dog, cat, parrot) and gains a bonus die when making Animal Handling rolls.
9. <strong id="master-of-disguise">Master of Disguise</strong>: may spend 10 Luck points to gain a bonus die to Disguise or Art/Craft (Acting) rolls; includes ventriloquism (able to throw voice over long distances so it appears that the sound is emanating from somewhere other than the hero). Note that if someone is trying to detect the disguise their Spot Hidden or Psychology roll's difficulty is raised to Hard.
10. <strong id="resourceful">Resourceful</strong>: always seems to have what they need to hand; may spend 10 Luck points (rather than make Luck roll) to find a certain useful piece of equipment (e.g. a flashlight, length of rope, a weapon, etc.) in their current location.

</main>
