---
layout: cthulhu.njk
---

<main>

# Weapons

## Hand-to-Hand Weapons

| Name | Skill | Damage | Range | Uses | Mag | Cost | Mal. |
| ---- | ----- | ------ | ----- |:----:|:---:|:----:|:----:|
| Blackjack | Fighting (Brawl) | 1D8+DB | Touch | 1 | – | $2 / $15 | – |
| Bow and Arrows | Firearms (Bow) | 1D6+half DB | 30 yd | 1 | 1 | $7 / $75 | 97 |
| Brass Knuckles | Fighting (Brawl) | 1D3+1+DB | Touch | 1 | – | $1 / $10 | – |
| Bullwhip | Fighting (Whip) | 1D3+half DB | 10 ft | 1 | – | $5 / $50 | – |
| Burning Torch | Fighting (Brawl) | 1D6+Burn | Touch | 1 | – | 5¢ / 50¢ | – |
| Chainsaw *(i)* | Fighting (Chainsaw) | 2D8 | Touch | 1 | – | – / $300 | 95 |
| Club, large | Fighting (Brawl) | 1D8+DB | Touch | 1 | – | $3 / $35 | – |
| Club, small | Fighting (Brawl) | 1D6+DB | Touch | 1 | – | $3 / $35 | – |
| Crossbow *(i)* | Firearms (Bow) | 1D8+2 | 50 yd | 1/2 | 1 | $10 / $100 | 96 |
| Garrote *(i)* | Fighting (Garrote) | 1D6+DB | Touch | 1 | – | 50¢ / $3 | – |
| Hatchet/Sickle *(i)* | Fighting (Axe) | 1D6+1+DB | Touch | 1 | – | $3 / $9 | – |
| Knife, large *(i)* | Fighting (Brawl) | 1D8+DB | Touch | 1 | – | $4 / $50 | – |
| Knife, medium *(i)* | Fighting (Brawl) | 1D4+2+DB | Touch | 1 | – | $2 / $15 | – |
| Knife, small *(i)* | Fighting (Brawl) | 1D4+DB | Touch | 1 | – | $2 / $6 | – |
| Live Wire, 220V | Fighting (Brawl) | 2D8+Stun | Touch | 1 | – | – | 95 |
| Mace Spray | Fighting (Brawl) | Stun | 6 ft | 1 | 25 | – / $10 | – |
| Nunchaku | Fighting (Flail) | 1D8+DB | Touch | 1 | – | $1 / $10 | – |
| Rock, thrown | Throw | 1D4+half DB | STR/5 yd | 1 | – | – | – |
| Shuriken *(i)* | Throw | 1D3+half DB | STR/5 yd | 2 | 1 | 50¢ / $3 | 100 |
| Spear *(i)* | Fighting (Spear) | 1D8+1 | Touch | 1 | – | $25 / $150 | – |
| Spear, thrown *(i)* | Throw | 1D8+half DB | STR/5 yd | 1 | – | $1 / $25 | – |
| Sword, heavy | Fighting (Sword) | 1D8+1+DB | Touch | 1 | – | $30 / $75 | – |
| Sword, medium *(i)* | Fighting (Sword) | 1D6+1+DB | Touch | 1 | – | $15 / $100 | – |
| Sword, light *(i)* | Fighting (Sword) | 1D6+DB | Touch | 1 | – | $25 / $100 | – |
| Taser (contact) | Fighting (Brawl) | 1D3+Stun | Touch | 1 | Varies | – / $200 | 97 |
| Taser (dart) | Firearms (Handgun) | 1D3+Stun | 15 ft | 1 | 3 | – / $400 | 95 |
| War Boomerang | Throw | 1D8+half DB | STR/5 yd | 1 | – | $2 / $4 | – |
| Wood Axe | Fighting (Axe) | 1D8+2+DB | Touch | 1 | – | $5 / $10 | – |

## Handguns *(i)*

| Name | Skill | Damage | Range | Uses | Mag | Cost | Mal. |
| ---- | ----- | ------ | ----- |:----:|:---:|:----:|:----:|
| Flintlock | Firearms (Handgun) | 1D6+1 | 10 yd | 1/4 | 1 | $30 / $300 | 95 |
| .22 Short Automatic | Firearms (Handgun) | 1D6 | 10 yd | 1 (3) | 6 | $25 / $190 | 100 |
| .25 Derringer (1B) | Firearms (Handgun) | 1D6 | 3 yd | 1 | 1 | $12 / $55 | 100 |
| .32 or 7.65mm Revolver | Firearms (Handgun) | 1D8 | 15 yd | 1 (3) | 6 | $15 / $200 | 100 |
| .32 or 7.65mm Automatic | Firearms (Handgun) | 1D8 | 15 yd | 1 (3) | 8 | $20 / $350 | 99 |
| .357 Magnum Revolver | Firearms (Handgun) | 1D8+1D4 | 15 yd | 1 (3) | 6 | – / $425 | 100 |
| .38 or 9mm Revolver | Firearms (Handgun) | 1D10 | 15 yd | 1 (3) | 6 | $25 / $200 | 100 |
| .38 Automatic | Firearms (Handgun) | 1D10 | 15 yd | 1 (3) | 8 | $30 / $375 | 99 |
| Beretta M9 | Firearms (Handgun) | 1D10 | 15 yd | 1 (3) | 15 | – / $500 | 98 |
| Glock 17 9mm Auto | Firearms (Handgun) | 1D10 | 15 yd | 1 (3) | 17 | – / $500 | 98 |
| Model P08 Luger | Firearms (Handgun) | 1D10 | 15 yd | 1 (3) | 8 | $75 / $600 | 99 |
| .41 Revolver | Firearms (Handgun) | 1D10 | 15 yd | 1 (3) | 8 | $30 / – | 100 |
| .44 Magnum Revolver | Firearms (Handgun) | 1D10+1D4+2 | 15 yd | 1 (3) | 6 | – / $475 | 100 |
| .45 Revolver | Firearms (Handgun) | 1D10+2 | 15 yd | 1 (3) | 6 | $30 / $300 | 100 |
| .45 Automatic | Firearms (Handgun) | 1D10+2 | 15 yd | 1 (3) | 7 | $40 / $375 | 100 |
| IMI Desert Eagle | Firearms (Handgun) | 1D10+1D6+3 | 15 yd | 1 (3) | 7 | – / $650 | 94 |

## Rifles *(i)*

| Name | Skill | Damage | Range | Uses | Mag | Cost | Mal. |
| ---- | ----- | ------ | ----- |:----:|:---:|:----:|:----:|
| .58 Springfield Rifle Musket | Firearms (Rifle) | 1D10+4 | 60 yd | 1/4 | 1 | $25 / $350 | 95 |
| .22 Bolt-Action Rifle | Firearms (Rifle) | 1D6+1 | 30 yd | 1 | 6 | $13 / $70 | 99 |
| .30 Lever-Action Carbine | Firearms (Rifle) | 2D6 | 50 yd | 1 | 6 | $19 / $150 | 98 |
| .45 Martini-Henry Rifle | Firearms (Rifle) | 1D8+1D6+3 | 80 yd | 1/3 | 1 | $20 / $200 | 100 |
| Col. Moran's Air Rifle | Firearms (Rifle) | 2D6+1 | 20 yd | 1/3 | 1 | $200 | 88 |
| Garand M1, M2 Rifle | Firearms (Rifle) | 2D6+4 | 110 yd | 1 | 8 | $400 | 100 |
| SKS Carbine | Firearms (Rifle) | 2D6+1 | 90 yd | 1 (2) | 10 | $500 | 97 |
| .303 Lee-Enfield | Firearms (Rifle) | 2D6+4 | 110 yd | 1 | 10 | $50 / $300 | 100 |
| .30–06 Bolt-Action Rifle | Firearms (Rifle) | 2D6+4 | 110 yd | 1 | 5 | $75 / $175 | 100 |
| .30–06 Semi-Automatic Rifle | Firearms (Rifle) | 2D6+4 | 110 yd | 1 | 5 | $275 | 100 |
| .444 Marlin Rifle | Firearms (Rifle) | 2D8+4 | 110 yd | 1 | 5 | $400 | 98 |
| Elephant Gun (2B) | Firearms (Rifle) | 3D6+4 | 100 yd | 1 or 2 | 2 | $400 / $1,800 | 100 |

## Shotguns

| Name | Skill | Damage | Range | Uses | Mag | Cost | Mal. |
| ---- | ----- | ------ | ----- |:----:|:---:|:----:|:----:|
| 20-gauge Shotgun (2B) | Firearms (Shotgun) | 2D6/1D6/1D3 | 10/20/50 yd | 1 or 2 | 2 | $35 | 100 |

</main>
