import actions from './data/actions.js'
import adventureTones from './data/adventure-tones.js'
import animalActions from './data/animal-actions.js'
import armyDescriptors from './data/army-descriptors.js'
import cavernDescriptors from './data/cavern-descriptors.js'
import characterAppearances from './data/character-appearances.js'
import characterBackgrounds from './data/character-backgrounds.js'
import characterCombatActions from './data/character-combat-actions.js'
import characterConversations from './data/character-conversations.js'
import characterDescriptors from './data/character-descriptors.js'
import characterGeneralActions from './data/character-general-actions.js'
import characterIdentities from './data/character-identities.js'
import characterMotivations from './data/character-motivations.js'
import characterPersonalities from './data/character-personalities.js'
import characterSkills from './data/character-skills.js'
import characterTraits from './data/character-traits.js'
import characters from './data/characters.js'
import cityDescriptors from './data/city-descriptors.js'
import civilizationDescriptors from './data/civilization-descriptors.js'
import creatureAbilities from './data/creature-abilities.js'
import creatureDescriptors from './data/creature-descriptors.js'
import crypticMessages from './data/cryptic-messages.js'
import curses from './data/curses.js'
import descriptions from './data/descriptions.js'
import domicileDescriptors from './data/domicile-descriptors.js'
import dungeonDescriptors from './data/dungeon-descriptors.js'
import dungeonTraps from './data/dungeon-traps.js'
import forestDescriptors from './data/forest-descriptors.js'
import gods from './data/gods.js'
import legends from './data/legends.js'
import locations from './data/locations.js'
import magicItemDescriptors from './data/magic-item-descriptors.js'
import mutationDescriptors from './data/mutation-descriptors.js'
import names from './data/names.js'
import nobleHouses from './data/noble-houses.js'
import objects from './data/objects.js'
import plotTwists from './data/plot-twists.js'
import powers from './data/powers.js'
import scavengingResults from './data/scavenging-results.js'
import smells from './data/smells.js'
import sounds from './data/sounds.js'
import spellEffects from './data/spell-effects.js'
import starshipDescriptors from './data/starship-descriptors.js'
import terrainDescriptors from './data/terrain-descriptors.js'
import undeadDescriptors from './data/undead-descriptors.js'
import visionsAndDreams from './data/visions-and-dreams.js'

const generators = {
  Action: () => pair2('Action', actions),
  'Adventure Tone': () => pair('Adventure Tone', adventureTones),
  'Animal Action': () => pair('Animal Action', animalActions),
  'Army Descriptor': () => pair('Army Descriptor', armyDescriptors),
  'Cavern Descriptor': () => pair('Cavern Descriptor', cavernDescriptors),
  'Character Appearance': () =>
    pair('Character Appearance', characterAppearances),
  'Character Background': () =>
    pair('Character Background', characterBackgrounds),
  'Character Combat Action': () =>
    pair('Character Combat Action', characterCombatActions),
  'Character Conversation': () =>
    pair('Character Conversation', characterConversations),
  'Character Descriptor': () =>
    pair('Character Descriptor', characterDescriptors),
  'Character General Action': () =>
    pair('Character General Action', characterGeneralActions),
  'Character Identity': () => pair('Character Identity', characterIdentities),
  'Character Motivation': () =>
    pair('Character Motivation', characterMotivations),
  'Character Personality': () =>
    pair('Character Personality', characterPersonalities),
  'Character Skill': () => pair('Character Skill', characterSkills),
  'Character Trait': () => pair('Character Trait', characterTraits),
  Character: () => pair('Character', characters),
  'City Descriptor': () => pair('City Descriptor', cityDescriptors),
  'Civilization Descriptor': () =>
    pair('Civilization Descriptor', civilizationDescriptors),
  'Creature Ability': () => pair('Creature Ability', creatureAbilities),
  'Creature Descriptor': () => pair('Creature Descriptor', creatureDescriptors),
  'Cryptic Message': () => pair('Cryptic Message', crypticMessages),
  Curse: () => pair('Curse', curses),
  Description: () => pair2('Description', descriptions),
  'Domicile Descriptor': () => pair('Domicile Descriptor', domicileDescriptors),
  'Dungeon Descriptor': () => pair('Dungeon Descriptor', dungeonDescriptors),
  'Dungeon Trap': () => pair('Dungeon Trap', dungeonTraps),
  'Forest Descriptor': () => pair('Forest Descriptor', forestDescriptors),
  God: () => pair('God', gods),
  Legend: () => pair('Legend', legends),
  Location: () => pair('Location', locations),
  'Magic Item Descriptor': () =>
    pair('Magic Item Descriptor', magicItemDescriptors),
  'Mutation Descriptor': () => pair('Mutation Descriptor', mutationDescriptors),
  Name: () => pair('Name', names),
  'Noble House': () => pair('Noble House', nobleHouses),
  Object: () => pair('Object', objects),
  'Plot Twist': () => pair('Plot Twist', plotTwists),
  Power: () => pair('Power', powers),
  'Scavenging Result': () => pair('Scavenging Result', scavengingResults),
  Smell: () => pair('Smell', smells),
  Sound: () => pair('Sound', sounds),
  'Spell Effect': () => pair('SpellEffect', spellEffects),
  'Starship Descriptor': () => pair('Starship Descriptor', starshipDescriptors),
  'Terrain Descriptor': () => pair('Terrain Descriptor', terrainDescriptors),
  'Undead Descriptor': () => pair('Undead Descriptor', undeadDescriptors),
  'Visions and Dream': () => pair('Visions and Dream', visionsAndDreams)
}

const defaultFavorites = [
  'Action',
  'Character',
  'Description',
  'Location',
  'Object'
]
const version = '1'

let savedVersion = localStorage.getItem('version')
if (savedVersion !== version) {
  localStorage.clear()
  savedVersion = null
}

if (savedVersion === null) {
  localStorage.setItem('version', version)
}

let favorites = localStorage.getItem('favorites')
favorites = favorites === null ? defaultFavorites : JSON.parse(favorites)

const generatorsDiv = document.getElementById('Generators')
const logDiv = document.getElementById('Log')

function capitalize(string) {
  return string[0].toUpperCase() + string.substring(1)
}

function draw(xs) {
  return xs[Math.floor(Math.random() * xs.length)]
}

function makeGenerator(title, generate) {
  const generator = document.createElement('div')
  generator.classList.add('generator')
  if (favorites.includes(title)) generator.classList.add('-favorited')
  generator.setAttribute('data-name', title)

  const favoriteButton = document.createElement('button')
  favoriteButton.classList.add('favorite')
  favoriteButton.textContent = '★'
  favoriteButton.addEventListener('click', () => {
    toggleFavorite(generator)
  })
  generator.appendChild(favoriteButton)

  const generateButton = document.createElement('button')
  generateButton.classList.add('generate')
  generateButton.textContent = title
  generateButton.addEventListener('click', () => {
    logDiv.prepend(generate())
  })
  generator.appendChild(generateButton)

  return generator
}

function makeResult(title, texts) {
  const article = document.createElement('article')
  const heading = document.createElement('h2')
  heading.textContent = title
  article.appendChild(heading)
  for (const text of texts) {
    const p = document.createElement('p')
    p.textContent = text
    article.appendChild(p)
  }
  return article
}

function makeSeparator() {
  const separator = document.createElement('div')
  separator.classList.add('separator')
  return separator
}

function pair(title, table) {
  const i = Math.floor(Math.random() * table.length)
  let j
  do {
    j = Math.floor(Math.random() * table.length)
  } while (j === i)
  return makeResult(title, [capitalize([table[i], table[j]].join(', '))])
}

function pair2(title, [tableA, tableB]) {
  return makeResult(title, [
    capitalize([draw(tableA), draw(tableB)].join(', '))
  ])
}

function toggleFavorite(generator) {
  const name = generator.getAttribute('data-name')
  favorites = generator.classList.contains('-favorited')
    ? favorites.filter((f) => f !== name)
    : favorites.concat(name).sort()
  localStorage.setItem('favorites', JSON.stringify(favorites))
  generator.classList.toggle('-favorited')
}

for (const [name, generator] of Object.entries(generators)) {
  generatorsDiv.appendChild(makeGenerator(name, generator))
}
generatorsDiv.appendChild(makeSeparator())

document.getElementById('EditGenerators').addEventListener('click', () => {
  generatorsDiv.classList.toggle('-editing')
})

document.getElementById('ClearLog').addEventListener('click', () => {
  logDiv.innerHTML = ''
})
