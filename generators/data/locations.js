export default [
  'abandoned',
  'active',
  'artistic',
  'atmosphere',
  'beautiful',
  'bleak',
  'bright',
  'business',
  'calm',
  'charming',
  'clean',
  'cluttered',
  'cold',
  'colorful',
  'colorless',
  'confusing',
  'cramped',
  'creepy',
  'crude',
  'cute',
  'damaged',
  'dangerous',
  'dark',
  'delightful',
  'dirty',
  'domestic',
  'empty',
  'enclosed',
  'enormous',
  'entrance',
  'exclusive',
  'exposed',
  'extravagant',
  'familiar',
  'fancy',
  'festive',
  'foreboding',
  'fortunate',
  'fragrant',
  'frantic',
  'frightening',
  'full',
  'harmful',
  'helpful',
  'horrible',
  'important',
  'impressive',
  'inactive',
  'intense',
  'intriguing',
  'lively',
  'lonely',
  'long',
  'loud',
  'meaningful',
  'messy',
  'mobile',
  'modern',
  'mundane',
  'mysterious',
  'natural',
  'new',
  'occupied',
  'odd',
  'official',
  'old',
  'open',
  'peaceful',
  'personal',
  'plain',
  'portal',
  'protected',
  'protection',
  'purposeful',
  'quiet',
  'reassuring',
  'remote',
  'resourceful',
  'ruined',
  'rustic',
  'safe',
  'services',
  'simple',
  'small',
  'spacious',
  'storage',
  'strange',
  'stylish',
  'suspicious',
  'tall',
  'threatening',
  'tranquil',
  'unexpected',
  'unpleasant',
  'unusual',
  'useful',
  'warm',
  'warning',
  'watery',
  'welcoming'
]
