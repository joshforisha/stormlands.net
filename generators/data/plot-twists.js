export default [
  'action',
  'attack',
  'bad',
  'barrier',
  'betray',
  'business',
  'change',
  'character',
  'conclude',
  'conditional',
  'conflict',
  'connection',
  'consequence',
  'control',
  'danger',
  'death',
  'delay',
  'destroy',
  'diminish',
  'disaster',
  'discover',
  'emotion',
  'enemy',
  'enhance',
  'enter',
  'escape',
  'evidence',
  'failure',
  'family',
  'free',
  'friend',
  'good',
  'group',
  'harm',
  'headquarters',
  'help',
  'helpless',
  'hidden',
  'idea',
  'immediate',
  'impending',
  'important',
  'incapacitate',
  'information',
  'injustice',
  'leader',
  'legal',
  'lethal',
  'lie',
  'limit',
  'location',
  'lucky',
  'mental',
  'missing',
  'mundane',
  'mystery',
  'necessary',
  'news',
  'object',
  'oppose',
  'outcast',
  'overcome',
  'past',
  'peace',
  'personal',
  'persuade',
  'physical',
  'plan',
  'power',
  'prepare',
  'problem',
  'promise',
  'protect',
  'public',
  'pursue',
  'rare',
  'remote',
  'repair',
  'repeat',
  'require',
  'rescue',
  'resource',
  'response',
  'reveal',
  'revenge',
  'reversal',
  'reward',
  'skill',
  'social',
  'solution',
  'strange',
  'success',
  'tension',
  'trap',
  'travel',
  'unknown',
  'unlikely',
  'unusual',
  'urgent',
  'useful'
]
