export default [
  [
    'adventurously',
    'aggressively',
    'anxiously',
    'awkwardly',
    'beautifully',
    'bleakly',
    'boldly',
    'bravely',
    'busily',
    'calmly',
    'carefully',
    'carelessly',
    'cautiously',
    'ceaselessly',
    'cheerfully',
    'combatively',
    'coolly',
    'crazily',
    'curiously',
    'dangerously',
    'defiantly',
    'deliberately',
    'delicately',
    'delightfully',
    'dimly',
    'efficiently',
    'emotionally',
    'energetically',
    'enormously',
    'enthusiastically',
    'excitedly',
    'fearfully',
    'ferociously',
    'fiercely',
    'foolishly',
    'fortunately',
    'frantically',
    'freely',
    'frighteningly',
    'fully',
    'generously',
    'gently',
    'gladly',
    'gracefully',
    'gratefully',
    'happily',
    'hastily',
    'healthily',
    'helpfully',
    'helplessly',
    'hopelessly',
    'innocently',
    'intensely',
    'interestingly',
    'irritatingly',
    'joyfully',
    'kindly',
    'lazily',
    'lightly',
    'loosely',
    'loudly',
    'lovingly',
    'loyally',
    'majestically',
    'meaningfully',
    'mechanically',
    'mildly',
    'miserably',
    'mockingly',
    'mysteriously',
    'naturally',
    'neatly',
    'nicely',
    'oddly',
    'offensively',
    'officially',
    'partially',
    'passively',
    'peacefully',
    'perfectly',
    'playfully',
    'politely',
    'positively',
    'powerfully',
    'quaintly',
    'quarrelsomely',
    'quietly',
    'roughly',
    'rudely',
    'ruthlessly',
    'slowly',
    'softly',
    'strangely',
    'swiftly',
    'threateningly',
    'timidly',
    'very',
    'violently',
    'wildly',
    'yieldingly'
  ],
  [
    'abnormal',
    'amusing',
    'artificial',
    'average',
    'beautiful',
    'bizarre',
    'boring',
    'bright',
    'broken',
    'clean',
    'cold',
    'colorful',
    'colorless',
    'comforting',
    'creepy',
    'cute',
    'damaged',
    'dark',
    'defeated',
    'dirty',
    'disagreeable',
    'dry',
    'dull',
    'empty',
    'enormous',
    'extraordinary',
    'extravagant',
    'faded',
    'familiar',
    'fancy',
    'feeble',
    'festive',
    'flawless',
    'forlorn',
    'fragile',
    'fragrant',
    'fresh',
    'full',
    'glorious',
    'graceful',
    'hard',
    'harsh',
    'healthy',
    'heavy',
    'historical',
    'horrible',
    'important',
    'interesting',
    'juvenile',
    'lacking',
    'large',
    'lavish',
    'lean',
    'less',
    'lethal',
    'lively',
    'lonely',
    'lovely',
    'magnificent',
    'mature',
    'messy',
    'mighty',
    'military',
    'modern',
    'mundane',
    'mysterious',
    'natural',
    'normal',
    'odd',
    'old',
    'pale',
    'peaceful',
    'petite',
    'plain',
    'poor',
    'powerful',
    'protective',
    'quaint',
    'rare',
    'reassuring',
    'remarkable',
    'rotten',
    'rough',
    'ruined',
    'rustic',
    'scary',
    'shocking',
    'simple',
    'small',
    'smooth',
    'soft',
    'strong',
    'stylish',
    'unpleasant',
    'valuable',
    'vibrant',
    'warm',
    'watery',
    'weak',
    'young'
  ]
]
