export default [
  'animal',
  'animate',
  'assist',
  'attack',
  'attract',
  'bestow',
  'bizarre',
  'block',
  'break',
  'bright',
  'burn',
  'change',
  'cloud',
  'cold',
  'communicate',
  'conceal',
  'conjure',
  'control',
  'counteract',
  'create',
  'creature',
  'curse',
  'damage',
  'dark',
  'death',
  'deceive',
  'decrease',
  'defense',
  'destroy',
  'detect',
  'diminish',
  'disease',
  'dominate',
  'duplicate',
  'earth',
  'elements',
  'emotion',
  'enemies',
  'energy',
  'enhance',
  'environment',
  'expose',
  'fire',
  'fix',
  'food',
  'free',
  'group',
  'guide',
  'hamper',
  'harm',
  'heal',
  'helpful',
  'ice',
  'illusion',
  'imbue',
  'immunity',
  'imprison',
  'information',
  'inspect',
  'life',
  'light',
  'limitation',
  'liquid',
  'loud',
  'manipulation',
  'mind',
  'nature',
  'object',
  'others',
  'pain',
  'physical',
  'plant',
  'poison',
  'portal',
  'powerful',
  'protect',
  'radius',
  'ranged',
  'resistance',
  'restore',
  'self',
  'senses',
  'shield',
  'soul',
  'strange',
  'strength',
  'stun',
  'summon',
  'time',
  'transform',
  'trap',
  'travel',
  'trigger',
  'uncertain',
  'undead',
  'wall',
  'water',
  'weak',
  'weapon',
  'weather'
]
