export default [
  'acrid',
  'animal',
  'antiseptic',
  'aromatic',
  'artificial',
  'attractive',
  'bad',
  'bizarre',
  'burnt',
  'chemical',
  'clean',
  'comforting',
  'cooking',
  'decrepit',
  'delicious',
  'delightful',
  'dirty',
  'disagreeable',
  'disgusting',
  'dry',
  'dull',
  'earthy',
  'electrical',
  'evocative',
  'faded',
  'faint',
  'familiar',
  'fetid',
  'fishy',
  'floral',
  'food',
  'foul',
  'fragrant',
  'fresh',
  'fruity',
  'funky',
  'good',
  'grassy',
  'gratifying',
  'heady',
  'heavy',
  'herbal',
  'horrible',
  'humid',
  'industrial',
  'interesting',
  'intoxicating',
  'irritating',
  'lacking',
  'laden',
  'malodorous',
  'meaningful',
  'medicinal',
  'metallic',
  'mildew',
  'moist',
  'mossy',
  'musky',
  'musty',
  'mysterious',
  'natural',
  'nature',
  'nauseating',
  'normal',
  'odd',
  'odorless',
  'offensive',
  'overpowering',
  'perfumed',
  'pleasurable',
  'powerful',
  'pungent',
  'punishing',
  'putrid',
  'rancid',
  'reassuring',
  'reek',
  'rich',
  'ripe',
  'rot',
  'rotten',
  'savory',
  'smelly',
  'smokey',
  'sour',
  'stagnant',
  'stale',
  'stench',
  'stinging',
  'strange',
  'strong',
  'stuffy',
  'sulphuric',
  'sweet',
  'warm',
  'waste',
  'watery',
  'weak',
  'weather',
  'woody'
]
